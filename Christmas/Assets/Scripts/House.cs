﻿using UnityEngine;
using System.Collections;

public class House : MonoBehaviour
{
    public HouseData HouseInfoList;
    
    public GameObject NameLoc;
    public GameObject ChimneyLoc;
    public GameObject NameUIPrefab;
    public GameObject NaughtyChimneyPrefab;

    [SerializeField]
    private SingleHouseData HouseInfo;

    void Start()
    {
        HouseInfo = HouseInfoList.Houses[Random.Range(0, HouseInfoList.Houses.Count - 1)];
        GameObject name = NGUITools.AddChild(UIHUD.instance.FollowParent, NameUIPrefab);
        name.GetComponent<UIFollowTarget>().target = NameLoc.transform;
        name.GetComponent<UIHouseName>().NameLabel.text = HouseInfo.KidName;
        if (!HouseInfo.Nice)
        {
            GameObject naughty = NGUITools.AddChild(UIHUD.instance.FollowParent, NaughtyChimneyPrefab);
            naughty.GetComponent<UIFollowTarget>().target = ChimneyLoc.transform;
            name.GetComponent<UIHouseName>().MakeNaughty();
        }
    }

    public SingleHouseData GetHouseInfo()
    {
        return HouseInfo;
    }
}
