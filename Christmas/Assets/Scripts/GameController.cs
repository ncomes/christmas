﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ScoreTime
{
    public eChimneyTargetScore ScoreType;
    public float TimeLoss = 0.0f;
}

public class GameController : MonoBehaviour
{
    public bool StartGame = false;

    public float InitialTime = 3.0f;
    public float GameTime = 30.0f;

    public List<ScoreTime> ScoreModifiers = new List<ScoreTime>();

    public static GameController instance;

    bool canScore = false;
    float originalGameTime = 0.0f;

    void Awake()
    {
        if (!instance)
            instance = this;
        else
            Debug.LogWarning(gameObject.name + " is trying to say it's the game controller but there is already one: " + GameController.instance.gameObject.name);

        originalGameTime = GameTime;
    }

    void Update()
    {
        if (StartGame)
        {
            StartGame = false;
            InitGame();
        }
    }

    public static void InitializeGame() { GameController.instance.InitGame(); }
    public void InitGame()
    {
        GameTime = originalGameTime;
        UIStartScreen.Show();
        UIHUD.Hide();
        HouseController.KillAll();
        StartCoroutine(GameLoop());
    }

    IEnumerator GameLoop()
    {
        float finishTime = Time.time + InitialTime;
        while (Time.time <= finishTime)
        {
            int timeDiff = (int)(finishTime - Time.time);
            if (timeDiff <= 3)
                UIStartScreen.UpdateCountdown(timeDiff);
            yield return 0;
        }
        UIStartScreen.Hide();
        HouseController.SetHCPaused(false);
        UIHUD.Show();
        canScore = true;
        while (GameTime > 0)
        {
            UITimeDisplay.UpdateTime(GameTime / originalGameTime);
            yield return 0;
        }
        EndGame();
    }

    public void EndGame()
    {
        canScore = false;
        HouseController.SetHCPaused(true);
        HouseController.KillAll();
        UIHUD.Hide();
        UIMainMenu.Show();
    }

    public static void ReportScore(eChimneyTargetScore scoreType) { GameController.instance.ReportNewScore(scoreType); }
    public void ReportNewScore(eChimneyTargetScore scoreType)
    {
        if (canScore)
        {
            //Debug.Log("ReportNewScore: " + scoreType);
            UIScoreLine.ShowScore(scoreType);
            GameTime -= ScoreModifiers.Find(score => scoreType == score.ScoreType).TimeLoss;
        }
    }

    public static bool CanScorePoints() { return GameController.instance.CanGCScore();  }
    public bool CanGCScore()
    {
        return canScore;
    }


}
