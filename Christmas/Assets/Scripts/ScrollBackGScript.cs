﻿using UnityEngine;
using System.Collections;

public class ScrollBackGScript : MonoBehaviour {

	public float scrollSpeed;
	public float startPosition;
	public float EndPosition;
	
	private Vector3 objPosition;
	private GameObject _myObj;
	

	// Update is called once per frame
	void Update () {
		// gets the objPosition OffMeshLink the background Plane
		objPosition = transform.position;

		// moves the plane to the left
		transform.Translate (Vector3.left * Time.deltaTime * scrollSpeed, Space.World);

		// checks to see if the plane reaches a point.  if it does it snaps to the start position
		// this gives the illusion of a non-stop background
		if (objPosition.x < EndPosition) {
			transform.position = new Vector3(startPosition, transform.position.y, transform.position.z);

		}


	}
}
