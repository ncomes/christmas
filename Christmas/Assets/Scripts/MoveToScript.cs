﻿using UnityEngine;
using System.Collections;

public class MoveToScript : MonoBehaviour
{
	public float speed = 10;
	public float rotationSpeed = 5;
	public CharacterController CharCon;
	public static bool attack;
	public int health;
	public GameObject opponent;
	
	private Animator anim;
	private Vector3 desiredPosition;
	
	
	void Start()
	{
		// get the current position of the player
		desiredPosition = transform.position;
		// get the animator so we can access the animations
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	// Checking each state the player is in every frame
	void Update () {
		if (!IsDead ()) {
			if (!attack) {
				if (Input.GetMouseButton (0)) {
					// Locate where the player clicked on the terrain
					locatePosition ();
				}
				moveToPosition ();
				//Debug.DrawLine (desiredPosition, transform.position);
			} else {
				
			}
		} else {
			anim.Play("Death");
		}
	}
	// check to see where the player clicked and update where the player needs to move to.
	void locatePosition()
	{
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		
		if (Physics.Raycast(ray, out hit, 1000))
		{
			if (hit.collider.tag != "Player"){
				desiredPosition = new Vector3(hit.point.x, hit.point.y, hit.point.z);
			}
			if (opponent != null && hit.collider.tag == "Enemy"){
				desiredPosition = opponent.transform.position;
			}
		}
		
	}
	// move the player character to the clicked position
	void moveToPosition()
	{
		// see if the player is close to the clicked position
		if (Vector3.Distance (transform.position, desiredPosition) > 4) {
			Quaternion newRotation = Quaternion.LookRotation (desiredPosition - transform.position);
			newRotation.x = 0f;
			newRotation.z = 0f;
			
			transform.rotation = Quaternion.Slerp (transform.rotation, newRotation, Time.deltaTime * rotationSpeed);
			CharCon.SimpleMove (transform.forward * speed);
			
			anim.Play("Run");
			
			
		}else{
			anim.Play("Idle");
		}
	}
	
	// Check to see if the player is dead
	bool IsDead(){
		if (health <= 0) {
			return true;
		} else {
			return false;
		}
	}
}