﻿using UnityEngine;
using System.Collections;

public class UIHUD : MonoBehaviour
{

    public static UIHUD instance;
    public GameObject ScreenParent;
    public GameObject FollowParent;

    void Awake()
    {
        if (!instance)
            instance = this;
        else
            Debug.LogWarning(gameObject.name + " is trying to create a new UITimeDisplay UI script and there can be only one! Current: " + instance.gameObject.name);

        HideUI();
    }

    public static void Show() { UIHUD.instance.ShowUI(); }
    public void ShowUI()
    {
        ScreenParent.SetActive(true);
        ScreenParent.BroadcastMessage("ShowUI", SendMessageOptions.DontRequireReceiver);
    }

    public static void Hide() { UIHUD.instance.HideUI(); }
    public void HideUI()
    {
        ScreenParent.SetActive(false);
        ScreenParent.BroadcastMessage("HideUI", SendMessageOptions.DontRequireReceiver);
    }
}
