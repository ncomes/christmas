﻿using UnityEngine;
using System.Collections;

public class Lifetime : MonoBehaviour
{
    public float TimeToLive = 60.0f;

	void Start ()
    {
        Invoke("Kill", TimeToLive);
	}
	
	void Kill()
    {
        DestroyObject(gameObject);
	}
}
