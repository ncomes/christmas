﻿using UnityEngine;
using System.Collections;

public class UIMainMenu : MonoBehaviour
{
    public static UIMainMenu instance;
    public GameObject ScreenParent;

    void Awake()
    {
        if (!instance)
            instance = this;
        else
            Debug.LogWarning(gameObject.name + " is trying to create a new MainMenu UI script and there can be only one! Current: " + instance.gameObject.name);

        ShowUI();
    }

    public static void Show() { UIMainMenu.instance.ShowUI(); }
    public void ShowUI()
    {
        ScreenParent.SetActive(true);
    }

    public static void Hide() { UIMainMenu.instance.HideUI(); }
    public void HideUI()
    {
        ScreenParent.SetActive(false);
    }

    public void MMStartGame()
    {
        GameController.InitializeGame();
        HideUI();
    }
}
