//Maya ASCII 2016 scene
//Name: SantaReindeer@Reindeer_Run.ma
//Last modified: Sun, Nov 22, 2015 05:42:53 PM
//Codeset: 1252
file -rdi 1 -dns -rpr "Reindeer_Rig" -rfn "Reindeer_RigRN" -typ "mayaAscii" "C:/Users/Nathan/Documents/christmas/SourceContent/Reindeer/Rig/Reindeer_Rig.ma";
file -rdi 2 -dns -rpr "Reindeer_Skin" -rfn "Reindeer_SkinRN" -typ "mayaAscii"
		 "C:/Users/Nathan/Documents/christmas/SourceContent/Reindeer/Rig/Reindeer_Skin.ma";
file -rdi 3 -dns -rpr "Reindeer_Skeleton" -rfn "Reindeer_SkeletonRN" -typ "mayaAscii"
		 "C:/Users/Nathan/Documents/christmas/SourceContent/Reindeer/Rig/Reindeer_Skeleton.ma";
file -rdi 3 -dns -rpr "Reindeer" -rfn "ReindeerRN" -typ "mayaAscii" "C:/Users/Nathan/Documents/christmas/SourceContent/Reindeer/Model/Reindeer.ma";
file -r -dns -rpr "Reindeer_Rig" -dr 1 -rfn "Reindeer_RigRN" -typ "mayaAscii" "C:/Users/Nathan/Documents/christmas/SourceContent/Reindeer/Rig/Reindeer_Rig.ma";
requires maya "2016";
requires -nodeType "ikSpringSolver" "ikSpringSolver" "1.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "3011729A-47E3-E545-C9FF-1E86EC1CA345";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1502.4432566220039 420.15801896432066 449.2658609351887 ;
	setAttr ".r" -type "double3" -8.7383527268551813 -77.399999999997434 3.6450300512419282e-015 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "2B18E658-425F-B320-17EA-09ADB473E8B1";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".ncp" 1;
	setAttr ".fcp" 1000000;
	setAttr ".coi" 1599.6187898533633;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "EC501CF4-42EA-A39D-45DD-D886CC91FB3A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "7CB20AED-4197-A35E-9502-7096AA620395";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "7D4A2AF9-4C2D-07E5-CD79-128D0927439A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "C921FB63-4E69-B3B9-A5E9-37806E93F3C2";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "EE4B30C0-4DA3-0A2A-B926-3094D5EAF3B1";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "AC211AD1-4A8D-E8D5-E9DC-A7A30183DB0B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "reindeer_rig";
	rename -uid "D84A38C4-4112-7BDB-8764-4392BAFFB773";
	setAttr ".rp" -type "double3" 0 286.42350193605307 0 ;
	setAttr ".sp" -type "double3" 0 286.42350193605307 0 ;
createNode transform -n "locator1";
	rename -uid "0591BDAC-4E87-88B2-8270-418A478D6762";
	setAttr ".rp" -type "double3" 0 277.9344943019363 591.092813687839 ;
	setAttr ".sp" -type "double3" 0 277.9344943019363 591.092813687839 ;
createNode locator -n "locatorShape1" -p "locator1";
	rename -uid "EC139789-49CD-542C-60FC-D88996C6F991";
	setAttr -k off ".v";
	setAttr ".lp" -type "double3" 0 277.9344943019363 591.092813687839 ;
	setAttr ".los" -type "double3" 63.300000000000004 63.300000000000004 63.300000000000004 ;
createNode fosterParent -n "Reindeer_RigRNfosterParent1";
	rename -uid "54D464DA-4BC7-CA99-1C65-898AC162E81A";
createNode orientConstraint -n "neckBaseCntl_orientConstraint1" -p "Reindeer_RigRNfosterParent1";
	rename -uid "A9FC3285-4A9A-7E78-DF19-4A99379B2599";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "locator1W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -25.920798102626918 -77.967872808071135 37.070257780478663 ;
	setAttr ".o" -type "double3" 88.658460462211494 63.653417907273578 88.919644316993541 ;
	setAttr ".rsrr" -type "double3" -4.8523275338533711 12.859125757996688 26.72568231096843 ;
	setAttr -k on ".w0";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "C0CB295D-4C9F-5A52-0EEB-5899F3E3615D";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode ikSpringSolver -n "ikSpringSolver";
	rename -uid "95926BB3-4F22-015E-F487-1B9620114279";
createNode displayLayerManager -n "layerManager";
	rename -uid "B2DE7F97-4999-9FA6-0523-3E9A03D6F201";
createNode displayLayer -n "defaultLayer";
	rename -uid "A10FD0C9-454B-FC40-F800-E3A948D0235D";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "3B5489F6-4333-65D9-2B2E-EB9CBF96DF9E";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "A98897FF-456E-AA58-3184-85B85AC8EF28";
	setAttr ".g" yes;
createNode reference -n "Reindeer_RigRN";
	rename -uid "06F682BE-422E-DE6B-5F82-659159C73BDA";
	setAttr -s 120 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".phl[81]" 0;
	setAttr ".phl[82]" 0;
	setAttr ".phl[83]" 0;
	setAttr ".phl[84]" 0;
	setAttr ".phl[85]" 0;
	setAttr ".phl[86]" 0;
	setAttr ".phl[87]" 0;
	setAttr ".phl[88]" 0;
	setAttr ".phl[89]" 0;
	setAttr ".phl[90]" 0;
	setAttr ".phl[91]" 0;
	setAttr ".phl[92]" 0;
	setAttr ".phl[93]" 0;
	setAttr ".phl[94]" 0;
	setAttr ".phl[95]" 0;
	setAttr ".phl[96]" 0;
	setAttr ".phl[97]" 0;
	setAttr ".phl[98]" 0;
	setAttr ".phl[99]" 0;
	setAttr ".phl[100]" 0;
	setAttr ".phl[101]" 0;
	setAttr ".phl[102]" 0;
	setAttr ".phl[103]" 0;
	setAttr ".phl[104]" 0;
	setAttr ".phl[105]" 0;
	setAttr ".phl[106]" 0;
	setAttr ".phl[107]" 0;
	setAttr ".phl[108]" 0;
	setAttr ".phl[109]" 0;
	setAttr ".phl[110]" 0;
	setAttr ".phl[111]" 0;
	setAttr ".phl[112]" 0;
	setAttr ".phl[113]" 0;
	setAttr ".phl[114]" 0;
	setAttr ".phl[115]" 0;
	setAttr ".phl[116]" 0;
	setAttr ".phl[117]" 0;
	setAttr ".phl[118]" 0;
	setAttr ".phl[119]" 0;
	setAttr ".phl[120]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"Reindeer_RigRN"
		"Reindeer_RigRN" 0
		"ReindeerRN" 0
		"Reindeer_SkeletonRN" 0
		"Reindeer_SkinRN" 0
		"Reindeer_RigRN" 196
		0 "|Reindeer_RigRNfosterParent1|neckBaseCntl_orientConstraint1" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl" 
		"-s -r "
		1 |reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl "blendParent1" 
		"blendParent1" " -ci 1 -k 1 -dv 1 -smn 0 -smx 1 -at \"double\""
		2 "|reindeer_rig|rig_pelvis" "rotate" " -type \"double3\" 0 0 -1.9901826778449121"
		
		2 "|reindeer_rig|rig_pelvis" "rotateX" " -av"
		2 "|reindeer_rig|rig_pelvis" "rotateY" " -av"
		2 "|reindeer_rig|rig_pelvis" "rotateZ" " -av"
		2 "|reindeer_rig|rig_pelvis" "segmentScaleCompensate" " 1"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl" 
		"visibility" " 1"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl" 
		"scale" " -type \"double3\" 16.503466400000001 16.503466400000001 16.503466400000001"
		
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl" 
		"autoVolume" " -k 1 0"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl" 
		"blendParent1" " -k 1 0"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_start_ctrl_grp|reindeer__flexi_start_ctrl" 
		"translate" " -type \"double3\" 0.52986320904756024 0 0"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_start_ctrl_grp|reindeer__flexi_start_ctrl" 
		"translateX" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_start_ctrl_grp|reindeer__flexi_start_ctrl" 
		"translateY" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_start_ctrl_grp|reindeer__flexi_start_ctrl" 
		"translateZ" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_start_ctrl_grp|reindeer__flexi_start_ctrl" 
		"rotate" " -type \"double3\" 0 0 11.021558009326185"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_start_ctrl_grp|reindeer__flexi_start_ctrl" 
		"rotateX" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_start_ctrl_grp|reindeer__flexi_start_ctrl" 
		"rotateY" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_start_ctrl_grp|reindeer__flexi_start_ctrl" 
		"rotateZ" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_mid_ctrl_grp|reindeer__flexi_mid_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_mid_ctrl_grp|reindeer__flexi_mid_ctrl" 
		"translateX" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_mid_ctrl_grp|reindeer__flexi_mid_ctrl" 
		"translateY" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_mid_ctrl_grp|reindeer__flexi_mid_ctrl" 
		"translateZ" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl" 
		"translate" " -type \"double3\" -0.77873565007180612 -1.1524045501407665 -0.25274284172352091"
		
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl" 
		"translateX" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl" 
		"translateY" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl" 
		"translateZ" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl" 
		"rotate" " -type \"double3\" 7.3213158627737354 -9.574488232712115 0.52992291871311592"
		
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl" 
		"rotateX" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl" 
		"rotateY" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl" 
		"rotateZ" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl" 
		"visibility" " 1"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl" 
		"rotate" " -type \"double3\" -0.28365064506576221 11.552358035689227 37.500696150667579"
		
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl" 
		"rotateX" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl" 
		"rotateY" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl" 
		"rotateZ" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl|neckCtrl" 
		"rotate" " -type \"double3\" 0 0 -30.623084331898461"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl|neckCtrl" 
		"rotateZ" " -av"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl|neckCtrl|headCtrl" 
		"visibility" " 1"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl|neckCtrl|headCtrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl|neckCtrl|headCtrl" 
		"rotate" " -type \"double3\" 0 0 -18.537475041179562"
		2 "|reindeer_rig|worldPlacement|r_bk_foot_ctrl" "translate" " -type \"double3\" -66.272386047791798 69.819075292856937 -156.02650314697735"
		
		2 "|reindeer_rig|worldPlacement|r_bk_foot_ctrl" "translateY" " -av"
		2 "|reindeer_rig|worldPlacement|r_bk_foot_ctrl" "translateZ" " -av"
		2 "|reindeer_rig|worldPlacement|r_bk_foot_ctrl" "rotate" " -type \"double3\" 65.938564396739054 0 0"
		
		2 "|reindeer_rig|worldPlacement|r_bk_foot_ctrl" "rotateX" " -av"
		2 "|reindeer_rig|worldPlacement|l_bk_foot_ctrl" "translate" " -type \"double3\" 0 0 -84.433557528556818"
		
		2 "|reindeer_rig|worldPlacement|l_bk_foot_ctrl" "translateX" " -av"
		2 "|reindeer_rig|worldPlacement|l_bk_foot_ctrl" "translateY" " -av"
		2 "|reindeer_rig|worldPlacement|l_bk_foot_ctrl" "translateZ" " -av"
		2 "|reindeer_rig|worldPlacement|l_bk_foot_ctrl" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|reindeer_rig|worldPlacement|l_bk_foot_ctrl" "rotateX" " -av"
		2 "|reindeer_rig|worldPlacement|r_bk_pv" "translate" " -type \"double3\" 0 0 0"
		
		2 "|reindeer_rig|worldPlacement|r_bk_pv" "translateX" " -av"
		2 "|reindeer_rig|worldPlacement|r_bk_pv" "translateY" " -av"
		2 "|reindeer_rig|worldPlacement|r_fnt_foot_ctrl" "translate" " -type \"double3\" -66.891773313485373 -2.6912969900649841e-005 -21.935714911525338"
		
		2 "|reindeer_rig|worldPlacement|r_fnt_foot_ctrl" "translateY" " -av"
		2 "|reindeer_rig|worldPlacement|r_fnt_foot_ctrl" "translateZ" " -av"
		2 "|reindeer_rig|worldPlacement|r_fnt_foot_ctrl" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|reindeer_rig|worldPlacement|r_fnt_foot_ctrl" "rotateX" " -av"
		2 "|reindeer_rig|worldPlacement|l_fnt_foot_ctrl" "translate" " -type \"double3\" 0 12.614674922997281 136.97723974365658"
		
		2 "|reindeer_rig|worldPlacement|l_fnt_foot_ctrl" "translateY" " -av"
		2 "|reindeer_rig|worldPlacement|l_fnt_foot_ctrl" "translateZ" " -av"
		2 "|reindeer_rig|worldPlacement|l_fnt_foot_ctrl" "rotate" " -type \"double3\" -24.823944400791021 0 0"
		
		2 "|reindeer_rig|worldPlacement|l_fnt_foot_ctrl" "rotateX" " -av"
		2 "|reindeer_rig|worldPlacement|f_fnt_pv" "translate" " -type \"double3\" 0 0 127.92337046984778"
		
		2 "|reindeer_rig|worldPlacement|f_fnt_pv" "translateZ" " -av"
		2 "|reindeer_rig|worldPlacement|f_fnt_pv1" "translate" " -type \"double3\" 0 0 0"
		
		2 "|reindeer_rig|worldPlacement|f_fnt_pv1" "translateZ" " -av"
		2 "|reindeer_rig|worldPlacement|center_ctrl" "translate" " -type \"double3\" 0 0 0"
		
		2 "|reindeer_rig|worldPlacement|center_ctrl" "translateX" " -av"
		2 "|reindeer_rig|worldPlacement|center_ctrl" "translateY" " -av"
		2 "|reindeer_rig|worldPlacement|center_ctrl" "rotate" " -type \"double3\" 1.9901826778449128 0 0"
		
		2 "|reindeer_rig|worldPlacement|center_ctrl" "rotateX" " -av"
		2 "ReindeerMeshLayer" "visibility" " 1"
		3 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_ctrl_parentConstraint1.constraintTranslateX" 
		"|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl.translateX" 
		""
		3 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_ctrl_parentConstraint1.constraintTranslateY" 
		"|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl.translateY" 
		""
		3 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_ctrl_parentConstraint1.constraintTranslateZ" 
		"|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl.translateZ" 
		""
		3 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_ctrl_parentConstraint1.constraintRotateX" 
		"|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl.rotateX" ""
		
		3 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_ctrl_parentConstraint1.constraintRotateY" 
		"|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl.rotateY" ""
		
		3 "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_ctrl_parentConstraint1.constraintRotateZ" 
		"|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl.rotateZ" ""
		
		5 0 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_ctrl_parentConstraint1.constraintTranslateX" 
		"|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl.translateX" 
		"Reindeer_RigRN.placeHolderList[1]" "Reindeer_RigRN.placeHolderList[2]" "reindeer__global_move_ctrl.tx"
		
		5 0 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_ctrl_parentConstraint1.constraintTranslateY" 
		"|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl.translateY" 
		"Reindeer_RigRN.placeHolderList[3]" "Reindeer_RigRN.placeHolderList[4]" "reindeer__global_move_ctrl.ty"
		
		5 0 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_ctrl_parentConstraint1.constraintTranslateZ" 
		"|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl.translateZ" 
		"Reindeer_RigRN.placeHolderList[5]" "Reindeer_RigRN.placeHolderList[6]" "reindeer__global_move_ctrl.tz"
		
		5 0 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_ctrl_parentConstraint1.constraintRotateX" 
		"|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl.rotateX" "Reindeer_RigRN.placeHolderList[7]" 
		"Reindeer_RigRN.placeHolderList[8]" "reindeer__global_move_ctrl.rx"
		5 0 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_ctrl_parentConstraint1.constraintRotateY" 
		"|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl.rotateY" "Reindeer_RigRN.placeHolderList[9]" 
		"Reindeer_RigRN.placeHolderList[10]" "reindeer__global_move_ctrl.ry"
		5 0 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_ctrl_parentConstraint1.constraintRotateZ" 
		"|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl.rotateZ" "Reindeer_RigRN.placeHolderList[11]" 
		"Reindeer_RigRN.placeHolderList[12]" "reindeer__global_move_ctrl.rz"
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_start_ctrl_grp|reindeer__flexi_start_ctrl.translateX" 
		"Reindeer_RigRN.placeHolderList[13]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_start_ctrl_grp|reindeer__flexi_start_ctrl.translateY" 
		"Reindeer_RigRN.placeHolderList[14]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_start_ctrl_grp|reindeer__flexi_start_ctrl.translateZ" 
		"Reindeer_RigRN.placeHolderList[15]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_start_ctrl_grp|reindeer__flexi_start_ctrl.rotateX" 
		"Reindeer_RigRN.placeHolderList[16]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_start_ctrl_grp|reindeer__flexi_start_ctrl.rotateY" 
		"Reindeer_RigRN.placeHolderList[17]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_start_ctrl_grp|reindeer__flexi_start_ctrl.rotateZ" 
		"Reindeer_RigRN.placeHolderList[18]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_mid_ctrl_grp|reindeer__flexi_mid_ctrl.followStart" 
		"Reindeer_RigRN.placeHolderList[19]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_mid_ctrl_grp|reindeer__flexi_mid_ctrl.translateX" 
		"Reindeer_RigRN.placeHolderList[20]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_mid_ctrl_grp|reindeer__flexi_mid_ctrl.translateY" 
		"Reindeer_RigRN.placeHolderList[21]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_mid_ctrl_grp|reindeer__flexi_mid_ctrl.translateZ" 
		"Reindeer_RigRN.placeHolderList[22]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_mid_ctrl_grp|reindeer__flexi_mid_ctrl.rotateX" 
		"Reindeer_RigRN.placeHolderList[23]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_mid_ctrl_grp|reindeer__flexi_mid_ctrl.rotateY" 
		"Reindeer_RigRN.placeHolderList[24]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_mid_ctrl_grp|reindeer__flexi_mid_ctrl.rotateZ" 
		"Reindeer_RigRN.placeHolderList[25]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl.followMid" 
		"Reindeer_RigRN.placeHolderList[26]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl.translateX" 
		"Reindeer_RigRN.placeHolderList[27]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl.translateY" 
		"Reindeer_RigRN.placeHolderList[28]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl.translateZ" 
		"Reindeer_RigRN.placeHolderList[29]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl.rotateX" 
		"Reindeer_RigRN.placeHolderList[30]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl.rotateY" 
		"Reindeer_RigRN.placeHolderList[31]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl.rotateZ" 
		"Reindeer_RigRN.placeHolderList[32]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl.rotateX" 
		"Reindeer_RigRN.placeHolderList[33]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl.rotateY" 
		"Reindeer_RigRN.placeHolderList[34]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl.rotateZ" 
		"Reindeer_RigRN.placeHolderList[35]" ""
		5 3 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl.rotateOrder" 
		"Reindeer_RigRN.placeHolderList[36]" ""
		5 3 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl.parentInverseMatrix" 
		"Reindeer_RigRN.placeHolderList[37]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl|neckCtrl.translateX" 
		"Reindeer_RigRN.placeHolderList[38]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl|neckCtrl.translateY" 
		"Reindeer_RigRN.placeHolderList[39]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl|neckCtrl.translateZ" 
		"Reindeer_RigRN.placeHolderList[40]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl|neckCtrl.rotateX" 
		"Reindeer_RigRN.placeHolderList[41]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl|neckCtrl.rotateY" 
		"Reindeer_RigRN.placeHolderList[42]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl|neckCtrl.rotateZ" 
		"Reindeer_RigRN.placeHolderList[43]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|reindeer__flexiPlane_grp_01|reindeer__global_move_ctrl|reindeer__global_move_group|reindeer__flexi_end_ctrl_grp|reindeer__flexi_end_ctrl|neckBaseCntl|neckCtrl.visibility" 
		"Reindeer_RigRN.placeHolderList[44]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement.translateX" "Reindeer_RigRN.placeHolderList[45]" 
		""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement.translateY" "Reindeer_RigRN.placeHolderList[46]" 
		""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement.translateZ" "Reindeer_RigRN.placeHolderList[47]" 
		""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement.visibility" "Reindeer_RigRN.placeHolderList[48]" 
		""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement.rotateX" "Reindeer_RigRN.placeHolderList[49]" 
		""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement.rotateY" "Reindeer_RigRN.placeHolderList[50]" 
		""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement.rotateZ" "Reindeer_RigRN.placeHolderList[51]" 
		""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement.scaleX" "Reindeer_RigRN.placeHolderList[52]" 
		""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement.scaleY" "Reindeer_RigRN.placeHolderList[53]" 
		""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement.scaleZ" "Reindeer_RigRN.placeHolderList[54]" 
		""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_bk_foot_ctrl.rotateX" 
		"Reindeer_RigRN.placeHolderList[55]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_bk_foot_ctrl.rotateY" 
		"Reindeer_RigRN.placeHolderList[56]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_bk_foot_ctrl.rotateZ" 
		"Reindeer_RigRN.placeHolderList[57]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_bk_foot_ctrl.translateX" 
		"Reindeer_RigRN.placeHolderList[58]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_bk_foot_ctrl.translateY" 
		"Reindeer_RigRN.placeHolderList[59]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_bk_foot_ctrl.translateZ" 
		"Reindeer_RigRN.placeHolderList[60]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_bk_foot_ctrl.visibility" 
		"Reindeer_RigRN.placeHolderList[61]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_bk_foot_ctrl.scaleX" 
		"Reindeer_RigRN.placeHolderList[62]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_bk_foot_ctrl.scaleY" 
		"Reindeer_RigRN.placeHolderList[63]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_bk_foot_ctrl.scaleZ" 
		"Reindeer_RigRN.placeHolderList[64]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_bk_foot_ctrl.rotateX" 
		"Reindeer_RigRN.placeHolderList[65]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_bk_foot_ctrl.rotateY" 
		"Reindeer_RigRN.placeHolderList[66]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_bk_foot_ctrl.rotateZ" 
		"Reindeer_RigRN.placeHolderList[67]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_bk_foot_ctrl.translateX" 
		"Reindeer_RigRN.placeHolderList[68]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_bk_foot_ctrl.translateY" 
		"Reindeer_RigRN.placeHolderList[69]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_bk_foot_ctrl.translateZ" 
		"Reindeer_RigRN.placeHolderList[70]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_bk_foot_ctrl.visibility" 
		"Reindeer_RigRN.placeHolderList[71]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_bk_foot_ctrl.scaleX" 
		"Reindeer_RigRN.placeHolderList[72]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_bk_foot_ctrl.scaleY" 
		"Reindeer_RigRN.placeHolderList[73]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_bk_foot_ctrl.scaleZ" 
		"Reindeer_RigRN.placeHolderList[74]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_bk_pv.translateX" 
		"Reindeer_RigRN.placeHolderList[75]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_bk_pv.translateY" 
		"Reindeer_RigRN.placeHolderList[76]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_bk_pv.translateZ" 
		"Reindeer_RigRN.placeHolderList[77]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_bk_pv.visibility" 
		"Reindeer_RigRN.placeHolderList[78]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_bk_pv.translateX" 
		"Reindeer_RigRN.placeHolderList[79]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_bk_pv.translateY" 
		"Reindeer_RigRN.placeHolderList[80]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_bk_pv.translateZ" 
		"Reindeer_RigRN.placeHolderList[81]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_bk_pv.visibility" 
		"Reindeer_RigRN.placeHolderList[82]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_fnt_foot_ctrl.rotateX" 
		"Reindeer_RigRN.placeHolderList[83]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_fnt_foot_ctrl.rotateY" 
		"Reindeer_RigRN.placeHolderList[84]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_fnt_foot_ctrl.rotateZ" 
		"Reindeer_RigRN.placeHolderList[85]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_fnt_foot_ctrl.translateX" 
		"Reindeer_RigRN.placeHolderList[86]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_fnt_foot_ctrl.translateY" 
		"Reindeer_RigRN.placeHolderList[87]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_fnt_foot_ctrl.translateZ" 
		"Reindeer_RigRN.placeHolderList[88]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_fnt_foot_ctrl.visibility" 
		"Reindeer_RigRN.placeHolderList[89]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_fnt_foot_ctrl.scaleX" 
		"Reindeer_RigRN.placeHolderList[90]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_fnt_foot_ctrl.scaleY" 
		"Reindeer_RigRN.placeHolderList[91]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|r_fnt_foot_ctrl.scaleZ" 
		"Reindeer_RigRN.placeHolderList[92]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_fnt_foot_ctrl.rotateX" 
		"Reindeer_RigRN.placeHolderList[93]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_fnt_foot_ctrl.rotateY" 
		"Reindeer_RigRN.placeHolderList[94]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_fnt_foot_ctrl.rotateZ" 
		"Reindeer_RigRN.placeHolderList[95]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_fnt_foot_ctrl.translateX" 
		"Reindeer_RigRN.placeHolderList[96]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_fnt_foot_ctrl.translateY" 
		"Reindeer_RigRN.placeHolderList[97]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_fnt_foot_ctrl.translateZ" 
		"Reindeer_RigRN.placeHolderList[98]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_fnt_foot_ctrl.visibility" 
		"Reindeer_RigRN.placeHolderList[99]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_fnt_foot_ctrl.scaleX" 
		"Reindeer_RigRN.placeHolderList[100]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_fnt_foot_ctrl.scaleY" 
		"Reindeer_RigRN.placeHolderList[101]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|l_fnt_foot_ctrl.scaleZ" 
		"Reindeer_RigRN.placeHolderList[102]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|f_fnt_pv.translateX" 
		"Reindeer_RigRN.placeHolderList[103]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|f_fnt_pv.translateY" 
		"Reindeer_RigRN.placeHolderList[104]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|f_fnt_pv.translateZ" 
		"Reindeer_RigRN.placeHolderList[105]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|f_fnt_pv.visibility" 
		"Reindeer_RigRN.placeHolderList[106]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|f_fnt_pv1.translateX" 
		"Reindeer_RigRN.placeHolderList[107]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|f_fnt_pv1.translateY" 
		"Reindeer_RigRN.placeHolderList[108]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|f_fnt_pv1.translateZ" 
		"Reindeer_RigRN.placeHolderList[109]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|f_fnt_pv1.visibility" 
		"Reindeer_RigRN.placeHolderList[110]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|center_ctrl.translateX" 
		"Reindeer_RigRN.placeHolderList[111]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|center_ctrl.translateY" 
		"Reindeer_RigRN.placeHolderList[112]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|center_ctrl.translateZ" 
		"Reindeer_RigRN.placeHolderList[113]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|center_ctrl.rotateX" 
		"Reindeer_RigRN.placeHolderList[114]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|center_ctrl.rotateY" 
		"Reindeer_RigRN.placeHolderList[115]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|center_ctrl.rotateZ" 
		"Reindeer_RigRN.placeHolderList[116]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|center_ctrl.scaleX" 
		"Reindeer_RigRN.placeHolderList[117]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|center_ctrl.scaleY" 
		"Reindeer_RigRN.placeHolderList[118]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|center_ctrl.scaleZ" 
		"Reindeer_RigRN.placeHolderList[119]" ""
		5 4 "Reindeer_RigRN" "|reindeer_rig|worldPlacement|center_ctrl.visibility" 
		"Reindeer_RigRN.placeHolderList[120]" ""
		"Reindeer_SkeletonRN" 29
		2 "|reindeer_rig|ref_frame|pelvis" "translate" " -type \"double3\" 0 232.42308711591741 -82.517332181750191"
		
		2 "|reindeer_rig|ref_frame|pelvis" "translateX" " -av"
		2 "|reindeer_rig|ref_frame|pelvis" "translateY" " -av"
		2 "|reindeer_rig|ref_frame|pelvis" "translateZ" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips" "translate" " -type \"double3\" -8.4401785154956031 3.1551051337804665 0"
		
		2 "|reindeer_rig|ref_frame|pelvis|hips" "translateX" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips" "translateY" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips" "translateZ" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips" "rotate" " -type \"double3\" 0 0 -11.021558009326185"
		
		2 "|reindeer_rig|ref_frame|pelvis|hips" "rotateX" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips" "rotateY" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips" "rotateZ" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips" "segmentScaleCompensate" " 1"
		2 "|reindeer_rig|ref_frame|pelvis|hips|l_femur" "rotate" " -type \"double3\" 1.9595806530088598 0.19121281072682597 -17.209480594223717"
		
		2 "|reindeer_rig|ref_frame|pelvis|hips|l_femur" "rotateZ" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips|l_femur" "rotateX" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips|l_femur" "rotateY" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"rotate" " -type \"double3\" -5.3444607051686575 14.918482638571497 45.396250873680813"
		
		2 "|reindeer_rig|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"rotateY" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"rotateZ" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"rotateX" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"rotate" " -type \"double3\" 11.357737553394024 -2.1444196310978039 37.316121797634217"
		
		2 "|reindeer_rig|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"rotateX" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"rotateY" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"rotateZ" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"rotate" " -type \"double3\" 360 -30.623084331898479 0"
		2 "|reindeer_rig|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"rotateX" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"rotateY" " -av"
		2 "|reindeer_rig|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"rotateZ" " -av";
lockNode -l 1 ;
createNode script -n "uiConfigurationScriptNode6";
	rename -uid "DCB95289-46BC-1263-C1DC-E78650EABB34";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 0\n                -polymeshes 1\n                -subdivSurfaces 0\n                -planes 0\n                -lights 0\n                -cameras 0\n                -controlVertices 0\n                -hulls 0\n                -grid 1\n                -imagePlane 0\n                -joints 0\n"
		+ "                -ikHandles 0\n                -deformers 0\n                -dynamics 0\n                -particleInstancers 0\n                -fluids 0\n                -hairSystems 0\n                -follicles 0\n                -nCloths 0\n                -nParticles 0\n                -nRigids 0\n                -dynamicConstraints 0\n                -locators 1\n                -manipulators 1\n                -pluginShapes 0\n                -dimensions 0\n                -handles 0\n                -pivots 0\n                -textures 0\n                -strokes 0\n                -motionTrails 0\n                -clipGhosts 0\n                -greasePencils 0\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1321\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 0 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 0\n            -polymeshes 1\n            -subdivSurfaces 0\n"
		+ "            -planes 0\n            -lights 0\n            -cameras 0\n            -controlVertices 0\n            -hulls 0\n            -grid 1\n            -imagePlane 0\n            -joints 0\n            -ikHandles 0\n            -deformers 0\n            -dynamics 0\n            -particleInstancers 0\n            -fluids 0\n            -hairSystems 0\n            -follicles 0\n            -nCloths 0\n            -nParticles 0\n            -nRigids 0\n            -dynamicConstraints 0\n            -locators 1\n            -manipulators 1\n            -pluginShapes 0\n            -dimensions 0\n            -handles 0\n            -pivots 0\n            -textures 0\n            -strokes 0\n            -motionTrails 0\n            -clipGhosts 0\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1321\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 0 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 1\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 1\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n"
		+ "                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n"
		+ "                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n"
		+ "                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n"
		+ "                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 19 100 -ps 2 81 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 0\\n    -polymeshes 1\\n    -subdivSurfaces 0\\n    -planes 0\\n    -lights 0\\n    -cameras 0\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 0\\n    -ikHandles 0\\n    -deformers 0\\n    -dynamics 0\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 0\\n    -handles 0\\n    -pivots 0\\n    -textures 0\\n    -strokes 0\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1321\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 0 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 0\\n    -polymeshes 1\\n    -subdivSurfaces 0\\n    -planes 0\\n    -lights 0\\n    -cameras 0\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 0\\n    -ikHandles 0\\n    -deformers 0\\n    -dynamics 0\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 0\\n    -handles 0\\n    -pivots 0\\n    -textures 0\\n    -strokes 0\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1321\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 0 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode6";
	rename -uid "18898A33-4B88-16E5-1F71-C68CFC1F4DD2";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 23 -ast 0 -aet 60 ";
	setAttr ".st" 6;
createNode animCurveTL -n "reindeer__flexi_end_ctrl_translateX";
	rename -uid "16FBCBEA-434B-F080-E26F-2E9647465B37";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -0.77873565007180612 23 -0.77873565007180612;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "reindeer__flexi_end_ctrl_translateY";
	rename -uid "BDC8F642-4B17-8E1B-F6F9-99B54E53A3E1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -1.1524045501407665 23 -1.1524045501407665;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "reindeer__flexi_end_ctrl_translateZ";
	rename -uid "F34A1E71-4B68-1E4D-1B31-05AF547876CA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -0.25274284172352091 23 -0.25274284172352091;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "reindeer__flexi_mid_ctrl_translateX";
	rename -uid "C3B42AB9-45BD-9FB8-6417-88BF938E41C1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0.11410967796798772 11 0.11410967796798772
		 15 0.10263490363561707 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "reindeer__flexi_mid_ctrl_translateY";
	rename -uid "5C1E5B33-48FD-A6E9-BB6E-CEB9702DEDD6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 3.2838057345473115 11 3.2838057345473115
		 15 2.9535889604201704 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "reindeer__flexi_mid_ctrl_translateZ";
	rename -uid "D1B3917D-42F1-CE94-290C-138275F86E58";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 -1.1456695804793689e-018
		 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "reindeer__flexi_start_ctrl_translateX";
	rename -uid "9C52413A-48DC-E46C-0BC9-D2A35116DB31";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0.52986320904756024 11 -0.79057195955336168
		 23 0.52986320904756024;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "reindeer__flexi_start_ctrl_translateY";
	rename -uid "47F2108E-45C7-DBF9-139D-51BBBC085A57";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 0 4 0.40800810094317108 8 0.32916223343331397
		 11 -1.6885901873796036 23 0;
	setAttr -s 5 ".kit[0:4]"  1 18 18 18 1;
	setAttr -s 5 ".kot[0:4]"  1 18 18 18 1;
	setAttr -s 5 ".kix[0:4]"  0.16740104556083679 1 0.49104702472686768 
		1 0.16938044130802155;
	setAttr -s 5 ".kiy[0:4]"  0.98588889837265015 0 -0.87113308906555176 
		0 0.98555076122283936;
	setAttr -s 5 ".kox[0:4]"  0.16740094125270844 1 0.49104699492454529 
		1 0.16938042640686035;
	setAttr -s 5 ".koy[0:4]"  0.98588889837265015 0 -0.87113302946090698 
		0 0.98555076122283936;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "reindeer__flexi_start_ctrl_translateZ";
	rename -uid "FB7772B8-4168-C4F8-C229-70AEAF572E43";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 -1.1765326691727476e-016 4 -1.1623770445508502e-016
		 8 -1.1623770445508502e-016 11 -1.1623770445508502e-016 15 -1.1623770445508502e-016
		 23 -1.1765326691727476e-016;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "worldPlacement_translateX";
	rename -uid "6297F4B8-4609-5B2E-E719-9594B1A7AC40";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "worldPlacement_translateY";
	rename -uid "0ABD1C7A-4841-F65D-9D9D-15B8CF4536EC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "worldPlacement_translateZ";
	rename -uid "F522192C-4540-F61F-B954-2886A970A030";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "center_ctrl_translateX";
	rename -uid "D8A56C08-4223-8F53-3223-F39C00518D03";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "center_ctrl_translateY";
	rename -uid "44C6144B-4278-86B0-27E4-2E9144450767";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "center_ctrl_translateZ";
	rename -uid "15F1BE5B-47CC-5CE3-7CF2-DBBAB11249F2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "f_fnt_pv_translateX";
	rename -uid "3A862451-435E-D911-F586-6CA829021D6F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 4 0 8 0 11 0 15 0 19 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "f_fnt_pv_translateY";
	rename -uid "9000AD80-4BAA-59CC-732C-48A079B01F77";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 4 0 8 0 11 0 15 0 19 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "f_fnt_pv_translateZ";
	rename -uid "36868C89-4962-907F-E8EA-DB8537296C20";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 127.92337046984778 4 0 8 0 11 0 15 138.7755173091299
		 19 199.98988628056088 23 127.92337046984778;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "f_fnt_pv1_translateX";
	rename -uid "47911661-4654-8EF1-68C7-2D88278A1DFC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "f_fnt_pv1_translateY";
	rename -uid "36A4B3E2-4CC6-9FFC-E701-31BA592C82C3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "f_fnt_pv1_translateZ";
	rename -uid "F689D8BC-46F1-9DE4-A7E9-42BF0CFA8D18";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 4 0 8 0 11 0 15 135.90280316426242 18 176.49729902476105
		 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "l_bk_foot_ctrl_translateX";
	rename -uid "C5A1C883-4132-3D6A-CF89-2894B095DD81";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "l_bk_foot_ctrl_translateY";
	rename -uid "46AA9314-479A-7FFC-6A1B-F185C38BA863";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 4 61.38144978038008 8 48.719329904992662
		 11 14.77218360294578 15 0 19 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "l_bk_foot_ctrl_translateZ";
	rename -uid "7DB7D244-4BC3-35BE-76CC-64AB8B246A98";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 -84.433557528556818 4 -144.41402178979763
		 8 -35.568428497790251 11 47.228430399784727 15 112.47406610027743 23 -84.433557528556818;
	setAttr -s 6 ".kit[2:5]"  18 18 18 1;
	setAttr -s 6 ".kot[2:5]"  18 18 2 1;
	setAttr -s 6 ".ktl[4:5]" no yes;
	setAttr -s 6 ".kix[0:5]"  0.00075066316640004516 0.0076619787141680717 
		0.0012175441952422261 0.0015761218965053558 1 0.0014244505437090993;
	setAttr -s 6 ".kiy[0:5]"  -0.9999997615814209 0.99997067451477051 
		0.99999922513961792 0.99999874830245972 0 -0.99999904632568359;
	setAttr -s 6 ".kox[0:5]"  0.00075066310819238424 0.0076619782485067844 
		0.0012175441952422261 0.0015761221293359995 0.0013542715460062027 0.0014244494959712029;
	setAttr -s 6 ".koy[0:5]"  -0.9999997615814209 0.99997067451477051 
		0.9999992847442627 0.99999880790710449 -0.99999904632568359 -0.99999898672103882;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "l_bk_pv_translateX";
	rename -uid "59E87C41-4892-E52D-E732-B5A51ED201FA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "l_bk_pv_translateY";
	rename -uid "60632BEF-421F-52E8-2CBC-0D865FFA550D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "l_bk_pv_translateZ";
	rename -uid "5A949B3E-4A9C-3EBD-0C3A-EDB3111C8652";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "l_fnt_foot_ctrl_translateX";
	rename -uid "F8B9279D-4369-A992-243D-2C8EB5A3BA15";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 1 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "l_fnt_foot_ctrl_translateY";
	rename -uid "28829462-4E10-A510-E6EF-D5A837C26254";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  0 12.614674922997281 1 0 4 0 7 0 8 2.4978262111006018
		 11 37.45954684377628 15 42.228021162681955 19 37.349238099368264 23 12.614674922997281;
	setAttr -s 9 ".kit[6:8]"  1 1 1;
	setAttr -s 9 ".kot[6:8]"  1 1 1;
	setAttr -s 9 ".kix[6:8]"  1 0.011814676225185394 1;
	setAttr -s 9 ".kiy[6:8]"  0 -0.99993020296096802 0;
	setAttr -s 9 ".kox[6:8]"  1 0.011814677156507969 1;
	setAttr -s 9 ".koy[6:8]"  -3.2587347959633917e-005 -0.99993026256561279 
		0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "l_fnt_foot_ctrl_translateZ";
	rename -uid "0B228E62-49CB-9643-C9E8-A9A48A3278D5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 136.97723974365658 1 112.81520017608364
		 8 -57.021492911105568 11 -60.426716805736376 15 1.9337379690195178 19 128.29036006697561
		 23 136.97723974365658;
	setAttr -s 7 ".kit[1:6]"  1 2 1 18 18 18;
	setAttr -s 7 ".kot[1:6]"  2 1 1 18 18 18;
	setAttr -s 7 ".ktl[1:6]" no no yes yes yes yes;
	setAttr -s 7 ".kix[1:6]"  0.0021817497909069061 0.0013738676207140088 
		0.004271938931196928 0.0014130485942587256 0.0051162051968276501 1;
	setAttr -s 7 ".kiy[1:6]"  -0.99999761581420898 -0.99999910593032837 
		0.99999094009399414 0.99999904632568359 0.99998688697814941 0;
	setAttr -s 7 ".kox[2:6]"  0.0025238937232643366 0.0042719370685517788 
		0.0014130485942587256 0.0051162051968276501 1;
	setAttr -s 7 ".koy[2:6]"  -0.9999968409538269 0.99999088048934937 
		0.99999904632568359 0.99998688697814941 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "r_bk_foot_ctrl_translateX";
	rename -uid "DB473678-4991-8036-F896-6EBB497D1C4C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 -66.272386047791798 4 -66.272386047791798
		 8 -66.272386047791798 11 -66.272386047791798 15 -66.272386047791798 23 -66.272386047791798;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "r_bk_foot_ctrl_translateY";
	rename -uid "5801E950-4BF0-12EA-FEF5-AB8EC65535E6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 69.819075292856937 4 96.529237076543026
		 8 53.826849264451553 11 0 15 0 23 69.819075292856937;
	setAttr -s 6 ".kit[0:5]"  1 18 1 18 18 1;
	setAttr -s 6 ".kot[0:5]"  1 18 1 18 18 1;
	setAttr -s 6 ".kix[0:5]"  0.0025101830251514912 1 0.0012534782290458679 
		1 1 0.0024852761998772621;
	setAttr -s 6 ".kiy[0:5]"  0.9999968409538269 0 -0.9999992847442627 
		0 0 0.99999696016311646;
	setAttr -s 6 ".kox[0:5]"  0.0025101827923208475 1 0.0012534779962152243 
		1 1 0.0024852768983691931;
	setAttr -s 6 ".koy[0:5]"  0.9999968409538269 0 -0.99999922513961792 
		0 0 0.99999690055847168;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "r_bk_foot_ctrl_translateZ";
	rename -uid "A4FC53E2-4A76-941F-2B57-B3B74E36AD1E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 -156.02650314697735 4 -128.38172917498341
		 8 -7.9426500279849961 11 72.705710604492864 19 -124.69233140661865 23 -156.02650314697735;
	setAttr -s 6 ".kit[1:5]"  1 18 18 2 18;
	setAttr -s 6 ".kot[1:5]"  1 18 2 18 18;
	setAttr -s 6 ".ktl[3:5]" no no yes;
	setAttr -s 6 ".kix[1:5]"  0.0016445021610707045 0.00116035679820925 
		1 0.0013509070267900825 1;
	setAttr -s 6 ".kiy[1:5]"  0.99999868869781494 0.99999934434890747 
		0 -0.99999904632568359 0;
	setAttr -s 6 ".kox[1:5]"  0.0016445022774860263 0.00116035679820925 
		0.0013509070267900825 0.0017487670993432403 1;
	setAttr -s 6 ".koy[1:5]"  0.99999868869781494 0.99999940395355225 
		-0.99999904632568359 -0.99999850988388062 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "r_bk_pv_translateX";
	rename -uid "1DE5D986-4843-E392-3931-FB8630BB0507";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 7.3379117302326193 8 7.3379117302326193
		 11 7.3379117302326193 15 7.3379117302326193 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "r_bk_pv_translateY";
	rename -uid "CBDA2759-4015-1CA8-4DBB-6BAFA47DE258";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 96.13705840410978 8 96.13705840410978
		 11 96.13705840410978 15 96.13705840410978 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "r_bk_pv_translateZ";
	rename -uid "63E895A8-4EAA-DC2C-D1F8-C1A18360CF55";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "r_fnt_foot_ctrl_translateX";
	rename -uid "17D7DB52-4871-0F29-8A65-BBAABC3912C0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -66.891773313485373 4 -66.891773313485373
		 8 -66.891773313485373 11 -66.891773313485373 15 -66.891773313485373 18 -66.891773313485373
		 23 -66.891773313485373;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "r_fnt_foot_ctrl_translateY";
	rename -uid "B2487C7C-4344-14B7-204B-A9AAC88EDA14";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -2.6912969900649841e-005 4 -2.6912969900649841e-005
		 8 35.786672523810523 15 65.601921082535313 18 20.363229246788787 19 0 23 -2.6912969900649841e-005;
	setAttr -s 7 ".kit[2:6]"  1 18 18 18 18;
	setAttr -s 7 ".kot[2:6]"  1 18 18 18 18;
	setAttr -s 7 ".kix[2:6]"  0.0039160475134849548 1 0.0020324564538896084 
		0.9999997615814209 1;
	setAttr -s 7 ".kiy[2:6]"  0.99999237060546875 0 -0.99999791383743286 
		-0.00060554168885573745 0;
	setAttr -s 7 ".kox[2:6]"  0.0039160451851785183 1 0.0020324564538896084 
		0.9999997615814209 1;
	setAttr -s 7 ".koy[2:6]"  0.99999237060546875 0 -0.99999791383743286 
		-0.00060554168885573745 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "r_fnt_foot_ctrl_translateZ";
	rename -uid "7C3CDA10-4EFE-C368-7196-D09EBB384448";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 -21.935714911525338 4 -88.443815025765701
		 8 -99.948635433074273 11 28.175805490843288 15 98.250401642036309 18 140.70240585275408
		 19 117.06191773286245 23 -21.935714911525338;
	setAttr -s 8 ".kit[0:7]"  2 2 1 1 18 18 18 1;
	setAttr -s 8 ".kot[0:7]"  2 1 1 1 18 18 2 1;
	setAttr -s 8 ".ktl[1:7]" no yes yes yes yes no yes;
	setAttr -s 8 ".kix[2:7]"  0.0048122871667146683 0.00078190694330260158 
		0.0020735797006636858 1 0.0010247692698612809 0.00093471707077696919;
	setAttr -s 8 ".kiy[2:7]"  0.99998843669891357 0.99999970197677612 
		0.99999785423278809 0 -0.9999995231628418 -0.99999958276748657;
	setAttr -s 8 ".kox[1:7]"  0.0026183563750237226 0.0048122839070856571 
		0.00078190688509494066 0.0020735794678330421 1 0.00095924845663830638 0.00093471707077696919;
	setAttr -s 8 ".koy[1:7]"  -0.9999966025352478 0.99998843669891357 
		0.99999970197677612 0.99999785423278809 0 -0.99999958276748657 -0.99999958276748657;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "reindeer__flexi_start_ctrl_rotateX";
	rename -uid "ABF71C7F-4922-DD21-DCFC-F9AEE4C5548F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 4 0 8 0 11 0 15 -4.06884373729857 19 -5.6049103349666005
		 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "reindeer__flexi_start_ctrl_rotateY";
	rename -uid "4B7F702B-4C2A-7228-A434-DFAC1E3A81C4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 4 0 8 0 11 0 15 -9.0903919138474745
		 19 -1.5360130903057112 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "reindeer__flexi_start_ctrl_rotateZ";
	rename -uid "1E6BA3B5-42ED-D93D-171F-908A0FDFDE1A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 11.021558009326185 4 5.6651128374478237
		 8 -20.45570698242922 11 -28.080414258012439 23 11.021558009326185;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "reindeer__flexi_mid_ctrl_rotateX";
	rename -uid "94B867D5-4AF2-FA86-8A14-01AD30E45F1F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "reindeer__flexi_mid_ctrl_rotateY";
	rename -uid "97F7578A-4D07-B6A6-8B9F-7D8E00C12ACC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "reindeer__flexi_mid_ctrl_rotateZ";
	rename -uid "A47A1C1D-4120-FB8A-F970-C8AC792EFD78";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "reindeer__flexi_mid_ctrl_followStart";
	rename -uid "2C46F3E2-448E-7240-0D76-60B2A93A7953";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "reindeer__flexi_end_ctrl_rotateX";
	rename -uid "6EF0406E-45C9-8D06-EDA1-5D9D72BEFA39";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 7.3213158627737354 4 -1.9224436504760278
		 8 -9.2423259674376741 11 -5.3872560719122271 15 0 19 -0.99378201533597099 23 7.3213158627737354;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "reindeer__flexi_end_ctrl_rotateY";
	rename -uid "F4F0BBFC-49CF-F890-B645-0380E347CF88";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 -9.574488232712115 4 -29.104286259048358
		 8 -17.611718755805153 11 -12.754885141361711 15 0 19 -4.8896842782878815 23 -9.574488232712115;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "reindeer__flexi_end_ctrl_rotateZ";
	rename -uid "4E374639-4455-14FE-D00F-DBB9CF6FFC32";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0.52992291871311592 4 7.4570034255795035
		 8 20.354868125788606 11 6.9637447065627924 15 0 19 15.707434296312995 23 0.52992291871311592;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "reindeer__flexi_end_ctrl_followMid";
	rename -uid "55E02BAA-40B5-F09E-DFAB-909581EF26B5";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "worldPlacement_visibility";
	rename -uid "F28F9AEC-4278-7C58-14EE-3094339A6523";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "worldPlacement_rotateX";
	rename -uid "BDB90994-4884-C440-E092-21A9D58A32C5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "worldPlacement_rotateY";
	rename -uid "B3297C93-4B6F-9181-20A8-4C8B53D31B46";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "worldPlacement_rotateZ";
	rename -uid "EFC20A50-4F31-55A3-7D83-E7885C0FC0BC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "worldPlacement_scaleX";
	rename -uid "BF23A2A1-4A1A-3CA4-FC18-D1B07056546C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "worldPlacement_scaleY";
	rename -uid "F8F239A0-4C76-49B0-7479-DEA35188BF38";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "worldPlacement_scaleZ";
	rename -uid "2277EC15-4075-0E77-E987-8A983E7564E0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "r_bk_foot_ctrl_visibility";
	rename -uid "E48DEF46-4A53-1EF6-C27F-9283E59554AC";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "r_bk_foot_ctrl_rotateX";
	rename -uid "3FEEE72F-43D6-281F-D63F-40A24B35C427";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 65.938564396739054 4 65.938564396739054
		 8 -14.38772571473945 10 -39.393754175768258 11 0 15 0 19 66.756163054945475 23 65.938564396739054;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "r_bk_foot_ctrl_rotateY";
	rename -uid "05396B86-46CA-3A7A-6B91-C9B2445E1DD4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "r_bk_foot_ctrl_rotateZ";
	rename -uid "D67ABA44-4E39-0AC3-A3A9-3E83F4CF28BD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "r_bk_foot_ctrl_scaleX";
	rename -uid "33DB577E-4883-F907-1024-6C8169320A38";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "r_bk_foot_ctrl_scaleY";
	rename -uid "76894AA0-4C76-A258-809B-5C921A3C94F6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "r_bk_foot_ctrl_scaleZ";
	rename -uid "04CDF50B-48D1-4ABA-A39C-E4B686F84F12";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "l_bk_foot_ctrl_visibility";
	rename -uid "BE7AC70D-47CA-14C4-1181-679BFB20D90F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "l_bk_foot_ctrl_rotateX";
	rename -uid "171B9A1D-45AF-C25C-7585-28A3F76BEF45";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 4 65.98614781799759 8 26.920582059506827
		 11 0 14 -25.344088860092569 15 0 19 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "l_bk_foot_ctrl_rotateY";
	rename -uid "CDCAA8AF-4D00-5E04-CC8A-1CB4747D6146";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "l_bk_foot_ctrl_rotateZ";
	rename -uid "25405DF1-4186-5B63-2806-10BC8FA9A6BE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "l_bk_foot_ctrl_scaleX";
	rename -uid "E9F0533E-4B42-D0AD-EE14-988C240318EE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "l_bk_foot_ctrl_scaleY";
	rename -uid "F9F2AF79-4E8D-4D77-8A03-3DAF53D56548";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "l_bk_foot_ctrl_scaleZ";
	rename -uid "F00C7831-4D1F-49A8-03B9-E5BAED81EBE1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "r_bk_pv_visibility";
	rename -uid "2226B0AD-4842-6D28-896D-CE9A1DD1E387";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "l_bk_pv_visibility";
	rename -uid "6EC7B45F-4339-30B4-CD89-AB88A15A840F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "r_fnt_foot_ctrl_visibility";
	rename -uid "78EA8DE8-47FB-DCA1-3B36-64BF81749B89";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 4 1 8 1 11 1 15 1 18 1 23 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "r_fnt_foot_ctrl_rotateX";
	rename -uid "CF681A8F-4397-E733-4B4C-9A965AF96687";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 4 0 8 80.097795410038955 11 80.097795410038955
		 15 43.575583359416768 18 -19.478524620652813 19 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "r_fnt_foot_ctrl_rotateY";
	rename -uid "8ED591D5-4546-3F39-686A-1284076D0C58";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 4 0 8 0 11 0 15 0 18 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "r_fnt_foot_ctrl_rotateZ";
	rename -uid "48B1E1C8-45FC-8A7C-E975-D5BCB8621D2F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 4 0 8 0 11 0 15 0 18 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "r_fnt_foot_ctrl_scaleX";
	rename -uid "AB5E3164-48AD-65DD-A4CB-85A777A28D6E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 4 1 8 1 11 1 15 1 18 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "r_fnt_foot_ctrl_scaleY";
	rename -uid "7EC45BFF-46C7-3DBE-E8B7-F68BF368608F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 4 1 8 1 11 1 15 1 18 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "r_fnt_foot_ctrl_scaleZ";
	rename -uid "27CEA6BE-46C7-2305-6610-588DFD2B9CDC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 4 1 8 1 11 1 15 1 18 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "l_fnt_foot_ctrl_visibility";
	rename -uid "02AFE74B-4A68-E493-83E9-649A297CF31F";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 1 1 4 1 8 1 11 1 15 1 23 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "l_fnt_foot_ctrl_rotateX";
	rename -uid "853BCDB7-4645-C3CE-C961-71AB5E159EBB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  0 -24.823944400791021 1 0 4 0 7 0 8 26.659502006303299
		 11 100.24393094744946 15 103.38726076523054 19 61.597993445680579 23 -24.823944400791021;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "l_fnt_foot_ctrl_rotateY";
	rename -uid "7C331A2A-4267-F200-F23C-609DC829F31B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 1 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "l_fnt_foot_ctrl_rotateZ";
	rename -uid "720DFE67-4355-0744-7460-A1A9B6217093";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 1 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "l_fnt_foot_ctrl_scaleX";
	rename -uid "7C546474-4221-0B4E-1A2A-319723C2BCB2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 1 1 4 1 8 1 11 1 15 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "l_fnt_foot_ctrl_scaleY";
	rename -uid "FA19A104-4C32-F695-15DC-15A3F21BAEA2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 1 1 4 1 8 1 11 1 15 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "l_fnt_foot_ctrl_scaleZ";
	rename -uid "30F7CE57-40FC-FEDA-E5C2-67A8088F8667";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 1 1 4 1 8 1 11 1 15 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "f_fnt_pv_visibility";
	rename -uid "9183635B-4640-F311-415F-DC9DB1BB8A87";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 1 4 1 8 1 11 1 15 1 19 1 23 1;
	setAttr -s 7 ".kot[0:6]"  5 5 5 5 5 5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "f_fnt_pv1_visibility";
	rename -uid "CC2DD94B-4259-9910-CC26-5C80192921A7";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "center_ctrl_visibility";
	rename -uid "341E8EA8-4793-2CEF-2704-2582B599E4D0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr -s 6 ".kot[0:5]"  5 5 5 5 5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "center_ctrl_rotateX";
	rename -uid "CE628703-4CB0-2A56-595F-92BCCB8EA757";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1.9901826778449128 11 -2.1302702861363776
		 23 1.9901826778449128;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "center_ctrl_rotateY";
	rename -uid "1E7C94F1-4C0C-4E72-1446-8E89948BF5BC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "center_ctrl_rotateZ";
	rename -uid "E784F4C3-4C6D-F7FE-AA36-6DBE55374455";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 0 4 0 8 0 11 0 15 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "center_ctrl_scaleX";
	rename -uid "691A2033-46D3-C1D9-4646-509A70D83660";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "center_ctrl_scaleY";
	rename -uid "F3B55621-470E-DA2F-21A7-57B9C8CB320A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "center_ctrl_scaleZ";
	rename -uid "B8224831-4827-1321-B8A3-809D81251715";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 1 4 1 8 1 11 1 15 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "locator1_visibility";
	rename -uid "5FBB2B23-4B8C-135A-CE42-84AC505E1E4B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 23 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "locator1_translateX";
	rename -uid "36CC6FF7-4DA6-C63A-BC9A-88822BA43455";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "locator1_translateY";
	rename -uid "363D0E24-4160-2BE6-0B04-98A37A57984A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "locator1_translateZ";
	rename -uid "344367BD-40E2-4FC2-17A4-43A76986289A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "locator1_rotateX";
	rename -uid "3959E6A4-4E47-D596-356E-F9AD70022697";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 14.283541698654391 12 -1.1708652999487499
		 23 14.283541698654391;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "locator1_rotateY";
	rename -uid "3D045F51-4677-C319-1DE6-20A4C2786C87";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "locator1_rotateZ";
	rename -uid "9154F372-4DD6-8220-AE08-4FA95DDD0CCB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "locator1_scaleX";
	rename -uid "D0272064-498B-C413-2FDE-A8BD1C9DA01F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "locator1_scaleY";
	rename -uid "EBFDD641-425D-20EF-6780-95A217655675";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "locator1_scaleZ";
	rename -uid "C10183A3-41F5-6D30-7172-DFA4ECCB7259";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 23 1;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "neckCtrl_rotateX";
	rename -uid "7553F832-43D6-C007-C635-CD99C46EC948";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 12 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "neckCtrl_rotateY";
	rename -uid "EBFEB2B6-4F99-0ACE-11F4-69967EA5BC37";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 12 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTA -n "neckCtrl_rotateZ";
	rename -uid "FA68F448-420D-D17E-C852-F9BC65AD4F51";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 -30.623084331898461 12 3.0327805390047344
		 23 -30.623084331898461;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTU -n "neckCtrl_visibility";
	rename -uid "D496408E-4D50-4531-8B87-029A3FE21EC0";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 12 1 23 1;
	setAttr -s 3 ".kot[0:2]"  5 5 5;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "neckCtrl_translateX";
	rename -uid "B5853315-4659-5FB1-1B10-969B9E30169B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 12 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "neckCtrl_translateY";
	rename -uid "B2B46733-4FE0-9491-EC1D-18B2D5F28846";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 12 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
createNode animCurveTL -n "neckCtrl_translateZ";
	rename -uid "A06A8026-4AFC-7E33-9EAE-FAA0C2985F87";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 12 0 23 0;
	setAttr ".pre" 3;
	setAttr ".pst" 3;
select -ne :time1;
	setAttr ".o" 0;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 21 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 5 ".r";
select -ne :defaultTextureList1;
select -ne :initialShadingGroup;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
connectAttr "Reindeer_RigRN.phl[1]" "Reindeer_RigRN.phl[2]";
connectAttr "Reindeer_RigRN.phl[3]" "Reindeer_RigRN.phl[4]";
connectAttr "Reindeer_RigRN.phl[5]" "Reindeer_RigRN.phl[6]";
connectAttr "Reindeer_RigRN.phl[7]" "Reindeer_RigRN.phl[8]";
connectAttr "Reindeer_RigRN.phl[9]" "Reindeer_RigRN.phl[10]";
connectAttr "Reindeer_RigRN.phl[11]" "Reindeer_RigRN.phl[12]";
connectAttr "reindeer__flexi_start_ctrl_translateX.o" "Reindeer_RigRN.phl[13]";
connectAttr "reindeer__flexi_start_ctrl_translateY.o" "Reindeer_RigRN.phl[14]";
connectAttr "reindeer__flexi_start_ctrl_translateZ.o" "Reindeer_RigRN.phl[15]";
connectAttr "reindeer__flexi_start_ctrl_rotateX.o" "Reindeer_RigRN.phl[16]";
connectAttr "reindeer__flexi_start_ctrl_rotateY.o" "Reindeer_RigRN.phl[17]";
connectAttr "reindeer__flexi_start_ctrl_rotateZ.o" "Reindeer_RigRN.phl[18]";
connectAttr "reindeer__flexi_mid_ctrl_followStart.o" "Reindeer_RigRN.phl[19]";
connectAttr "reindeer__flexi_mid_ctrl_translateX.o" "Reindeer_RigRN.phl[20]";
connectAttr "reindeer__flexi_mid_ctrl_translateY.o" "Reindeer_RigRN.phl[21]";
connectAttr "reindeer__flexi_mid_ctrl_translateZ.o" "Reindeer_RigRN.phl[22]";
connectAttr "reindeer__flexi_mid_ctrl_rotateX.o" "Reindeer_RigRN.phl[23]";
connectAttr "reindeer__flexi_mid_ctrl_rotateY.o" "Reindeer_RigRN.phl[24]";
connectAttr "reindeer__flexi_mid_ctrl_rotateZ.o" "Reindeer_RigRN.phl[25]";
connectAttr "reindeer__flexi_end_ctrl_followMid.o" "Reindeer_RigRN.phl[26]";
connectAttr "reindeer__flexi_end_ctrl_translateX.o" "Reindeer_RigRN.phl[27]";
connectAttr "reindeer__flexi_end_ctrl_translateY.o" "Reindeer_RigRN.phl[28]";
connectAttr "reindeer__flexi_end_ctrl_translateZ.o" "Reindeer_RigRN.phl[29]";
connectAttr "reindeer__flexi_end_ctrl_rotateX.o" "Reindeer_RigRN.phl[30]";
connectAttr "reindeer__flexi_end_ctrl_rotateY.o" "Reindeer_RigRN.phl[31]";
connectAttr "reindeer__flexi_end_ctrl_rotateZ.o" "Reindeer_RigRN.phl[32]";
connectAttr "neckBaseCntl_orientConstraint1.crx" "Reindeer_RigRN.phl[33]";
connectAttr "neckBaseCntl_orientConstraint1.cry" "Reindeer_RigRN.phl[34]";
connectAttr "neckBaseCntl_orientConstraint1.crz" "Reindeer_RigRN.phl[35]";
connectAttr "Reindeer_RigRN.phl[36]" "neckBaseCntl_orientConstraint1.cro";
connectAttr "Reindeer_RigRN.phl[37]" "neckBaseCntl_orientConstraint1.cpim";
connectAttr "neckCtrl_translateX.o" "Reindeer_RigRN.phl[38]";
connectAttr "neckCtrl_translateY.o" "Reindeer_RigRN.phl[39]";
connectAttr "neckCtrl_translateZ.o" "Reindeer_RigRN.phl[40]";
connectAttr "neckCtrl_rotateX.o" "Reindeer_RigRN.phl[41]";
connectAttr "neckCtrl_rotateY.o" "Reindeer_RigRN.phl[42]";
connectAttr "neckCtrl_rotateZ.o" "Reindeer_RigRN.phl[43]";
connectAttr "neckCtrl_visibility.o" "Reindeer_RigRN.phl[44]";
connectAttr "worldPlacement_translateX.o" "Reindeer_RigRN.phl[45]";
connectAttr "worldPlacement_translateY.o" "Reindeer_RigRN.phl[46]";
connectAttr "worldPlacement_translateZ.o" "Reindeer_RigRN.phl[47]";
connectAttr "worldPlacement_visibility.o" "Reindeer_RigRN.phl[48]";
connectAttr "worldPlacement_rotateX.o" "Reindeer_RigRN.phl[49]";
connectAttr "worldPlacement_rotateY.o" "Reindeer_RigRN.phl[50]";
connectAttr "worldPlacement_rotateZ.o" "Reindeer_RigRN.phl[51]";
connectAttr "worldPlacement_scaleX.o" "Reindeer_RigRN.phl[52]";
connectAttr "worldPlacement_scaleY.o" "Reindeer_RigRN.phl[53]";
connectAttr "worldPlacement_scaleZ.o" "Reindeer_RigRN.phl[54]";
connectAttr "r_bk_foot_ctrl_rotateX.o" "Reindeer_RigRN.phl[55]";
connectAttr "r_bk_foot_ctrl_rotateY.o" "Reindeer_RigRN.phl[56]";
connectAttr "r_bk_foot_ctrl_rotateZ.o" "Reindeer_RigRN.phl[57]";
connectAttr "r_bk_foot_ctrl_translateX.o" "Reindeer_RigRN.phl[58]";
connectAttr "r_bk_foot_ctrl_translateY.o" "Reindeer_RigRN.phl[59]";
connectAttr "r_bk_foot_ctrl_translateZ.o" "Reindeer_RigRN.phl[60]";
connectAttr "r_bk_foot_ctrl_visibility.o" "Reindeer_RigRN.phl[61]";
connectAttr "r_bk_foot_ctrl_scaleX.o" "Reindeer_RigRN.phl[62]";
connectAttr "r_bk_foot_ctrl_scaleY.o" "Reindeer_RigRN.phl[63]";
connectAttr "r_bk_foot_ctrl_scaleZ.o" "Reindeer_RigRN.phl[64]";
connectAttr "l_bk_foot_ctrl_rotateX.o" "Reindeer_RigRN.phl[65]";
connectAttr "l_bk_foot_ctrl_rotateY.o" "Reindeer_RigRN.phl[66]";
connectAttr "l_bk_foot_ctrl_rotateZ.o" "Reindeer_RigRN.phl[67]";
connectAttr "l_bk_foot_ctrl_translateX.o" "Reindeer_RigRN.phl[68]";
connectAttr "l_bk_foot_ctrl_translateY.o" "Reindeer_RigRN.phl[69]";
connectAttr "l_bk_foot_ctrl_translateZ.o" "Reindeer_RigRN.phl[70]";
connectAttr "l_bk_foot_ctrl_visibility.o" "Reindeer_RigRN.phl[71]";
connectAttr "l_bk_foot_ctrl_scaleX.o" "Reindeer_RigRN.phl[72]";
connectAttr "l_bk_foot_ctrl_scaleY.o" "Reindeer_RigRN.phl[73]";
connectAttr "l_bk_foot_ctrl_scaleZ.o" "Reindeer_RigRN.phl[74]";
connectAttr "r_bk_pv_translateX.o" "Reindeer_RigRN.phl[75]";
connectAttr "r_bk_pv_translateY.o" "Reindeer_RigRN.phl[76]";
connectAttr "r_bk_pv_translateZ.o" "Reindeer_RigRN.phl[77]";
connectAttr "r_bk_pv_visibility.o" "Reindeer_RigRN.phl[78]";
connectAttr "l_bk_pv_translateX.o" "Reindeer_RigRN.phl[79]";
connectAttr "l_bk_pv_translateY.o" "Reindeer_RigRN.phl[80]";
connectAttr "l_bk_pv_translateZ.o" "Reindeer_RigRN.phl[81]";
connectAttr "l_bk_pv_visibility.o" "Reindeer_RigRN.phl[82]";
connectAttr "r_fnt_foot_ctrl_rotateX.o" "Reindeer_RigRN.phl[83]";
connectAttr "r_fnt_foot_ctrl_rotateY.o" "Reindeer_RigRN.phl[84]";
connectAttr "r_fnt_foot_ctrl_rotateZ.o" "Reindeer_RigRN.phl[85]";
connectAttr "r_fnt_foot_ctrl_translateX.o" "Reindeer_RigRN.phl[86]";
connectAttr "r_fnt_foot_ctrl_translateY.o" "Reindeer_RigRN.phl[87]";
connectAttr "r_fnt_foot_ctrl_translateZ.o" "Reindeer_RigRN.phl[88]";
connectAttr "r_fnt_foot_ctrl_visibility.o" "Reindeer_RigRN.phl[89]";
connectAttr "r_fnt_foot_ctrl_scaleX.o" "Reindeer_RigRN.phl[90]";
connectAttr "r_fnt_foot_ctrl_scaleY.o" "Reindeer_RigRN.phl[91]";
connectAttr "r_fnt_foot_ctrl_scaleZ.o" "Reindeer_RigRN.phl[92]";
connectAttr "l_fnt_foot_ctrl_rotateX.o" "Reindeer_RigRN.phl[93]";
connectAttr "l_fnt_foot_ctrl_rotateY.o" "Reindeer_RigRN.phl[94]";
connectAttr "l_fnt_foot_ctrl_rotateZ.o" "Reindeer_RigRN.phl[95]";
connectAttr "l_fnt_foot_ctrl_translateX.o" "Reindeer_RigRN.phl[96]";
connectAttr "l_fnt_foot_ctrl_translateY.o" "Reindeer_RigRN.phl[97]";
connectAttr "l_fnt_foot_ctrl_translateZ.o" "Reindeer_RigRN.phl[98]";
connectAttr "l_fnt_foot_ctrl_visibility.o" "Reindeer_RigRN.phl[99]";
connectAttr "l_fnt_foot_ctrl_scaleX.o" "Reindeer_RigRN.phl[100]";
connectAttr "l_fnt_foot_ctrl_scaleY.o" "Reindeer_RigRN.phl[101]";
connectAttr "l_fnt_foot_ctrl_scaleZ.o" "Reindeer_RigRN.phl[102]";
connectAttr "f_fnt_pv_translateX.o" "Reindeer_RigRN.phl[103]";
connectAttr "f_fnt_pv_translateY.o" "Reindeer_RigRN.phl[104]";
connectAttr "f_fnt_pv_translateZ.o" "Reindeer_RigRN.phl[105]";
connectAttr "f_fnt_pv_visibility.o" "Reindeer_RigRN.phl[106]";
connectAttr "f_fnt_pv1_translateX.o" "Reindeer_RigRN.phl[107]";
connectAttr "f_fnt_pv1_translateY.o" "Reindeer_RigRN.phl[108]";
connectAttr "f_fnt_pv1_translateZ.o" "Reindeer_RigRN.phl[109]";
connectAttr "f_fnt_pv1_visibility.o" "Reindeer_RigRN.phl[110]";
connectAttr "center_ctrl_translateX.o" "Reindeer_RigRN.phl[111]";
connectAttr "center_ctrl_translateY.o" "Reindeer_RigRN.phl[112]";
connectAttr "center_ctrl_translateZ.o" "Reindeer_RigRN.phl[113]";
connectAttr "center_ctrl_rotateX.o" "Reindeer_RigRN.phl[114]";
connectAttr "center_ctrl_rotateY.o" "Reindeer_RigRN.phl[115]";
connectAttr "center_ctrl_rotateZ.o" "Reindeer_RigRN.phl[116]";
connectAttr "center_ctrl_scaleX.o" "Reindeer_RigRN.phl[117]";
connectAttr "center_ctrl_scaleY.o" "Reindeer_RigRN.phl[118]";
connectAttr "center_ctrl_scaleZ.o" "Reindeer_RigRN.phl[119]";
connectAttr "center_ctrl_visibility.o" "Reindeer_RigRN.phl[120]";
connectAttr "locator1_rotateX.o" "locator1.rx";
connectAttr "locator1_rotateY.o" "locator1.ry";
connectAttr "locator1_rotateZ.o" "locator1.rz";
connectAttr "locator1_visibility.o" "locator1.v";
connectAttr "locator1_translateX.o" "locator1.tx";
connectAttr "locator1_translateY.o" "locator1.ty";
connectAttr "locator1_translateZ.o" "locator1.tz";
connectAttr "locator1_scaleX.o" "locator1.sx";
connectAttr "locator1_scaleY.o" "locator1.sy";
connectAttr "locator1_scaleZ.o" "locator1.sz";
connectAttr "locator1.r" "neckBaseCntl_orientConstraint1.tg[0].tr";
connectAttr "locator1.ro" "neckBaseCntl_orientConstraint1.tg[0].tro";
connectAttr "locator1.pm" "neckBaseCntl_orientConstraint1.tg[0].tpm";
connectAttr "neckBaseCntl_orientConstraint1.w0" "neckBaseCntl_orientConstraint1.tg[0].tw"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "reindeer_rig.msg" "Reindeer_RigRN.asn[0]";
connectAttr "Reindeer_RigRNfosterParent1.msg" "Reindeer_RigRN.fp";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of SantaReindeer@Reindeer_Run.ma
