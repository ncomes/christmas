﻿using UnityEngine;
using System.Collections;

public class HouseKiller : MonoBehaviour
{

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.GetComponent<House>())
            Destroy(col.gameObject);
    }
}
