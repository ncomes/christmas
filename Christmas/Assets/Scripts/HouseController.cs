﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HouseController : MonoBehaviour
{
    public static HouseController instance;
    public bool Paused = true;

    // Tweakable Data
    [Tooltip("List of houses that can be spawned in the game.")]
    public List<GameObject> PossibleHouses = new List<GameObject>();

    [Tooltip("Time before we spawn the next house. Does not wait until a house is fully in view.")]
    public float TimeBetweenHouses = 5.0f;

    [Tooltip("GameObject of where houses are spawned.")]
    public GameObject StartLoc;

    [Tooltip("GameObject of where houses will be destroyed.")]
    public GameObject EndLoc;

    [Tooltip("Time it takes for a house to go from start to end.")]
    public float TravelTime = 10.0f;
    
    //Private
    bool lastPaused = true;
    float nextSpawnTime = 0.0f;
    float pausedTime = 0.0f;

    List<GameObject> houses = new List<GameObject>();

    void Awake()
    {
        if (!instance)
            instance = this;
        else
            Debug.LogWarning(gameObject.name + " is trying to register itself as the HouseController when there already is one on " + HouseController.instance.gameObject.name);
    }

    void Update()
    {
        if (Paused)
            pausedTime += Time.deltaTime;
        else
            CheckHouseSpawn();

        if (lastPaused != Paused)
            SetPaused(Paused);

    }

    public static void SetHCPaused(bool set) { HouseController.instance.SetPaused(set); }
    public void SetPaused(bool set)
    {
        Paused = set;
        lastPaused = set;
        if (Paused)
            pausedTime = 0.0f; 
        else
            nextSpawnTime += pausedTime;
    }

    void CheckHouseSpawn()
    {
        if (Time.time > nextSpawnTime)
            SpawnHouse();
    }

    void SpawnHouse()
    {
        GameObject houseToSpawn = PossibleHouses[Random.Range(0, PossibleHouses.Count - 1)];
        GameObject house = Instantiate(houseToSpawn, StartLoc.transform.position, StartLoc.transform.rotation) as GameObject;
        house.transform.parent = transform;
        nextSpawnTime = Time.time + TimeBetweenHouses;
        houses.Add(house);
        MoveHouse(house);
    }

    void MoveHouse(GameObject house)
    {
        TweenPosition.Begin(house, TravelTime, EndLoc.transform.position, true);
    }

    public static void RemoveHouse(GameObject house) { HouseController.instance.RemoveOneHouse(house); }
    public void RemoveOneHouse(GameObject house)
    {
        houses.Remove(house);
    }

    public static void KillAll() { HouseController.instance.KillAllHouses(); }
    public void KillAllHouses()
    {
        foreach (GameObject house in houses)
            Destroy(house);

        houses.Clear();
    }






}
