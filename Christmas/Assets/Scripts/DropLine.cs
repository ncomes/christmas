﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DropLine : MonoBehaviour
{
    public static DropLine instance;

    List<ChimneyTarget> Chimneys = new List<ChimneyTarget>();

    public eChimneyTargetScore LastScore = eChimneyTargetScore.CTS_BAD;
    public bool bScored = false;

    House currentHouse;

    void Awake()
    {
        if (!instance)
            instance = this;
        else
            Debug.LogWarning(gameObject + " is trying to say it's the drop line but there should only be one!");
    }

    /* - Removed since we're using a fulls screen button now.
    void Update()
    {
        if (Input.anyKey && !bScored)
        {
            CheckScore();
        }
    }
    */

    public void CheckScore()
    {
        eChimneyTargetScore bs = eChimneyTargetScore.CTS_BAD;
        foreach (ChimneyTarget ct in Chimneys)
        {
            //Debug.Log("Score Check: " + ct.Score + " current best: " + bs);
            if (ct.Score < bs)
                bs = ct.Score;
        }
        LastScore = bs;
        bScored = true;

        if (currentHouse && !currentHouse.GetHouseInfo().Nice)
            LastScore = eChimneyTargetScore.CTS_BAD;

        GameController.ReportScore(LastScore);
    }

    public void ResetScoreLine() { DropLine.instance.ResetScore();  }
    public void ResetScore()
    {
        LastScore = eChimneyTargetScore.CTS_BAD;
    }


    void OnTriggerEnter(Collider col)
    {
        ChimneyTarget ct = col.gameObject.GetComponent<ChimneyTarget>();
        if (ct)
        {
            House hs = ct.gameObject.GetComponentInParent<House>();
            if (hs)
                currentHouse = hs;

            if (ct.Score == eChimneyTargetScore.CTS_BAD)
                ResetScore();
            else
                Chimneys.Add(ct);
        }
    }

    void OnTriggerExit(Collider col)
    {
        ChimneyTarget ct = col.gameObject.GetComponent<ChimneyTarget>();
        if (ct)
        {
            if (ct.Score == eChimneyTargetScore.CTS_BAD)
            {
                if (!bScored)
                {
                    if (currentHouse && !currentHouse.GetHouseInfo().Nice)
                        GameController.ReportScore(eChimneyTargetScore.CTS_PERFECT); 
                    else
                        GameController.ReportScore(eChimneyTargetScore.CTS_BAD);

                    currentHouse = null;
                }

                bScored = false;
                LastScore = eChimneyTargetScore.CTS_BAD;
            }
            else
                Chimneys.Remove(ct);
        }
    }

}
