﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SingleHouseData
{
    public string KidName;
    public bool Nice = true;
}

public class HouseData : ScriptableObject
{
    public List<SingleHouseData> Houses = new List<SingleHouseData>();

    [MenuItem("Assets/Create/HouseData")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<HouseData>();
    }
}
