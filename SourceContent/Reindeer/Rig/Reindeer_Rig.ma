//Maya ASCII 2016 scene
//Name: Reindeer_Rig.ma
//Last modified: Sat, Nov 21, 2015 08:24:19 PM
//Codeset: 1252
file -rdi 1 -dns -rpr "Reindeer_Skin" -rfn "Reindeer_SkinRN" -typ "mayaAscii"
		 "C:/Users/Nathan/Documents/christmas/SourceContent/Reindeer/Rig/Reindeer_Skin.ma";
file -rdi 2 -dns -rpr "Reindeer_Skeleton" -rfn "Reindeer_SkeletonRN" -typ "mayaAscii"
		 "C:/Users/Nathan/Documents/christmas/SourceContent/Reindeer/Rig/Reindeer_Skeleton.ma";
file -rdi 2 -dns -rpr "Reindeer" -rfn "ReindeerRN" -typ "mayaAscii" "C:/Users/Nathan/Documents/christmas/SourceContent/Reindeer/Model/Reindeer.ma";
file -r -dns -rpr "Reindeer_Skin" -dr 1 -rfn "Reindeer_SkinRN" -typ "mayaAscii" "C:/Users/Nathan/Documents/christmas/SourceContent/Reindeer/Rig/Reindeer_Skin.ma";
requires maya "2016";
requires -nodeType "ikSpringSolver" "ikSpringSolver" "1.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "37597B21-4FB4-9DDE-CF9B-2296727840EE";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1236.2341975886004 638.76529018438066 740.15371597489661 ;
	setAttr ".r" -type "double3" -16.538352729531294 58.999999999950923 -1.5438444439347672e-015 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "35D943CB-4D07-6AC6-C674-8185926F7A22";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 1504.8479279820012;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -4.4408920985006262e-016 0 5.5511151231257827e-017 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "B1855A96-4683-673E-44B1-019D3664483A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 94.154919418896924 100.10000000000002 -77.16436825761086 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "067145E7-4330-4E0F-C697-508F08FC1605";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 125.88328827945023;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "4BDA2132-49F4-C5C9-A580-5483C4B51518";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "3F51ADB1-4523-A6FE-79B0-D9BFF86FC90B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "003CBAFB-4D70-725B-3EF4-FB81FA043484";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "A83AA57A-4BC7-91F8-4E1D-7A9CDD4CB5B2";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode joint -n "rig_pelvis";
	rename -uid "53A0C111-4C54-946C-6304-93A07FDB1521";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 -89.999999999999986 0 ;
	setAttr ".bps" -type "matrix" 0 0 1.0000000000000002 0 0 1 0 0 -1.0000000000000002 0 0 0
		 0 232.42308711591741 -72.195067408442185 1;
	setAttr ".ds" 2;
	setAttr ".radi" 0.5;
createNode joint -n "rig_hips" -p "rig_pelvis";
	rename -uid "0474EED7-4B69-B994-325D-C1A81839EDDC";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0 0 1.0000000000000002 0 0 1 0 0 -1.0000000000000002 0 0 0
		 0 232.42308711591741 -72.195067408442185 1;
	setAttr ".radi" 0.5;
createNode joint -n "rig_l_femur" -p "rig_hips";
	rename -uid "D760D594-4D01-396B-0D07-9BB06165C56A";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -3.4364080829390105 -11.426279360077302 -48.218898531538571 ;
	setAttr ".r" -type "double3" 2.2787314097468014 -0.7262578192239495 -0.020612191206249821 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.3444665671425866 -4.7445566075906012 -74.158893655731205 ;
	setAttr ".bps" -type "matrix" -0.082713528313459739 -0.95872590144880609 0.27203514134227674 0
		 0.023382815039709078 0.27102834184994606 0.96228731773565834 0 -0.99629900943178584 0.085955126695519135 0 0
		 48.218898531538585 220.99680775584011 -75.63147549138121 1;
	setAttr ".radi" 0.5;
createNode joint -n "rig_l_tibia" -p "rig_l_femur";
	rename -uid "D3FF822B-415F-D4A1-78A6-558D50B2EF4E";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 67.471276475757293 1.2434497875801753e-014 -2.8421709430404007e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.8075669678561739 -1.3094328841458875 -69.876583099952242 ;
	setAttr ".bps" -type "matrix" -0.073166617250642441 -0.58220963005696413 -0.80973983030911301 0
		 -0.10096630584242022 -0.80342043655458839 0.58678906534713904 0 -0.99219577261981584 0.12468981031089565 0 0
		 42.638111194415792 156.31034739471806 -57.276917258754736 1;
	setAttr ".radi" 0.5;
createNode joint -n "rig_l_metatarsal" -p "rig_l_tibia";
	rename -uid "4794384B-4C46-600A-B562-66895ED5F7B1";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 53.790473552188971 5.6843418860808015e-014 1.0658141036401503e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0.75091548435952526 3.2985114700267695 66.903056016919379 ;
	setAttr ".bps" -type "matrix" -0.064285018712655365 -0.97298579392550089 0.22173425803938313 0
		 0.014618076783577729 0.22125187687209616 0.97510713196639287 0 -0.99782450771679454 0.065926108634872596 0 0
		 38.702444204291957 124.99301568730917 -100.83320618515103 1;
	setAttr ".radi" 0.5;
createNode joint -n "rig_l_pastern" -p "rig_l_metatarsal";
	rename -uid "4769FE3B-4571-52E5-2885-99A4DE8331D2";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 86.58717486543074 1.5631940186722204e-013 -2.1316282072803006e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -0.064285018712655365 -0.97298579392550089 0.22173425803938313 0
		 0.014618076783577729 0.22125187687209616 0.97510713196639287 0 -0.99782450771679454 0.065926108634872596 0 0
		 33.136186047791803 40.744924607101893 -81.633863210638268 1;
	setAttr ".radi" 0.5;
createNode orientConstraint -n "rig_l_pastern_orientConstraint1" -p "rig_l_pastern";
	rename -uid "7BEE1C3F-4236-5C94-8431-C792FCBEE8C2";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_bk_foot_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -58.191712520753995 94.537782519912824 18.876641542274953 ;
	setAttr ".o" -type "double3" 89.999999999999972 -12.810914757217271 -93.78002932380214 ;
	setAttr ".rsrr" -type "double3" -6.3611093629270351e-015 -3.1805546814635168e-014 
		6.3611093629270351e-015 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector1" -p "rig_l_metatarsal";
	rename -uid "709704D2-4D26-4E84-58E5-2582EDAFCEE4";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "rig_r_femur" -p "rig_hips";
	rename -uid "AF7F95D0-4444-5EF4-DE06-F58272ECED17";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -3.436432591557832 -11.426087115917397 48.2189 ;
	setAttr ".r" -type "double3" 2.2789352229414566 -0.7263227860226279 -0.020615730952803356 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.3444665671428035 -4.7445566075905958 105.84110634426879 ;
	setAttr ".bps" -type "matrix" -0.082713528313459753 0.95872590144880598 -0.27203514134227708 0
		 0.023382815039712446 -0.27102834184994617 -0.96228731773565823 0 -0.99629900943178573 -0.085955126695520079 0 0
		 -48.218900000000012 220.99700000000001 -75.631500000000003 1;
	setAttr ".radi" 0.5;
createNode joint -n "rig_r_tibia" -p "rig_r_femur";
	rename -uid "E4BA11D0-4886-0E86-9AC0-05A012FDD87B";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -67.471806251111616 0.00010636131018237904 3.3768716139803701e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.80756696785683 -1.3094328841457075 -69.876583099952242 ;
	setAttr ".bps" -type "matrix" -0.073166617250642468 0.58220963005696436 0.80973983030911267 0
		 -0.10096630584243044 0.80342043655458684 -0.58678906534713937 0 -0.99219577261981473 -0.12468981031090395 0 0
		 -42.638100000000016 156.31000000000003 -57.276900000000012 1;
	setAttr ".radi" 0.5;
createNode joint -n "rig_r_metatarsal" -p "rig_r_tibia";
	rename -uid "F28E3BB6-479C-84DB-01D9-CE9420BE36A1";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -53.790129862934023 0.00015230768249807625 -7.4112793495118012e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0.75091548436025601 3.2985114700273126 66.903056016919422 ;
	setAttr ".bps" -type "matrix" -0.064285018712655365 0.97298579392550077 -0.22173425803938399 0
		 0.01461807678356106 -0.22125187687209802 -0.97510713196639276 0 -0.99782450771679465 -0.065926108634868891 0 0
		 -38.702400000000019 124.99300000000007 -100.833 1;
	setAttr ".radi" 0.5;
createNode joint -n "rig_r_pastern" -p "rig_r_metatarsal";
	rename -uid "1C098641-4BEF-166B-CCE9-0098C57E2402";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -86.587125929897354 0.00023804956489925644 5.8618028372592335e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -0.064285018712655365 0.97298579392550077 -0.22173425803938399 0
		 0.01461807678356106 -0.22125187687209802 -0.97510713196639276 0 -0.99782450771679465 -0.065926108634868891 0 0
		 -33.136200000000009 40.744900000000044 -81.633899999999997 1;
	setAttr ".radi" 0.5;
createNode orientConstraint -n "rig_r_pastern_orientConstraint1" -p "rig_r_pastern";
	rename -uid "031D4EA4-4BFD-95C4-5513-0D8761B458CC";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_bk_foot_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 121.81053884598067 94.537900908894002 18.878882510848456 ;
	setAttr ".o" -type "double3" -87.548489086822471 12.83976057747673 94.403632443360465 ;
	setAttr ".rsrr" -type "double3" 1.9083328088781101e-014 -6.3611093629270367e-015 
		9.5416640443905487e-015 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector2" -p "rig_r_metatarsal";
	rename -uid "6E57B780-4E87-2618-232D-E89D37C05EAC";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode parentConstraint -n "rig_hips_parentConstraint1" -p "rig_hips";
	rename -uid "6219F569-4D33-A614-22D2-62975BA4E75B";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_start_ctrlW0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 1.0000000110128489 0 -2.0429260159873637e-015 ;
	setAttr ".tg[0].tor" -type "double3" 0 180.00000000000003 0 ;
	setAttr ".lr" -type "double3" 0 -1.4033418597069752e-014 0 ;
	setAttr ".rsrr" -type "double3" 0 -1.4033418597069752e-014 0 ;
	setAttr -k on ".w0";
createNode joint -n "rig_thorax" -p "rig_pelvis";
	rename -uid "94EBD496-4700-47D1-D57D-CAB26F2C603B";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0 0 1.0000000000000002 0 0 1 0 0 -1.0000000000000002 0 0 0
		 0 232.42308711591741 92.839596955058198 1;
	setAttr ".radi" 0.5;
createNode joint -n "rig_l_fnt_humerus" -p "rig_thorax";
	rename -uid "D84F1EB9-4BC0-B375-6AB6-26864C99A3EF";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 5.8593266959954633 -18.341454555869063 -45.988075806042318 ;
	setAttr ".r" -type "double3" -5.743609309077641 0.6722483024797633 0.044391480718941147 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.49067112834925419 -5.0405447002565289 -84.432799647354784 ;
	setAttr ".bps" -type "matrix" -0.087860666602631421 -0.99143411462521025 0.096637982291988098 0
		 0.0085306042079501057 0.096260731418963852 0.99531959710363072 0 -0.9960962463818388 0.088273823662575329 0 0
		 45.988075806042325 214.08163256004835 98.698923651053647 1;
	setAttr ".radi" 0.5;
createNode joint -n "rig_l_fnt_metacarpal" -p "rig_l_fnt_humerus";
	rename -uid "FB006FEC-4D02-DD9B-A0CA-C484907FC9D8";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 103.81894612777182 4.2632564145606011e-014 -1.0658141036401503e-014 ;
	setAttr ".r" -type "double3" -4.6637693115545027e-030 6.3426674521371124e-015 -5.3522648988836651e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.40713785203786163 1.9744237182410487 -17.199454977620345 ;
	setAttr ".bps" -type "matrix" -0.052083927011659203 -0.9780246901410814 -0.20187860218826026 0
		 -0.010735671937347103 -0.20159294474147518 0.97941055231119212 0 -0.99858500384048343 0.053178850165288311 0 0
		 36.866473993273623 111.15198762453849 108.73177712852217 1;
	setAttr ".radi" 0.5;
createNode joint -n "rig_l_fnt_pastern" -p "rig_l_fnt_metacarpal";
	rename -uid "00073E0F-4174-EE3C-9B40-4ABE16B5985E";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 65.674784449770016 -4.2632564145606011e-014 -1.0658141036401503e-014 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -0.052083927011659203 -0.9780246901410814 -0.20187860218826026 0
		 -0.010735671937347103 -0.20159294474147518 0.97941055231119212 0 -0.99858500384048343 0.053178850165288311 0 0
		 33.445873313485365 46.920426912969873 95.473443444787264 1;
	setAttr ".radi" 0.5;
createNode orientConstraint -n "rig_l_fnt_pastern_orientConstraint1" -p "rig_l_fnt_pastern";
	rename -uid "5A602859-437A-F89F-8639-D6A7553D4AC6";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_fnt_foot_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 26.7931536289015 83.521275958974314 128.7194192714901 ;
	setAttr ".o" -type "double3" 84.095886559137625 11.7175364955599 -94.133507131243391 ;
	setAttr ".rsrr" -type "double3" 2.6483437788300953e-030 1.9083328088781101e-014 
		1.5902773407317584e-014 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector3" -p "rig_l_fnt_metacarpal";
	rename -uid "5971E569-49DB-AD6B-E6FC-9BB8114BAE1B";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "rig_r_fnt_humerus" -p "rig_thorax";
	rename -uid "F3D17AFF-402B-AF66-C36F-E9BD462FE290";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 5.8593030449418109 -18.341087115917418 45.9881 ;
	setAttr ".r" -type "double3" -5.7431365129729679 0.67219330918286924 0.044391683997298967 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.4906711283492326 -5.0405447002565245 95.567200352645273 ;
	setAttr ".bps" -type "matrix" -0.087860666602631393 0.99143411462521025 -0.096637982291988958 0
		 0.0085306042079492782 -0.096260731418964962 -0.99531959710363072 0 -0.9960962463818388 -0.088273823662575232 0 0
		 -45.98810000000001 214.08199999999999 98.698900000000023 1;
	setAttr ".radi" 0.5;
createNode joint -n "rig_r_fnt_metacarpal" -p "rig_r_fnt_humerus";
	rename -uid "9794D965-48AF-48AB-ACC1-A9A5CFD9C93F";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -103.81932181498919 -0.00021120540316132974 3.3148592294196533e-005 ;
	setAttr ".r" -type "double3" -8.2975138955025689e-020 -2.3763028367494193e-014 2.0257563503547265e-013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -0.40713785203882702 1.97442371824103 -17.199454977620352 ;
	setAttr ".bps" -type "matrix" -0.052083927011659231 0.97802469014108162 0.20187860218825962 0
		 -0.010735671937331083 0.20159294474147529 -0.97941055231119234 0 -0.99858500384048354 -0.053178850165285105 0 0
		 -36.866500000000002 111.152 108.73200000000003 1;
	setAttr ".radi" 0.5;
createNode joint -n "rig_r_fnt_pastern" -p "rig_r_fnt_metacarpal";
	rename -uid "E3AF9FCB-4016-8378-212D-B2AD7DF833E2";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -65.674876602975232 0.00025291998777277058 2.7681399785706162e-006 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -0.052083927011659231 0.97802469014108162 0.20187860218825962 0
		 -0.010735671937331083 0.20159294474147529 -0.97941055231119234 0 -0.99858500384048354 -0.053178850165285105 0 0
		 -33.445900000000002 46.920399999999987 95.473400000000041 1;
	setAttr ".radi" 0.5;
createNode orientConstraint -n "rig_r_fnt_pastern_orientConstraint1" -p "rig_r_fnt_pastern";
	rename -uid "5A47F24E-4A44-43F6-6554-0F9D916A89BA";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_fnt_foot_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 26.795142131431067 96.47830441251223 -51.278616366283963 ;
	setAttr ".o" -type "double3" -95.903627416291542 -11.71753410766987 94.133417106278841 ;
	setAttr ".rsrr" -type "double3" 0 -1.2722218725854067e-014 0 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector4" -p "rig_r_fnt_metacarpal";
	rename -uid "31285977-4CDF-B145-6D25-9AAE3DAE0C65";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "rig_neckBase" -p "rig_thorax";
	rename -uid "D2B13556-483F-B10F-FFEB-008FAA854CE5";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 179.99999999999997 2.8249000307521015e-030 77.846986151410164 ;
	setAttr ".bps" -type "matrix" 0 0.97758886526498945 0.21052318283246224 0 0 -0.2105231828324623 0.97758886526498967 0
		 1.0000000000000002 0 0 0 0 283.99561524671532 100.16416561500731 1;
	setAttr ".radi" 0.5;
createNode joint -n "rig_neck" -p "rig_neckBase";
	rename -uid "BD69B5DA-472B-707B-04E7-DCB5A6A9CC5F";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90 0 41.269944540061879 ;
	setAttr ".bps" -type "matrix" 0 0.59590313857010757 0.80305631772765196 0 1.0000000000000004 0 0 0
		 0 0.80305631772765185 -0.59590313857010802 0 0 339.31475830078125 112.07711029052739 1;
	setAttr ".radi" 0.5;
createNode joint -n "rig_head" -p "rig_neck";
	rename -uid "E3B60329-49F6-2166-DD3A-21A304D6C61B";
	addAttr -ci true -sn "bindJoint" -ln "bindJoint" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 126.57704161134828 0 89.999999999999986 ;
	setAttr ".bps" -type "matrix" 1.0000000000000004 0 0 0 0 1.0000000000000002 0 0 0 0 1.0000000000000004 0
		 0 386.21650480897489 175.28326143825211 1;
	setAttr ".radi" 0.5;
createNode pointConstraint -n "rig_head_pointConstraint1" -p "rig_head";
	rename -uid "DC14C890-42FC-4A63-A262-31993B736137";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "headCtrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -2.8421709430404007e-013 4.6522219735418202e-014 1.1368683772161603e-013 ;
	setAttr ".rst" -type "double3" 78.706996947081393 -9.6654688499407832e-015 -2.8421709430404007e-014 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "rig_head_orientConstraint1" -p "rig_head";
	rename -uid "1114BA42-4772-FFB4-29BB-52824ACD5493";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "headCtrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.1244958915987353e-014 89.999999999999957 0 ;
	setAttr ".o" -type "double3" 0 -89.999999999999957 0 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "rig_neck_pointConstraint1" -p "rig_neck";
	rename -uid "9B57BBD6-4676-94AB-CF2D-B09FFE5ED5CB";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "neckCtrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -1.1368683772161603e-013 -2.6290081223123707e-013 5.0891335745905836e-014 ;
	setAttr ".rst" -type "double3" 56.587329315653449 0 0 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "rig_neck_orientConstraint1" -p "rig_neck";
	rename -uid "69064301-4EDE-530D-7B4D-E0BC9339EBB6";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "neckCtrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -90 -143.42295838865172 0 ;
	setAttr ".o" -type "double3" 90.000000000000014 3.180554681463516e-015 143.42295838865172 ;
	setAttr ".rsrr" -type "double3" 180 -180 180 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "rig_neckBase_pointConstraint1" -p "rig_neckBase";
	rename -uid "0A30D30C-44EB-2F18-AF40-118D67C62F56";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "neckBaseCntlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -3.4106051316484809e-013 2.2737367544323206e-013 -5.0891335745906007e-014 ;
	setAttr ".rst" -type "double3" 7.324568659949108 51.572528130797906 0 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "rig_neckBase_orientConstraint1" -p "rig_neckBase";
	rename -uid "73700CAF-4E89-A037-C4F9-319C75840DF9";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "neckBaseCntlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -6.8338239322527978e-015 -7.2749113347264637e-016 
		-102.15301384858985 ;
	setAttr ".o" -type "double3" 0 0 102.15301384858985 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "rig_thorax_parentConstraint1" -p "rig_thorax";
	rename -uid "4DBD1DCF-4A98-9B43-DAC6-EDB2C3E1A163";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_end_ctrlW0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -1.0000000110128484 3.5527136788005009e-015 
		1.9319037135248481e-015 ;
	setAttr ".tg[0].tor" -type "double3" 0 180.00000000000003 0 ;
	setAttr ".lr" -type "double3" 0 -1.4033418597069752e-014 0 ;
	setAttr ".rst" -type "double3" 165.03466436350035 5.6843418860808015e-014 3.6645056847528573e-014 ;
	setAttr ".rsrr" -type "double3" 0 -1.4033418597069752e-014 0 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "rig_pelvis_pointConstraint1" -p "rig_pelvis";
	rename -uid "3C5D14B6-42D8-7FE3-A5A0-13AC02A4F734";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "center_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0 -2.8421709430404007e-014 0 ;
	setAttr ".rst" -type "double3" 0 232.42308711591741 -72.195067408442185 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "rig_pelvis_orientConstraint1" -p "rig_pelvis";
	rename -uid "CCFEE327-404A-D6C0-FAF3-B38D82697B65";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "center_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".o" -type "double3" 0 -89.999999999999986 0 ;
	setAttr -k on ".w0";
createNode transform -n "reindeer__flexiPlane_grp_01";
	rename -uid "3C5CFB63-414F-6E47-3742-2D906EBBCCE5";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "reindeer__flexi_hidden" -p "reindeer__flexiPlane_grp_01";
	rename -uid "B4D11DA2-42BE-3F13-0631-6A89311A5175";
	setAttr ".v" no;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0 0 5 ;
	setAttr ".sp" -type "double3" 0 0 5 ;
createNode transform -n "reindeer__flexi_cWire_surface_01" -p "reindeer__flexi_hidden";
	rename -uid "42163F92-48B8-6C3F-538B-BDB69F963D58";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode nurbsCurve -n "reindeer__flexi_cWire_surface_0Shape1" -p "reindeer__flexi_cWire_surface_01";
	rename -uid "0940CAE0-4287-0AF0-BB3C-61BE4D0ABC78";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".tw" yes;
createNode nurbsCurve -n "reindeer__flexi_cWire_surface_0Shape1Orig" -p "reindeer__flexi_cWire_surface_01";
	rename -uid "2C7D2508-4C1C-DC48-5F10-04BBE2815345";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".cc" -type "nurbsCurve" 
		3 2 0 no 3
		7 0 0 0 1 2 2 2
		5
		5 0 5
		3 0 5
		0 0 5
		-3 0 5
		-5 0 5
		;
createNode transform -n "reindeer__flexi_surface_bsShps_01" -p "reindeer__flexi_hidden";
	rename -uid "B74E78EC-408F-5144-F8FF-20BDA95F5CE4";
	setAttr ".t" -type "double3" 0 0 5 ;
createNode nurbsSurface -n "reindeer__flexi_surface_bsShps_01Shape" -p "reindeer__flexi_surface_bsShps_01";
	rename -uid "890BA3A2-4F16-FDBB-7920-8390956AC7D7";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".tw" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".nufa" 4.5;
	setAttr ".nvfa" 4.5;
createNode nurbsSurface -n "reindeer__flexi_surface_bsShps_01ShapeOrig" -p "reindeer__flexi_surface_bsShps_01";
	rename -uid "CCABE71D-4015-671E-476C-0FB3DABC6F4F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 0 0 no 
		6 0 0 0 1 1 1
		10 0 0 0 0.20000000000000001 0.40000000000000002 0.60000000000000009 0.80000000000000004
		 1 1 1
		
		32
		5 -3.0616169978683831e-016 1.0000000000000011
		4.333333333333333 -2.6534013981525983e-016 1.0000000000000009
		2.9999999999999991 -1.8369701987210294e-016 1.0000000000000007
		0.999999999999999 -6.1232339957367611e-017 1.0000000000000002
		-1.0000000000000011 6.123233995736771e-017 0.99999999999999978
		-3.0000000000000009 1.8369701987210304e-016 0.99999999999999933
		-4.3333333333333339 2.6534013981525988e-016 0.999999999999999
		-5 3.0616169978683831e-016 0.99999999999999889
		5 -3.0616169978683831e-016 0.33333333333333448
		4.333333333333333 -2.6534013981525983e-016 0.33333333333333431
		2.9999999999999996 -1.8369701987210294e-016 0.33333333333333404
		0.99999999999999911 -6.1232339957367611e-017 0.33333333333333359
		-1.0000000000000009 6.123233995736771e-017 0.33333333333333315
		-3.0000000000000009 1.8369701987210304e-016 0.3333333333333327
		-4.3333333333333339 2.6534013981525988e-016 0.33333333333333243
		-5 3.0616169978683831e-016 0.33333333333333226
		5 -3.0616169978683831e-016 -0.33333333333333215
		4.333333333333333 -2.6534013981525983e-016 -0.33333333333333232
		2.9999999999999996 -1.8369701987210294e-016 -0.33333333333333259
		0.99999999999999933 -6.1232339957367611e-017 -0.33333333333333304
		-1.0000000000000009 6.123233995736771e-017 -0.33333333333333348
		-3.0000000000000009 1.8369701987210304e-016 -0.33333333333333393
		-4.3333333333333339 2.6534013981525988e-016 -0.3333333333333342
		-5 3.0616169978683831e-016 -0.33333333333333437
		5 -3.0616169978683831e-016 -0.99999999999999889
		4.333333333333333 -2.6534013981525983e-016 -0.999999999999999
		3 -1.8369701987210294e-016 -0.99999999999999933
		0.99999999999999944 -6.1232339957367611e-017 -0.99999999999999978
		-1.0000000000000007 6.123233995736771e-017 -1.0000000000000002
		-3.0000000000000009 1.8369701987210304e-016 -1.0000000000000007
		-4.3333333333333339 2.6534013981525988e-016 -1.0000000000000009
		-5 3.0616169978683831e-016 -1.0000000000000011
		
		;
createNode transform -n "reindeer__flexi_twistHdl_01" -p "reindeer__flexi_hidden";
	rename -uid "FDADEAF9-425A-803A-B273-339AD1B1A584";
	setAttr ".t" -type "double3" 0 0 5 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 5 5 5 ;
	setAttr ".smd" 7;
createNode deformTwist -n "reindeer__flexi_twistHdl_01Shape" -p "reindeer__flexi_twistHdl_01";
	rename -uid "207F8E6D-4E1F-997A-2057-08BA8A2C6C2A";
	setAttr -k off ".v";
	setAttr ".dd" -type "doubleArray" 4 -1 1 0 0 ;
	setAttr ".hw" 1.100000000000001;
createNode transform -n "reindeer__flexi_cWire_surface_01BaseWire" -p "reindeer__flexi_hidden";
	rename -uid "A3944A85-4E4D-90ED-5DBF-9B88FCF9E095";
	setAttr ".v" no;
createNode nurbsCurve -n "reindeer__flexi_cWire_surface_01BaseWireShape" -p "reindeer__flexi_cWire_surface_01BaseWire";
	rename -uid "621C0FFA-48A3-2292-D0CC-D083A555C25C";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 2 0 no 3
		7 0 0 0 1 2 2 2
		5
		5 0 5
		3 0 5
		0 0 5
		-3 0 5
		-5 0 5
		;
createNode transform -n "reindeer__flexi_jnt_cWire_01" -p "reindeer__flexi_hidden";
	rename -uid "BE472E3D-4519-35C8-9E18-44BC9426B3D8";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "reindeer__flexi_grp_end_01" -p "reindeer__flexi_jnt_cWire_01";
	rename -uid "1A56B969-44A5-FFE8-0E0B-B697404BE206";
createNode joint -n "reindeer__flexi_jnt_end_01" -p "reindeer__flexi_grp_end_01";
	rename -uid "721CAC1E-432B-95CB-872B-979539CBF6DB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -4 0 5 1;
createNode parentConstraint -n "reindeer__flexi_grp_end_01_parentConstraint1" -p "reindeer__flexi_grp_end_01";
	rename -uid "085034D5-44ED-9247-4878-6EBCDCD9D57B";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_jnt_mid_01W0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -4 0 0 ;
	setAttr ".rst" -type "double3" -4 0 5 ;
	setAttr -k on ".w0";
createNode transform -n "reindeer__flexi_grp_start_01" -p "reindeer__flexi_jnt_cWire_01";
	rename -uid "1BFB4C1A-4BA2-588A-3C8A-7FB2E5326D7C";
	setAttr ".t" -type "double3" 4 0 5 ;
createNode joint -n "reindeer__flexi_jnt_start_01" -p "reindeer__flexi_grp_start_01";
	rename -uid "74B404F3-4964-1943-F89E-7FBB82FF251E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 4 0 5 1;
createNode transform -n "reindeer__flexi_grp_mid_01" -p "reindeer__flexi_jnt_cWire_01";
	rename -uid "ADC4DBFC-4E37-2E5A-A014-36A55D5FAB2A";
createNode joint -n "reindeer__flexi_jnt_mid_01" -p "reindeer__flexi_grp_mid_01";
	rename -uid "F1ACA208-4BC8-44F0-C1ED-D6A86DB7E49F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 5 1;
createNode parentConstraint -n "reindeer__flexi_grp_mid_01_parentConstraint1" -p "reindeer__flexi_grp_mid_01";
	rename -uid "CC0CE1B1-4002-569B-96D3-64AB10FAB347";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_jnt_start_01W0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -4 0 0 ;
	setAttr ".rst" -type "double3" 0 0 5 ;
	setAttr -k on ".w0";
createNode transform -n "reindeer__global_move_ctrl" -p "reindeer__flexiPlane_grp_01";
	rename -uid "B88EE893-4114-73A5-FA54-3AA24993BEC3";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "reindeer_moveControl" -ln "reindeer_moveControl" -at "double";
	addAttr -ci true -sn "volumeControl" -ln "volumeControl" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "autoVolume" -ln "autoVolume" -min 0 -max 1 -at "bool";
	setAttr ".s" -type "double3" 16.503466400000001 16.503466400000001 16.503466400000001 ;
	setAttr -cb on ".volumeControl";
	setAttr -k on ".autoVolume";
createNode nurbsCurve -n "reindeer__global_move_ctrlShape" -p "reindeer__global_move_ctrl";
	rename -uid "2891A06E-42BC-DA97-FCCE-289998B8496F";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		-4 0 2
		4 0 2
		4 0 -2
		-4 0 -2
		-4 0 2
		;
createNode transform -n "reindeer__global_move_group" -p "reindeer__global_move_ctrl";
	rename -uid "5C0173B5-48CE-D4F8-DF31-E184A78FC207";
	addAttr -ci true -sn "reindeer_moveGroup" -ln "reindeer_moveGroup" -at "double";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
createNode transform -n "reindeer__flexi_surface_01" -p "reindeer__global_move_group";
	rename -uid "8D969DB9-43F4-E076-1319-9C8FE116318D";
	setAttr -k on ".tmp" yes;
createNode nurbsSurface -n "reindeer__flexi_surface_01Shape" -p "reindeer__flexi_surface_01";
	rename -uid "848E91AD-4932-54E7-B433-CFAA09D16EE8";
	setAttr -k off ".v" no;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".tw" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".nufa" 4.5;
	setAttr ".nvfa" 4.5;
createNode nurbsSurface -n "reindeer__flexi_surface_01ShapeOrig" -p "reindeer__flexi_surface_01";
	rename -uid "8D1A9BC2-49F7-3827-0D98-89B85FB18E37";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 0 0 no 
		6 0 0 0 1 1 1
		10 0 0 0 0.20000000000000001 0.40000000000000002 0.60000000000000009 0.80000000000000004
		 1 1 1
		
		32
		5 -3.0616169978683831e-016 1.0000000000000011
		4.333333333333333 -2.6534013981525983e-016 1.0000000000000009
		2.9999999999999991 -1.8369701987210294e-016 1.0000000000000007
		0.999999999999999 -6.1232339957367611e-017 1.0000000000000002
		-1.0000000000000011 6.123233995736771e-017 0.99999999999999978
		-3.0000000000000009 1.8369701987210304e-016 0.99999999999999933
		-4.3333333333333339 2.6534013981525988e-016 0.999999999999999
		-5 3.0616169978683831e-016 0.99999999999999889
		5 -3.0616169978683831e-016 0.33333333333333448
		4.333333333333333 -2.6534013981525983e-016 0.33333333333333431
		2.9999999999999996 -1.8369701987210294e-016 0.33333333333333404
		0.99999999999999911 -6.1232339957367611e-017 0.33333333333333359
		-1.0000000000000009 6.123233995736771e-017 0.33333333333333315
		-3.0000000000000009 1.8369701987210304e-016 0.3333333333333327
		-4.3333333333333339 2.6534013981525988e-016 0.33333333333333243
		-5 3.0616169978683831e-016 0.33333333333333226
		5 -3.0616169978683831e-016 -0.33333333333333215
		4.333333333333333 -2.6534013981525983e-016 -0.33333333333333232
		2.9999999999999996 -1.8369701987210294e-016 -0.33333333333333259
		0.99999999999999933 -6.1232339957367611e-017 -0.33333333333333304
		-1.0000000000000009 6.123233995736771e-017 -0.33333333333333348
		-3.0000000000000009 1.8369701987210304e-016 -0.33333333333333393
		-4.3333333333333339 2.6534013981525988e-016 -0.3333333333333342
		-5 3.0616169978683831e-016 -0.33333333333333437
		5 -3.0616169978683831e-016 -0.99999999999999889
		4.333333333333333 -2.6534013981525983e-016 -0.999999999999999
		3 -1.8369701987210294e-016 -0.99999999999999933
		0.99999999999999944 -6.1232339957367611e-017 -0.99999999999999978
		-1.0000000000000007 6.123233995736771e-017 -1.0000000000000002
		-3.0000000000000009 1.8369701987210304e-016 -1.0000000000000007
		-4.3333333333333339 2.6534013981525988e-016 -1.0000000000000009
		-5 3.0616169978683831e-016 -1.0000000000000011
		
		;
createNode transform -n "reindeer__flexi_start_ctrl_grp" -p "reindeer__global_move_group";
	rename -uid "F37B9AEA-480B-87B3-D60A-2F9991423B5D";
	setAttr ".rp" -type "double3" 6 0 0 ;
	setAttr ".sp" -type "double3" 6 0 0 ;
createNode transform -n "reindeer__flexi_start_ctrl" -p "reindeer__flexi_start_ctrl_grp";
	rename -uid "21E9EDB7-4EF1-E0F6-48BB-2B920C22596B";
	addAttr -ci true -sn "reindeer_startCtrl" -ln "reindeer_startCtrl" -at "double";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 4 -2.4492935982947069e-016 9.3270298891686341e-016 ;
	setAttr ".sp" -type "double3" 4 -2.4492935982947069e-016 9.3270298891686341e-016 ;
createNode nurbsCurve -n "reindeer__flexi_start_ctrlShape" -p "reindeer__flexi_start_ctrl";
	rename -uid "3CEDF707-4E8B-EF3A-66D3-99B84FAD4868";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		6.0000000000000151 -5.6897946803802242 1.4637165544813277e-016
		6.0000000000000142 1.2866395417859068e-015 5.6897946803802251
		6.0000000000000133 5.6897946803802304 1.4637165544813277e-016
		6.0000000000000142 1.2866395417859068e-015 -5.6897946803802251
		6.0000000000000151 -5.6897946803802242 1.4637165544813277e-016
		;
createNode transform -n "reindeer__flexi_mid_ctrl_grp" -p "reindeer__global_move_group";
	rename -uid "4E0C4953-45B2-C638-DA5E-9DBCFEAD292D";
createNode transform -n "reindeer__flexi_mid_ctrl" -p "reindeer__flexi_mid_ctrl_grp";
	rename -uid "2FAE4FF5-46F7-744B-AB2D-FC8E947F0A1F";
	addAttr -ci true -sn "reindeer_midCtrl" -ln "reindeer_midCtrl" -at "double";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "driver" -ln "driver" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "followStart" -ln "followStart" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -cb on ".driver";
	setAttr -k on ".followStart";
createNode nurbsCurve -n "reindeer__flexi_mid_ctrlShape" -p "reindeer__flexi_mid_ctrl";
	rename -uid "7EF97793-4C8B-F202-210D-129613BA02E1";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		0 5.986135891041207 0
		0 0 5.986135891041207
		0 -5.986135891041207 0
		0 0 -5.986135891041207
		0 5.986135891041207 0
		;
createNode parentConstraint -n "reindeer__flexi_mid_ctrl_grp_parentConstraint1" -p
		 "reindeer__flexi_mid_ctrl_grp";
	rename -uid "E4EB4E4F-4AE0-6FA0-7236-1D9CC7B91E31";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_start_ctrlW0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -4 2.4492935982947069e-016 -9.3270298891686341e-016 ;
	setAttr -k on ".w0";
createNode transform -n "reindeer__flexi_end_ctrl_grp" -p "reindeer__global_move_group";
	rename -uid "37DB5D1F-428D-04D0-142E-CEAE7228A5B1";
	setAttr ".rp" -type "double3" -6 0 0 ;
	setAttr ".sp" -type "double3" -6 0 0 ;
createNode transform -n "reindeer__flexi_end_ctrl" -p "reindeer__flexi_end_ctrl_grp";
	rename -uid "A8099C07-42F8-143A-D520-919187E69287";
	addAttr -ci true -sn "reindeer_endCtrl" -ln "reindeer_endCtrl" -at "double";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "driver" -ln "driver" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "followMid" -ln "followMid" -min 0 -max 1 -at "bool";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -4.0000000000000009 2.4492935982947069e-016 -8.2168068645434766e-016 ;
	setAttr ".sp" -type "double3" -4.0000000000000009 2.4492935982947069e-016 -8.2168068645434766e-016 ;
	setAttr -cb on ".driver";
	setAttr -k on ".followMid";
createNode nurbsCurve -n "reindeer__flexi_end_ctrlShape" -p "reindeer__flexi_end_ctrl";
	rename -uid "FC24ACFB-426C-A587-C016-B781D5C10121";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		-11.17899251204879 -5.1789925120487839 -2.3642518810956623e-017
		-6.0000000000000151 -7.1906720375156382e-015 7.3242014499681005
		-0.82100748795121259 5.1789925120487839 -2.3642518810956623e-017
		-6.0000000000000151 -7.1906720375156382e-015 -7.3242014499681005
		-11.17899251204879 -5.1789925120487839 -2.3642518810956623e-017
		;
createNode transform -n "neckBaseCntl" -p "reindeer__flexi_end_ctrl";
	rename -uid "36FCEC3D-410A-659A-178C-B3A1292BAE3D";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -5.4438200232709688 3.1249512605907883 -1.7763568394002497e-015 ;
	setAttr ".sp" -type "double3" -5.4438200232709635 3.1249512605908025 -1.7763568394002509e-015 ;
createNode nurbsCurve -n "neckBaseCntlShape" -p "neckBaseCntl";
	rename -uid "F1AAC491-407B-6C7A-A1DE-01809CDD8B9C";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-2.4397930536710928 5.5652781778376195 3.8703195600780496
		-1.1954843411282754 6.576094683585378 -3.3441330160475604e-015
		-2.4397930536710928 5.5652781778376195 -3.8703195600780504
		-5.4438200232709626 3.1249512605908016 -5.473458412580249
		-8.4478469928708293 0.68462434334398559 -3.8703195600780504
		-9.6921557054136507 -0.32619216240377469 -2.4822966616425353e-015
		-8.4478469928708328 0.68462434334398559 3.8703195600780451
		-5.4438200232709644 3.1249512605907981 5.4734584125802455
		-2.4397930536710928 5.5652781778376195 3.8703195600780496
		-1.1954843411282754 6.576094683585378 -3.3441330160475604e-015
		-2.4397930536710928 5.5652781778376195 -3.8703195600780504
		;
createNode transform -n "neckCtrl" -p "neckBaseCntl";
	rename -uid "6C763F20-4BBB-F862-0C7C-DF9CF110FFDF";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -6.1656650215750837 6.4769223988521549 -1.4557932644691551e-015 ;
	setAttr ".sp" -type "double3" -6.1656650215750837 6.4769223988521549 -1.4557932644691551e-015 ;
createNode nurbsCurve -n "neckCtrlShape" -p "neckCtrl";
	rename -uid "4224CB52-4295-9493-F9E3-5CAB1FB610E1";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-2.8981379211342366 8.5512030319891821 3.8703195600780496
		-1.5446838807100023 9.4103982024023942 -3.1063133547755852e-015
		-2.8981379211342366 8.5512030319891821 -3.8703195600780504
		-6.1656650215750801 6.4769223988521532 -5.473458412580249
		-9.4331921220159245 4.4026417657151171 -3.8703195600780504
		-10.78664616244016 3.543446595301905 -2.0789891730523194e-015
		-9.4331921220159245 4.4026417657151171 3.8703195600780451
		-6.1656650215750828 6.4769223988521496 5.4734584125802455
		-2.8981379211342366 8.5512030319891821 3.8703195600780496
		-1.5446838807100023 9.4103982024023942 -3.1063133547755852e-015
		-2.8981379211342366 8.5512030319891821 -3.8703195600780504
		;
createNode transform -n "headCtrl" -p "neckCtrl";
	rename -uid "78DBE9EE-411C-C9EA-5943-6BA238ACC301";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -9.995536250793009 9.3188554431848072 -2.1228867457624686e-015 ;
	setAttr ".sp" -type "double3" -9.995536250793009 9.3188554431848072 -2.1228867457624686e-015 ;
createNode nurbsCurve -n "headCtrlShape" -p "headCtrl";
	rename -uid "BC834A41-4FFF-270F-6823-129C97BBB074";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-8.5988699882269302 13.874375946809298 3.5969103471091657
		-8.0203518801631155 15.761334323079282 -3.1418087669608813e-015
		-8.5988699882269302 13.874375946809302 -3.5969103471091666
		-9.9955362507930055 9.3188554431848054 -5.0867993955218997
		-11.392202513359081 4.7633349395603091 -3.596910347109167
		-11.970720621422888 2.8763765632903215 -3.2170596449816139e-015
		-11.392202513359081 4.7633349395603091 3.5969103471091604
		-9.9955362507930019 9.3188554431848019 5.0867993955218953
		-8.5988699882269302 13.874375946809298 3.5969103471091657
		-8.0203518801631155 15.761334323079282 -3.1418087669608813e-015
		-8.5988699882269302 13.874375946809302 -3.5969103471091666
		;
createNode parentConstraint -n "reindeer__flexi_end_ctrl_grp_parentConstraint1" -p
		 "reindeer__flexi_end_ctrl_grp";
	rename -uid "92FAC9F8-40D9-7E23-C037-2FB03D7050FC";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_mid_ctrlW0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -6 0 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "reindeer__global_move_ctrl_parentConstraint1" -p "reindeer__global_move_ctrl";
	rename -uid "FD57BEAE-4A42-C26F-CAE4-E3B6E3B92BC0";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "center_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0 -2.8421709430404007e-014 82.517332181750191 ;
	setAttr ".tg[0].tor" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".lr" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".rst" -type "double3" 0 232.42308711591741 10.322264773308007 ;
	setAttr ".rsrr" -type "double3" 0 89.999999999999986 0 ;
	setAttr -k on ".w0";
createNode transform -n "reindeer__flexi_flcs01" -p "reindeer__flexiPlane_grp_01";
	rename -uid "A6C4AE24-4942-A531-74F5-34BB3E92141A";
createNode transform -n "reindeer__flexi_surface_01_twist_follicle_01" -p "reindeer__flexi_flcs01";
	rename -uid "7CADB41A-4217-FFC2-A1CE-3CA3B4EFD467";
createNode follicle -n "reindeer__flexi_surface_01Shape_twist_follicle_01" -p "reindeer__flexi_surface_01_twist_follicle_01";
	rename -uid "A3DBB56D-4590-B39C-DAB9-82AB2AC2177D";
	setAttr -k off ".v" no;
	setAttr ".pu" 0.5;
	setAttr ".pv" 0.1;
	setAttr -s 2 ".sts[0:1]"  0 1 3 1 0.2 3;
	setAttr -s 2 ".cws[0:1]"  0 1 3 1 0.2 3;
	setAttr -s 2 ".ats[0:1]"  0 1 3 1 0.2 3;
createNode scaleConstraint -n "reindeer__flexi_surface_01_twist_follicle_01_scaleConstraint1" 
		-p "reindeer__flexi_surface_01_twist_follicle_01";
	rename -uid "E6496DDD-4EFA-B159-DE4A-62B6131C8F20";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__global_move_ctrlW0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode transform -n "reindeer__midCtrl_01" -p "reindeer__flexi_surface_01_twist_follicle_01";
	rename -uid "5C828E49-42E4-1D26-DE8A-229EDE58E2F3";
	addAttr -ci true -sn "reindeer_midCt01" -ln "reindeer_midCt01" -at "double";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "volumeControl" -ln "volumeControl" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "magnitude" -ln "magnitude" -at "double";
	setAttr -l on -k off ".v";
	setAttr -cb on ".volumeControl";
	setAttr -k on ".magnitude";
createNode nurbsCurve -n "reindeer__midCtrl_0Shape1" -p "reindeer__midCtrl_01";
	rename -uid "32FE9B15-41BD-D253-4A59-CAA3B6F9FEF4";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 9;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		-0.99999999999999989 -5.5511151231257827e-016 1
		-1 -1.1102230246251565e-016 -1
		0.99999999999999989 5.5511151231257827e-016 -1
		1 1.1102230246251565e-016 1
		-0.99999999999999989 -5.5511151231257827e-016 1
		;
createNode joint -n "reindeer__flexi_twist_01" -p "reindeer__midCtrl_01";
	rename -uid "1AB35EE5-4905-33D0-6025-50A8CCC876BA";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90 0 90 ;
	setAttr ".radi" 0.025;
createNode transform -n "reindeer__flexi_surface_01_twist_follicle_02" -p "reindeer__flexi_flcs01";
	rename -uid "D003A494-4B70-49E7-D969-9D973508F99A";
createNode follicle -n "reindeer__flexi_surface_01Shape_twist_follicle_02" -p "reindeer__flexi_surface_01_twist_follicle_02";
	rename -uid "7CC4C3A3-419C-5B1C-654A-3783C74D84FE";
	setAttr -k off ".v" no;
	setAttr ".pu" 0.5;
	setAttr ".pv" 0.3;
	setAttr -s 2 ".sts[0:1]"  0 1 3 1 0.2 3;
	setAttr -s 2 ".cws[0:1]"  0 1 3 1 0.2 3;
	setAttr -s 2 ".ats[0:1]"  0 1 3 1 0.2 3;
createNode scaleConstraint -n "reindeer__flexi_surface_01_twist_follicle_02_scaleConstraint1" 
		-p "reindeer__flexi_surface_01_twist_follicle_02";
	rename -uid "3874CEA4-49DB-F2B1-7369-BBB4B37F9CCC";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__global_move_ctrlW0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode transform -n "reindeer__midCtrl_02" -p "reindeer__flexi_surface_01_twist_follicle_02";
	rename -uid "86D9C207-4D31-F753-D1C3-5E896C394C97";
	addAttr -ci true -sn "reindeer_midCt02" -ln "reindeer_midCt02" -at "double";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "volumeControl" -ln "volumeControl" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "magnitude" -ln "magnitude" -at "double";
	setAttr -l on -k off ".v";
	setAttr -cb on ".volumeControl";
	setAttr -k on ".magnitude" -2.5;
createNode nurbsCurve -n "reindeer__midCtrl_0Shape2" -p "reindeer__midCtrl_02";
	rename -uid "57A1AB38-4644-288B-DAC6-279095D70992";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 9;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		-0.99999999999999989 -5.5511151231257827e-016 1
		-1 -1.1102230246251565e-016 -1
		0.99999999999999989 5.5511151231257827e-016 -1
		1 1.1102230246251565e-016 1
		-0.99999999999999989 -5.5511151231257827e-016 1
		;
createNode joint -n "reindeer__flexi_twist_02" -p "reindeer__midCtrl_02";
	rename -uid "D30CE873-464D-1FDC-EF99-8DBA7EA33D24";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90 0 90 ;
	setAttr ".radi" 0.025;
createNode transform -n "reindeer__flexi_surface_01_twist_follicle_03" -p "reindeer__flexi_flcs01";
	rename -uid "1621066F-4FC6-55DB-626A-4A81BEAC269D";
createNode follicle -n "reindeer__flexi_surface_01Shape_twist_follicle_03" -p "reindeer__flexi_surface_01_twist_follicle_03";
	rename -uid "A4CAB239-43B3-CF2A-78AA-E2A512B2E993";
	setAttr -k off ".v" no;
	setAttr ".pu" 0.5;
	setAttr ".pv" 0.5;
	setAttr -s 2 ".sts[0:1]"  0 1 3 1 0.2 3;
	setAttr -s 2 ".cws[0:1]"  0 1 3 1 0.2 3;
	setAttr -s 2 ".ats[0:1]"  0 1 3 1 0.2 3;
createNode scaleConstraint -n "reindeer__flexi_surface_01_twist_follicle_03_scaleConstraint1" 
		-p "reindeer__flexi_surface_01_twist_follicle_03";
	rename -uid "C123EE37-4B04-7510-CAE1-9DAC857CD8D0";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__global_move_ctrlW0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode transform -n "reindeer__midCtrl_03" -p "reindeer__flexi_surface_01_twist_follicle_03";
	rename -uid "7E12DE87-41AB-6DD7-5B1A-8AB0DC4568E4";
	addAttr -ci true -sn "reindeer_midCt03" -ln "reindeer_midCt03" -at "double";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "volumeControl" -ln "volumeControl" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "magnitude" -ln "magnitude" -at "double";
	setAttr -l on -k off ".v";
	setAttr -cb on ".volumeControl";
	setAttr -k on ".magnitude" -5;
createNode nurbsCurve -n "reindeer__midCtrl_0Shape3" -p "reindeer__midCtrl_03";
	rename -uid "47D67CAF-471A-6517-2B4A-6A8591192189";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 9;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		-0.99999999999999989 -5.5511151231257827e-016 1
		-1 -1.1102230246251565e-016 -1
		0.99999999999999989 5.5511151231257827e-016 -1
		1 1.1102230246251565e-016 1
		-0.99999999999999989 -5.5511151231257827e-016 1
		;
createNode joint -n "reindeer__flexi_twist_03" -p "reindeer__midCtrl_03";
	rename -uid "7E4A4064-4E6F-BE80-AF53-9487E6D71AB4";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90 0 90 ;
	setAttr ".radi" 0.025;
createNode transform -n "reindeer__flexi_surface_01_twist_follicle_04" -p "reindeer__flexi_flcs01";
	rename -uid "309213F5-4E3B-9CAA-B642-999B04869C61";
createNode follicle -n "reindeer__flexi_surface_01Shape_twist_follicle_04" -p "reindeer__flexi_surface_01_twist_follicle_04";
	rename -uid "8FFC365E-4A23-CB91-151C-6CB52A618202";
	setAttr -k off ".v" no;
	setAttr ".pu" 0.5;
	setAttr ".pv" 0.7;
	setAttr -s 2 ".sts[0:1]"  0 1 3 1 0.2 3;
	setAttr -s 2 ".cws[0:1]"  0 1 3 1 0.2 3;
	setAttr -s 2 ".ats[0:1]"  0 1 3 1 0.2 3;
createNode scaleConstraint -n "reindeer__flexi_surface_01_twist_follicle_04_scaleConstraint1" 
		-p "reindeer__flexi_surface_01_twist_follicle_04";
	rename -uid "B43F5B32-425F-CD62-0712-12AC3E238DCF";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__global_move_ctrlW0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode transform -n "reindeer__midCtrl_04" -p "reindeer__flexi_surface_01_twist_follicle_04";
	rename -uid "56F2F262-41DD-A98B-93F0-0A9AB191EB88";
	addAttr -ci true -sn "reindeer_midCt04" -ln "reindeer_midCt04" -at "double";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "volumeControl" -ln "volumeControl" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "magnitude" -ln "magnitude" -at "double";
	setAttr -l on -k off ".v";
	setAttr -cb on ".volumeControl";
	setAttr -k on ".magnitude" -2.5;
createNode nurbsCurve -n "reindeer__midCtrl_0Shape4" -p "reindeer__midCtrl_04";
	rename -uid "D9FA2B38-45C7-16C2-5412-14AC3A447C75";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 9;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		-0.99999999999999989 -5.5511151231257827e-016 1
		-1 -1.1102230246251565e-016 -1
		0.99999999999999989 5.5511151231257827e-016 -1
		1 1.1102230246251565e-016 1
		-0.99999999999999989 -5.5511151231257827e-016 1
		;
createNode joint -n "reindeer__flexi_twist_04" -p "reindeer__midCtrl_04";
	rename -uid "9A9D187E-42C8-ED7B-DFB7-F8A4E9D74145";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90 0 90 ;
	setAttr ".radi" 0.025;
createNode transform -n "reindeer__flexi_surface_01_twist_follicle_05" -p "reindeer__flexi_flcs01";
	rename -uid "68BD01A1-4E71-F79F-E0D9-A9861DEFE065";
createNode follicle -n "reindeer__flexi_surface_01Shape_twist_follicle_05" -p "reindeer__flexi_surface_01_twist_follicle_05";
	rename -uid "6BD87566-41AF-67D5-5C99-0F96D0C6C816";
	setAttr -k off ".v" no;
	setAttr ".pu" 0.5;
	setAttr ".pv" 0.9;
	setAttr -s 2 ".sts[0:1]"  0 1 3 1 0.2 3;
	setAttr -s 2 ".cws[0:1]"  0 1 3 1 0.2 3;
	setAttr -s 2 ".ats[0:1]"  0 1 3 1 0.2 3;
createNode scaleConstraint -n "reindeer__flexi_surface_01_twist_follicle_05_scaleConstraint1" 
		-p "reindeer__flexi_surface_01_twist_follicle_05";
	rename -uid "B239FFDA-46CD-C05F-2101-D093E5632ED3";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__global_move_ctrlW0" -dv 
		1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode transform -n "reindeer__midCtrl_05" -p "reindeer__flexi_surface_01_twist_follicle_05";
	rename -uid "BA1D27BE-46FF-6903-94B4-A1BCF611CBCA";
	addAttr -ci true -sn "reindeer_midCt05" -ln "reindeer_midCt05" -at "double";
	addAttr -ci true -sn "NJC_autoRigSystem" -ln "NJC_autoRigSystem" -at "double";
	addAttr -ci true -sn "volumeControl" -ln "volumeControl" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "magnitude" -ln "magnitude" -at "double";
	setAttr -l on -k off ".v";
	setAttr -cb on ".volumeControl";
	setAttr -k on ".magnitude";
createNode nurbsCurve -n "reindeer__midCtrl_0Shape5" -p "reindeer__midCtrl_05";
	rename -uid "7C0B3EA8-475F-DC49-C65F-8386596D15E6";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 9;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		-0.99999999999999989 -5.5511151231257827e-016 1
		-1 -1.1102230246251565e-016 -1
		0.99999999999999989 5.5511151231257827e-016 -1
		1 1.1102230246251565e-016 1
		-0.99999999999999989 -5.5511151231257827e-016 1
		;
createNode joint -n "reindeer__flexi_twist_05" -p "reindeer__midCtrl_05";
	rename -uid "1F7C1424-47BD-4879-8805-B88131C8943C";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90 0 90 ;
	setAttr ".radi" 0.025;
createNode transform -n "worldPlacement";
	rename -uid "DD356010-4535-1FF3-AB53-3F86BAB87594";
	setAttr ".ove" yes;
	setAttr ".ovc" 28;
createNode nurbsCurve -n "worldPlacementShape" -p "worldPlacement";
	rename -uid "DCBFE875-4AD3-0C26-3F58-DD9068F78111";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		-487.87900683085161 1.3149834325813923 487.87900683085161
		-487.87900683085161 1.3149834325813923 -487.87900683085161
		487.87900683085161 1.3149834325813923 -487.87900683085161
		487.87900683085161 1.3149834325813923 487.87900683085161
		-487.87900683085161 1.3149834325813923 487.87900683085161
		-487.87900683085161 -1.3149834325813923 487.87900683085161
		-487.87900683085161 -1.3149834325813923 -487.87900683085161
		-487.87900683085161 1.3149834325813923 -487.87900683085161
		-487.87900683085161 1.3149834325813923 487.87900683085161
		-487.87900683085161 -1.3149834325813923 487.87900683085161
		487.87900683085161 -1.3149834325813923 487.87900683085161
		487.87900683085161 1.3149834325813923 487.87900683085161
		487.87900683085161 1.3149834325813923 -487.87900683085161
		487.87900683085161 -1.3149834325813923 -487.87900683085161
		487.87900683085161 -1.3149834325813923 487.87900683085161
		487.87900683085161 -1.3149834325813923 -487.87900683085161
		-487.87900683085161 -1.3149834325813923 -487.87900683085161
		;
createNode transform -n "r_bk_foot_ctrl" -p "worldPlacement";
	rename -uid "3096325C-4EF7-C4A0-7CD7-528E546BD38C";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".t" -type "double3" -66.272386047791798 -2.4607101799745124e-005 -3.6789361800515508e-005 ;
	setAttr ".rp" -type "double3" 33.136186047791803 40.744924607101844 -81.633863210638154 ;
	setAttr ".sp" -type "double3" 33.136186047791803 40.744924607101844 -81.633863210638154 ;
createNode nurbsCurve -n "r_bk_foot_ctrlShape" -p "r_bk_foot_ctrl";
	rename -uid "D9A7F06B-4A35-9FD6-F321-A9B32177ACAF";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		3.5676710600820982 47.292612895697587 -38.8326672791109
		3.5676710600820982 47.292612895697587 -103.6405386889767
		58.515759113065151 47.292612895697587 -103.6405386889767
		58.515759113065151 47.292612895697587 -38.8326672791109
		3.5676710600820982 47.292612895697587 -38.8326672791109
		3.5676710600820982 0.84909809613383658 -38.8326672791109
		3.5676710600820982 0.84909809613383658 -103.6405386889767
		3.5676710600820982 47.292612895697587 -103.6405386889767
		3.5676710600820982 47.292612895697587 -38.8326672791109
		3.5676710600820982 0.84909809613383658 -38.8326672791109
		58.515759113065151 0.84909809613383658 -38.8326672791109
		58.515759113065151 47.292612895697587 -38.8326672791109
		58.515759113065151 47.292612895697587 -103.6405386889767
		58.515759113065151 0.84909809613383658 -103.6405386889767
		58.515759113065151 0.84909809613383658 -38.8326672791109
		58.515759113065151 0.84909809613383658 -103.6405386889767
		3.5676710600820982 0.84909809613383658 -103.6405386889767
		;
createNode ikHandle -n "r_bk_ik" -p "r_bk_foot_ctrl";
	rename -uid "9DC12C6B-438D-88DB-9CE5-87860834E91C";
	addAttr -ci true -h true -sn "srp" -ln "springRestPose" -dv 1 -at "long";
	addAttr -ci true -h true -sn "srpv" -ln "springRestPoleVector" -at "double3" -nc 
		3;
	addAttr -ci true -h true -sn "srpv0" -ln "springRestPoleVector0" -at "double" -p "springRestPoleVector";
	addAttr -ci true -h true -sn "srpv1" -ln "springRestPoleVector1" -at "double" -p "springRestPoleVector";
	addAttr -ci true -h true -sn "srpv2" -ln "springRestPoleVector2" -at "double" -p "springRestPoleVector";
	addAttr -ci true -m -sn "sab" -ln "springAngleBias" -at "compound" -nc 3;
	addAttr -ci true -sn "sbp" -ln "springAngleBias_Position" -at "float" -p "springAngleBias";
	addAttr -ci true -sn "sbfv" -ln "springAngleBias_FloatValue" -dv 1 -at "float" -p "springAngleBias";
	addAttr -ci true -sn "sbi" -ln "springAngleBias_Interp" -dv 3 -min 0 -max 3 -en 
		"None:Linear:Smooth:Spline" -at "enum" -p "springAngleBias";
	setAttr ".t" -type "double3" 33.136186047791796 40.744924607101844 -81.633863210638154 ;
	setAttr ".roc" yes;
	setAttr ".srpv" -type "double3" 2.5631870820302187 1.5427466417843023 -39.887968170637329 ;
	setAttr -s 2 ".sab[0:1]"  0 0.5 3 1 0.5 3;
	setAttr -l on ".sab[0].sbp";
	setAttr -l on ".sab[1].sbp";
createNode poleVectorConstraint -n "r_bk_ik_poleVectorConstraint1" -p "r_bk_ik";
	rename -uid "A4C80820-4878-131C-3833-FEA6BC8C4BD1";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "r_bk_pvW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 5.5808000000000106 -33.582924472884201 -109.96229835384931 ;
	setAttr -k on ".w0";
createNode transform -n "l_bk_foot_ctrl" -p "worldPlacement";
	rename -uid "EB0CE76D-473E-9C1F-C305-7F9D627A0A50";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".rp" -type "double3" 33.136186047791803 40.744924607101844 -81.633863210638154 ;
	setAttr ".sp" -type "double3" 33.136186047791803 40.744924607101844 -81.633863210638154 ;
createNode nurbsCurve -n "l_bk_foot_ctrlShape" -p "l_bk_foot_ctrl";
	rename -uid "E0FC5647-485A-7A5C-DD0F-1685D1DA917E";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		3.5676710600820982 47.292612895697587 -38.8326672791109
		3.5676710600820982 47.292612895697587 -103.6405386889767
		58.515759113065151 47.292612895697587 -103.6405386889767
		58.515759113065151 47.292612895697587 -38.8326672791109
		3.5676710600820982 47.292612895697587 -38.8326672791109
		3.5676710600820982 0.84909809613383658 -38.8326672791109
		3.5676710600820982 0.84909809613383658 -103.6405386889767
		3.5676710600820982 47.292612895697587 -103.6405386889767
		3.5676710600820982 47.292612895697587 -38.8326672791109
		3.5676710600820982 0.84909809613383658 -38.8326672791109
		58.515759113065151 0.84909809613383658 -38.8326672791109
		58.515759113065151 47.292612895697587 -38.8326672791109
		58.515759113065151 47.292612895697587 -103.6405386889767
		58.515759113065151 0.84909809613383658 -103.6405386889767
		58.515759113065151 0.84909809613383658 -38.8326672791109
		58.515759113065151 0.84909809613383658 -103.6405386889767
		3.5676710600820982 0.84909809613383658 -103.6405386889767
		;
createNode ikHandle -n "l_bk_ik" -p "l_bk_foot_ctrl";
	rename -uid "994B1F90-43C2-C594-F3D8-4E8C1716544B";
	addAttr -ci true -h true -sn "srp" -ln "springRestPose" -dv 1 -at "long";
	addAttr -ci true -h true -sn "srpv" -ln "springRestPoleVector" -at "double3" -nc 
		3;
	addAttr -ci true -h true -sn "srpv0" -ln "springRestPoleVector0" -at "double" -p "springRestPoleVector";
	addAttr -ci true -h true -sn "srpv1" -ln "springRestPoleVector1" -at "double" -p "springRestPoleVector";
	addAttr -ci true -h true -sn "srpv2" -ln "springRestPoleVector2" -at "double" -p "springRestPoleVector";
	addAttr -ci true -m -sn "sab" -ln "springAngleBias" -at "compound" -nc 3;
	addAttr -ci true -sn "sbp" -ln "springAngleBias_Position" -at "float" -p "springAngleBias";
	addAttr -ci true -sn "sbfv" -ln "springAngleBias_FloatValue" -dv 1 -at "float" -p "springAngleBias";
	addAttr -ci true -sn "sbi" -ln "springAngleBias_Interp" -dv 3 -min 0 -max 3 -en 
		"None:Linear:Smooth:Spline" -at "enum" -p "springAngleBias";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 33.136186047791803 40.744924607101851 -81.633863210638154 ;
	setAttr ".roc" yes;
	setAttr ".srpv" -type "double3" -2.5630777667830769 1.5427370569124306 -39.88797556575981 ;
	setAttr -s 2 ".sab[0:1]"  0 0.5 3 1 0.5 3;
	setAttr -l on ".sab[0].sbp";
	setAttr -l on ".sab[1].sbp";
createNode poleVectorConstraint -n "l_bk_ik_poleVectorConstraint1" -p "l_bk_ik";
	rename -uid "6028E4D7-44CD-6E10-23C9-F5B790D1CCEC";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "l_bk_pvW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -5.5808985315385868 -33.582732228724296 -109.9623228624681 ;
	setAttr -k on ".w0";
createNode transform -n "r_bk_pv" -p "worldPlacement";
	rename -uid "BE6EB178-49D9-7FD2-DD6C-A781CC0B90CF";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -42.6381 187.41407552711581 -185.59379835384931 ;
	setAttr ".sp" -type "double3" -42.6381 187.41407552711581 -185.59379835384931 ;
createNode nurbsCurve -n "r_bk_pvShape" -p "r_bk_pv";
	rename -uid "678ED817-44B9-51DB-6A01-11956C9FE1EC";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 69 0 no 3
		70 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69
		70
		-42.638100000000001 187.41407552711581 -164.93383828078922
		-42.638100000000001 187.41407552711581 -175.25253797911938
		-42.638100000000001 191.37150120909052 -176.0397134478431
		-42.638100000000001 194.72645146539466 -178.28142241557046
		-42.638100000000001 196.96816043312205 -181.63637267187462
		-42.638100000000001 197.75533590184574 -185.59379835384931
		-42.638100000000001 208.0740356001759 -185.59379835384931
		-42.638100000000001 197.75533590184574 -185.59379835384931
		-42.638100000000001 196.96816043312205 -189.55122403582402
		-42.638100000000001 194.72645146539466 -192.90617429212818
		-42.638100000000001 191.37150120909052 -195.14788325985558
		-42.638100000000001 187.41407552711581 -195.93505872857926
		-42.638100000000001 187.41407552711581 -206.25375842690943
		-42.638100000000001 187.41407552711581 -195.93505872857926
		-42.638100000000001 183.4566498451411 -195.14788325985558
		-42.638100000000001 180.10169958883696 -192.90617429212818
		-42.638100000000001 177.85999062110957 -189.55122403582402
		-42.638100000000001 177.07281515238589 -185.59379835384931
		-42.638100000000001 166.75411545405572 -185.59379835384931
		-42.638100000000001 177.07281515238589 -185.59379835384931
		-42.638100000000001 177.85999062110957 -181.63637267187462
		-42.638100000000001 180.10169958883696 -178.28142241557046
		-42.638100000000001 183.4566498451411 -176.0397134478431
		-42.638100000000001 187.41407552711581 -175.25253797911938
		-46.595525681974699 187.41407552711581 -176.0397134478431
		-49.950475938278856 187.41407552711581 -178.28142241557046
		-52.19218490600624 187.41407552711581 -181.63637267187462
		-52.979360374729943 187.41407552711581 -185.59379835384931
		-63.2980600730601 187.41407552711581 -185.59379835384931
		-52.979360374729943 187.41407552711581 -185.59379835384931
		-52.19218490600624 187.41407552711581 -189.55122403582402
		-49.950475938278856 187.41407552711581 -192.90617429212818
		-46.595525681974699 187.41407552711581 -195.14788325985558
		-42.638100000000001 187.41407552711581 -195.93505872857926
		-38.680674318025304 187.41407552711581 -195.14788325985558
		-35.325724061721147 187.41407552711581 -192.90617429212818
		-33.084015093993763 187.41407552711581 -189.55122403582402
		-32.29683962527006 187.41407552711581 -185.59379835384931
		-21.978139926939907 187.41407552711581 -185.59379835384931
		-32.29683962527006 187.41407552711581 -185.59379835384931
		-33.084015093993763 191.37150120909052 -185.59379835384931
		-35.325724061721147 194.72645146539466 -185.59379835384931
		-38.680674318025304 196.96816043312205 -185.59379835384931
		-42.638100000000001 197.75533590184574 -185.59379835384931
		-46.595525681974699 196.96816043312205 -185.59379835384931
		-49.950475938278856 194.72645146539466 -185.59379835384931
		-52.19218490600624 191.37150120909052 -185.59379835384931
		-52.979360374729943 187.41407552711581 -185.59379835384931
		-52.19218490600624 183.4566498451411 -185.59379835384931
		-49.950475938278856 180.10169958883696 -185.59379835384931
		-46.595525681974699 177.85999062110957 -185.59379835384931
		-42.638100000000001 177.07281515238589 -185.59379835384931
		-38.680674318025304 177.85999062110957 -185.59379835384931
		-35.325724061721147 180.10169958883696 -185.59379835384931
		-33.084015093993763 183.4566498451411 -185.59379835384931
		-32.29683962527006 187.41407552711581 -185.59379835384931
		-33.084015093993763 187.41407552711581 -181.63637267187462
		-35.325724061721147 187.41407552711581 -178.28142241557046
		-38.680674318025304 187.41407552711581 -176.0397134478431
		-42.638100000000001 187.41407552711581 -175.25253797911938
		-42.638100000000001 187.41407552711581 -185.59379835384931
		-42.638100000000001 187.41407552711581 -195.93505872857926
		-42.638100000000001 187.41407552711581 -185.59379835384931
		-32.29683962527006 187.41407552711581 -185.59379835384931
		-42.638100000000001 187.41407552711581 -185.59379835384931
		-52.979360374729943 187.41407552711581 -185.59379835384931
		-42.638100000000001 187.41407552711581 -185.59379835384931
		-42.638100000000001 177.07281515238589 -185.59379835384931
		-42.638100000000001 187.41407552711581 -185.59379835384931
		-42.638100000000001 197.75533590184574 -185.59379835384931
		;
createNode transform -n "l_bk_pv" -p "worldPlacement";
	rename -uid "07236E4F-4EE0-8816-3283-95AE2AD55BC8";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 42.638 187.41407552711581 -185.59379835384931 ;
	setAttr ".sp" -type "double3" 42.638 187.41407552711581 -185.59379835384931 ;
createNode nurbsCurve -n "l_bk_pvShape" -p "l_bk_pv";
	rename -uid "032EFFBA-415A-57E9-1254-B1A3185E43EF";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 69 0 no 3
		70 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69
		70
		42.637999999999998 187.41407552711581 -164.93383828078922
		42.637999999999998 187.41407552711581 -175.25253797911938
		42.637999999999998 191.37150120909052 -176.0397134478431
		42.637999999999998 194.72645146539466 -178.28142241557046
		42.637999999999998 196.96816043312205 -181.63637267187462
		42.637999999999998 197.75533590184574 -185.59379835384931
		42.637999999999998 208.0740356001759 -185.59379835384931
		42.637999999999998 197.75533590184574 -185.59379835384931
		42.637999999999998 196.96816043312205 -189.55122403582402
		42.637999999999998 194.72645146539466 -192.90617429212818
		42.637999999999998 191.37150120909052 -195.14788325985558
		42.637999999999998 187.41407552711581 -195.93505872857926
		42.637999999999998 187.41407552711581 -206.25375842690943
		42.637999999999998 187.41407552711581 -195.93505872857926
		42.637999999999998 183.4566498451411 -195.14788325985558
		42.637999999999998 180.10169958883696 -192.90617429212818
		42.637999999999998 177.85999062110957 -189.55122403582402
		42.637999999999998 177.07281515238589 -185.59379835384931
		42.637999999999998 166.75411545405572 -185.59379835384931
		42.637999999999998 177.07281515238589 -185.59379835384931
		42.637999999999998 177.85999062110957 -181.63637267187462
		42.637999999999998 180.10169958883696 -178.28142241557046
		42.637999999999998 183.4566498451411 -176.0397134478431
		42.637999999999998 187.41407552711581 -175.25253797911938
		38.6805743180253 187.41407552711581 -176.0397134478431
		35.325624061721143 187.41407552711581 -178.28142241557046
		33.08391509399376 187.41407552711581 -181.63637267187462
		32.296739625270057 187.41407552711581 -185.59379835384931
		21.978039926939903 187.41407552711581 -185.59379835384931
		32.296739625270057 187.41407552711581 -185.59379835384931
		33.08391509399376 187.41407552711581 -189.55122403582402
		35.325624061721143 187.41407552711581 -192.90617429212818
		38.6805743180253 187.41407552711581 -195.14788325985558
		42.637999999999998 187.41407552711581 -195.93505872857926
		46.595425681974696 187.41407552711581 -195.14788325985558
		49.950375938278853 187.41407552711581 -192.90617429212818
		52.192084906006237 187.41407552711581 -189.55122403582402
		52.97926037472994 187.41407552711581 -185.59379835384931
		63.297960073060096 187.41407552711581 -185.59379835384931
		52.97926037472994 187.41407552711581 -185.59379835384931
		52.192084906006237 191.37150120909052 -185.59379835384931
		49.950375938278853 194.72645146539466 -185.59379835384931
		46.595425681974696 196.96816043312205 -185.59379835384931
		42.637999999999998 197.75533590184574 -185.59379835384931
		38.6805743180253 196.96816043312205 -185.59379835384931
		35.325624061721143 194.72645146539466 -185.59379835384931
		33.08391509399376 191.37150120909052 -185.59379835384931
		32.296739625270057 187.41407552711581 -185.59379835384931
		33.08391509399376 183.4566498451411 -185.59379835384931
		35.325624061721143 180.10169958883696 -185.59379835384931
		38.6805743180253 177.85999062110957 -185.59379835384931
		42.637999999999998 177.07281515238589 -185.59379835384931
		46.595425681974696 177.85999062110957 -185.59379835384931
		49.950375938278853 180.10169958883696 -185.59379835384931
		52.192084906006237 183.4566498451411 -185.59379835384931
		52.97926037472994 187.41407552711581 -185.59379835384931
		52.192084906006237 187.41407552711581 -181.63637267187462
		49.950375938278853 187.41407552711581 -178.28142241557046
		46.595425681974696 187.41407552711581 -176.0397134478431
		42.637999999999998 187.41407552711581 -175.25253797911938
		42.637999999999998 187.41407552711581 -185.59379835384931
		42.637999999999998 187.41407552711581 -195.93505872857926
		42.637999999999998 187.41407552711581 -185.59379835384931
		52.97926037472994 187.41407552711581 -185.59379835384931
		42.637999999999998 187.41407552711581 -185.59379835384931
		32.296739625270057 187.41407552711581 -185.59379835384931
		42.637999999999998 187.41407552711581 -185.59379835384931
		42.637999999999998 177.07281515238589 -185.59379835384931
		42.637999999999998 187.41407552711581 -185.59379835384931
		42.637999999999998 197.75533590184574 -185.59379835384931
		;
createNode transform -n "r_fnt_foot_ctrl" -p "worldPlacement";
	rename -uid "4BE9194B-48CC-33F0-95BA-668B008C16B7";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".t" -type "double3" -66.891773313485373 -2.6912969900649841e-005 -4.3444787209523383e-005 ;
	setAttr ".rp" -type "double3" 33.445873313485365 46.920426912969944 95.47344344478735 ;
	setAttr ".sp" -type "double3" 33.445873313485365 46.920426912969944 95.47344344478735 ;
createNode nurbsCurve -n "r_fnt_foot_ctrlShape" -p "r_fnt_foot_ctrl";
	rename -uid "28CEFB18-47B3-2ECD-4DFE-16BAF3F55F60";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		0.16064456084689738 47.292612895697587 133.7882236172818
		0.16064456084689738 47.292612895697587 68.98035220741599
		66.73110206612381 47.292612895697587 68.98035220741599
		66.73110206612381 47.292612895697587 133.7882236172818
		0.16064456084689738 47.292612895697587 133.7882236172818
		0.16064456084689738 0.84909809613383302 133.7882236172818
		0.16064456084689738 0.84909809613383302 68.98035220741599
		0.16064456084689738 47.292612895697587 68.98035220741599
		0.16064456084689738 47.292612895697587 133.7882236172818
		0.16064456084689738 0.84909809613383302 133.7882236172818
		66.73110206612381 0.84909809613383302 133.7882236172818
		66.73110206612381 47.292612895697587 133.7882236172818
		66.73110206612381 47.292612895697587 68.98035220741599
		66.73110206612381 0.84909809613383302 68.98035220741599
		66.73110206612381 0.84909809613383302 133.7882236172818
		66.73110206612381 0.84909809613383302 68.98035220741599
		0.16064456084689738 0.84909809613383302 68.98035220741599
		;
createNode ikHandle -n "r_fnt_ik" -p "r_fnt_foot_ctrl";
	rename -uid "01C90692-42F4-5B6F-5FFA-D3A058F2C4E8";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 33.445873313485372 46.920426912969944 95.47344344478735 ;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "r_fnt_ik_poleVectorConstraint1" -p "r_fnt_ik";
	rename -uid "14A67DF7-4CCE-D644-5C4D-9AA3FADFFA0C";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "f_fnt_pv1W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 9.1221000000000103 -102.93001237546147 93.557430035450594 ;
	setAttr -k on ".w0";
createNode transform -n "l_fnt_foot_ctrl" -p "worldPlacement";
	rename -uid "39E00024-4A58-5DA8-B57A-B59891B17A27";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".rp" -type "double3" 33.445873313485365 46.920426912969944 95.47344344478735 ;
	setAttr ".sp" -type "double3" 33.445873313485365 46.920426912969944 95.47344344478735 ;
createNode nurbsCurve -n "l_fnt_foot_ctrlShape" -p "l_fnt_foot_ctrl";
	rename -uid "A24E38A5-4BF2-BEE7-8103-D6A051226F73";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 16 0 no 3
		17 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
		17
		0.16064456084689738 47.292612895697587 133.7882236172818
		0.16064456084689738 47.292612895697587 68.98035220741599
		66.73110206612381 47.292612895697587 68.98035220741599
		66.73110206612381 47.292612895697587 133.7882236172818
		0.16064456084689738 47.292612895697587 133.7882236172818
		0.16064456084689738 0.84909809613383302 133.7882236172818
		0.16064456084689738 0.84909809613383302 68.98035220741599
		0.16064456084689738 47.292612895697587 68.98035220741599
		0.16064456084689738 47.292612895697587 133.7882236172818
		0.16064456084689738 0.84909809613383302 133.7882236172818
		66.73110206612381 0.84909809613383302 133.7882236172818
		66.73110206612381 47.292612895697587 133.7882236172818
		66.73110206612381 47.292612895697587 68.98035220741599
		66.73110206612381 0.84909809613383302 68.98035220741599
		66.73110206612381 0.84909809613383302 133.7882236172818
		66.73110206612381 0.84909809613383302 68.98035220741599
		0.16064456084689738 0.84909809613383302 68.98035220741599
		;
createNode ikHandle -n "l_fnt_ik" -p "l_fnt_foot_ctrl";
	rename -uid "76BCBF96-478C-3103-2A99-DF9EFFF1CBE6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 33.445873313485365 46.920426912969944 95.47344344478735 ;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "l_fnt_ik_poleVectorConstraint1" -p "l_fnt_ik";
	rename -uid "BB117E8A-4DE4-04CB-8EC7-F0B064DAD2CC";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "f_fnt_pvW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -9.1216018127686951 -102.92964493550983 93.55740638439697 ;
	setAttr -k on ".w0";
createNode transform -n "f_fnt_pv" -p "worldPlacement";
	rename -uid "34BA75FC-40F2-DBC7-1FF9-F8ADF56A9A02";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 36.86647399327363 111.15198762453858 192.25633003545062 ;
	setAttr ".sp" -type "double3" 36.86647399327363 111.15198762453858 192.25633003545062 ;
createNode nurbsCurve -n "f_fnt_pvShape" -p "f_fnt_pv";
	rename -uid "19A7872F-41E3-C726-ED27-4795D6883DDC";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 69 0 no 3
		70 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69
		70
		36.86647399327363 111.15198762453858 211.18349226057336
		36.86647399327363 111.15198762453858 201.73024537858691
		36.86647399327363 114.77749501234194 201.00909210706638
		36.86647399327363 117.85105795891741 198.95540036982945
		36.86647399327363 119.90474969615435 195.88183742325398
		36.86647399327363 120.62590296767486 192.25633003545062
		36.86647399327363 130.07914984966132 192.25633003545062
		36.86647399327363 120.62590296767486 192.25633003545062
		36.86647399327363 119.90474969615435 188.63082264764725
		36.86647399327363 117.85105795891741 185.55725970107179
		36.86647399327363 114.77749501234194 183.50356796383485
		36.86647399327363 111.15198762453858 182.78241469231432
		36.86647399327363 111.15198762453858 173.32916781032787
		36.86647399327363 111.15198762453858 182.78241469231432
		36.86647399327363 107.52648023673521 183.50356796383485
		36.86647399327363 104.45291729015975 185.55725970107179
		36.86647399327363 102.3992255529228 188.63082264764725
		36.86647399327363 101.67807228140229 192.25633003545062
		36.86647399327363 92.224825399415835 192.25633003545062
		36.86647399327363 101.67807228140229 192.25633003545062
		36.86647399327363 102.3992255529228 195.88183742325398
		36.86647399327363 104.45291729015975 198.95540036982945
		36.86647399327363 107.52648023673521 201.00909210706638
		36.86647399327363 111.15198762453858 201.73024537858691
		33.240966605470256 111.15198762453858 201.00909210706638
		30.167403658894798 111.15198762453858 198.95540036982945
		28.113711921657856 111.15198762453858 195.88183742325398
		27.392558650137339 111.15198762453858 192.25633003545062
		17.939311768150887 111.15198762453858 192.25633003545062
		27.392558650137339 111.15198762453858 192.25633003545062
		28.113711921657856 111.15198762453858 188.63082264764725
		30.167403658894798 111.15198762453858 185.55725970107179
		33.240966605470256 111.15198762453858 183.50356796383485
		36.86647399327363 111.15198762453858 182.78241469231432
		40.491981381077004 111.15198762453858 183.50356796383485
		43.565544327652461 111.15198762453858 185.55725970107179
		45.619236064889407 111.15198762453858 188.63082264764725
		46.34038933640992 111.15198762453858 192.25633003545062
		55.793636218396372 111.15198762453858 192.25633003545062
		46.34038933640992 111.15198762453858 192.25633003545062
		45.619236064889407 114.77749501234194 192.25633003545062
		43.565544327652461 117.85105795891741 192.25633003545062
		40.491981381077004 119.90474969615435 192.25633003545062
		36.86647399327363 120.62590296767486 192.25633003545062
		33.240966605470256 119.90474969615435 192.25633003545062
		30.167403658894798 117.85105795891741 192.25633003545062
		28.113711921657856 114.77749501234194 192.25633003545062
		27.392558650137339 111.15198762453858 192.25633003545062
		28.113711921657856 107.52648023673521 192.25633003545062
		30.167403658894798 104.45291729015975 192.25633003545062
		33.240966605470256 102.3992255529228 192.25633003545062
		36.86647399327363 101.67807228140229 192.25633003545062
		40.491981381077004 102.3992255529228 192.25633003545062
		43.565544327652461 104.45291729015975 192.25633003545062
		45.619236064889407 107.52648023673521 192.25633003545062
		46.34038933640992 111.15198762453858 192.25633003545062
		45.619236064889407 111.15198762453858 195.88183742325398
		43.565544327652461 111.15198762453858 198.95540036982945
		40.491981381077004 111.15198762453858 201.00909210706638
		36.86647399327363 111.15198762453858 201.73024537858691
		36.86647399327363 111.15198762453858 192.25633003545062
		36.86647399327363 111.15198762453858 182.78241469231432
		36.86647399327363 111.15198762453858 192.25633003545062
		46.34038933640992 111.15198762453858 192.25633003545062
		36.86647399327363 111.15198762453858 192.25633003545062
		27.392558650137339 111.15198762453858 192.25633003545062
		36.86647399327363 111.15198762453858 192.25633003545062
		36.86647399327363 101.67807228140229 192.25633003545062
		36.86647399327363 111.15198762453858 192.25633003545062
		36.86647399327363 120.62590296767486 192.25633003545062
		;
createNode transform -n "f_fnt_pv1" -p "worldPlacement";
	rename -uid "72B4D4A7-4168-6889-5B7C-839B3EB452FB";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -36.866 111.15198762453858 192.25633003545062 ;
	setAttr ".sp" -type "double3" -36.866 111.15198762453858 192.25633003545062 ;
createNode nurbsCurve -n "f_fnt_pv1Shape" -p "f_fnt_pv1";
	rename -uid "3457ECF4-469B-4910-E27C-488B0117A3F0";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 69 0 no 3
		70 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54
		 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69
		70
		-36.866 111.15198762453858 211.18349226057336
		-36.866 111.15198762453858 201.73024537858691
		-36.866 114.77749501234194 201.00909210706638
		-36.866 117.85105795891741 198.95540036982945
		-36.866 119.90474969615435 195.88183742325398
		-36.866 120.62590296767486 192.25633003545062
		-36.866 130.07914984966132 192.25633003545062
		-36.866 120.62590296767486 192.25633003545062
		-36.866 119.90474969615435 188.63082264764725
		-36.866 117.85105795891741 185.55725970107179
		-36.866 114.77749501234194 183.50356796383485
		-36.866 111.15198762453858 182.78241469231432
		-36.866 111.15198762453858 173.32916781032787
		-36.866 111.15198762453858 182.78241469231432
		-36.866 107.52648023673521 183.50356796383485
		-36.866 104.45291729015975 185.55725970107179
		-36.866 102.3992255529228 188.63082264764725
		-36.866 101.67807228140229 192.25633003545062
		-36.866 92.224825399415835 192.25633003545062
		-36.866 101.67807228140229 192.25633003545062
		-36.866 102.3992255529228 195.88183742325398
		-36.866 104.45291729015975 198.95540036982945
		-36.866 107.52648023673521 201.00909210706638
		-36.866 111.15198762453858 201.73024537858691
		-40.491507387803374 111.15198762453858 201.00909210706638
		-43.565070334378831 111.15198762453858 198.95540036982945
		-45.618762071615777 111.15198762453858 195.88183742325398
		-46.33991534313629 111.15198762453858 192.25633003545062
		-55.793162225122742 111.15198762453858 192.25633003545062
		-46.33991534313629 111.15198762453858 192.25633003545062
		-45.618762071615777 111.15198762453858 188.63082264764725
		-43.565070334378831 111.15198762453858 185.55725970107179
		-40.491507387803374 111.15198762453858 183.50356796383485
		-36.866 111.15198762453858 182.78241469231432
		-33.240492612196626 111.15198762453858 183.50356796383485
		-30.166929665621169 111.15198762453858 185.55725970107179
		-28.113237928384226 111.15198762453858 188.63082264764725
		-27.392084656863709 111.15198762453858 192.25633003545062
		-17.938837774877257 111.15198762453858 192.25633003545062
		-27.392084656863709 111.15198762453858 192.25633003545062
		-28.113237928384226 114.77749501234194 192.25633003545062
		-30.166929665621169 117.85105795891741 192.25633003545062
		-33.240492612196626 119.90474969615435 192.25633003545062
		-36.866 120.62590296767486 192.25633003545062
		-40.491507387803374 119.90474969615435 192.25633003545062
		-43.565070334378831 117.85105795891741 192.25633003545062
		-45.618762071615777 114.77749501234194 192.25633003545062
		-46.33991534313629 111.15198762453858 192.25633003545062
		-45.618762071615777 107.52648023673521 192.25633003545062
		-43.565070334378831 104.45291729015975 192.25633003545062
		-40.491507387803374 102.3992255529228 192.25633003545062
		-36.866 101.67807228140229 192.25633003545062
		-33.240492612196626 102.3992255529228 192.25633003545062
		-30.166929665621169 104.45291729015975 192.25633003545062
		-28.113237928384226 107.52648023673521 192.25633003545062
		-27.392084656863709 111.15198762453858 192.25633003545062
		-28.113237928384226 111.15198762453858 195.88183742325398
		-30.166929665621169 111.15198762453858 198.95540036982945
		-33.240492612196626 111.15198762453858 201.00909210706638
		-36.866 111.15198762453858 201.73024537858691
		-36.866 111.15198762453858 192.25633003545062
		-36.866 111.15198762453858 182.78241469231432
		-36.866 111.15198762453858 192.25633003545062
		-27.392084656863709 111.15198762453858 192.25633003545062
		-36.866 111.15198762453858 192.25633003545062
		-46.33991534313629 111.15198762453858 192.25633003545062
		-36.866 111.15198762453858 192.25633003545062
		-36.866 101.67807228140229 192.25633003545062
		-36.866 111.15198762453858 192.25633003545062
		-36.866 120.62590296767486 192.25633003545062
		;
createNode transform -n "center_ctrl" -p "worldPlacement";
	rename -uid "01A2581C-4A87-A95B-16F5-7E9D8801C614";
	setAttr ".rp" -type "double3" 0 232.42308711591744 -72.195067408442185 ;
	setAttr ".sp" -type "double3" 0 232.42308711591744 -72.195067408442185 ;
createNode nurbsCurve -n "center_ctrlShape" -p "center_ctrl";
	rename -uid "8E92E37D-4B41-F928-5AB8-49A3BEB483E4";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 8 0 no 3
		9 0 1 2 3 4 5 6 7 8
		9
		26.000000000000043 468.8640334123603 -72.195067408442213
		-27.000000000000014 468.8640334123603 -72.195067408442213
		-27.000000000000014 404.8640334123603 -72.195067408442199
		27.000000000000043 404.8640334123603 -72.195067408442199
		27.000000000000043 422.8640334123603 -72.195067408442199
		-8.9999999999999858 422.8640334123603 -72.195067408442199
		-8.9999999999999858 453.8640334123603 -72.195067408442199
		26.000000000000043 453.8640334123603 -72.195067408442199
		26.000000000000043 468.8640334123603 -72.195067408442213
		;
createNode fosterParent -n "Reindeer_SkinRNfosterParent1";
	rename -uid "02647D6E-491E-FB6E-0616-BEBA08E23778";
createNode scaleConstraint -n "pelvis_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "84F70AC9-4659-F049-CA1B-7C8501B1611E";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_pelvisW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode orientConstraint -n "pelvis_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "F022BBA5-4B38-79C9-323C-D9924DE08F62";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_pelvisW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "pelvis_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "C0BB51C9-4D8C-BAE3-45F1-85AE33033A61";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_pelvisW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -9.1612641917038103e-016 232.42308711591741 -82.517332181750191 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "hips_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "F5AD2D72-4344-39CD-2622-34B8FD0C6EE1";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_hipsW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "hips_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "64A426EE-4EC5-35C0-8D29-55AD419F7603";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_hipsW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "hips_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "76720FD5-4AE8-BF05-417A-DDBC5577F6F6";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_hipsW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode scaleConstraint -n "r_femur_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "8921EB65-4EBC-B686-E23A-B9B234B0FC38";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_femurW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "r_femur_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "2EC2B505-4B63-1928-3DE4-D3ACFCDAB4F9";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_femurW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.2789352229414566 -0.72632278602262978 -0.020615730952793794 ;
	setAttr ".rsrr" -type "double3" -7.9513867036587919e-016 -3.9756933518293969e-016 
		-1.2722218725854067e-014 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "r_femur_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "7AFADD11-4198-1C35-A636-FF9E3CA7D461";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_femurW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -3.436432591557832 -11.426087115917397 48.2189 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "r_tibia_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "19CC7708-4BC2-49F4-F8EC-66ACC4AD3A31";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_tibiaW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1 0.99999999999999978 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "r_tibia_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "9E0A28AC-4B28-ECDF-13BA-A0B04B6FB880";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_tibiaW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -3.975693351829396e-016 -2.981770013872047e-016 9.3180312933501483e-018 ;
	setAttr ".rsrr" -type "double3" -3.975693351829396e-016 -2.981770013872047e-016 
		9.3180312933501483e-018 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "r_tibia_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "8F6B5921-457E-43D5-E399-87B967C15767";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_tibiaW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 2.8421709430404007e-014 -2.8421709430404007e-014 7.1054273576010019e-015 ;
	setAttr ".rst" -type "double3" -67.471806251111616 0.00010636131018237904 3.3768716139803701e-005 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "r_metatarsal_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "0C20F26F-4CD3-ABDE-E06C-A78EC0BC0B9F";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_metatarsalW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.99999999999999978 1 1 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "r_metatarsal_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "A68E26EF-4FBE-BB92-B6A3-50BF38BDAA6E";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_metatarsalW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.789062008323228e-015 7.9513867036587988e-016 4.4502917457040303e-014 ;
	setAttr ".rsrr" -type "double3" -1.9878466759146977e-016 1.9878466759146985e-016 
		1.2734642767578533e-014 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "r_metatarsal_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "5D2F0FAB-44D5-0E9C-FA6F-67AFB9F98292";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_metatarsalW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 2.8421709430404007e-014 2.8421709430404007e-014 -1.0658141036401503e-014 ;
	setAttr ".rst" -type "double3" -53.790129862934023 0.00015230768249807625 -7.4112793495118012e-005 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "r_pastern_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "42904559-45ED-DFA7-4CF1-FB84F199B016";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_pasternW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1.0000000000000002 1 1 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "r_pastern_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "6DFA6F5E-41B9-CDB2-7051-BE904A22D7E0";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_pasternW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "r_pastern_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "45AA8840-402B-8FE7-452A-3B820CBE99AE";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_pasternW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 5.6843418860808015e-014 -2.8421709430404007e-014 7.1054273576010019e-015 ;
	setAttr ".rst" -type "double3" -86.587125929897354 0.00023804956489925644 5.8618028372592335e-005 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "spine_01_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "A61CB504-4BB5-0AE1-CC06-EFA4DE4A1B29";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_twist_01W0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.060593330865326568 0.060593330865326568 0.060593330865326568 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "spine_01_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "37D96125-45CE-146E-774C-A890B548645D";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_twist_01W0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.9083328088781097e-014 6.3611093629270351e-015 1.9083328088781097e-014 ;
	setAttr ".rsrr" -type "double3" -1.9083328088781097e-014 6.3611093629270351e-015 
		1.9083328088781097e-014 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "spine_01_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "807619B0-4DBA-5657-9E10-089C197B8DC2";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_twist_01W0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -1.4210854715202004e-014 0 -1.2621774483536189e-029 ;
	setAttr ".rst" -type "double3" 16.503466581750175 0 -2.6386349428456973e-014 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "spine_02_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "1D7DFDAE-41FD-13D1-66DE-9E84EE35D7E0";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_twist_02W0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.060593330865326568 0.060593330865326568 0.060593330865326568 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "spine_02_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "95BC7D28-477F-3298-C62F-15B7B9C017C9";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_twist_02W0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.9083328088781097e-014 6.3611093629270351e-015 1.9083328088781097e-014 ;
	setAttr ".rsrr" -type "double3" -1.9083328088781097e-014 6.3611093629270351e-015 
		1.9083328088781097e-014 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "spine_02_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "53A6D311-451A-CC8C-7AFB-2AA5341AC12E";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_twist_02W0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -7.1054273576010019e-015 0 0 ;
	setAttr ".rst" -type "double3" 33.006932800000008 0 2.1815260356494735e-014 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "spine_03_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "C8FB277D-415B-124E-5CC6-289BD2ECF538";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_twist_03W0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.060593330865326568 0.060593330865326568 0.060593330865326568 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "spine_03_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "8ABB212D-447C-3D5F-B110-CC95FCFB0666";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_twist_03W0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.9083328088781097e-014 6.3611093629270351e-015 1.9083328088781097e-014 ;
	setAttr ".rsrr" -type "double3" -1.9083328088781097e-014 6.3611093629270351e-015 
		1.9083328088781097e-014 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "spine_03_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "A7376C33-47EF-0948-CCE7-BBB2C5B1DC9C";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_twist_03W0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0 0 3.1554436208840472e-030 ;
	setAttr ".rst" -type "double3" 33.006932800000001 0 2.1977491076556148e-014 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "spine_04_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "FDD7AFB8-4D4C-1B86-5506-C484FE019140";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_twist_04W0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.060593330865326568 0.060593330865326568 0.060593330865326568 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "spine_04_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "ED2C9238-438B-F22E-94B5-A4AF42691F2D";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_twist_04W0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.9083328088781097e-014 6.3611093629270351e-015 1.9083328088781097e-014 ;
	setAttr ".rsrr" -type "double3" -1.9083328088781097e-014 6.3611093629270351e-015 
		1.9083328088781097e-014 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "spine_04_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "2558413D-4CD4-241F-FBDC-E4A19B28032D";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_twist_04W0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0 0 6.3108872417680944e-030 ;
	setAttr ".rst" -type "double3" 33.006932799999987 0 2.1977491076556138e-014 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "spine_05_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "CEDF5B24-403F-D02B-4ADC-7E8482A32F5C";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_twist_05W0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.060593330865326568 0.060593330865326568 0.060593330865326568 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "spine_05_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "14FD6E1E-4317-2C28-373F-258020476A7C";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_twist_05W0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.9083328088781097e-014 6.3611093629270351e-015 1.9083328088781097e-014 ;
	setAttr ".rsrr" -type "double3" -1.9083328088781097e-014 6.3611093629270351e-015 
		1.9083328088781097e-014 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "spine_05_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "538F0D14-4FC6-DAD7-6E3C-D98E5646E8AF";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "reindeer__flexi_twist_05W0" -dv 1 
		-min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 7.1054273576010019e-015 0 9.4663308626521417e-030 ;
	setAttr ".rst" -type "double3" 33.006932800000023 0 2.1815260356494741e-014 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "thorax_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "67FB6416-4CC0-5529-F7ED-3083DF853D09";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_thoraxW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "thorax_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "083DC944-495F-E2C4-B797-F184652C9C50";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_thoraxW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "thorax_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "161DE3BF-4A16-ADDE-0155-27AF8A9930C0";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_thoraxW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0 0 9.4663308626521417e-030 ;
	setAttr ".rst" -type "double3" 16.50346658175016 0 -2.4554096590116211e-014 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "neckBase_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "59A02284-45EE-DF9E-EE91-659F323DAB52";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_neckBaseW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "neckBase_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "779C880D-4502-B3E9-43DF-8584079EBF4C";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_neckBaseW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "neckBase_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "A60DA392-4327-42C9-5481-1D92BB9A1685";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_neckBaseW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0 0 6.3108872417680944e-030 ;
	setAttr ".rst" -type "double3" 7.324568659949108 51.572528130797906 0 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "neck_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "F47AE86E-420C-3C8E-C6B9-87B9430BD7C3";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_neckW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1 0.99999999999999978 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "neck_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "D4C75036-4850-2B8C-ACDB-369EF921F158";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_neckW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 180 -180 180 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "neck_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "9E85B620-4439-1CC3-D790-76B81B77AC32";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_neckW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0 0 -6.7053176943786003e-030 ;
	setAttr ".rst" -type "double3" 56.587329315653449 0 0 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "head_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "A60EC8BC-452C-936E-34C5-8DBEB4FDE8AC";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_headW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.99999999999999978 0.99999999999999956 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "head_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "3E27151E-4DA7-794C-16C9-39AC56140FA9";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_headW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "head_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "05C00343-47DA-055B-3F70-928FC8EF3BC2";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_headW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -5.6843418860808015e-014 -2.5243548967072378e-029 0 ;
	setAttr ".rst" -type "double3" 78.706996947081393 -9.6654688499407832e-015 -2.8421709430404007e-014 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "r_fnt_humerus_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "5312E152-4E83-8542-1B0F-018A92DA96D0";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_fnt_humerusW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "r_fnt_humerus_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "8F0D35B1-4A65-814F-7519-3690350B5B25";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_fnt_humerusW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -5.7431365129729697 0.67219330918286879 0.044391683997335944 ;
	setAttr ".rsrr" -type "double3" 1.987846675914698e-015 -5.5173828725627002e-031 
		3.1805546814635168e-014 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "r_fnt_humerus_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "043EBD09-4BF7-4543-95C6-22B2FE644112";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_fnt_humerusW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 5.8593030449418109 -18.341087115917418 45.9881 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "r_fnt_metacarpal_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "56EA5152-4720-B09F-15A7-DEA825EAB91C";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_fnt_metacarpalW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1 0.99999999999999978 1 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "r_fnt_metacarpal_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "F1E446CB-42BC-6052-99B6-2B971D74A5E3";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_fnt_metacarpalW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -1.490885006936024e-016 1.1958140159799355e-015 -3.1793899275518481e-015 ;
	setAttr ".rsrr" -type "double3" -1.490885006936024e-016 1.1958140159799355e-015 
		-3.1793899275518481e-015 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "r_fnt_metacarpal_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "50E73AF8-4DD4-F14A-B175-B78CA70717C3";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_fnt_metacarpalW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1.4210854715202004e-014 5.6843418860808015e-014 0 ;
	setAttr ".rst" -type "double3" -103.81932181498919 -0.00021120540316132974 3.3148592294196533e-005 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "r_fnt_pastern_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "590D2981-4E0E-AC7E-9896-75AFB7989CE0";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_fnt_pasternW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1 0.99999999999999978 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "r_fnt_pastern_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "2792794C-4570-E6AC-CE64-7B92C038E775";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_fnt_pasternW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "r_fnt_pastern_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "405C967E-400C-ED4E-5116-D8AE77A1CA94";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_r_fnt_pasternW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 2.8421709430404007e-014 8.5265128291212022e-014 -3.5527136788005009e-015 ;
	setAttr ".rst" -type "double3" -65.674876602975232 0.00025291998777277058 2.7681399785706162e-006 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "l_fnt_humerus_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "20FFDB70-4F73-9688-1D91-BEB97582FEDB";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_fnt_humerusW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "l_fnt_humerus_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "2EC14397-459A-EE94-1536-40A4F2465E15";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_fnt_humerusW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -5.743609309077641 0.67224830247976264 0.044391480718972753 ;
	setAttr ".rsrr" -type "double3" 1.5902773407317588e-015 3.975693351829392e-016 3.1780698731186236e-014 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "l_fnt_humerus_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "38F3ACE4-4E33-7681-5FBE-159F5A1A2730";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_fnt_humerusW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 5.8593266959954633 -18.341454555869063 -45.988075806042318 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "l_fnt_metacarpal_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "DA2BBB62-45F7-B296-F161-8FA4CC416D29";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_fnt_metacarpalW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1 0.99999999999999978 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "l_fnt_metacarpal_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "9F17A08B-4E7F-29FF-CEAB-6D89B22E0DC7";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_fnt_metacarpalW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.4848083448933705e-016 2.4599602614444388e-015 -9.5315695104894216e-015 ;
	setAttr ".rsrr" -type "double3" 2.4848083448933705e-016 2.4599602614444388e-015 
		-9.5315695104894216e-015 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "l_fnt_metacarpal_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "2324568E-4567-0ED9-8858-37B46E99FA7A";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_fnt_metacarpalW0" -dv 1 -min 
		0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1.4210854715202004e-014 -5.6843418860808015e-014 3.5527136788005009e-015 ;
	setAttr ".rst" -type "double3" 103.81894612777182 4.2632564145606011e-014 -1.0658141036401503e-014 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "l_fnt_pastern_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "34452D6E-4A50-27EB-EA36-E8876B6DCEC6";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_fnt_pasternW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1 1 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "l_fnt_pastern_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "18170457-44C6-5FA9-C6E5-43AE311FCCE0";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_fnt_pasternW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode pointConstraint -n "l_fnt_pastern_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "F2E012C9-475F-6C14-43F8-049469BE7A0B";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_fnt_pasternW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -1.4210854715202004e-014 -4.2632564145606011e-014 3.5527136788005009e-015 ;
	setAttr ".rst" -type "double3" 65.674784449770016 -4.2632564145606011e-014 -1.0658141036401503e-014 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "l_femur_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "48431D38-46D3-61DB-BF90-0CBE5637B832";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_femurW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "l_femur_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "067DE9E1-49FA-E13A-6538-849DBA3A082A";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_femurW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 2.2787314097468032 -0.72625781922395205 -0.020612191206207775 ;
	setAttr ".rsrr" -type "double3" 1.192708005548818e-015 -1.987846675914698e-015 5.0901298945140722e-014 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "l_femur_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "A3CF754E-49C8-1677-6EE3-C4B091B1C2B1";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_femurW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -3.4364080829390105 -11.426279360077302 -48.218898531538571 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "l_tibia_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "C396EDFA-48AD-486E-BD14-A4B791736855";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_tibiaW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1 0.99999999999999978 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "l_tibia_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "89C07461-4B0C-CA4A-4164-3C94AC243C77";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_tibiaW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -5.9635400277440939e-016 5.466578358765419e-016 -3.1060104311167183e-018 ;
	setAttr ".rsrr" -type "double3" -7.9513867036587919e-016 1.0436195048552165e-015 
		-9.3180312933501529e-018 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "l_tibia_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "294493E5-4E8A-19A9-0B6A-67B54F6512A7";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_tibiaW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -2.8421709430404007e-014 -2.1316282072803006e-014 0 ;
	setAttr ".rst" -type "double3" 67.471276475757293 1.2434497875801753e-014 -2.8421709430404007e-014 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "l_metatarsal_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "B6FE095F-45A0-9FD2-E511-8B9EB06CA780";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_metatarsalW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 0.99999999999999978 1 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "l_metatarsal_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "80445D94-41C3-6F65-A47A-FEB01C74DF27";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_metatarsalW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0 9.9392333795734899e-017 0 ;
	setAttr ".rsrr" -type "double3" 0 9.9392333795734899e-017 0 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "l_metatarsal_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "52A7299A-41ED-85CF-3416-37BD8FFC5629";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_metatarsalW0" -dv 1 -min 0 
		-at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -7.1054273576010019e-015 -8.5265128291212022e-014 7.1054273576010019e-015 ;
	setAttr ".rst" -type "double3" 53.790473552188971 5.6843418860808015e-014 1.0658141036401503e-014 ;
	setAttr -k on ".w0";
createNode scaleConstraint -n "l_pastern_scaleConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "40A0D882-49BE-51FC-DC71-0AB12C365B5C";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_pasternW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" 1.0000000000000002 0.99999999999999978 0.99999999999999978 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "l_pastern_orientConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "127627C7-49D0-1E9D-C327-708ACBB6DF77";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_pasternW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.3131616981611427 0.60626605647379339 -0.054091652670945893 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "l_pastern_pointConstraint1" -p "Reindeer_SkinRNfosterParent1";
	rename -uid "6F3A12CE-4DCA-65A2-FADC-69B80F393DB3";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "rig_l_pasternW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".o" -type "double3" -5.6843418860808015e-014 -5.6843418860808015e-014 0 ;
	setAttr ".rst" -type "double3" 86.58717486543074 1.5631940186722204e-013 -2.1316282072803006e-014 ;
	setAttr -k on ".w0";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "00B5127E-4EEF-C207-690C-4C8EA2FDC954";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "C12DECE3-436F-1A18-FA49-D4BF2E6CB3A8";
	setAttr ".cdl" 1;
	setAttr -s 2 ".dli[1]"  1;
	setAttr -s 2 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "BE1F539C-4F24-64F9-A332-D89C2906A72D";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "D9DBCD1D-4671-4388-9579-6FAFEB66F09C";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "C66C28E1-4A2C-FAA3-AA70-928E7E878E63";
	setAttr ".g" yes;
createNode reference -n "Reindeer_SkinRN";
	rename -uid "BD2D4885-41BF-4C6F-9F07-FAA12E3EAA78";
	setAttr -s 453 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".phl[81]" 0;
	setAttr ".phl[82]" 0;
	setAttr ".phl[83]" 0;
	setAttr ".phl[84]" 0;
	setAttr ".phl[85]" 0;
	setAttr ".phl[86]" 0;
	setAttr ".phl[87]" 0;
	setAttr ".phl[88]" 0;
	setAttr ".phl[89]" 0;
	setAttr ".phl[90]" 0;
	setAttr ".phl[91]" 0;
	setAttr ".phl[92]" 0;
	setAttr ".phl[93]" 0;
	setAttr ".phl[94]" 0;
	setAttr ".phl[95]" 0;
	setAttr ".phl[96]" 0;
	setAttr ".phl[97]" 0;
	setAttr ".phl[98]" 0;
	setAttr ".phl[99]" 0;
	setAttr ".phl[100]" 0;
	setAttr ".phl[101]" 0;
	setAttr ".phl[102]" 0;
	setAttr ".phl[103]" 0;
	setAttr ".phl[104]" 0;
	setAttr ".phl[105]" 0;
	setAttr ".phl[106]" 0;
	setAttr ".phl[107]" 0;
	setAttr ".phl[108]" 0;
	setAttr ".phl[109]" 0;
	setAttr ".phl[110]" 0;
	setAttr ".phl[111]" 0;
	setAttr ".phl[112]" 0;
	setAttr ".phl[113]" 0;
	setAttr ".phl[114]" 0;
	setAttr ".phl[115]" 0;
	setAttr ".phl[116]" 0;
	setAttr ".phl[117]" 0;
	setAttr ".phl[118]" 0;
	setAttr ".phl[119]" 0;
	setAttr ".phl[120]" 0;
	setAttr ".phl[121]" 0;
	setAttr ".phl[122]" 0;
	setAttr ".phl[123]" 0;
	setAttr ".phl[124]" 0;
	setAttr ".phl[125]" 0;
	setAttr ".phl[126]" 0;
	setAttr ".phl[127]" 0;
	setAttr ".phl[128]" 0;
	setAttr ".phl[129]" 0;
	setAttr ".phl[130]" 0;
	setAttr ".phl[131]" 0;
	setAttr ".phl[132]" 0;
	setAttr ".phl[133]" 0;
	setAttr ".phl[134]" 0;
	setAttr ".phl[135]" 0;
	setAttr ".phl[136]" 0;
	setAttr ".phl[137]" 0;
	setAttr ".phl[138]" 0;
	setAttr ".phl[139]" 0;
	setAttr ".phl[140]" 0;
	setAttr ".phl[141]" 0;
	setAttr ".phl[142]" 0;
	setAttr ".phl[143]" 0;
	setAttr ".phl[144]" 0;
	setAttr ".phl[145]" 0;
	setAttr ".phl[146]" 0;
	setAttr ".phl[147]" 0;
	setAttr ".phl[148]" 0;
	setAttr ".phl[149]" 0;
	setAttr ".phl[150]" 0;
	setAttr ".phl[151]" 0;
	setAttr ".phl[152]" 0;
	setAttr ".phl[153]" 0;
	setAttr ".phl[154]" 0;
	setAttr ".phl[155]" 0;
	setAttr ".phl[156]" 0;
	setAttr ".phl[157]" 0;
	setAttr ".phl[158]" 0;
	setAttr ".phl[159]" 0;
	setAttr ".phl[160]" 0;
	setAttr ".phl[161]" 0;
	setAttr ".phl[162]" 0;
	setAttr ".phl[163]" 0;
	setAttr ".phl[164]" 0;
	setAttr ".phl[165]" 0;
	setAttr ".phl[166]" 0;
	setAttr ".phl[167]" 0;
	setAttr ".phl[168]" 0;
	setAttr ".phl[169]" 0;
	setAttr ".phl[170]" 0;
	setAttr ".phl[171]" 0;
	setAttr ".phl[172]" 0;
	setAttr ".phl[173]" 0;
	setAttr ".phl[174]" 0;
	setAttr ".phl[175]" 0;
	setAttr ".phl[176]" 0;
	setAttr ".phl[177]" 0;
	setAttr ".phl[178]" 0;
	setAttr ".phl[179]" 0;
	setAttr ".phl[180]" 0;
	setAttr ".phl[181]" 0;
	setAttr ".phl[182]" 0;
	setAttr ".phl[183]" 0;
	setAttr ".phl[184]" 0;
	setAttr ".phl[185]" 0;
	setAttr ".phl[186]" 0;
	setAttr ".phl[187]" 0;
	setAttr ".phl[188]" 0;
	setAttr ".phl[189]" 0;
	setAttr ".phl[190]" 0;
	setAttr ".phl[191]" 0;
	setAttr ".phl[192]" 0;
	setAttr ".phl[193]" 0;
	setAttr ".phl[194]" 0;
	setAttr ".phl[195]" 0;
	setAttr ".phl[196]" 0;
	setAttr ".phl[197]" 0;
	setAttr ".phl[198]" 0;
	setAttr ".phl[199]" 0;
	setAttr ".phl[200]" 0;
	setAttr ".phl[201]" 0;
	setAttr ".phl[202]" 0;
	setAttr ".phl[203]" 0;
	setAttr ".phl[204]" 0;
	setAttr ".phl[205]" 0;
	setAttr ".phl[206]" 0;
	setAttr ".phl[207]" 0;
	setAttr ".phl[208]" 0;
	setAttr ".phl[209]" 0;
	setAttr ".phl[210]" 0;
	setAttr ".phl[211]" 0;
	setAttr ".phl[212]" 0;
	setAttr ".phl[213]" 0;
	setAttr ".phl[214]" 0;
	setAttr ".phl[215]" 0;
	setAttr ".phl[216]" 0;
	setAttr ".phl[217]" 0;
	setAttr ".phl[218]" 0;
	setAttr ".phl[219]" 0;
	setAttr ".phl[220]" 0;
	setAttr ".phl[221]" 0;
	setAttr ".phl[222]" 0;
	setAttr ".phl[223]" 0;
	setAttr ".phl[224]" 0;
	setAttr ".phl[225]" 0;
	setAttr ".phl[226]" 0;
	setAttr ".phl[227]" 0;
	setAttr ".phl[228]" 0;
	setAttr ".phl[229]" 0;
	setAttr ".phl[230]" 0;
	setAttr ".phl[231]" 0;
	setAttr ".phl[232]" 0;
	setAttr ".phl[233]" 0;
	setAttr ".phl[234]" 0;
	setAttr ".phl[235]" 0;
	setAttr ".phl[236]" 0;
	setAttr ".phl[237]" 0;
	setAttr ".phl[238]" 0;
	setAttr ".phl[239]" 0;
	setAttr ".phl[240]" 0;
	setAttr ".phl[241]" 0;
	setAttr ".phl[242]" 0;
	setAttr ".phl[243]" 0;
	setAttr ".phl[244]" 0;
	setAttr ".phl[245]" 0;
	setAttr ".phl[246]" 0;
	setAttr ".phl[247]" 0;
	setAttr ".phl[248]" 0;
	setAttr ".phl[249]" 0;
	setAttr ".phl[250]" 0;
	setAttr ".phl[251]" 0;
	setAttr ".phl[252]" 0;
	setAttr ".phl[253]" 0;
	setAttr ".phl[254]" 0;
	setAttr ".phl[255]" 0;
	setAttr ".phl[256]" 0;
	setAttr ".phl[257]" 0;
	setAttr ".phl[258]" 0;
	setAttr ".phl[259]" 0;
	setAttr ".phl[260]" 0;
	setAttr ".phl[261]" 0;
	setAttr ".phl[262]" 0;
	setAttr ".phl[263]" 0;
	setAttr ".phl[264]" 0;
	setAttr ".phl[265]" 0;
	setAttr ".phl[266]" 0;
	setAttr ".phl[267]" 0;
	setAttr ".phl[268]" 0;
	setAttr ".phl[269]" 0;
	setAttr ".phl[270]" 0;
	setAttr ".phl[271]" 0;
	setAttr ".phl[272]" 0;
	setAttr ".phl[273]" 0;
	setAttr ".phl[274]" 0;
	setAttr ".phl[275]" 0;
	setAttr ".phl[276]" 0;
	setAttr ".phl[277]" 0;
	setAttr ".phl[278]" 0;
	setAttr ".phl[279]" 0;
	setAttr ".phl[280]" 0;
	setAttr ".phl[281]" 0;
	setAttr ".phl[282]" 0;
	setAttr ".phl[283]" 0;
	setAttr ".phl[284]" 0;
	setAttr ".phl[285]" 0;
	setAttr ".phl[286]" 0;
	setAttr ".phl[287]" 0;
	setAttr ".phl[288]" 0;
	setAttr ".phl[289]" 0;
	setAttr ".phl[290]" 0;
	setAttr ".phl[291]" 0;
	setAttr ".phl[292]" 0;
	setAttr ".phl[293]" 0;
	setAttr ".phl[294]" 0;
	setAttr ".phl[295]" 0;
	setAttr ".phl[296]" 0;
	setAttr ".phl[297]" 0;
	setAttr ".phl[298]" 0;
	setAttr ".phl[299]" 0;
	setAttr ".phl[300]" 0;
	setAttr ".phl[301]" 0;
	setAttr ".phl[302]" 0;
	setAttr ".phl[303]" 0;
	setAttr ".phl[304]" 0;
	setAttr ".phl[305]" 0;
	setAttr ".phl[306]" 0;
	setAttr ".phl[307]" 0;
	setAttr ".phl[308]" 0;
	setAttr ".phl[309]" 0;
	setAttr ".phl[310]" 0;
	setAttr ".phl[311]" 0;
	setAttr ".phl[312]" 0;
	setAttr ".phl[313]" 0;
	setAttr ".phl[314]" 0;
	setAttr ".phl[315]" 0;
	setAttr ".phl[316]" 0;
	setAttr ".phl[317]" 0;
	setAttr ".phl[318]" 0;
	setAttr ".phl[319]" 0;
	setAttr ".phl[320]" 0;
	setAttr ".phl[321]" 0;
	setAttr ".phl[322]" 0;
	setAttr ".phl[323]" 0;
	setAttr ".phl[324]" 0;
	setAttr ".phl[325]" 0;
	setAttr ".phl[326]" 0;
	setAttr ".phl[327]" 0;
	setAttr ".phl[328]" 0;
	setAttr ".phl[329]" 0;
	setAttr ".phl[330]" 0;
	setAttr ".phl[331]" 0;
	setAttr ".phl[332]" 0;
	setAttr ".phl[333]" 0;
	setAttr ".phl[334]" 0;
	setAttr ".phl[335]" 0;
	setAttr ".phl[336]" 0;
	setAttr ".phl[337]" 0;
	setAttr ".phl[338]" 0;
	setAttr ".phl[339]" 0;
	setAttr ".phl[340]" 0;
	setAttr ".phl[341]" 0;
	setAttr ".phl[342]" 0;
	setAttr ".phl[343]" 0;
	setAttr ".phl[344]" 0;
	setAttr ".phl[345]" 0;
	setAttr ".phl[346]" 0;
	setAttr ".phl[347]" 0;
	setAttr ".phl[348]" 0;
	setAttr ".phl[349]" 0;
	setAttr ".phl[350]" 0;
	setAttr ".phl[351]" 0;
	setAttr ".phl[352]" 0;
	setAttr ".phl[353]" 0;
	setAttr ".phl[354]" 0;
	setAttr ".phl[355]" 0;
	setAttr ".phl[356]" 0;
	setAttr ".phl[357]" 0;
	setAttr ".phl[358]" 0;
	setAttr ".phl[359]" 0;
	setAttr ".phl[360]" 0;
	setAttr ".phl[361]" 0;
	setAttr ".phl[362]" 0;
	setAttr ".phl[363]" 0;
	setAttr ".phl[364]" 0;
	setAttr ".phl[365]" 0;
	setAttr ".phl[366]" 0;
	setAttr ".phl[367]" 0;
	setAttr ".phl[368]" 0;
	setAttr ".phl[369]" 0;
	setAttr ".phl[370]" 0;
	setAttr ".phl[371]" 0;
	setAttr ".phl[372]" 0;
	setAttr ".phl[373]" 0;
	setAttr ".phl[374]" 0;
	setAttr ".phl[375]" 0;
	setAttr ".phl[376]" 0;
	setAttr ".phl[377]" 0;
	setAttr ".phl[378]" 0;
	setAttr ".phl[379]" 0;
	setAttr ".phl[380]" 0;
	setAttr ".phl[381]" 0;
	setAttr ".phl[382]" 0;
	setAttr ".phl[383]" 0;
	setAttr ".phl[384]" 0;
	setAttr ".phl[385]" 0;
	setAttr ".phl[386]" 0;
	setAttr ".phl[387]" 0;
	setAttr ".phl[388]" 0;
	setAttr ".phl[389]" 0;
	setAttr ".phl[390]" 0;
	setAttr ".phl[391]" 0;
	setAttr ".phl[392]" 0;
	setAttr ".phl[393]" 0;
	setAttr ".phl[394]" 0;
	setAttr ".phl[395]" 0;
	setAttr ".phl[396]" 0;
	setAttr ".phl[397]" 0;
	setAttr ".phl[398]" 0;
	setAttr ".phl[399]" 0;
	setAttr ".phl[400]" 0;
	setAttr ".phl[401]" 0;
	setAttr ".phl[402]" 0;
	setAttr ".phl[403]" 0;
	setAttr ".phl[404]" 0;
	setAttr ".phl[405]" 0;
	setAttr ".phl[406]" 0;
	setAttr ".phl[407]" 0;
	setAttr ".phl[408]" 0;
	setAttr ".phl[409]" 0;
	setAttr ".phl[410]" 0;
	setAttr ".phl[411]" 0;
	setAttr ".phl[412]" 0;
	setAttr ".phl[413]" 0;
	setAttr ".phl[414]" 0;
	setAttr ".phl[415]" 0;
	setAttr ".phl[416]" 0;
	setAttr ".phl[417]" 0;
	setAttr ".phl[418]" 0;
	setAttr ".phl[419]" 0;
	setAttr ".phl[420]" 0;
	setAttr ".phl[421]" 0;
	setAttr ".phl[422]" 0;
	setAttr ".phl[423]" 0;
	setAttr ".phl[424]" 0;
	setAttr ".phl[425]" 0;
	setAttr ".phl[426]" 0;
	setAttr ".phl[427]" 0;
	setAttr ".phl[428]" 0;
	setAttr ".phl[429]" 0;
	setAttr ".phl[430]" 0;
	setAttr ".phl[431]" 0;
	setAttr ".phl[432]" 0;
	setAttr ".phl[433]" 0;
	setAttr ".phl[434]" 0;
	setAttr ".phl[435]" 0;
	setAttr ".phl[436]" 0;
	setAttr ".phl[437]" 0;
	setAttr ".phl[438]" 0;
	setAttr ".phl[439]" 0;
	setAttr ".phl[440]" 0;
	setAttr ".phl[441]" 0;
	setAttr ".phl[442]" 0;
	setAttr ".phl[443]" 0;
	setAttr ".phl[444]" 0;
	setAttr ".phl[445]" 0;
	setAttr ".phl[446]" 0;
	setAttr ".phl[447]" 0;
	setAttr ".phl[448]" 0;
	setAttr ".phl[449]" 0;
	setAttr ".phl[450]" 0;
	setAttr ".phl[451]" 0;
	setAttr ".phl[452]" 0;
	setAttr ".phl[453]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"Reindeer_SkinRN"
		"Reindeer_SkinRN" 0
		"ReindeerRN" 0
		"Reindeer_SkeletonRN" 0
		"Reindeer_SkinRN" 1
		2 "|ReindeerRNfosterParent1|RudolfShapeDeformed" "uvPivot" " -type \"double2\" 0.064219609834253788 0.73671886324882507"
		
		"ReindeerRN" 3
		5 4 "Reindeer_SkinRN" "|Ribon.drawOverride" "Reindeer_SkinRN.placeHolderList[451]" 
		""
		5 4 "Reindeer_SkinRN" "|Rudolf.drawOverride" "Reindeer_SkinRN.placeHolderList[452]" 
		""
		5 4 "Reindeer_SkinRN" "|Satle.drawOverride" "Reindeer_SkinRN.placeHolderList[453]" 
		""
		"Reindeer_SkeletonRN" 540
		0 "|Reindeer_SkinRNfosterParent1|l_pastern_pointConstraint1" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_pastern_orientConstraint1" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_pastern_scaleConstraint1" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_metatarsal_pointConstraint1" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_metatarsal_orientConstraint1" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_metatarsal_scaleConstraint1" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_tibia_pointConstraint1" "|ref_frame|pelvis|hips|l_femur|l_tibia" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_tibia_orientConstraint1" "|ref_frame|pelvis|hips|l_femur|l_tibia" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_tibia_scaleConstraint1" "|ref_frame|pelvis|hips|l_femur|l_tibia" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_femur_pointConstraint1" "|ref_frame|pelvis|hips|l_femur" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_femur_orientConstraint1" "|ref_frame|pelvis|hips|l_femur" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_femur_scaleConstraint1" "|ref_frame|pelvis|hips|l_femur" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_fnt_pastern_pointConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_fnt_pastern_orientConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_fnt_pastern_scaleConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_fnt_metacarpal_pointConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_fnt_metacarpal_orientConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_fnt_metacarpal_scaleConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_fnt_humerus_pointConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_fnt_humerus_orientConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|l_fnt_humerus_scaleConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_fnt_pastern_pointConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_fnt_pastern_orientConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_fnt_pastern_scaleConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_fnt_metacarpal_pointConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_fnt_metacarpal_orientConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_fnt_metacarpal_scaleConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_fnt_humerus_pointConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_fnt_humerus_orientConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_fnt_humerus_scaleConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|head_pointConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|head_orientConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|head_scaleConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|neck_pointConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|neck_orientConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|neck_scaleConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|neckBase_pointConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|neckBase_orientConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|neckBase_scaleConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|thorax_pointConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|thorax_orientConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|thorax_scaleConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|spine_05_pointConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|spine_05_orientConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|spine_05_scaleConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|spine_04_pointConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|spine_04_orientConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|spine_04_scaleConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|spine_03_pointConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|spine_03_orientConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|spine_03_scaleConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|spine_02_pointConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|spine_02_orientConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|spine_02_scaleConstraint1" "|ref_frame|pelvis|hips|spine_01|spine_02" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|spine_01_pointConstraint1" "|ref_frame|pelvis|hips|spine_01" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|spine_01_orientConstraint1" "|ref_frame|pelvis|hips|spine_01" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|spine_01_scaleConstraint1" "|ref_frame|pelvis|hips|spine_01" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_pastern_pointConstraint1" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_pastern_orientConstraint1" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_pastern_scaleConstraint1" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_metatarsal_pointConstraint1" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_metatarsal_orientConstraint1" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_metatarsal_scaleConstraint1" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_tibia_pointConstraint1" "|ref_frame|pelvis|hips|r_femur|r_tibia" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_tibia_orientConstraint1" "|ref_frame|pelvis|hips|r_femur|r_tibia" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_tibia_scaleConstraint1" "|ref_frame|pelvis|hips|r_femur|r_tibia" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_femur_pointConstraint1" "|ref_frame|pelvis|hips|r_femur" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_femur_orientConstraint1" "|ref_frame|pelvis|hips|r_femur" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|r_femur_scaleConstraint1" "|ref_frame|pelvis|hips|r_femur" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|hips_pointConstraint1" "|ref_frame|pelvis|hips" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|hips_orientConstraint1" "|ref_frame|pelvis|hips" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|hips_scaleConstraint1" "|ref_frame|pelvis|hips" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|pelvis_pointConstraint1" "|ref_frame|pelvis" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|pelvis_orientConstraint1" "|ref_frame|pelvis" 
		"-s -r "
		0 "|Reindeer_SkinRNfosterParent1|pelvis_scaleConstraint1" "|ref_frame|pelvis" 
		"-s -r "
		2 "|ref_frame" "visibility" " 0"
		2 "|ref_frame" "segmentScaleCompensate" " 1"
		2 "|ref_frame|pelvis|hips|l_femur" "rotate" " -type \"double3\" 2.2787314097468032 -0.72625781922395205 -0.020612191206207775"
		
		2 "|ref_frame|pelvis|hips|l_femur" "rotateZ" " -av"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"rotate" " -type \"double3\" -5.743609309077641 0.67224830247976264 0.044391480718972753"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"rotateY" " -av"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"rotateZ" " -av"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"rotateZ" " -av"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"rotateX" " -av"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"rotateY" " -av"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"rotate" " -type \"double3\" 180 -180 180"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"rotateY" " -av"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"rotateX" " -av"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"rotateZ" " -av"
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis.inverseScale" "Reindeer_SkinRN.placeHolderList[1]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis.scaleX" "Reindeer_SkinRN.placeHolderList[2]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis.scaleY" "Reindeer_SkinRN.placeHolderList[3]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis.scaleZ" "Reindeer_SkinRN.placeHolderList[4]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis.translateX" "Reindeer_SkinRN.placeHolderList[5]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis.translateY" "Reindeer_SkinRN.placeHolderList[6]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis.translateZ" "Reindeer_SkinRN.placeHolderList[7]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis.parentInverseMatrix" "Reindeer_SkinRN.placeHolderList[8]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis.parentInverseMatrix" "Reindeer_SkinRN.placeHolderList[9]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis.parentInverseMatrix" "Reindeer_SkinRN.placeHolderList[10]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis.rotatePivot" "Reindeer_SkinRN.placeHolderList[11]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis.rotatePivotTranslate" "Reindeer_SkinRN.placeHolderList[12]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis.rotateX" "Reindeer_SkinRN.placeHolderList[13]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis.rotateY" "Reindeer_SkinRN.placeHolderList[14]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis.rotateZ" "Reindeer_SkinRN.placeHolderList[15]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis.rotateOrder" "Reindeer_SkinRN.placeHolderList[16]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis.jointOrient" "Reindeer_SkinRN.placeHolderList[17]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis.segmentScaleCompensate" "Reindeer_SkinRN.placeHolderList[18]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.inverseScale" "Reindeer_SkinRN.placeHolderList[19]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.scaleX" "Reindeer_SkinRN.placeHolderList[20]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.scaleY" "Reindeer_SkinRN.placeHolderList[21]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.scaleZ" "Reindeer_SkinRN.placeHolderList[22]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.translateX" "Reindeer_SkinRN.placeHolderList[23]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.translateY" "Reindeer_SkinRN.placeHolderList[24]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.translateZ" "Reindeer_SkinRN.placeHolderList[25]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.parentInverseMatrix" "Reindeer_SkinRN.placeHolderList[26]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.parentInverseMatrix" "Reindeer_SkinRN.placeHolderList[27]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.parentInverseMatrix" "Reindeer_SkinRN.placeHolderList[28]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.rotatePivot" "Reindeer_SkinRN.placeHolderList[29]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[30]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.rotateX" "Reindeer_SkinRN.placeHolderList[31]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.rotateY" "Reindeer_SkinRN.placeHolderList[32]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.rotateZ" "Reindeer_SkinRN.placeHolderList[33]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.rotateOrder" "Reindeer_SkinRN.placeHolderList[34]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.jointOrient" "Reindeer_SkinRN.placeHolderList[35]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[36]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[37]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.scaleX" "Reindeer_SkinRN.placeHolderList[38]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.scaleY" "Reindeer_SkinRN.placeHolderList[39]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.scaleZ" "Reindeer_SkinRN.placeHolderList[40]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.rotateZ" "Reindeer_SkinRN.placeHolderList[41]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.rotateX" "Reindeer_SkinRN.placeHolderList[42]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.rotateY" "Reindeer_SkinRN.placeHolderList[43]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.translateX" "Reindeer_SkinRN.placeHolderList[44]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.translateY" "Reindeer_SkinRN.placeHolderList[45]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.translateZ" "Reindeer_SkinRN.placeHolderList[46]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[47]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[48]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[49]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.rotatePivot" "Reindeer_SkinRN.placeHolderList[50]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[51]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.rotateOrder" "Reindeer_SkinRN.placeHolderList[52]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.jointOrient" "Reindeer_SkinRN.placeHolderList[53]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[54]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[55]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.scaleX" 
		"Reindeer_SkinRN.placeHolderList[56]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.scaleY" 
		"Reindeer_SkinRN.placeHolderList[57]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[58]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.translateX" 
		"Reindeer_SkinRN.placeHolderList[59]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.translateY" 
		"Reindeer_SkinRN.placeHolderList[60]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.translateZ" 
		"Reindeer_SkinRN.placeHolderList[61]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[62]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[63]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[64]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[65]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[66]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.rotateX" 
		"Reindeer_SkinRN.placeHolderList[67]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.rotateY" 
		"Reindeer_SkinRN.placeHolderList[68]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[69]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[70]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[71]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[72]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[73]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.scaleX" 
		"Reindeer_SkinRN.placeHolderList[74]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.scaleY" 
		"Reindeer_SkinRN.placeHolderList[75]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[76]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.translateX" 
		"Reindeer_SkinRN.placeHolderList[77]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.translateY" 
		"Reindeer_SkinRN.placeHolderList[78]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.translateZ" 
		"Reindeer_SkinRN.placeHolderList[79]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[80]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[81]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[82]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[83]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[84]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.rotateX" 
		"Reindeer_SkinRN.placeHolderList[85]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.rotateY" 
		"Reindeer_SkinRN.placeHolderList[86]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[87]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[88]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[89]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[90]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[91]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.translateX" 
		"Reindeer_SkinRN.placeHolderList[92]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.translateY" 
		"Reindeer_SkinRN.placeHolderList[93]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.translateZ" 
		"Reindeer_SkinRN.placeHolderList[94]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[95]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[96]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[97]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[98]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[99]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.rotateX" 
		"Reindeer_SkinRN.placeHolderList[100]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.rotateY" 
		"Reindeer_SkinRN.placeHolderList[101]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[102]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[103]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[104]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.scaleX" 
		"Reindeer_SkinRN.placeHolderList[105]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.scaleY" 
		"Reindeer_SkinRN.placeHolderList[106]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[107]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[108]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[109]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.scaleX" "Reindeer_SkinRN.placeHolderList[110]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.scaleY" "Reindeer_SkinRN.placeHolderList[111]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.scaleZ" "Reindeer_SkinRN.placeHolderList[112]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.translateX" "Reindeer_SkinRN.placeHolderList[113]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.translateY" "Reindeer_SkinRN.placeHolderList[114]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.translateZ" "Reindeer_SkinRN.placeHolderList[115]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[116]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[117]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[118]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[119]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[120]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.rotateX" "Reindeer_SkinRN.placeHolderList[121]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.rotateY" "Reindeer_SkinRN.placeHolderList[122]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.rotateZ" "Reindeer_SkinRN.placeHolderList[123]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[124]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[125]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[126]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[127]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.scaleX" 
		"Reindeer_SkinRN.placeHolderList[128]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.scaleY" 
		"Reindeer_SkinRN.placeHolderList[129]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[130]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.translateX" 
		"Reindeer_SkinRN.placeHolderList[131]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.translateY" 
		"Reindeer_SkinRN.placeHolderList[132]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.translateZ" 
		"Reindeer_SkinRN.placeHolderList[133]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[134]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[135]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[136]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[137]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[138]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.rotateX" 
		"Reindeer_SkinRN.placeHolderList[139]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.rotateY" 
		"Reindeer_SkinRN.placeHolderList[140]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[141]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[142]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[143]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[144]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[145]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.scaleX" 
		"Reindeer_SkinRN.placeHolderList[146]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.scaleY" 
		"Reindeer_SkinRN.placeHolderList[147]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[148]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.translateX" 
		"Reindeer_SkinRN.placeHolderList[149]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.translateY" 
		"Reindeer_SkinRN.placeHolderList[150]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.translateZ" 
		"Reindeer_SkinRN.placeHolderList[151]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[152]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[153]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[154]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[155]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[156]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.rotateX" 
		"Reindeer_SkinRN.placeHolderList[157]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.rotateY" 
		"Reindeer_SkinRN.placeHolderList[158]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[159]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[160]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[161]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[162]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[163]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.scaleX" 
		"Reindeer_SkinRN.placeHolderList[164]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.scaleY" 
		"Reindeer_SkinRN.placeHolderList[165]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[166]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.translateX" 
		"Reindeer_SkinRN.placeHolderList[167]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.translateY" 
		"Reindeer_SkinRN.placeHolderList[168]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.translateZ" 
		"Reindeer_SkinRN.placeHolderList[169]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[170]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[171]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[172]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[173]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[174]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.rotateX" 
		"Reindeer_SkinRN.placeHolderList[175]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.rotateY" 
		"Reindeer_SkinRN.placeHolderList[176]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[177]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[178]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[179]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[180]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[181]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.scaleX" 
		"Reindeer_SkinRN.placeHolderList[182]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.scaleY" 
		"Reindeer_SkinRN.placeHolderList[183]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[184]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.translateX" 
		"Reindeer_SkinRN.placeHolderList[185]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.translateY" 
		"Reindeer_SkinRN.placeHolderList[186]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.translateZ" 
		"Reindeer_SkinRN.placeHolderList[187]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[188]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[189]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[190]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[191]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[192]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.rotateX" 
		"Reindeer_SkinRN.placeHolderList[193]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.rotateY" 
		"Reindeer_SkinRN.placeHolderList[194]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[195]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[196]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[197]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[198]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[199]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.scaleX" 
		"Reindeer_SkinRN.placeHolderList[200]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.scaleY" 
		"Reindeer_SkinRN.placeHolderList[201]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[202]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.translateX" 
		"Reindeer_SkinRN.placeHolderList[203]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.translateY" 
		"Reindeer_SkinRN.placeHolderList[204]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.translateZ" 
		"Reindeer_SkinRN.placeHolderList[205]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[206]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[207]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[208]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[209]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[210]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.rotateX" 
		"Reindeer_SkinRN.placeHolderList[211]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.rotateY" 
		"Reindeer_SkinRN.placeHolderList[212]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[213]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[214]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[215]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[216]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[217]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.scaleX" 
		"Reindeer_SkinRN.placeHolderList[218]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.scaleY" 
		"Reindeer_SkinRN.placeHolderList[219]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[220]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.rotateY" 
		"Reindeer_SkinRN.placeHolderList[221]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[222]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.rotateX" 
		"Reindeer_SkinRN.placeHolderList[223]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.translateX" 
		"Reindeer_SkinRN.placeHolderList[224]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.translateY" 
		"Reindeer_SkinRN.placeHolderList[225]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.translateZ" 
		"Reindeer_SkinRN.placeHolderList[226]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[227]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[228]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[229]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[230]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[231]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[232]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[233]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[234]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[235]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.scaleX" 
		"Reindeer_SkinRN.placeHolderList[236]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.scaleY" 
		"Reindeer_SkinRN.placeHolderList[237]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[238]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.translateX" 
		"Reindeer_SkinRN.placeHolderList[239]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.translateY" 
		"Reindeer_SkinRN.placeHolderList[240]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.translateZ" 
		"Reindeer_SkinRN.placeHolderList[241]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[242]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[243]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[244]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[245]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[246]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.rotateX" 
		"Reindeer_SkinRN.placeHolderList[247]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.rotateY" 
		"Reindeer_SkinRN.placeHolderList[248]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[249]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[250]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[251]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[252]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[253]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.translateX" 
		"Reindeer_SkinRN.placeHolderList[254]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.translateY" 
		"Reindeer_SkinRN.placeHolderList[255]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.translateZ" 
		"Reindeer_SkinRN.placeHolderList[256]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[257]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[258]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[259]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[260]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[261]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.rotateX" 
		"Reindeer_SkinRN.placeHolderList[262]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.rotateY" 
		"Reindeer_SkinRN.placeHolderList[263]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[264]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[265]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[266]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.scaleX" 
		"Reindeer_SkinRN.placeHolderList[267]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.scaleY" 
		"Reindeer_SkinRN.placeHolderList[268]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[269]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[270]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[271]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.scaleX" 
		"Reindeer_SkinRN.placeHolderList[272]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.scaleY" 
		"Reindeer_SkinRN.placeHolderList[273]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[274]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.translateX" 
		"Reindeer_SkinRN.placeHolderList[275]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.translateY" 
		"Reindeer_SkinRN.placeHolderList[276]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.translateZ" 
		"Reindeer_SkinRN.placeHolderList[277]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[278]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[279]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[280]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[281]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[282]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.rotateX" 
		"Reindeer_SkinRN.placeHolderList[283]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.rotateY" 
		"Reindeer_SkinRN.placeHolderList[284]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[285]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[286]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[287]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[288]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[289]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.scaleX" 
		"Reindeer_SkinRN.placeHolderList[290]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.scaleY" 
		"Reindeer_SkinRN.placeHolderList[291]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[292]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.translateX" 
		"Reindeer_SkinRN.placeHolderList[293]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.translateY" 
		"Reindeer_SkinRN.placeHolderList[294]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.translateZ" 
		"Reindeer_SkinRN.placeHolderList[295]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[296]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[297]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[298]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[299]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[300]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.rotateX" 
		"Reindeer_SkinRN.placeHolderList[301]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.rotateY" 
		"Reindeer_SkinRN.placeHolderList[302]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[303]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[304]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[305]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[306]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[307]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.translateX" 
		"Reindeer_SkinRN.placeHolderList[308]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.translateY" 
		"Reindeer_SkinRN.placeHolderList[309]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.translateZ" 
		"Reindeer_SkinRN.placeHolderList[310]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[311]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[312]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[313]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[314]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[315]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.rotateX" 
		"Reindeer_SkinRN.placeHolderList[316]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.rotateY" 
		"Reindeer_SkinRN.placeHolderList[317]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[318]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[319]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[320]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.scaleX" 
		"Reindeer_SkinRN.placeHolderList[321]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.scaleY" 
		"Reindeer_SkinRN.placeHolderList[322]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[323]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[324]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[325]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.scaleX" 
		"Reindeer_SkinRN.placeHolderList[326]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.scaleY" 
		"Reindeer_SkinRN.placeHolderList[327]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[328]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[329]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.rotateX" 
		"Reindeer_SkinRN.placeHolderList[330]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.rotateY" 
		"Reindeer_SkinRN.placeHolderList[331]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.translateX" 
		"Reindeer_SkinRN.placeHolderList[332]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.translateY" 
		"Reindeer_SkinRN.placeHolderList[333]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.translateZ" 
		"Reindeer_SkinRN.placeHolderList[334]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[335]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[336]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[337]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[338]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[339]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[340]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[341]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[342]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[343]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.scaleX" 
		"Reindeer_SkinRN.placeHolderList[344]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.scaleY" 
		"Reindeer_SkinRN.placeHolderList[345]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[346]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.rotateY" 
		"Reindeer_SkinRN.placeHolderList[347]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.rotateX" 
		"Reindeer_SkinRN.placeHolderList[348]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[349]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.translateX" 
		"Reindeer_SkinRN.placeHolderList[350]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.translateY" 
		"Reindeer_SkinRN.placeHolderList[351]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.translateZ" 
		"Reindeer_SkinRN.placeHolderList[352]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[353]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[354]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[355]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[356]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[357]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[358]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[359]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[360]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[361]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.translateX" 
		"Reindeer_SkinRN.placeHolderList[362]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.translateY" 
		"Reindeer_SkinRN.placeHolderList[363]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.translateZ" 
		"Reindeer_SkinRN.placeHolderList[364]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[365]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[366]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[367]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[368]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[369]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.rotateX" 
		"Reindeer_SkinRN.placeHolderList[370]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.rotateY" 
		"Reindeer_SkinRN.placeHolderList[371]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[372]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[373]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[374]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.scaleX" 
		"Reindeer_SkinRN.placeHolderList[375]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.scaleY" 
		"Reindeer_SkinRN.placeHolderList[376]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[377]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[378]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[379]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.scaleX" "Reindeer_SkinRN.placeHolderList[380]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.scaleY" "Reindeer_SkinRN.placeHolderList[381]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.scaleZ" "Reindeer_SkinRN.placeHolderList[382]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.translateX" "Reindeer_SkinRN.placeHolderList[383]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.translateY" "Reindeer_SkinRN.placeHolderList[384]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.translateZ" "Reindeer_SkinRN.placeHolderList[385]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[386]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[387]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[388]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.rotatePivot" "Reindeer_SkinRN.placeHolderList[389]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[390]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.rotateX" "Reindeer_SkinRN.placeHolderList[391]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.rotateY" "Reindeer_SkinRN.placeHolderList[392]" 
		""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.rotateZ" "Reindeer_SkinRN.placeHolderList[393]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.rotateOrder" "Reindeer_SkinRN.placeHolderList[394]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.jointOrient" "Reindeer_SkinRN.placeHolderList[395]" 
		""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[396]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[397]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.scaleX" 
		"Reindeer_SkinRN.placeHolderList[398]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.scaleY" 
		"Reindeer_SkinRN.placeHolderList[399]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[400]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.translateX" 
		"Reindeer_SkinRN.placeHolderList[401]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.translateY" 
		"Reindeer_SkinRN.placeHolderList[402]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.translateZ" 
		"Reindeer_SkinRN.placeHolderList[403]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[404]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[405]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[406]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[407]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[408]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.rotateX" 
		"Reindeer_SkinRN.placeHolderList[409]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.rotateY" 
		"Reindeer_SkinRN.placeHolderList[410]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[411]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[412]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[413]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[414]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[415]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.scaleX" 
		"Reindeer_SkinRN.placeHolderList[416]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.scaleY" 
		"Reindeer_SkinRN.placeHolderList[417]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[418]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.translateX" 
		"Reindeer_SkinRN.placeHolderList[419]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.translateY" 
		"Reindeer_SkinRN.placeHolderList[420]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.translateZ" 
		"Reindeer_SkinRN.placeHolderList[421]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[422]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[423]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[424]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[425]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[426]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.rotateX" 
		"Reindeer_SkinRN.placeHolderList[427]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.rotateY" 
		"Reindeer_SkinRN.placeHolderList[428]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[429]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[430]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[431]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[432]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.inverseScale" 
		"Reindeer_SkinRN.placeHolderList[433]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.translateX" 
		"Reindeer_SkinRN.placeHolderList[434]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.translateY" 
		"Reindeer_SkinRN.placeHolderList[435]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.translateZ" 
		"Reindeer_SkinRN.placeHolderList[436]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[437]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[438]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.parentInverseMatrix" 
		"Reindeer_SkinRN.placeHolderList[439]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.rotatePivot" 
		"Reindeer_SkinRN.placeHolderList[440]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.rotatePivotTranslate" 
		"Reindeer_SkinRN.placeHolderList[441]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.rotateX" 
		"Reindeer_SkinRN.placeHolderList[442]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.rotateY" 
		"Reindeer_SkinRN.placeHolderList[443]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.rotateZ" 
		"Reindeer_SkinRN.placeHolderList[444]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.rotateOrder" 
		"Reindeer_SkinRN.placeHolderList[445]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.jointOrient" 
		"Reindeer_SkinRN.placeHolderList[446]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.scaleX" 
		"Reindeer_SkinRN.placeHolderList[447]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.scaleY" 
		"Reindeer_SkinRN.placeHolderList[448]" ""
		5 4 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.scaleZ" 
		"Reindeer_SkinRN.placeHolderList[449]" ""
		5 3 "Reindeer_SkinRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.segmentScaleCompensate" 
		"Reindeer_SkinRN.placeHolderList[450]" "";
lockNode -l 1 ;
createNode script -n "uiConfigurationScriptNode4";
	rename -uid "BE34805A-4AF7-CF98-4996-E8AD7884CF8B";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1211\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1211\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n"
		+ "                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n"
		+ "                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n"
		+ "                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n"
		+ "                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n"
		+ "                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 20 100 -ps 2 80 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1211\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1211\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode4";
	rename -uid "A0E964C4-42F8-43E8-EC94-1194E988A5EC";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode displayLayer -n "ReindeerMeshLayer";
	rename -uid "97C62586-469F-EA5C-32DB-15A85585C620";
	setAttr ".dt" 2;
	setAttr ".do" 1;
createNode blendShape -n "reindeer__flexi_bsShps_surface_01";
	rename -uid "A410B57E-48BB-E43A-F810-EDB5C4016A4E";
	addAttr -ci true -h true -sn "aal" -ln "attributeAliasList" -dt "attributeAlias";
	setAttr ".w[0]"  1;
	setAttr ".aal" -type "attributeAlias" {"reindeer__flexi_surface_bsShps_01","weight[0]"
		} ;
createNode tweak -n "tweak4";
	rename -uid "BD1E0F31-4965-5946-9621-669A4FEA12CE";
createNode objectSet -n "reindeer__flexi_bsShps_surface_01Set";
	rename -uid "CAB0C999-42CF-D539-B1F9-E4B756E534D9";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "reindeer__flexi_bsShps_surface_01GroupId";
	rename -uid "A52E9724-4BAC-B1C5-D262-00B46E30AB62";
	setAttr ".ihi" 0;
createNode groupParts -n "reindeer__flexi_bsShps_surface_01GroupParts";
	rename -uid "6F838BDB-431E-9FEB-EE19-A9B5C4FDEA65";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*][*]";
createNode objectSet -n "tweakSet4";
	rename -uid "B46FA468-4B40-0C62-CE9E-6F9F7B89E89E";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8";
	rename -uid "F19FCFE5-4DFA-1410-E431-E484B0C5CA30";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts8";
	rename -uid "12EBD46B-443F-6657-A37D-DD82251F637B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*][*]";
createNode nonLinear -n "reindeer__flexi_twist_06";
	rename -uid "A340C322-40F5-B19E-04EF-36A13822E413";
	addAttr -is true -ci true -k true -sn "sa" -ln "startAngle" -smn -15 -smx 15 -at "doubleAngle";
	addAttr -is true -ci true -k true -sn "ea" -ln "endAngle" -smn -15 -smx 15 -at "doubleAngle";
	addAttr -is true -ci true -k true -sn "lb" -ln "lowBound" -dv -1 -max 0 -smn -10 
		-smx 0 -at "double";
	addAttr -is true -ci true -k true -sn "hb" -ln "highBound" -dv 1 -min 0 -smn 0 -smx 
		10 -at "double";
	setAttr -k on ".sa";
	setAttr -k on ".ea";
	setAttr -k on ".lb";
	setAttr -k on ".hb";
createNode tweak -n "tweak5";
	rename -uid "9187F4D5-47C8-E33C-354B-77BE817EC5DA";
createNode objectSet -n "twist1Set";
	rename -uid "B16C5B1E-47DF-60D9-8465-90914C910F77";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "twist1GroupId";
	rename -uid "D8AC5361-4CC7-154D-8CB3-CAB2EE83382D";
	setAttr ".ihi" 0;
createNode groupParts -n "twist1GroupParts";
	rename -uid "534AC9D3-49D6-8620-6A93-1D83F6E5E3F8";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*][*]";
createNode objectSet -n "tweakSet5";
	rename -uid "87B9A394-4FAD-691F-B20F-09B4CC7C9943";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId10";
	rename -uid "F19F12B3-46DE-0B57-E93C-0AADC6A98BD9";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts10";
	rename -uid "A0005FCF-4B3C-A7CF-6035-E0914B6B89EB";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*][*]";
createNode wire -n "reindeer__flexi_wire_surface_01";
	rename -uid "5D35AB76-49AE-2FF3-2681-0C86667AA42F";
	setAttr ".ce" 1;
	setAttr ".dds[0]"  20;
	setAttr ".sc[0]"  1;
createNode objectSet -n "reindeer__flexi_wire_surface_01Set";
	rename -uid "80C8DE39-4071-454A-C44C-25B234A28D24";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "reindeer__flexi_wire_surface_01GroupId";
	rename -uid "893D6475-4CE2-049E-AA73-45856B0791AF";
	setAttr ".ihi" 0;
createNode groupParts -n "reindeer__flexi_wire_surface_01GroupParts";
	rename -uid "58A29934-438A-77E2-244A-30885218DE1F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*][*]";
createNode skinCluster -n "skinCluster4";
	rename -uid "690B0897-4B55-FDE9-9D85-3AA7BF9B7357";
	setAttr -s 5 ".wl";
	setAttr -s 3 ".wl[0].w[0:2]"  0.00031989221708774628 0.99966317839920726 
		1.692938370504508e-005;
	setAttr -s 3 ".wl[1].w[0:2]"  0.004098117821070231 0.99584263052006616 
		5.9251658863572674e-005;
	setAttr ".wl[2].w[0]"  1;
	setAttr -s 3 ".wl[3].w[0:2]"  0.004098117821070231 5.9251658863572674e-005 
		0.99584263052006616;
	setAttr -s 3 ".wl[4].w[0:2]"  0.00031989221708774628 1.692938370504508e-005 
		0.99966317839920726;
	setAttr -s 3 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -5 1;
	setAttr ".pm[1]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -4 0 -5 1;
	setAttr ".pm[2]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 4 0 -5 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 3 ".ma";
	setAttr -s 3 ".dpf[0:2]"  5 5 5;
	setAttr -s 3 ".lw";
	setAttr -s 3 ".lw";
	setAttr ".mi" 4;
	setAttr ".bm" 1;
	setAttr ".ucm" yes;
	setAttr -s 3 ".ifcl";
	setAttr -s 3 ".ifcl";
createNode tweak -n "tweak6";
	rename -uid "19781AFF-4C07-9EE6-3F5B-B7B94EC9B85C";
createNode objectSet -n "skinCluster4Set";
	rename -uid "8B0EB7FE-4A87-9668-28CF-D0B62D116299";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster4GroupId";
	rename -uid "7E784D24-4E02-D7B0-FBDF-8A8224D0DDA8";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster4GroupParts";
	rename -uid "D94BB1B2-4FBF-CCDD-30CA-E6BB19E13CC9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode objectSet -n "tweakSet6";
	rename -uid "B9DBAB47-4F63-63F7-4A82-98856ADE9F31";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId12";
	rename -uid "AFD46D4F-4BEE-5462-75FC-A688C4F61CB8";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts12";
	rename -uid "B4980327-4CEE-8441-8AF1-BDB9365CCEE4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "cv[*]";
createNode dagPose -n "bindPose2";
	rename -uid "BC66615D-42E1-379F-D9C6-99B0D704E319";
	setAttr -s 9 ".wm";
	setAttr ".wm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wm[1]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wm[2]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wm[3]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 5 1;
	setAttr ".wm[5]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 4 0 5 1;
	setAttr ".wm[7]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -4 0 5 1;
	setAttr -s 9 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 5 0 0
		 0 0 0 5 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 5 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 0 0 0 0 4 0 5 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 0 0 0 0 -4 0 5 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr -s 9 ".m";
	setAttr -s 9 ".p";
	setAttr -s 9 ".g[0:8]" yes yes yes yes no yes no yes no;
	setAttr ".bp" yes;
createNode plusMinusAverage -n "reindeer__flexi_pma_twist_end_mid01";
	rename -uid "1244315B-4CD6-2225-E835-3B9059A06F2C";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode plusMinusAverage -n "reindeer__flexi_pma_twist_end_all01";
	rename -uid "54032261-42D5-BE04-1726-36842684E09A";
	setAttr -s 3 ".i1";
	setAttr -s 3 ".i1";
createNode unitConversion -n "unitConversion1";
	rename -uid "44B5AB9D-4A96-6713-0E58-AA976F43B463";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion2";
	rename -uid "CF0D77E5-4F2E-19FD-4BC7-3584D96343F2";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion3";
	rename -uid "83D07B8B-49D8-92FC-F996-32876980A2E1";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion4";
	rename -uid "F7ECE66A-4BC3-E453-61B4-4CB80F3CFFC7";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion5";
	rename -uid "748D2053-42C5-AEB1-1DF6-D8A98DD03287";
	setAttr ".cf" 57.295779513082323;
createNode condition -n "reindeer__flexi_cond_twist_end01";
	rename -uid "47A45E25-46B3-3B72-B5EA-B5922D9D8911";
	setAttr ".st" 1;
createNode condition -n "reindeer__flexi_cond_twist_mid01";
	rename -uid "D1B960A6-4EB1-7D94-12E3-0FAC982A63FE";
	setAttr ".st" 1;
createNode unitConversion -n "unitConversion6";
	rename -uid "4DC42305-4479-414B-7DD9-EEA82A35B116";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion7";
	rename -uid "DA64CF35-4032-12F7-2B0A-09829E93CC15";
	setAttr ".cf" 0.017453292519943295;
createNode curveInfo -n "reindeer__flexiArcLen_node_01";
	rename -uid "B1C2F22C-4E04-69EB-F185-998748A62E2B";
createNode condition -n "reindeer__flexi_cond_volume01";
	rename -uid "93280228-4F49-7570-DDD6-919657DB68D4";
	setAttr ".st" 1;
createNode multiplyDivide -n "reindeer__flexi_div_volume01";
	rename -uid "985C8653-4F0D-A3B7-6C59-CE80ECD6F8B8";
	setAttr ".op" 2;
	setAttr ".i2" -type "float3" 10 1 1 ;
createNode multiplyDivide -n "reindeer__flexi_inverse_volume01";
	rename -uid "A5760FFB-4BCD-1857-E102-14B74FD47C42";
	setAttr ".i1" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "reindeer__flexi_pow_volume_a01";
	rename -uid "F5B5F7D0-4036-0C33-9DD9-64BE32464FB3";
	setAttr ".op" 3;
createNode multiplyDivide -n "reindeer__flexi_pow_volume_b01";
	rename -uid "C5C6A502-4C55-3F50-8776-88AB5939D9BB";
	setAttr ".op" 3;
createNode multiplyDivide -n "reindeer__flexi_pow_volume_c01";
	rename -uid "DD013154-44BA-C72D-1885-3E9591295B98";
	setAttr ".op" 3;
createNode multiplyDivide -n "reindeer__flexi_pow_volume_d01";
	rename -uid "3994DD7E-4936-D102-3806-C2A0AE00296D";
	setAttr ".op" 3;
createNode multiplyDivide -n "reindeer__flexi_pow_volume_e01";
	rename -uid "70DD52D8-43E0-4302-2FDC-8F9323DBA2F1";
	setAttr ".op" 3;
createNode ikRPsolver -n "ikRPsolver";
	rename -uid "8189A9AD-4F22-E19A-B76D-009CEC817445";
createNode ikSpringSolver -s -n "ikSpringSolver";
	rename -uid "95926BB3-4F22-015E-F487-1B9620114279";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 21 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 4 ".r";
select -ne :defaultTextureList1;
select -ne :initialShadingGroup;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
connectAttr "ReindeerMeshLayer.di" "Reindeer_SkinRN.phl[451]";
connectAttr "ReindeerMeshLayer.di" "Reindeer_SkinRN.phl[452]";
connectAttr "ReindeerMeshLayer.di" "Reindeer_SkinRN.phl[453]";
connectAttr "Reindeer_SkinRN.phl[1]" "pelvis_orientConstraint1.is";
connectAttr "pelvis_scaleConstraint1.csx" "Reindeer_SkinRN.phl[2]";
connectAttr "pelvis_scaleConstraint1.csy" "Reindeer_SkinRN.phl[3]";
connectAttr "pelvis_scaleConstraint1.csz" "Reindeer_SkinRN.phl[4]";
connectAttr "pelvis_pointConstraint1.ctx" "Reindeer_SkinRN.phl[5]";
connectAttr "pelvis_pointConstraint1.cty" "Reindeer_SkinRN.phl[6]";
connectAttr "pelvis_pointConstraint1.ctz" "Reindeer_SkinRN.phl[7]";
connectAttr "Reindeer_SkinRN.phl[8]" "pelvis_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[9]" "pelvis_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[10]" "pelvis_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[11]" "pelvis_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[12]" "pelvis_pointConstraint1.crt";
connectAttr "pelvis_orientConstraint1.crx" "Reindeer_SkinRN.phl[13]";
connectAttr "pelvis_orientConstraint1.cry" "Reindeer_SkinRN.phl[14]";
connectAttr "pelvis_orientConstraint1.crz" "Reindeer_SkinRN.phl[15]";
connectAttr "Reindeer_SkinRN.phl[16]" "pelvis_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[17]" "pelvis_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[18]" "pelvis_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[19]" "hips_orientConstraint1.is";
connectAttr "hips_scaleConstraint1.csx" "Reindeer_SkinRN.phl[20]";
connectAttr "hips_scaleConstraint1.csy" "Reindeer_SkinRN.phl[21]";
connectAttr "hips_scaleConstraint1.csz" "Reindeer_SkinRN.phl[22]";
connectAttr "hips_pointConstraint1.ctx" "Reindeer_SkinRN.phl[23]";
connectAttr "hips_pointConstraint1.cty" "Reindeer_SkinRN.phl[24]";
connectAttr "hips_pointConstraint1.ctz" "Reindeer_SkinRN.phl[25]";
connectAttr "Reindeer_SkinRN.phl[26]" "hips_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[27]" "hips_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[28]" "hips_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[29]" "hips_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[30]" "hips_pointConstraint1.crt";
connectAttr "hips_orientConstraint1.crx" "Reindeer_SkinRN.phl[31]";
connectAttr "hips_orientConstraint1.cry" "Reindeer_SkinRN.phl[32]";
connectAttr "hips_orientConstraint1.crz" "Reindeer_SkinRN.phl[33]";
connectAttr "Reindeer_SkinRN.phl[34]" "hips_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[35]" "hips_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[36]" "hips_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[37]" "l_femur_orientConstraint1.is";
connectAttr "l_femur_scaleConstraint1.csx" "Reindeer_SkinRN.phl[38]";
connectAttr "l_femur_scaleConstraint1.csy" "Reindeer_SkinRN.phl[39]";
connectAttr "l_femur_scaleConstraint1.csz" "Reindeer_SkinRN.phl[40]";
connectAttr "l_femur_orientConstraint1.crz" "Reindeer_SkinRN.phl[41]";
connectAttr "l_femur_orientConstraint1.crx" "Reindeer_SkinRN.phl[42]";
connectAttr "l_femur_orientConstraint1.cry" "Reindeer_SkinRN.phl[43]";
connectAttr "l_femur_pointConstraint1.ctx" "Reindeer_SkinRN.phl[44]";
connectAttr "l_femur_pointConstraint1.cty" "Reindeer_SkinRN.phl[45]";
connectAttr "l_femur_pointConstraint1.ctz" "Reindeer_SkinRN.phl[46]";
connectAttr "Reindeer_SkinRN.phl[47]" "l_femur_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[48]" "l_femur_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[49]" "l_femur_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[50]" "l_femur_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[51]" "l_femur_pointConstraint1.crt";
connectAttr "Reindeer_SkinRN.phl[52]" "l_femur_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[53]" "l_femur_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[54]" "l_femur_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[55]" "l_tibia_orientConstraint1.is";
connectAttr "l_tibia_scaleConstraint1.csx" "Reindeer_SkinRN.phl[56]";
connectAttr "l_tibia_scaleConstraint1.csy" "Reindeer_SkinRN.phl[57]";
connectAttr "l_tibia_scaleConstraint1.csz" "Reindeer_SkinRN.phl[58]";
connectAttr "l_tibia_pointConstraint1.ctx" "Reindeer_SkinRN.phl[59]";
connectAttr "l_tibia_pointConstraint1.cty" "Reindeer_SkinRN.phl[60]";
connectAttr "l_tibia_pointConstraint1.ctz" "Reindeer_SkinRN.phl[61]";
connectAttr "Reindeer_SkinRN.phl[62]" "l_tibia_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[63]" "l_tibia_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[64]" "l_tibia_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[65]" "l_tibia_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[66]" "l_tibia_pointConstraint1.crt";
connectAttr "l_tibia_orientConstraint1.crx" "Reindeer_SkinRN.phl[67]";
connectAttr "l_tibia_orientConstraint1.cry" "Reindeer_SkinRN.phl[68]";
connectAttr "l_tibia_orientConstraint1.crz" "Reindeer_SkinRN.phl[69]";
connectAttr "Reindeer_SkinRN.phl[70]" "l_tibia_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[71]" "l_tibia_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[72]" "l_tibia_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[73]" "l_metatarsal_orientConstraint1.is";
connectAttr "l_metatarsal_scaleConstraint1.csx" "Reindeer_SkinRN.phl[74]";
connectAttr "l_metatarsal_scaleConstraint1.csy" "Reindeer_SkinRN.phl[75]";
connectAttr "l_metatarsal_scaleConstraint1.csz" "Reindeer_SkinRN.phl[76]";
connectAttr "l_metatarsal_pointConstraint1.ctx" "Reindeer_SkinRN.phl[77]";
connectAttr "l_metatarsal_pointConstraint1.cty" "Reindeer_SkinRN.phl[78]";
connectAttr "l_metatarsal_pointConstraint1.ctz" "Reindeer_SkinRN.phl[79]";
connectAttr "Reindeer_SkinRN.phl[80]" "l_metatarsal_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[81]" "l_metatarsal_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[82]" "l_metatarsal_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[83]" "l_metatarsal_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[84]" "l_metatarsal_pointConstraint1.crt";
connectAttr "l_metatarsal_orientConstraint1.crx" "Reindeer_SkinRN.phl[85]";
connectAttr "l_metatarsal_orientConstraint1.cry" "Reindeer_SkinRN.phl[86]";
connectAttr "l_metatarsal_orientConstraint1.crz" "Reindeer_SkinRN.phl[87]";
connectAttr "Reindeer_SkinRN.phl[88]" "l_metatarsal_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[89]" "l_metatarsal_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[90]" "l_metatarsal_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[91]" "l_pastern_orientConstraint1.is";
connectAttr "l_pastern_pointConstraint1.ctx" "Reindeer_SkinRN.phl[92]";
connectAttr "l_pastern_pointConstraint1.cty" "Reindeer_SkinRN.phl[93]";
connectAttr "l_pastern_pointConstraint1.ctz" "Reindeer_SkinRN.phl[94]";
connectAttr "Reindeer_SkinRN.phl[95]" "l_pastern_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[96]" "l_pastern_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[97]" "l_pastern_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[98]" "l_pastern_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[99]" "l_pastern_pointConstraint1.crt";
connectAttr "l_pastern_orientConstraint1.crx" "Reindeer_SkinRN.phl[100]";
connectAttr "l_pastern_orientConstraint1.cry" "Reindeer_SkinRN.phl[101]";
connectAttr "l_pastern_orientConstraint1.crz" "Reindeer_SkinRN.phl[102]";
connectAttr "Reindeer_SkinRN.phl[103]" "l_pastern_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[104]" "l_pastern_orientConstraint1.cjo";
connectAttr "l_pastern_scaleConstraint1.csx" "Reindeer_SkinRN.phl[105]";
connectAttr "l_pastern_scaleConstraint1.csy" "Reindeer_SkinRN.phl[106]";
connectAttr "l_pastern_scaleConstraint1.csz" "Reindeer_SkinRN.phl[107]";
connectAttr "Reindeer_SkinRN.phl[108]" "l_pastern_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[109]" "spine_01_orientConstraint1.is";
connectAttr "spine_01_scaleConstraint1.csx" "Reindeer_SkinRN.phl[110]";
connectAttr "spine_01_scaleConstraint1.csy" "Reindeer_SkinRN.phl[111]";
connectAttr "spine_01_scaleConstraint1.csz" "Reindeer_SkinRN.phl[112]";
connectAttr "spine_01_pointConstraint1.ctx" "Reindeer_SkinRN.phl[113]";
connectAttr "spine_01_pointConstraint1.cty" "Reindeer_SkinRN.phl[114]";
connectAttr "spine_01_pointConstraint1.ctz" "Reindeer_SkinRN.phl[115]";
connectAttr "Reindeer_SkinRN.phl[116]" "spine_01_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[117]" "spine_01_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[118]" "spine_01_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[119]" "spine_01_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[120]" "spine_01_pointConstraint1.crt";
connectAttr "spine_01_orientConstraint1.crx" "Reindeer_SkinRN.phl[121]";
connectAttr "spine_01_orientConstraint1.cry" "Reindeer_SkinRN.phl[122]";
connectAttr "spine_01_orientConstraint1.crz" "Reindeer_SkinRN.phl[123]";
connectAttr "Reindeer_SkinRN.phl[124]" "spine_01_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[125]" "spine_01_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[126]" "spine_01_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[127]" "spine_02_orientConstraint1.is";
connectAttr "spine_02_scaleConstraint1.csx" "Reindeer_SkinRN.phl[128]";
connectAttr "spine_02_scaleConstraint1.csy" "Reindeer_SkinRN.phl[129]";
connectAttr "spine_02_scaleConstraint1.csz" "Reindeer_SkinRN.phl[130]";
connectAttr "spine_02_pointConstraint1.ctx" "Reindeer_SkinRN.phl[131]";
connectAttr "spine_02_pointConstraint1.cty" "Reindeer_SkinRN.phl[132]";
connectAttr "spine_02_pointConstraint1.ctz" "Reindeer_SkinRN.phl[133]";
connectAttr "Reindeer_SkinRN.phl[134]" "spine_02_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[135]" "spine_02_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[136]" "spine_02_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[137]" "spine_02_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[138]" "spine_02_pointConstraint1.crt";
connectAttr "spine_02_orientConstraint1.crx" "Reindeer_SkinRN.phl[139]";
connectAttr "spine_02_orientConstraint1.cry" "Reindeer_SkinRN.phl[140]";
connectAttr "spine_02_orientConstraint1.crz" "Reindeer_SkinRN.phl[141]";
connectAttr "Reindeer_SkinRN.phl[142]" "spine_02_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[143]" "spine_02_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[144]" "spine_02_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[145]" "spine_03_orientConstraint1.is";
connectAttr "spine_03_scaleConstraint1.csx" "Reindeer_SkinRN.phl[146]";
connectAttr "spine_03_scaleConstraint1.csy" "Reindeer_SkinRN.phl[147]";
connectAttr "spine_03_scaleConstraint1.csz" "Reindeer_SkinRN.phl[148]";
connectAttr "spine_03_pointConstraint1.ctx" "Reindeer_SkinRN.phl[149]";
connectAttr "spine_03_pointConstraint1.cty" "Reindeer_SkinRN.phl[150]";
connectAttr "spine_03_pointConstraint1.ctz" "Reindeer_SkinRN.phl[151]";
connectAttr "Reindeer_SkinRN.phl[152]" "spine_03_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[153]" "spine_03_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[154]" "spine_03_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[155]" "spine_03_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[156]" "spine_03_pointConstraint1.crt";
connectAttr "spine_03_orientConstraint1.crx" "Reindeer_SkinRN.phl[157]";
connectAttr "spine_03_orientConstraint1.cry" "Reindeer_SkinRN.phl[158]";
connectAttr "spine_03_orientConstraint1.crz" "Reindeer_SkinRN.phl[159]";
connectAttr "Reindeer_SkinRN.phl[160]" "spine_03_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[161]" "spine_03_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[162]" "spine_03_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[163]" "spine_04_orientConstraint1.is";
connectAttr "spine_04_scaleConstraint1.csx" "Reindeer_SkinRN.phl[164]";
connectAttr "spine_04_scaleConstraint1.csy" "Reindeer_SkinRN.phl[165]";
connectAttr "spine_04_scaleConstraint1.csz" "Reindeer_SkinRN.phl[166]";
connectAttr "spine_04_pointConstraint1.ctx" "Reindeer_SkinRN.phl[167]";
connectAttr "spine_04_pointConstraint1.cty" "Reindeer_SkinRN.phl[168]";
connectAttr "spine_04_pointConstraint1.ctz" "Reindeer_SkinRN.phl[169]";
connectAttr "Reindeer_SkinRN.phl[170]" "spine_04_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[171]" "spine_04_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[172]" "spine_04_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[173]" "spine_04_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[174]" "spine_04_pointConstraint1.crt";
connectAttr "spine_04_orientConstraint1.crx" "Reindeer_SkinRN.phl[175]";
connectAttr "spine_04_orientConstraint1.cry" "Reindeer_SkinRN.phl[176]";
connectAttr "spine_04_orientConstraint1.crz" "Reindeer_SkinRN.phl[177]";
connectAttr "Reindeer_SkinRN.phl[178]" "spine_04_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[179]" "spine_04_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[180]" "spine_04_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[181]" "spine_05_orientConstraint1.is";
connectAttr "spine_05_scaleConstraint1.csx" "Reindeer_SkinRN.phl[182]";
connectAttr "spine_05_scaleConstraint1.csy" "Reindeer_SkinRN.phl[183]";
connectAttr "spine_05_scaleConstraint1.csz" "Reindeer_SkinRN.phl[184]";
connectAttr "spine_05_pointConstraint1.ctx" "Reindeer_SkinRN.phl[185]";
connectAttr "spine_05_pointConstraint1.cty" "Reindeer_SkinRN.phl[186]";
connectAttr "spine_05_pointConstraint1.ctz" "Reindeer_SkinRN.phl[187]";
connectAttr "Reindeer_SkinRN.phl[188]" "spine_05_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[189]" "spine_05_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[190]" "spine_05_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[191]" "spine_05_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[192]" "spine_05_pointConstraint1.crt";
connectAttr "spine_05_orientConstraint1.crx" "Reindeer_SkinRN.phl[193]";
connectAttr "spine_05_orientConstraint1.cry" "Reindeer_SkinRN.phl[194]";
connectAttr "spine_05_orientConstraint1.crz" "Reindeer_SkinRN.phl[195]";
connectAttr "Reindeer_SkinRN.phl[196]" "spine_05_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[197]" "spine_05_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[198]" "spine_05_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[199]" "thorax_orientConstraint1.is";
connectAttr "thorax_scaleConstraint1.csx" "Reindeer_SkinRN.phl[200]";
connectAttr "thorax_scaleConstraint1.csy" "Reindeer_SkinRN.phl[201]";
connectAttr "thorax_scaleConstraint1.csz" "Reindeer_SkinRN.phl[202]";
connectAttr "thorax_pointConstraint1.ctx" "Reindeer_SkinRN.phl[203]";
connectAttr "thorax_pointConstraint1.cty" "Reindeer_SkinRN.phl[204]";
connectAttr "thorax_pointConstraint1.ctz" "Reindeer_SkinRN.phl[205]";
connectAttr "Reindeer_SkinRN.phl[206]" "thorax_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[207]" "thorax_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[208]" "thorax_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[209]" "thorax_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[210]" "thorax_pointConstraint1.crt";
connectAttr "thorax_orientConstraint1.crx" "Reindeer_SkinRN.phl[211]";
connectAttr "thorax_orientConstraint1.cry" "Reindeer_SkinRN.phl[212]";
connectAttr "thorax_orientConstraint1.crz" "Reindeer_SkinRN.phl[213]";
connectAttr "Reindeer_SkinRN.phl[214]" "thorax_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[215]" "thorax_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[216]" "thorax_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[217]" "l_fnt_humerus_orientConstraint1.is";
connectAttr "l_fnt_humerus_scaleConstraint1.csx" "Reindeer_SkinRN.phl[218]";
connectAttr "l_fnt_humerus_scaleConstraint1.csy" "Reindeer_SkinRN.phl[219]";
connectAttr "l_fnt_humerus_scaleConstraint1.csz" "Reindeer_SkinRN.phl[220]";
connectAttr "l_fnt_humerus_orientConstraint1.cry" "Reindeer_SkinRN.phl[221]";
connectAttr "l_fnt_humerus_orientConstraint1.crz" "Reindeer_SkinRN.phl[222]";
connectAttr "l_fnt_humerus_orientConstraint1.crx" "Reindeer_SkinRN.phl[223]";
connectAttr "l_fnt_humerus_pointConstraint1.ctx" "Reindeer_SkinRN.phl[224]";
connectAttr "l_fnt_humerus_pointConstraint1.cty" "Reindeer_SkinRN.phl[225]";
connectAttr "l_fnt_humerus_pointConstraint1.ctz" "Reindeer_SkinRN.phl[226]";
connectAttr "Reindeer_SkinRN.phl[227]" "l_fnt_humerus_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[228]" "l_fnt_humerus_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[229]" "l_fnt_humerus_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[230]" "l_fnt_humerus_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[231]" "l_fnt_humerus_pointConstraint1.crt";
connectAttr "Reindeer_SkinRN.phl[232]" "l_fnt_humerus_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[233]" "l_fnt_humerus_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[234]" "l_fnt_humerus_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[235]" "l_fnt_metacarpal_orientConstraint1.is";
connectAttr "l_fnt_metacarpal_scaleConstraint1.csx" "Reindeer_SkinRN.phl[236]";
connectAttr "l_fnt_metacarpal_scaleConstraint1.csy" "Reindeer_SkinRN.phl[237]";
connectAttr "l_fnt_metacarpal_scaleConstraint1.csz" "Reindeer_SkinRN.phl[238]";
connectAttr "l_fnt_metacarpal_pointConstraint1.ctx" "Reindeer_SkinRN.phl[239]";
connectAttr "l_fnt_metacarpal_pointConstraint1.cty" "Reindeer_SkinRN.phl[240]";
connectAttr "l_fnt_metacarpal_pointConstraint1.ctz" "Reindeer_SkinRN.phl[241]";
connectAttr "Reindeer_SkinRN.phl[242]" "l_fnt_metacarpal_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[243]" "l_fnt_metacarpal_orientConstraint1.cpim"
		;
connectAttr "Reindeer_SkinRN.phl[244]" "l_fnt_metacarpal_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[245]" "l_fnt_metacarpal_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[246]" "l_fnt_metacarpal_pointConstraint1.crt";
connectAttr "l_fnt_metacarpal_orientConstraint1.crx" "Reindeer_SkinRN.phl[247]";
connectAttr "l_fnt_metacarpal_orientConstraint1.cry" "Reindeer_SkinRN.phl[248]";
connectAttr "l_fnt_metacarpal_orientConstraint1.crz" "Reindeer_SkinRN.phl[249]";
connectAttr "Reindeer_SkinRN.phl[250]" "l_fnt_metacarpal_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[251]" "l_fnt_metacarpal_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[252]" "l_fnt_metacarpal_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[253]" "l_fnt_pastern_orientConstraint1.is";
connectAttr "l_fnt_pastern_pointConstraint1.ctx" "Reindeer_SkinRN.phl[254]";
connectAttr "l_fnt_pastern_pointConstraint1.cty" "Reindeer_SkinRN.phl[255]";
connectAttr "l_fnt_pastern_pointConstraint1.ctz" "Reindeer_SkinRN.phl[256]";
connectAttr "Reindeer_SkinRN.phl[257]" "l_fnt_pastern_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[258]" "l_fnt_pastern_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[259]" "l_fnt_pastern_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[260]" "l_fnt_pastern_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[261]" "l_fnt_pastern_pointConstraint1.crt";
connectAttr "l_fnt_pastern_orientConstraint1.crx" "Reindeer_SkinRN.phl[262]";
connectAttr "l_fnt_pastern_orientConstraint1.cry" "Reindeer_SkinRN.phl[263]";
connectAttr "l_fnt_pastern_orientConstraint1.crz" "Reindeer_SkinRN.phl[264]";
connectAttr "Reindeer_SkinRN.phl[265]" "l_fnt_pastern_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[266]" "l_fnt_pastern_orientConstraint1.cjo";
connectAttr "l_fnt_pastern_scaleConstraint1.csx" "Reindeer_SkinRN.phl[267]";
connectAttr "l_fnt_pastern_scaleConstraint1.csy" "Reindeer_SkinRN.phl[268]";
connectAttr "l_fnt_pastern_scaleConstraint1.csz" "Reindeer_SkinRN.phl[269]";
connectAttr "Reindeer_SkinRN.phl[270]" "l_fnt_pastern_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[271]" "r_fnt_humerus_orientConstraint1.is";
connectAttr "r_fnt_humerus_scaleConstraint1.csx" "Reindeer_SkinRN.phl[272]";
connectAttr "r_fnt_humerus_scaleConstraint1.csy" "Reindeer_SkinRN.phl[273]";
connectAttr "r_fnt_humerus_scaleConstraint1.csz" "Reindeer_SkinRN.phl[274]";
connectAttr "r_fnt_humerus_pointConstraint1.ctx" "Reindeer_SkinRN.phl[275]";
connectAttr "r_fnt_humerus_pointConstraint1.cty" "Reindeer_SkinRN.phl[276]";
connectAttr "r_fnt_humerus_pointConstraint1.ctz" "Reindeer_SkinRN.phl[277]";
connectAttr "Reindeer_SkinRN.phl[278]" "r_fnt_humerus_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[279]" "r_fnt_humerus_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[280]" "r_fnt_humerus_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[281]" "r_fnt_humerus_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[282]" "r_fnt_humerus_pointConstraint1.crt";
connectAttr "r_fnt_humerus_orientConstraint1.crx" "Reindeer_SkinRN.phl[283]";
connectAttr "r_fnt_humerus_orientConstraint1.cry" "Reindeer_SkinRN.phl[284]";
connectAttr "r_fnt_humerus_orientConstraint1.crz" "Reindeer_SkinRN.phl[285]";
connectAttr "Reindeer_SkinRN.phl[286]" "r_fnt_humerus_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[287]" "r_fnt_humerus_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[288]" "r_fnt_humerus_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[289]" "r_fnt_metacarpal_orientConstraint1.is";
connectAttr "r_fnt_metacarpal_scaleConstraint1.csx" "Reindeer_SkinRN.phl[290]";
connectAttr "r_fnt_metacarpal_scaleConstraint1.csy" "Reindeer_SkinRN.phl[291]";
connectAttr "r_fnt_metacarpal_scaleConstraint1.csz" "Reindeer_SkinRN.phl[292]";
connectAttr "r_fnt_metacarpal_pointConstraint1.ctx" "Reindeer_SkinRN.phl[293]";
connectAttr "r_fnt_metacarpal_pointConstraint1.cty" "Reindeer_SkinRN.phl[294]";
connectAttr "r_fnt_metacarpal_pointConstraint1.ctz" "Reindeer_SkinRN.phl[295]";
connectAttr "Reindeer_SkinRN.phl[296]" "r_fnt_metacarpal_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[297]" "r_fnt_metacarpal_orientConstraint1.cpim"
		;
connectAttr "Reindeer_SkinRN.phl[298]" "r_fnt_metacarpal_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[299]" "r_fnt_metacarpal_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[300]" "r_fnt_metacarpal_pointConstraint1.crt";
connectAttr "r_fnt_metacarpal_orientConstraint1.crx" "Reindeer_SkinRN.phl[301]";
connectAttr "r_fnt_metacarpal_orientConstraint1.cry" "Reindeer_SkinRN.phl[302]";
connectAttr "r_fnt_metacarpal_orientConstraint1.crz" "Reindeer_SkinRN.phl[303]";
connectAttr "Reindeer_SkinRN.phl[304]" "r_fnt_metacarpal_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[305]" "r_fnt_metacarpal_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[306]" "r_fnt_metacarpal_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[307]" "r_fnt_pastern_orientConstraint1.is";
connectAttr "r_fnt_pastern_pointConstraint1.ctx" "Reindeer_SkinRN.phl[308]";
connectAttr "r_fnt_pastern_pointConstraint1.cty" "Reindeer_SkinRN.phl[309]";
connectAttr "r_fnt_pastern_pointConstraint1.ctz" "Reindeer_SkinRN.phl[310]";
connectAttr "Reindeer_SkinRN.phl[311]" "r_fnt_pastern_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[312]" "r_fnt_pastern_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[313]" "r_fnt_pastern_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[314]" "r_fnt_pastern_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[315]" "r_fnt_pastern_pointConstraint1.crt";
connectAttr "r_fnt_pastern_orientConstraint1.crx" "Reindeer_SkinRN.phl[316]";
connectAttr "r_fnt_pastern_orientConstraint1.cry" "Reindeer_SkinRN.phl[317]";
connectAttr "r_fnt_pastern_orientConstraint1.crz" "Reindeer_SkinRN.phl[318]";
connectAttr "Reindeer_SkinRN.phl[319]" "r_fnt_pastern_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[320]" "r_fnt_pastern_orientConstraint1.cjo";
connectAttr "r_fnt_pastern_scaleConstraint1.csx" "Reindeer_SkinRN.phl[321]";
connectAttr "r_fnt_pastern_scaleConstraint1.csy" "Reindeer_SkinRN.phl[322]";
connectAttr "r_fnt_pastern_scaleConstraint1.csz" "Reindeer_SkinRN.phl[323]";
connectAttr "Reindeer_SkinRN.phl[324]" "r_fnt_pastern_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[325]" "neckBase_orientConstraint1.is";
connectAttr "neckBase_scaleConstraint1.csx" "Reindeer_SkinRN.phl[326]";
connectAttr "neckBase_scaleConstraint1.csy" "Reindeer_SkinRN.phl[327]";
connectAttr "neckBase_scaleConstraint1.csz" "Reindeer_SkinRN.phl[328]";
connectAttr "neckBase_orientConstraint1.crz" "Reindeer_SkinRN.phl[329]";
connectAttr "neckBase_orientConstraint1.crx" "Reindeer_SkinRN.phl[330]";
connectAttr "neckBase_orientConstraint1.cry" "Reindeer_SkinRN.phl[331]";
connectAttr "neckBase_pointConstraint1.ctx" "Reindeer_SkinRN.phl[332]";
connectAttr "neckBase_pointConstraint1.cty" "Reindeer_SkinRN.phl[333]";
connectAttr "neckBase_pointConstraint1.ctz" "Reindeer_SkinRN.phl[334]";
connectAttr "Reindeer_SkinRN.phl[335]" "neckBase_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[336]" "neckBase_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[337]" "neckBase_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[338]" "neckBase_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[339]" "neckBase_pointConstraint1.crt";
connectAttr "Reindeer_SkinRN.phl[340]" "neckBase_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[341]" "neckBase_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[342]" "neckBase_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[343]" "neck_orientConstraint1.is";
connectAttr "neck_scaleConstraint1.csx" "Reindeer_SkinRN.phl[344]";
connectAttr "neck_scaleConstraint1.csy" "Reindeer_SkinRN.phl[345]";
connectAttr "neck_scaleConstraint1.csz" "Reindeer_SkinRN.phl[346]";
connectAttr "neck_orientConstraint1.cry" "Reindeer_SkinRN.phl[347]";
connectAttr "neck_orientConstraint1.crx" "Reindeer_SkinRN.phl[348]";
connectAttr "neck_orientConstraint1.crz" "Reindeer_SkinRN.phl[349]";
connectAttr "neck_pointConstraint1.ctx" "Reindeer_SkinRN.phl[350]";
connectAttr "neck_pointConstraint1.cty" "Reindeer_SkinRN.phl[351]";
connectAttr "neck_pointConstraint1.ctz" "Reindeer_SkinRN.phl[352]";
connectAttr "Reindeer_SkinRN.phl[353]" "neck_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[354]" "neck_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[355]" "neck_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[356]" "neck_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[357]" "neck_pointConstraint1.crt";
connectAttr "Reindeer_SkinRN.phl[358]" "neck_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[359]" "neck_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[360]" "neck_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[361]" "head_orientConstraint1.is";
connectAttr "head_pointConstraint1.ctx" "Reindeer_SkinRN.phl[362]";
connectAttr "head_pointConstraint1.cty" "Reindeer_SkinRN.phl[363]";
connectAttr "head_pointConstraint1.ctz" "Reindeer_SkinRN.phl[364]";
connectAttr "Reindeer_SkinRN.phl[365]" "head_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[366]" "head_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[367]" "head_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[368]" "head_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[369]" "head_pointConstraint1.crt";
connectAttr "head_orientConstraint1.crx" "Reindeer_SkinRN.phl[370]";
connectAttr "head_orientConstraint1.cry" "Reindeer_SkinRN.phl[371]";
connectAttr "head_orientConstraint1.crz" "Reindeer_SkinRN.phl[372]";
connectAttr "Reindeer_SkinRN.phl[373]" "head_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[374]" "head_orientConstraint1.cjo";
connectAttr "head_scaleConstraint1.csx" "Reindeer_SkinRN.phl[375]";
connectAttr "head_scaleConstraint1.csy" "Reindeer_SkinRN.phl[376]";
connectAttr "head_scaleConstraint1.csz" "Reindeer_SkinRN.phl[377]";
connectAttr "Reindeer_SkinRN.phl[378]" "head_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[379]" "r_femur_orientConstraint1.is";
connectAttr "r_femur_scaleConstraint1.csx" "Reindeer_SkinRN.phl[380]";
connectAttr "r_femur_scaleConstraint1.csy" "Reindeer_SkinRN.phl[381]";
connectAttr "r_femur_scaleConstraint1.csz" "Reindeer_SkinRN.phl[382]";
connectAttr "r_femur_pointConstraint1.ctx" "Reindeer_SkinRN.phl[383]";
connectAttr "r_femur_pointConstraint1.cty" "Reindeer_SkinRN.phl[384]";
connectAttr "r_femur_pointConstraint1.ctz" "Reindeer_SkinRN.phl[385]";
connectAttr "Reindeer_SkinRN.phl[386]" "r_femur_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[387]" "r_femur_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[388]" "r_femur_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[389]" "r_femur_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[390]" "r_femur_pointConstraint1.crt";
connectAttr "r_femur_orientConstraint1.crx" "Reindeer_SkinRN.phl[391]";
connectAttr "r_femur_orientConstraint1.cry" "Reindeer_SkinRN.phl[392]";
connectAttr "r_femur_orientConstraint1.crz" "Reindeer_SkinRN.phl[393]";
connectAttr "Reindeer_SkinRN.phl[394]" "r_femur_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[395]" "r_femur_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[396]" "r_femur_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[397]" "r_tibia_orientConstraint1.is";
connectAttr "r_tibia_scaleConstraint1.csx" "Reindeer_SkinRN.phl[398]";
connectAttr "r_tibia_scaleConstraint1.csy" "Reindeer_SkinRN.phl[399]";
connectAttr "r_tibia_scaleConstraint1.csz" "Reindeer_SkinRN.phl[400]";
connectAttr "r_tibia_pointConstraint1.ctx" "Reindeer_SkinRN.phl[401]";
connectAttr "r_tibia_pointConstraint1.cty" "Reindeer_SkinRN.phl[402]";
connectAttr "r_tibia_pointConstraint1.ctz" "Reindeer_SkinRN.phl[403]";
connectAttr "Reindeer_SkinRN.phl[404]" "r_tibia_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[405]" "r_tibia_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[406]" "r_tibia_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[407]" "r_tibia_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[408]" "r_tibia_pointConstraint1.crt";
connectAttr "r_tibia_orientConstraint1.crx" "Reindeer_SkinRN.phl[409]";
connectAttr "r_tibia_orientConstraint1.cry" "Reindeer_SkinRN.phl[410]";
connectAttr "r_tibia_orientConstraint1.crz" "Reindeer_SkinRN.phl[411]";
connectAttr "Reindeer_SkinRN.phl[412]" "r_tibia_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[413]" "r_tibia_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[414]" "r_tibia_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[415]" "r_metatarsal_orientConstraint1.is";
connectAttr "r_metatarsal_scaleConstraint1.csx" "Reindeer_SkinRN.phl[416]";
connectAttr "r_metatarsal_scaleConstraint1.csy" "Reindeer_SkinRN.phl[417]";
connectAttr "r_metatarsal_scaleConstraint1.csz" "Reindeer_SkinRN.phl[418]";
connectAttr "r_metatarsal_pointConstraint1.ctx" "Reindeer_SkinRN.phl[419]";
connectAttr "r_metatarsal_pointConstraint1.cty" "Reindeer_SkinRN.phl[420]";
connectAttr "r_metatarsal_pointConstraint1.ctz" "Reindeer_SkinRN.phl[421]";
connectAttr "Reindeer_SkinRN.phl[422]" "r_metatarsal_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[423]" "r_metatarsal_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[424]" "r_metatarsal_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[425]" "r_metatarsal_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[426]" "r_metatarsal_pointConstraint1.crt";
connectAttr "r_metatarsal_orientConstraint1.crx" "Reindeer_SkinRN.phl[427]";
connectAttr "r_metatarsal_orientConstraint1.cry" "Reindeer_SkinRN.phl[428]";
connectAttr "r_metatarsal_orientConstraint1.crz" "Reindeer_SkinRN.phl[429]";
connectAttr "Reindeer_SkinRN.phl[430]" "r_metatarsal_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[431]" "r_metatarsal_orientConstraint1.cjo";
connectAttr "Reindeer_SkinRN.phl[432]" "r_metatarsal_scaleConstraint1.tsc";
connectAttr "Reindeer_SkinRN.phl[433]" "r_pastern_orientConstraint1.is";
connectAttr "r_pastern_pointConstraint1.ctx" "Reindeer_SkinRN.phl[434]";
connectAttr "r_pastern_pointConstraint1.cty" "Reindeer_SkinRN.phl[435]";
connectAttr "r_pastern_pointConstraint1.ctz" "Reindeer_SkinRN.phl[436]";
connectAttr "Reindeer_SkinRN.phl[437]" "r_pastern_pointConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[438]" "r_pastern_orientConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[439]" "r_pastern_scaleConstraint1.cpim";
connectAttr "Reindeer_SkinRN.phl[440]" "r_pastern_pointConstraint1.crp";
connectAttr "Reindeer_SkinRN.phl[441]" "r_pastern_pointConstraint1.crt";
connectAttr "r_pastern_orientConstraint1.crx" "Reindeer_SkinRN.phl[442]";
connectAttr "r_pastern_orientConstraint1.cry" "Reindeer_SkinRN.phl[443]";
connectAttr "r_pastern_orientConstraint1.crz" "Reindeer_SkinRN.phl[444]";
connectAttr "Reindeer_SkinRN.phl[445]" "r_pastern_orientConstraint1.cro";
connectAttr "Reindeer_SkinRN.phl[446]" "r_pastern_orientConstraint1.cjo";
connectAttr "r_pastern_scaleConstraint1.csx" "Reindeer_SkinRN.phl[447]";
connectAttr "r_pastern_scaleConstraint1.csy" "Reindeer_SkinRN.phl[448]";
connectAttr "r_pastern_scaleConstraint1.csz" "Reindeer_SkinRN.phl[449]";
connectAttr "Reindeer_SkinRN.phl[450]" "r_pastern_scaleConstraint1.tsc";
connectAttr "rig_pelvis_orientConstraint1.crx" "rig_pelvis.rx";
connectAttr "rig_pelvis_orientConstraint1.cry" "rig_pelvis.ry";
connectAttr "rig_pelvis_orientConstraint1.crz" "rig_pelvis.rz";
connectAttr "rig_pelvis_pointConstraint1.ctx" "rig_pelvis.tx";
connectAttr "rig_pelvis_pointConstraint1.cty" "rig_pelvis.ty";
connectAttr "rig_pelvis_pointConstraint1.ctz" "rig_pelvis.tz";
connectAttr "rig_pelvis.s" "rig_hips.is";
connectAttr "rig_hips_parentConstraint1.crx" "rig_hips.rx";
connectAttr "rig_hips_parentConstraint1.cry" "rig_hips.ry";
connectAttr "rig_hips_parentConstraint1.crz" "rig_hips.rz";
connectAttr "rig_hips_parentConstraint1.ctx" "rig_hips.tx";
connectAttr "rig_hips_parentConstraint1.cty" "rig_hips.ty";
connectAttr "rig_hips_parentConstraint1.ctz" "rig_hips.tz";
connectAttr "rig_hips.s" "rig_l_femur.is";
connectAttr "rig_l_femur.s" "rig_l_tibia.is";
connectAttr "rig_l_tibia.s" "rig_l_metatarsal.is";
connectAttr "rig_l_metatarsal.s" "rig_l_pastern.is";
connectAttr "rig_l_pastern_orientConstraint1.crx" "rig_l_pastern.rx";
connectAttr "rig_l_pastern_orientConstraint1.cry" "rig_l_pastern.ry";
connectAttr "rig_l_pastern_orientConstraint1.crz" "rig_l_pastern.rz";
connectAttr "rig_l_pastern.ro" "rig_l_pastern_orientConstraint1.cro";
connectAttr "rig_l_pastern.pim" "rig_l_pastern_orientConstraint1.cpim";
connectAttr "rig_l_pastern.jo" "rig_l_pastern_orientConstraint1.cjo";
connectAttr "rig_l_pastern.is" "rig_l_pastern_orientConstraint1.is";
connectAttr "l_bk_foot_ctrl.r" "rig_l_pastern_orientConstraint1.tg[0].tr";
connectAttr "l_bk_foot_ctrl.ro" "rig_l_pastern_orientConstraint1.tg[0].tro";
connectAttr "l_bk_foot_ctrl.pm" "rig_l_pastern_orientConstraint1.tg[0].tpm";
connectAttr "rig_l_pastern_orientConstraint1.w0" "rig_l_pastern_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_l_pastern.tx" "effector1.tx";
connectAttr "rig_l_pastern.ty" "effector1.ty";
connectAttr "rig_l_pastern.tz" "effector1.tz";
connectAttr "rig_hips.s" "rig_r_femur.is";
connectAttr "rig_r_femur.s" "rig_r_tibia.is";
connectAttr "rig_r_tibia.s" "rig_r_metatarsal.is";
connectAttr "rig_r_metatarsal.s" "rig_r_pastern.is";
connectAttr "rig_r_pastern_orientConstraint1.crx" "rig_r_pastern.rx";
connectAttr "rig_r_pastern_orientConstraint1.cry" "rig_r_pastern.ry";
connectAttr "rig_r_pastern_orientConstraint1.crz" "rig_r_pastern.rz";
connectAttr "rig_r_pastern.ro" "rig_r_pastern_orientConstraint1.cro";
connectAttr "rig_r_pastern.pim" "rig_r_pastern_orientConstraint1.cpim";
connectAttr "rig_r_pastern.jo" "rig_r_pastern_orientConstraint1.cjo";
connectAttr "rig_r_pastern.is" "rig_r_pastern_orientConstraint1.is";
connectAttr "r_bk_foot_ctrl.r" "rig_r_pastern_orientConstraint1.tg[0].tr";
connectAttr "r_bk_foot_ctrl.ro" "rig_r_pastern_orientConstraint1.tg[0].tro";
connectAttr "r_bk_foot_ctrl.pm" "rig_r_pastern_orientConstraint1.tg[0].tpm";
connectAttr "rig_r_pastern_orientConstraint1.w0" "rig_r_pastern_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_r_pastern.tx" "effector2.tx";
connectAttr "rig_r_pastern.ty" "effector2.ty";
connectAttr "rig_r_pastern.tz" "effector2.tz";
connectAttr "rig_hips.ro" "rig_hips_parentConstraint1.cro";
connectAttr "rig_hips.pim" "rig_hips_parentConstraint1.cpim";
connectAttr "rig_hips.rp" "rig_hips_parentConstraint1.crp";
connectAttr "rig_hips.rpt" "rig_hips_parentConstraint1.crt";
connectAttr "rig_hips.jo" "rig_hips_parentConstraint1.cjo";
connectAttr "reindeer__flexi_start_ctrl.t" "rig_hips_parentConstraint1.tg[0].tt"
		;
connectAttr "reindeer__flexi_start_ctrl.rp" "rig_hips_parentConstraint1.tg[0].trp"
		;
connectAttr "reindeer__flexi_start_ctrl.rpt" "rig_hips_parentConstraint1.tg[0].trt"
		;
connectAttr "reindeer__flexi_start_ctrl.r" "rig_hips_parentConstraint1.tg[0].tr"
		;
connectAttr "reindeer__flexi_start_ctrl.ro" "rig_hips_parentConstraint1.tg[0].tro"
		;
connectAttr "reindeer__flexi_start_ctrl.s" "rig_hips_parentConstraint1.tg[0].ts"
		;
connectAttr "reindeer__flexi_start_ctrl.pm" "rig_hips_parentConstraint1.tg[0].tpm"
		;
connectAttr "rig_hips_parentConstraint1.w0" "rig_hips_parentConstraint1.tg[0].tw"
		;
connectAttr "rig_pelvis.s" "rig_thorax.is";
connectAttr "rig_thorax_parentConstraint1.crx" "rig_thorax.rx";
connectAttr "rig_thorax_parentConstraint1.cry" "rig_thorax.ry";
connectAttr "rig_thorax_parentConstraint1.crz" "rig_thorax.rz";
connectAttr "rig_thorax_parentConstraint1.ctx" "rig_thorax.tx";
connectAttr "rig_thorax_parentConstraint1.cty" "rig_thorax.ty";
connectAttr "rig_thorax_parentConstraint1.ctz" "rig_thorax.tz";
connectAttr "rig_thorax.s" "rig_l_fnt_humerus.is";
connectAttr "rig_l_fnt_humerus.s" "rig_l_fnt_metacarpal.is";
connectAttr "rig_l_fnt_metacarpal.s" "rig_l_fnt_pastern.is";
connectAttr "rig_l_fnt_pastern_orientConstraint1.crx" "rig_l_fnt_pastern.rx";
connectAttr "rig_l_fnt_pastern_orientConstraint1.cry" "rig_l_fnt_pastern.ry";
connectAttr "rig_l_fnt_pastern_orientConstraint1.crz" "rig_l_fnt_pastern.rz";
connectAttr "rig_l_fnt_pastern.ro" "rig_l_fnt_pastern_orientConstraint1.cro";
connectAttr "rig_l_fnt_pastern.pim" "rig_l_fnt_pastern_orientConstraint1.cpim";
connectAttr "rig_l_fnt_pastern.jo" "rig_l_fnt_pastern_orientConstraint1.cjo";
connectAttr "rig_l_fnt_pastern.is" "rig_l_fnt_pastern_orientConstraint1.is";
connectAttr "l_fnt_foot_ctrl.r" "rig_l_fnt_pastern_orientConstraint1.tg[0].tr";
connectAttr "l_fnt_foot_ctrl.ro" "rig_l_fnt_pastern_orientConstraint1.tg[0].tro"
		;
connectAttr "l_fnt_foot_ctrl.pm" "rig_l_fnt_pastern_orientConstraint1.tg[0].tpm"
		;
connectAttr "rig_l_fnt_pastern_orientConstraint1.w0" "rig_l_fnt_pastern_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_l_fnt_pastern.tx" "effector3.tx";
connectAttr "rig_l_fnt_pastern.ty" "effector3.ty";
connectAttr "rig_l_fnt_pastern.tz" "effector3.tz";
connectAttr "rig_thorax.s" "rig_r_fnt_humerus.is";
connectAttr "rig_r_fnt_humerus.s" "rig_r_fnt_metacarpal.is";
connectAttr "rig_r_fnt_metacarpal.s" "rig_r_fnt_pastern.is";
connectAttr "rig_r_fnt_pastern_orientConstraint1.crx" "rig_r_fnt_pastern.rx";
connectAttr "rig_r_fnt_pastern_orientConstraint1.cry" "rig_r_fnt_pastern.ry";
connectAttr "rig_r_fnt_pastern_orientConstraint1.crz" "rig_r_fnt_pastern.rz";
connectAttr "rig_r_fnt_pastern.ro" "rig_r_fnt_pastern_orientConstraint1.cro";
connectAttr "rig_r_fnt_pastern.pim" "rig_r_fnt_pastern_orientConstraint1.cpim";
connectAttr "rig_r_fnt_pastern.jo" "rig_r_fnt_pastern_orientConstraint1.cjo";
connectAttr "rig_r_fnt_pastern.is" "rig_r_fnt_pastern_orientConstraint1.is";
connectAttr "r_fnt_foot_ctrl.r" "rig_r_fnt_pastern_orientConstraint1.tg[0].tr";
connectAttr "r_fnt_foot_ctrl.ro" "rig_r_fnt_pastern_orientConstraint1.tg[0].tro"
		;
connectAttr "r_fnt_foot_ctrl.pm" "rig_r_fnt_pastern_orientConstraint1.tg[0].tpm"
		;
connectAttr "rig_r_fnt_pastern_orientConstraint1.w0" "rig_r_fnt_pastern_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_r_fnt_pastern.tx" "effector4.tx";
connectAttr "rig_r_fnt_pastern.ty" "effector4.ty";
connectAttr "rig_r_fnt_pastern.tz" "effector4.tz";
connectAttr "rig_neckBase_orientConstraint1.crz" "rig_neckBase.rz";
connectAttr "rig_neckBase_orientConstraint1.crx" "rig_neckBase.rx";
connectAttr "rig_neckBase_orientConstraint1.cry" "rig_neckBase.ry";
connectAttr "rig_thorax.s" "rig_neckBase.is";
connectAttr "rig_neckBase_pointConstraint1.ctx" "rig_neckBase.tx";
connectAttr "rig_neckBase_pointConstraint1.cty" "rig_neckBase.ty";
connectAttr "rig_neckBase_pointConstraint1.ctz" "rig_neckBase.tz";
connectAttr "rig_neck_orientConstraint1.cry" "rig_neck.ry";
connectAttr "rig_neck_orientConstraint1.crx" "rig_neck.rx";
connectAttr "rig_neck_orientConstraint1.crz" "rig_neck.rz";
connectAttr "rig_neckBase.s" "rig_neck.is";
connectAttr "rig_neck_pointConstraint1.ctx" "rig_neck.tx";
connectAttr "rig_neck_pointConstraint1.cty" "rig_neck.ty";
connectAttr "rig_neck_pointConstraint1.ctz" "rig_neck.tz";
connectAttr "rig_neck.s" "rig_head.is";
connectAttr "rig_head_orientConstraint1.crx" "rig_head.rx";
connectAttr "rig_head_orientConstraint1.cry" "rig_head.ry";
connectAttr "rig_head_orientConstraint1.crz" "rig_head.rz";
connectAttr "rig_head_pointConstraint1.ctx" "rig_head.tx";
connectAttr "rig_head_pointConstraint1.cty" "rig_head.ty";
connectAttr "rig_head_pointConstraint1.ctz" "rig_head.tz";
connectAttr "rig_head.pim" "rig_head_pointConstraint1.cpim";
connectAttr "rig_head.rp" "rig_head_pointConstraint1.crp";
connectAttr "rig_head.rpt" "rig_head_pointConstraint1.crt";
connectAttr "headCtrl.t" "rig_head_pointConstraint1.tg[0].tt";
connectAttr "headCtrl.rp" "rig_head_pointConstraint1.tg[0].trp";
connectAttr "headCtrl.rpt" "rig_head_pointConstraint1.tg[0].trt";
connectAttr "headCtrl.pm" "rig_head_pointConstraint1.tg[0].tpm";
connectAttr "rig_head_pointConstraint1.w0" "rig_head_pointConstraint1.tg[0].tw";
connectAttr "rig_head.ro" "rig_head_orientConstraint1.cro";
connectAttr "rig_head.pim" "rig_head_orientConstraint1.cpim";
connectAttr "rig_head.jo" "rig_head_orientConstraint1.cjo";
connectAttr "rig_head.is" "rig_head_orientConstraint1.is";
connectAttr "headCtrl.r" "rig_head_orientConstraint1.tg[0].tr";
connectAttr "headCtrl.ro" "rig_head_orientConstraint1.tg[0].tro";
connectAttr "headCtrl.pm" "rig_head_orientConstraint1.tg[0].tpm";
connectAttr "rig_head_orientConstraint1.w0" "rig_head_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_neck.pim" "rig_neck_pointConstraint1.cpim";
connectAttr "rig_neck.rp" "rig_neck_pointConstraint1.crp";
connectAttr "rig_neck.rpt" "rig_neck_pointConstraint1.crt";
connectAttr "neckCtrl.t" "rig_neck_pointConstraint1.tg[0].tt";
connectAttr "neckCtrl.rp" "rig_neck_pointConstraint1.tg[0].trp";
connectAttr "neckCtrl.rpt" "rig_neck_pointConstraint1.tg[0].trt";
connectAttr "neckCtrl.pm" "rig_neck_pointConstraint1.tg[0].tpm";
connectAttr "rig_neck_pointConstraint1.w0" "rig_neck_pointConstraint1.tg[0].tw";
connectAttr "rig_neck.ro" "rig_neck_orientConstraint1.cro";
connectAttr "rig_neck.pim" "rig_neck_orientConstraint1.cpim";
connectAttr "rig_neck.jo" "rig_neck_orientConstraint1.cjo";
connectAttr "rig_neck.is" "rig_neck_orientConstraint1.is";
connectAttr "neckCtrl.r" "rig_neck_orientConstraint1.tg[0].tr";
connectAttr "neckCtrl.ro" "rig_neck_orientConstraint1.tg[0].tro";
connectAttr "neckCtrl.pm" "rig_neck_orientConstraint1.tg[0].tpm";
connectAttr "rig_neck_orientConstraint1.w0" "rig_neck_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_neckBase.pim" "rig_neckBase_pointConstraint1.cpim";
connectAttr "rig_neckBase.rp" "rig_neckBase_pointConstraint1.crp";
connectAttr "rig_neckBase.rpt" "rig_neckBase_pointConstraint1.crt";
connectAttr "neckBaseCntl.t" "rig_neckBase_pointConstraint1.tg[0].tt";
connectAttr "neckBaseCntl.rp" "rig_neckBase_pointConstraint1.tg[0].trp";
connectAttr "neckBaseCntl.rpt" "rig_neckBase_pointConstraint1.tg[0].trt";
connectAttr "neckBaseCntl.pm" "rig_neckBase_pointConstraint1.tg[0].tpm";
connectAttr "rig_neckBase_pointConstraint1.w0" "rig_neckBase_pointConstraint1.tg[0].tw"
		;
connectAttr "rig_neckBase.ro" "rig_neckBase_orientConstraint1.cro";
connectAttr "rig_neckBase.pim" "rig_neckBase_orientConstraint1.cpim";
connectAttr "rig_neckBase.jo" "rig_neckBase_orientConstraint1.cjo";
connectAttr "rig_neckBase.is" "rig_neckBase_orientConstraint1.is";
connectAttr "neckBaseCntl.r" "rig_neckBase_orientConstraint1.tg[0].tr";
connectAttr "neckBaseCntl.ro" "rig_neckBase_orientConstraint1.tg[0].tro";
connectAttr "neckBaseCntl.pm" "rig_neckBase_orientConstraint1.tg[0].tpm";
connectAttr "rig_neckBase_orientConstraint1.w0" "rig_neckBase_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_thorax.ro" "rig_thorax_parentConstraint1.cro";
connectAttr "rig_thorax.pim" "rig_thorax_parentConstraint1.cpim";
connectAttr "rig_thorax.rp" "rig_thorax_parentConstraint1.crp";
connectAttr "rig_thorax.rpt" "rig_thorax_parentConstraint1.crt";
connectAttr "rig_thorax.jo" "rig_thorax_parentConstraint1.cjo";
connectAttr "reindeer__flexi_end_ctrl.t" "rig_thorax_parentConstraint1.tg[0].tt"
		;
connectAttr "reindeer__flexi_end_ctrl.rp" "rig_thorax_parentConstraint1.tg[0].trp"
		;
connectAttr "reindeer__flexi_end_ctrl.rpt" "rig_thorax_parentConstraint1.tg[0].trt"
		;
connectAttr "reindeer__flexi_end_ctrl.r" "rig_thorax_parentConstraint1.tg[0].tr"
		;
connectAttr "reindeer__flexi_end_ctrl.ro" "rig_thorax_parentConstraint1.tg[0].tro"
		;
connectAttr "reindeer__flexi_end_ctrl.s" "rig_thorax_parentConstraint1.tg[0].ts"
		;
connectAttr "reindeer__flexi_end_ctrl.pm" "rig_thorax_parentConstraint1.tg[0].tpm"
		;
connectAttr "rig_thorax_parentConstraint1.w0" "rig_thorax_parentConstraint1.tg[0].tw"
		;
connectAttr "rig_pelvis.pim" "rig_pelvis_pointConstraint1.cpim";
connectAttr "rig_pelvis.rp" "rig_pelvis_pointConstraint1.crp";
connectAttr "rig_pelvis.rpt" "rig_pelvis_pointConstraint1.crt";
connectAttr "center_ctrl.t" "rig_pelvis_pointConstraint1.tg[0].tt";
connectAttr "center_ctrl.rp" "rig_pelvis_pointConstraint1.tg[0].trp";
connectAttr "center_ctrl.rpt" "rig_pelvis_pointConstraint1.tg[0].trt";
connectAttr "center_ctrl.pm" "rig_pelvis_pointConstraint1.tg[0].tpm";
connectAttr "rig_pelvis_pointConstraint1.w0" "rig_pelvis_pointConstraint1.tg[0].tw"
		;
connectAttr "rig_pelvis.ro" "rig_pelvis_orientConstraint1.cro";
connectAttr "rig_pelvis.pim" "rig_pelvis_orientConstraint1.cpim";
connectAttr "rig_pelvis.jo" "rig_pelvis_orientConstraint1.cjo";
connectAttr "rig_pelvis.is" "rig_pelvis_orientConstraint1.is";
connectAttr "center_ctrl.r" "rig_pelvis_orientConstraint1.tg[0].tr";
connectAttr "center_ctrl.ro" "rig_pelvis_orientConstraint1.tg[0].tro";
connectAttr "center_ctrl.pm" "rig_pelvis_orientConstraint1.tg[0].tpm";
connectAttr "rig_pelvis_orientConstraint1.w0" "rig_pelvis_orientConstraint1.tg[0].tw"
		;
connectAttr "skinCluster4.og[0]" "reindeer__flexi_cWire_surface_0Shape1.cr";
connectAttr "tweak6.pl[0].cp[0]" "reindeer__flexi_cWire_surface_0Shape1.twl";
connectAttr "skinCluster4GroupId.id" "reindeer__flexi_cWire_surface_0Shape1.iog.og[0].gid"
		;
connectAttr "skinCluster4Set.mwc" "reindeer__flexi_cWire_surface_0Shape1.iog.og[0].gco"
		;
connectAttr "groupId12.id" "reindeer__flexi_cWire_surface_0Shape1.iog.og[1].gid"
		;
connectAttr "tweakSet6.mwc" "reindeer__flexi_cWire_surface_0Shape1.iog.og[1].gco"
		;
connectAttr "twist1GroupId.id" "reindeer__flexi_surface_bsShps_01Shape.iog.og[0].gid"
		;
connectAttr "twist1Set.mwc" "reindeer__flexi_surface_bsShps_01Shape.iog.og[0].gco"
		;
connectAttr "groupId10.id" "reindeer__flexi_surface_bsShps_01Shape.iog.og[1].gid"
		;
connectAttr "tweakSet5.mwc" "reindeer__flexi_surface_bsShps_01Shape.iog.og[1].gco"
		;
connectAttr "reindeer__flexi_wire_surface_01GroupId.id" "reindeer__flexi_surface_bsShps_01Shape.iog.og[2].gid"
		;
connectAttr "reindeer__flexi_wire_surface_01Set.mwc" "reindeer__flexi_surface_bsShps_01Shape.iog.og[2].gco"
		;
connectAttr "reindeer__flexi_wire_surface_01.og[0]" "reindeer__flexi_surface_bsShps_01Shape.cr"
		;
connectAttr "tweak5.pl[0].cp[0]" "reindeer__flexi_surface_bsShps_01Shape.twl";
connectAttr "reindeer__flexi_twist_06.msg" "reindeer__flexi_twistHdl_01.sml";
connectAttr "reindeer__flexi_twist_06.sa" "reindeer__flexi_twistHdl_01Shape.sa";
connectAttr "reindeer__flexi_twist_06.ea" "reindeer__flexi_twistHdl_01Shape.ea";
connectAttr "reindeer__flexi_twist_06.lb" "reindeer__flexi_twistHdl_01Shape.lb";
connectAttr "reindeer__flexi_twist_06.hb" "reindeer__flexi_twistHdl_01Shape.hb";
connectAttr "reindeer__flexi_grp_end_01_parentConstraint1.ctx" "reindeer__flexi_grp_end_01.tx"
		;
connectAttr "reindeer__flexi_grp_end_01_parentConstraint1.cty" "reindeer__flexi_grp_end_01.ty"
		;
connectAttr "reindeer__flexi_grp_end_01_parentConstraint1.ctz" "reindeer__flexi_grp_end_01.tz"
		;
connectAttr "reindeer__flexi_grp_end_01_parentConstraint1.crx" "reindeer__flexi_grp_end_01.rx"
		;
connectAttr "reindeer__flexi_grp_end_01_parentConstraint1.cry" "reindeer__flexi_grp_end_01.ry"
		;
connectAttr "reindeer__flexi_grp_end_01_parentConstraint1.crz" "reindeer__flexi_grp_end_01.rz"
		;
connectAttr "reindeer__flexi_end_ctrl.t" "reindeer__flexi_jnt_end_01.t";
connectAttr "reindeer__flexi_end_ctrl.r" "reindeer__flexi_jnt_end_01.r";
connectAttr "reindeer__flexi_grp_end_01.ro" "reindeer__flexi_grp_end_01_parentConstraint1.cro"
		;
connectAttr "reindeer__flexi_grp_end_01.pim" "reindeer__flexi_grp_end_01_parentConstraint1.cpim"
		;
connectAttr "reindeer__flexi_grp_end_01.rp" "reindeer__flexi_grp_end_01_parentConstraint1.crp"
		;
connectAttr "reindeer__flexi_grp_end_01.rpt" "reindeer__flexi_grp_end_01_parentConstraint1.crt"
		;
connectAttr "reindeer__flexi_jnt_mid_01.t" "reindeer__flexi_grp_end_01_parentConstraint1.tg[0].tt"
		;
connectAttr "reindeer__flexi_jnt_mid_01.rp" "reindeer__flexi_grp_end_01_parentConstraint1.tg[0].trp"
		;
connectAttr "reindeer__flexi_jnt_mid_01.rpt" "reindeer__flexi_grp_end_01_parentConstraint1.tg[0].trt"
		;
connectAttr "reindeer__flexi_jnt_mid_01.r" "reindeer__flexi_grp_end_01_parentConstraint1.tg[0].tr"
		;
connectAttr "reindeer__flexi_jnt_mid_01.ro" "reindeer__flexi_grp_end_01_parentConstraint1.tg[0].tro"
		;
connectAttr "reindeer__flexi_jnt_mid_01.s" "reindeer__flexi_grp_end_01_parentConstraint1.tg[0].ts"
		;
connectAttr "reindeer__flexi_jnt_mid_01.pm" "reindeer__flexi_grp_end_01_parentConstraint1.tg[0].tpm"
		;
connectAttr "reindeer__flexi_jnt_mid_01.jo" "reindeer__flexi_grp_end_01_parentConstraint1.tg[0].tjo"
		;
connectAttr "reindeer__flexi_jnt_mid_01.ssc" "reindeer__flexi_grp_end_01_parentConstraint1.tg[0].tsc"
		;
connectAttr "reindeer__flexi_jnt_mid_01.is" "reindeer__flexi_grp_end_01_parentConstraint1.tg[0].tis"
		;
connectAttr "reindeer__flexi_grp_end_01_parentConstraint1.w0" "reindeer__flexi_grp_end_01_parentConstraint1.tg[0].tw"
		;
connectAttr "reindeer__flexi_end_ctrl.followMid" "reindeer__flexi_grp_end_01_parentConstraint1.w0"
		;
connectAttr "reindeer__flexi_start_ctrl.t" "reindeer__flexi_jnt_start_01.t";
connectAttr "reindeer__flexi_start_ctrl.r" "reindeer__flexi_jnt_start_01.r";
connectAttr "reindeer__flexi_grp_mid_01_parentConstraint1.ctx" "reindeer__flexi_grp_mid_01.tx"
		;
connectAttr "reindeer__flexi_grp_mid_01_parentConstraint1.cty" "reindeer__flexi_grp_mid_01.ty"
		;
connectAttr "reindeer__flexi_grp_mid_01_parentConstraint1.ctz" "reindeer__flexi_grp_mid_01.tz"
		;
connectAttr "reindeer__flexi_grp_mid_01_parentConstraint1.crx" "reindeer__flexi_grp_mid_01.rx"
		;
connectAttr "reindeer__flexi_grp_mid_01_parentConstraint1.cry" "reindeer__flexi_grp_mid_01.ry"
		;
connectAttr "reindeer__flexi_grp_mid_01_parentConstraint1.crz" "reindeer__flexi_grp_mid_01.rz"
		;
connectAttr "reindeer__flexi_mid_ctrl.t" "reindeer__flexi_jnt_mid_01.t";
connectAttr "reindeer__flexi_mid_ctrl.r" "reindeer__flexi_jnt_mid_01.r";
connectAttr "reindeer__flexi_grp_mid_01.ro" "reindeer__flexi_grp_mid_01_parentConstraint1.cro"
		;
connectAttr "reindeer__flexi_grp_mid_01.pim" "reindeer__flexi_grp_mid_01_parentConstraint1.cpim"
		;
connectAttr "reindeer__flexi_grp_mid_01.rp" "reindeer__flexi_grp_mid_01_parentConstraint1.crp"
		;
connectAttr "reindeer__flexi_grp_mid_01.rpt" "reindeer__flexi_grp_mid_01_parentConstraint1.crt"
		;
connectAttr "reindeer__flexi_jnt_start_01.t" "reindeer__flexi_grp_mid_01_parentConstraint1.tg[0].tt"
		;
connectAttr "reindeer__flexi_jnt_start_01.rp" "reindeer__flexi_grp_mid_01_parentConstraint1.tg[0].trp"
		;
connectAttr "reindeer__flexi_jnt_start_01.rpt" "reindeer__flexi_grp_mid_01_parentConstraint1.tg[0].trt"
		;
connectAttr "reindeer__flexi_jnt_start_01.r" "reindeer__flexi_grp_mid_01_parentConstraint1.tg[0].tr"
		;
connectAttr "reindeer__flexi_jnt_start_01.ro" "reindeer__flexi_grp_mid_01_parentConstraint1.tg[0].tro"
		;
connectAttr "reindeer__flexi_jnt_start_01.s" "reindeer__flexi_grp_mid_01_parentConstraint1.tg[0].ts"
		;
connectAttr "reindeer__flexi_jnt_start_01.pm" "reindeer__flexi_grp_mid_01_parentConstraint1.tg[0].tpm"
		;
connectAttr "reindeer__flexi_jnt_start_01.jo" "reindeer__flexi_grp_mid_01_parentConstraint1.tg[0].tjo"
		;
connectAttr "reindeer__flexi_jnt_start_01.ssc" "reindeer__flexi_grp_mid_01_parentConstraint1.tg[0].tsc"
		;
connectAttr "reindeer__flexi_jnt_start_01.is" "reindeer__flexi_grp_mid_01_parentConstraint1.tg[0].tis"
		;
connectAttr "reindeer__flexi_grp_mid_01_parentConstraint1.w0" "reindeer__flexi_grp_mid_01_parentConstraint1.tg[0].tw"
		;
connectAttr "reindeer__flexi_mid_ctrl.followStart" "reindeer__flexi_grp_mid_01_parentConstraint1.w0"
		;
connectAttr "reindeer__global_move_ctrl_parentConstraint1.ctx" "reindeer__global_move_ctrl.tx"
		;
connectAttr "reindeer__global_move_ctrl_parentConstraint1.cty" "reindeer__global_move_ctrl.ty"
		;
connectAttr "reindeer__global_move_ctrl_parentConstraint1.ctz" "reindeer__global_move_ctrl.tz"
		;
connectAttr "reindeer__global_move_ctrl_parentConstraint1.crx" "reindeer__global_move_ctrl.rx"
		;
connectAttr "reindeer__global_move_ctrl_parentConstraint1.cry" "reindeer__global_move_ctrl.ry"
		;
connectAttr "reindeer__global_move_ctrl_parentConstraint1.crz" "reindeer__global_move_ctrl.rz"
		;
connectAttr "reindeer__flexi_bsShps_surface_01GroupId.id" "reindeer__flexi_surface_01Shape.iog.og[0].gid"
		;
connectAttr "reindeer__flexi_bsShps_surface_01Set.mwc" "reindeer__flexi_surface_01Shape.iog.og[0].gco"
		;
connectAttr "groupId8.id" "reindeer__flexi_surface_01Shape.iog.og[1].gid";
connectAttr "tweakSet4.mwc" "reindeer__flexi_surface_01Shape.iog.og[1].gco";
connectAttr "reindeer__flexi_bsShps_surface_01.og[0]" "reindeer__flexi_surface_01Shape.cr"
		;
connectAttr "tweak4.pl[0].cp[0]" "reindeer__flexi_surface_01Shape.twl";
connectAttr "reindeer__flexi_mid_ctrl_grp_parentConstraint1.ctx" "reindeer__flexi_mid_ctrl_grp.tx"
		;
connectAttr "reindeer__flexi_mid_ctrl_grp_parentConstraint1.cty" "reindeer__flexi_mid_ctrl_grp.ty"
		;
connectAttr "reindeer__flexi_mid_ctrl_grp_parentConstraint1.ctz" "reindeer__flexi_mid_ctrl_grp.tz"
		;
connectAttr "reindeer__flexi_mid_ctrl_grp_parentConstraint1.crx" "reindeer__flexi_mid_ctrl_grp.rx"
		;
connectAttr "reindeer__flexi_mid_ctrl_grp_parentConstraint1.cry" "reindeer__flexi_mid_ctrl_grp.ry"
		;
connectAttr "reindeer__flexi_mid_ctrl_grp_parentConstraint1.crz" "reindeer__flexi_mid_ctrl_grp.rz"
		;
connectAttr "reindeer__flexi_mid_ctrl_grp.ro" "reindeer__flexi_mid_ctrl_grp_parentConstraint1.cro"
		;
connectAttr "reindeer__flexi_mid_ctrl_grp.pim" "reindeer__flexi_mid_ctrl_grp_parentConstraint1.cpim"
		;
connectAttr "reindeer__flexi_mid_ctrl_grp.rp" "reindeer__flexi_mid_ctrl_grp_parentConstraint1.crp"
		;
connectAttr "reindeer__flexi_mid_ctrl_grp.rpt" "reindeer__flexi_mid_ctrl_grp_parentConstraint1.crt"
		;
connectAttr "reindeer__flexi_start_ctrl.t" "reindeer__flexi_mid_ctrl_grp_parentConstraint1.tg[0].tt"
		;
connectAttr "reindeer__flexi_start_ctrl.rp" "reindeer__flexi_mid_ctrl_grp_parentConstraint1.tg[0].trp"
		;
connectAttr "reindeer__flexi_start_ctrl.rpt" "reindeer__flexi_mid_ctrl_grp_parentConstraint1.tg[0].trt"
		;
connectAttr "reindeer__flexi_start_ctrl.r" "reindeer__flexi_mid_ctrl_grp_parentConstraint1.tg[0].tr"
		;
connectAttr "reindeer__flexi_start_ctrl.ro" "reindeer__flexi_mid_ctrl_grp_parentConstraint1.tg[0].tro"
		;
connectAttr "reindeer__flexi_start_ctrl.s" "reindeer__flexi_mid_ctrl_grp_parentConstraint1.tg[0].ts"
		;
connectAttr "reindeer__flexi_start_ctrl.pm" "reindeer__flexi_mid_ctrl_grp_parentConstraint1.tg[0].tpm"
		;
connectAttr "reindeer__flexi_mid_ctrl_grp_parentConstraint1.w0" "reindeer__flexi_mid_ctrl_grp_parentConstraint1.tg[0].tw"
		;
connectAttr "reindeer__flexi_mid_ctrl.followStart" "reindeer__flexi_mid_ctrl_grp_parentConstraint1.w0"
		;
connectAttr "reindeer__flexi_end_ctrl_grp_parentConstraint1.ctx" "reindeer__flexi_end_ctrl_grp.tx"
		;
connectAttr "reindeer__flexi_end_ctrl_grp_parentConstraint1.cty" "reindeer__flexi_end_ctrl_grp.ty"
		;
connectAttr "reindeer__flexi_end_ctrl_grp_parentConstraint1.ctz" "reindeer__flexi_end_ctrl_grp.tz"
		;
connectAttr "reindeer__flexi_end_ctrl_grp_parentConstraint1.crx" "reindeer__flexi_end_ctrl_grp.rx"
		;
connectAttr "reindeer__flexi_end_ctrl_grp_parentConstraint1.cry" "reindeer__flexi_end_ctrl_grp.ry"
		;
connectAttr "reindeer__flexi_end_ctrl_grp_parentConstraint1.crz" "reindeer__flexi_end_ctrl_grp.rz"
		;
connectAttr "reindeer__flexi_end_ctrl_grp.ro" "reindeer__flexi_end_ctrl_grp_parentConstraint1.cro"
		;
connectAttr "reindeer__flexi_end_ctrl_grp.pim" "reindeer__flexi_end_ctrl_grp_parentConstraint1.cpim"
		;
connectAttr "reindeer__flexi_end_ctrl_grp.rp" "reindeer__flexi_end_ctrl_grp_parentConstraint1.crp"
		;
connectAttr "reindeer__flexi_end_ctrl_grp.rpt" "reindeer__flexi_end_ctrl_grp_parentConstraint1.crt"
		;
connectAttr "reindeer__flexi_mid_ctrl.t" "reindeer__flexi_end_ctrl_grp_parentConstraint1.tg[0].tt"
		;
connectAttr "reindeer__flexi_mid_ctrl.rp" "reindeer__flexi_end_ctrl_grp_parentConstraint1.tg[0].trp"
		;
connectAttr "reindeer__flexi_mid_ctrl.rpt" "reindeer__flexi_end_ctrl_grp_parentConstraint1.tg[0].trt"
		;
connectAttr "reindeer__flexi_mid_ctrl.r" "reindeer__flexi_end_ctrl_grp_parentConstraint1.tg[0].tr"
		;
connectAttr "reindeer__flexi_mid_ctrl.ro" "reindeer__flexi_end_ctrl_grp_parentConstraint1.tg[0].tro"
		;
connectAttr "reindeer__flexi_mid_ctrl.s" "reindeer__flexi_end_ctrl_grp_parentConstraint1.tg[0].ts"
		;
connectAttr "reindeer__flexi_mid_ctrl.pm" "reindeer__flexi_end_ctrl_grp_parentConstraint1.tg[0].tpm"
		;
connectAttr "reindeer__flexi_end_ctrl_grp_parentConstraint1.w0" "reindeer__flexi_end_ctrl_grp_parentConstraint1.tg[0].tw"
		;
connectAttr "reindeer__flexi_end_ctrl.followMid" "reindeer__flexi_end_ctrl_grp_parentConstraint1.w0"
		;
connectAttr "reindeer__global_move_ctrl.ro" "reindeer__global_move_ctrl_parentConstraint1.cro"
		;
connectAttr "reindeer__global_move_ctrl.pim" "reindeer__global_move_ctrl_parentConstraint1.cpim"
		;
connectAttr "reindeer__global_move_ctrl.rp" "reindeer__global_move_ctrl_parentConstraint1.crp"
		;
connectAttr "reindeer__global_move_ctrl.rpt" "reindeer__global_move_ctrl_parentConstraint1.crt"
		;
connectAttr "center_ctrl.t" "reindeer__global_move_ctrl_parentConstraint1.tg[0].tt"
		;
connectAttr "center_ctrl.rp" "reindeer__global_move_ctrl_parentConstraint1.tg[0].trp"
		;
connectAttr "center_ctrl.rpt" "reindeer__global_move_ctrl_parentConstraint1.tg[0].trt"
		;
connectAttr "center_ctrl.r" "reindeer__global_move_ctrl_parentConstraint1.tg[0].tr"
		;
connectAttr "center_ctrl.ro" "reindeer__global_move_ctrl_parentConstraint1.tg[0].tro"
		;
connectAttr "center_ctrl.s" "reindeer__global_move_ctrl_parentConstraint1.tg[0].ts"
		;
connectAttr "center_ctrl.pm" "reindeer__global_move_ctrl_parentConstraint1.tg[0].tpm"
		;
connectAttr "reindeer__global_move_ctrl_parentConstraint1.w0" "reindeer__global_move_ctrl_parentConstraint1.tg[0].tw"
		;
connectAttr "reindeer__flexi_surface_01Shape_twist_follicle_01.or" "reindeer__flexi_surface_01_twist_follicle_01.r"
		 -l on;
connectAttr "reindeer__flexi_surface_01Shape_twist_follicle_01.ot" "reindeer__flexi_surface_01_twist_follicle_01.t"
		 -l on;
connectAttr "reindeer__flexi_surface_01_twist_follicle_01_scaleConstraint1.csx" "reindeer__flexi_surface_01_twist_follicle_01.sx"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_01_scaleConstraint1.csy" "reindeer__flexi_surface_01_twist_follicle_01.sy"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_01_scaleConstraint1.csz" "reindeer__flexi_surface_01_twist_follicle_01.sz"
		;
connectAttr "reindeer__flexi_surface_01Shape.l" "reindeer__flexi_surface_01Shape_twist_follicle_01.is"
		;
connectAttr "reindeer__flexi_surface_01Shape.wm" "reindeer__flexi_surface_01Shape_twist_follicle_01.iwm"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_01.pim" "reindeer__flexi_surface_01_twist_follicle_01_scaleConstraint1.cpim"
		;
connectAttr "reindeer__global_move_ctrl.s" "reindeer__flexi_surface_01_twist_follicle_01_scaleConstraint1.tg[0].ts"
		;
connectAttr "reindeer__global_move_ctrl.pm" "reindeer__flexi_surface_01_twist_follicle_01_scaleConstraint1.tg[0].tpm"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_01_scaleConstraint1.w0" "reindeer__flexi_surface_01_twist_follicle_01_scaleConstraint1.tg[0].tw"
		;
connectAttr "reindeer__flexi_pow_volume_a01.ox" "reindeer__flexi_twist_01.sy";
connectAttr "reindeer__flexi_pow_volume_a01.ox" "reindeer__flexi_twist_01.sz";
connectAttr "reindeer__flexi_surface_01Shape_twist_follicle_02.or" "reindeer__flexi_surface_01_twist_follicle_02.r"
		 -l on;
connectAttr "reindeer__flexi_surface_01Shape_twist_follicle_02.ot" "reindeer__flexi_surface_01_twist_follicle_02.t"
		 -l on;
connectAttr "reindeer__flexi_surface_01_twist_follicle_02_scaleConstraint1.csx" "reindeer__flexi_surface_01_twist_follicle_02.sx"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_02_scaleConstraint1.csy" "reindeer__flexi_surface_01_twist_follicle_02.sy"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_02_scaleConstraint1.csz" "reindeer__flexi_surface_01_twist_follicle_02.sz"
		;
connectAttr "reindeer__flexi_surface_01Shape.l" "reindeer__flexi_surface_01Shape_twist_follicle_02.is"
		;
connectAttr "reindeer__flexi_surface_01Shape.wm" "reindeer__flexi_surface_01Shape_twist_follicle_02.iwm"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_02.pim" "reindeer__flexi_surface_01_twist_follicle_02_scaleConstraint1.cpim"
		;
connectAttr "reindeer__global_move_ctrl.s" "reindeer__flexi_surface_01_twist_follicle_02_scaleConstraint1.tg[0].ts"
		;
connectAttr "reindeer__global_move_ctrl.pm" "reindeer__flexi_surface_01_twist_follicle_02_scaleConstraint1.tg[0].tpm"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_02_scaleConstraint1.w0" "reindeer__flexi_surface_01_twist_follicle_02_scaleConstraint1.tg[0].tw"
		;
connectAttr "reindeer__flexi_pow_volume_b01.ox" "reindeer__flexi_twist_02.sy";
connectAttr "reindeer__flexi_pow_volume_b01.ox" "reindeer__flexi_twist_02.sz";
connectAttr "reindeer__flexi_surface_01Shape_twist_follicle_03.or" "reindeer__flexi_surface_01_twist_follicle_03.r"
		 -l on;
connectAttr "reindeer__flexi_surface_01Shape_twist_follicle_03.ot" "reindeer__flexi_surface_01_twist_follicle_03.t"
		 -l on;
connectAttr "reindeer__flexi_surface_01_twist_follicle_03_scaleConstraint1.csx" "reindeer__flexi_surface_01_twist_follicle_03.sx"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_03_scaleConstraint1.csy" "reindeer__flexi_surface_01_twist_follicle_03.sy"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_03_scaleConstraint1.csz" "reindeer__flexi_surface_01_twist_follicle_03.sz"
		;
connectAttr "reindeer__flexi_surface_01Shape.l" "reindeer__flexi_surface_01Shape_twist_follicle_03.is"
		;
connectAttr "reindeer__flexi_surface_01Shape.wm" "reindeer__flexi_surface_01Shape_twist_follicle_03.iwm"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_03.pim" "reindeer__flexi_surface_01_twist_follicle_03_scaleConstraint1.cpim"
		;
connectAttr "reindeer__global_move_ctrl.s" "reindeer__flexi_surface_01_twist_follicle_03_scaleConstraint1.tg[0].ts"
		;
connectAttr "reindeer__global_move_ctrl.pm" "reindeer__flexi_surface_01_twist_follicle_03_scaleConstraint1.tg[0].tpm"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_03_scaleConstraint1.w0" "reindeer__flexi_surface_01_twist_follicle_03_scaleConstraint1.tg[0].tw"
		;
connectAttr "reindeer__flexi_pow_volume_c01.ox" "reindeer__flexi_twist_03.sy";
connectAttr "reindeer__flexi_pow_volume_c01.ox" "reindeer__flexi_twist_03.sz";
connectAttr "reindeer__flexi_surface_01Shape_twist_follicle_04.or" "reindeer__flexi_surface_01_twist_follicle_04.r"
		 -l on;
connectAttr "reindeer__flexi_surface_01Shape_twist_follicle_04.ot" "reindeer__flexi_surface_01_twist_follicle_04.t"
		 -l on;
connectAttr "reindeer__flexi_surface_01_twist_follicle_04_scaleConstraint1.csx" "reindeer__flexi_surface_01_twist_follicle_04.sx"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_04_scaleConstraint1.csy" "reindeer__flexi_surface_01_twist_follicle_04.sy"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_04_scaleConstraint1.csz" "reindeer__flexi_surface_01_twist_follicle_04.sz"
		;
connectAttr "reindeer__flexi_surface_01Shape.l" "reindeer__flexi_surface_01Shape_twist_follicle_04.is"
		;
connectAttr "reindeer__flexi_surface_01Shape.wm" "reindeer__flexi_surface_01Shape_twist_follicle_04.iwm"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_04.pim" "reindeer__flexi_surface_01_twist_follicle_04_scaleConstraint1.cpim"
		;
connectAttr "reindeer__global_move_ctrl.s" "reindeer__flexi_surface_01_twist_follicle_04_scaleConstraint1.tg[0].ts"
		;
connectAttr "reindeer__global_move_ctrl.pm" "reindeer__flexi_surface_01_twist_follicle_04_scaleConstraint1.tg[0].tpm"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_04_scaleConstraint1.w0" "reindeer__flexi_surface_01_twist_follicle_04_scaleConstraint1.tg[0].tw"
		;
connectAttr "reindeer__flexi_pow_volume_d01.ox" "reindeer__flexi_twist_04.sy";
connectAttr "reindeer__flexi_pow_volume_d01.ox" "reindeer__flexi_twist_04.sz";
connectAttr "reindeer__flexi_surface_01Shape_twist_follicle_05.or" "reindeer__flexi_surface_01_twist_follicle_05.r"
		 -l on;
connectAttr "reindeer__flexi_surface_01Shape_twist_follicle_05.ot" "reindeer__flexi_surface_01_twist_follicle_05.t"
		 -l on;
connectAttr "reindeer__flexi_surface_01_twist_follicle_05_scaleConstraint1.csx" "reindeer__flexi_surface_01_twist_follicle_05.sx"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_05_scaleConstraint1.csy" "reindeer__flexi_surface_01_twist_follicle_05.sy"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_05_scaleConstraint1.csz" "reindeer__flexi_surface_01_twist_follicle_05.sz"
		;
connectAttr "reindeer__flexi_surface_01Shape.l" "reindeer__flexi_surface_01Shape_twist_follicle_05.is"
		;
connectAttr "reindeer__flexi_surface_01Shape.wm" "reindeer__flexi_surface_01Shape_twist_follicle_05.iwm"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_05.pim" "reindeer__flexi_surface_01_twist_follicle_05_scaleConstraint1.cpim"
		;
connectAttr "reindeer__global_move_ctrl.s" "reindeer__flexi_surface_01_twist_follicle_05_scaleConstraint1.tg[0].ts"
		;
connectAttr "reindeer__global_move_ctrl.pm" "reindeer__flexi_surface_01_twist_follicle_05_scaleConstraint1.tg[0].tpm"
		;
connectAttr "reindeer__flexi_surface_01_twist_follicle_05_scaleConstraint1.w0" "reindeer__flexi_surface_01_twist_follicle_05_scaleConstraint1.tg[0].tw"
		;
connectAttr "reindeer__flexi_pow_volume_e01.ox" "reindeer__flexi_twist_05.sy";
connectAttr "reindeer__flexi_pow_volume_e01.ox" "reindeer__flexi_twist_05.sz";
connectAttr "rig_r_femur.msg" "r_bk_ik.hsj";
connectAttr "effector2.hp" "r_bk_ik.hee";
connectAttr ":ikSpringSolver.msg" "r_bk_ik.hsv";
connectAttr "r_bk_ik_poleVectorConstraint1.ctx" "r_bk_ik.pvx";
connectAttr "r_bk_ik_poleVectorConstraint1.cty" "r_bk_ik.pvy";
connectAttr "r_bk_ik_poleVectorConstraint1.ctz" "r_bk_ik.pvz";
connectAttr "r_bk_ik.pim" "r_bk_ik_poleVectorConstraint1.cpim";
connectAttr "rig_r_femur.pm" "r_bk_ik_poleVectorConstraint1.ps";
connectAttr "rig_r_femur.t" "r_bk_ik_poleVectorConstraint1.crp";
connectAttr "r_bk_pv.t" "r_bk_ik_poleVectorConstraint1.tg[0].tt";
connectAttr "r_bk_pv.rp" "r_bk_ik_poleVectorConstraint1.tg[0].trp";
connectAttr "r_bk_pv.rpt" "r_bk_ik_poleVectorConstraint1.tg[0].trt";
connectAttr "r_bk_pv.pm" "r_bk_ik_poleVectorConstraint1.tg[0].tpm";
connectAttr "r_bk_ik_poleVectorConstraint1.w0" "r_bk_ik_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "rig_l_femur.msg" "l_bk_ik.hsj";
connectAttr "effector1.hp" "l_bk_ik.hee";
connectAttr ":ikSpringSolver.msg" "l_bk_ik.hsv";
connectAttr "l_bk_ik_poleVectorConstraint1.ctx" "l_bk_ik.pvx";
connectAttr "l_bk_ik_poleVectorConstraint1.cty" "l_bk_ik.pvy";
connectAttr "l_bk_ik_poleVectorConstraint1.ctz" "l_bk_ik.pvz";
connectAttr "l_bk_ik.pim" "l_bk_ik_poleVectorConstraint1.cpim";
connectAttr "rig_l_femur.pm" "l_bk_ik_poleVectorConstraint1.ps";
connectAttr "rig_l_femur.t" "l_bk_ik_poleVectorConstraint1.crp";
connectAttr "l_bk_pv.t" "l_bk_ik_poleVectorConstraint1.tg[0].tt";
connectAttr "l_bk_pv.rp" "l_bk_ik_poleVectorConstraint1.tg[0].trp";
connectAttr "l_bk_pv.rpt" "l_bk_ik_poleVectorConstraint1.tg[0].trt";
connectAttr "l_bk_pv.pm" "l_bk_ik_poleVectorConstraint1.tg[0].tpm";
connectAttr "l_bk_ik_poleVectorConstraint1.w0" "l_bk_ik_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "rig_r_fnt_humerus.msg" "r_fnt_ik.hsj";
connectAttr "effector4.hp" "r_fnt_ik.hee";
connectAttr "ikRPsolver.msg" "r_fnt_ik.hsv";
connectAttr "r_fnt_ik_poleVectorConstraint1.ctx" "r_fnt_ik.pvx";
connectAttr "r_fnt_ik_poleVectorConstraint1.cty" "r_fnt_ik.pvy";
connectAttr "r_fnt_ik_poleVectorConstraint1.ctz" "r_fnt_ik.pvz";
connectAttr "r_fnt_ik.pim" "r_fnt_ik_poleVectorConstraint1.cpim";
connectAttr "rig_r_fnt_humerus.pm" "r_fnt_ik_poleVectorConstraint1.ps";
connectAttr "rig_r_fnt_humerus.t" "r_fnt_ik_poleVectorConstraint1.crp";
connectAttr "f_fnt_pv1.t" "r_fnt_ik_poleVectorConstraint1.tg[0].tt";
connectAttr "f_fnt_pv1.rp" "r_fnt_ik_poleVectorConstraint1.tg[0].trp";
connectAttr "f_fnt_pv1.rpt" "r_fnt_ik_poleVectorConstraint1.tg[0].trt";
connectAttr "f_fnt_pv1.pm" "r_fnt_ik_poleVectorConstraint1.tg[0].tpm";
connectAttr "r_fnt_ik_poleVectorConstraint1.w0" "r_fnt_ik_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "rig_l_fnt_humerus.msg" "l_fnt_ik.hsj";
connectAttr "effector3.hp" "l_fnt_ik.hee";
connectAttr "ikRPsolver.msg" "l_fnt_ik.hsv";
connectAttr "l_fnt_ik_poleVectorConstraint1.ctx" "l_fnt_ik.pvx";
connectAttr "l_fnt_ik_poleVectorConstraint1.cty" "l_fnt_ik.pvy";
connectAttr "l_fnt_ik_poleVectorConstraint1.ctz" "l_fnt_ik.pvz";
connectAttr "l_fnt_ik.pim" "l_fnt_ik_poleVectorConstraint1.cpim";
connectAttr "rig_l_fnt_humerus.pm" "l_fnt_ik_poleVectorConstraint1.ps";
connectAttr "rig_l_fnt_humerus.t" "l_fnt_ik_poleVectorConstraint1.crp";
connectAttr "f_fnt_pv.t" "l_fnt_ik_poleVectorConstraint1.tg[0].tt";
connectAttr "f_fnt_pv.rp" "l_fnt_ik_poleVectorConstraint1.tg[0].trp";
connectAttr "f_fnt_pv.rpt" "l_fnt_ik_poleVectorConstraint1.tg[0].trt";
connectAttr "f_fnt_pv.pm" "l_fnt_ik_poleVectorConstraint1.tg[0].tpm";
connectAttr "l_fnt_ik_poleVectorConstraint1.w0" "l_fnt_ik_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "rig_pelvis.s" "pelvis_scaleConstraint1.tg[0].ts";
connectAttr "rig_pelvis.pm" "pelvis_scaleConstraint1.tg[0].tpm";
connectAttr "pelvis_scaleConstraint1.w0" "pelvis_scaleConstraint1.tg[0].tw";
connectAttr "rig_pelvis.r" "pelvis_orientConstraint1.tg[0].tr";
connectAttr "rig_pelvis.ro" "pelvis_orientConstraint1.tg[0].tro";
connectAttr "rig_pelvis.pm" "pelvis_orientConstraint1.tg[0].tpm";
connectAttr "rig_pelvis.jo" "pelvis_orientConstraint1.tg[0].tjo";
connectAttr "pelvis_orientConstraint1.w0" "pelvis_orientConstraint1.tg[0].tw";
connectAttr "rig_pelvis.t" "pelvis_pointConstraint1.tg[0].tt";
connectAttr "rig_pelvis.rp" "pelvis_pointConstraint1.tg[0].trp";
connectAttr "rig_pelvis.rpt" "pelvis_pointConstraint1.tg[0].trt";
connectAttr "rig_pelvis.pm" "pelvis_pointConstraint1.tg[0].tpm";
connectAttr "pelvis_pointConstraint1.w0" "pelvis_pointConstraint1.tg[0].tw";
connectAttr "rig_hips.s" "hips_scaleConstraint1.tg[0].ts";
connectAttr "rig_hips.pm" "hips_scaleConstraint1.tg[0].tpm";
connectAttr "hips_scaleConstraint1.w0" "hips_scaleConstraint1.tg[0].tw";
connectAttr "rig_hips.r" "hips_orientConstraint1.tg[0].tr";
connectAttr "rig_hips.ro" "hips_orientConstraint1.tg[0].tro";
connectAttr "rig_hips.pm" "hips_orientConstraint1.tg[0].tpm";
connectAttr "rig_hips.jo" "hips_orientConstraint1.tg[0].tjo";
connectAttr "hips_orientConstraint1.w0" "hips_orientConstraint1.tg[0].tw";
connectAttr "rig_hips.t" "hips_pointConstraint1.tg[0].tt";
connectAttr "rig_hips.rp" "hips_pointConstraint1.tg[0].trp";
connectAttr "rig_hips.rpt" "hips_pointConstraint1.tg[0].trt";
connectAttr "rig_hips.pm" "hips_pointConstraint1.tg[0].tpm";
connectAttr "hips_pointConstraint1.w0" "hips_pointConstraint1.tg[0].tw";
connectAttr "rig_r_femur.s" "r_femur_scaleConstraint1.tg[0].ts";
connectAttr "rig_r_femur.pm" "r_femur_scaleConstraint1.tg[0].tpm";
connectAttr "r_femur_scaleConstraint1.w0" "r_femur_scaleConstraint1.tg[0].tw";
connectAttr "rig_r_femur.r" "r_femur_orientConstraint1.tg[0].tr";
connectAttr "rig_r_femur.ro" "r_femur_orientConstraint1.tg[0].tro";
connectAttr "rig_r_femur.pm" "r_femur_orientConstraint1.tg[0].tpm";
connectAttr "rig_r_femur.jo" "r_femur_orientConstraint1.tg[0].tjo";
connectAttr "r_femur_orientConstraint1.w0" "r_femur_orientConstraint1.tg[0].tw";
connectAttr "rig_r_femur.t" "r_femur_pointConstraint1.tg[0].tt";
connectAttr "rig_r_femur.rp" "r_femur_pointConstraint1.tg[0].trp";
connectAttr "rig_r_femur.rpt" "r_femur_pointConstraint1.tg[0].trt";
connectAttr "rig_r_femur.pm" "r_femur_pointConstraint1.tg[0].tpm";
connectAttr "r_femur_pointConstraint1.w0" "r_femur_pointConstraint1.tg[0].tw";
connectAttr "rig_r_tibia.s" "r_tibia_scaleConstraint1.tg[0].ts";
connectAttr "rig_r_tibia.pm" "r_tibia_scaleConstraint1.tg[0].tpm";
connectAttr "r_tibia_scaleConstraint1.w0" "r_tibia_scaleConstraint1.tg[0].tw";
connectAttr "rig_r_tibia.r" "r_tibia_orientConstraint1.tg[0].tr";
connectAttr "rig_r_tibia.ro" "r_tibia_orientConstraint1.tg[0].tro";
connectAttr "rig_r_tibia.pm" "r_tibia_orientConstraint1.tg[0].tpm";
connectAttr "rig_r_tibia.jo" "r_tibia_orientConstraint1.tg[0].tjo";
connectAttr "r_tibia_orientConstraint1.w0" "r_tibia_orientConstraint1.tg[0].tw";
connectAttr "rig_r_tibia.t" "r_tibia_pointConstraint1.tg[0].tt";
connectAttr "rig_r_tibia.rp" "r_tibia_pointConstraint1.tg[0].trp";
connectAttr "rig_r_tibia.rpt" "r_tibia_pointConstraint1.tg[0].trt";
connectAttr "rig_r_tibia.pm" "r_tibia_pointConstraint1.tg[0].tpm";
connectAttr "r_tibia_pointConstraint1.w0" "r_tibia_pointConstraint1.tg[0].tw";
connectAttr "rig_r_metatarsal.s" "r_metatarsal_scaleConstraint1.tg[0].ts";
connectAttr "rig_r_metatarsal.pm" "r_metatarsal_scaleConstraint1.tg[0].tpm";
connectAttr "r_metatarsal_scaleConstraint1.w0" "r_metatarsal_scaleConstraint1.tg[0].tw"
		;
connectAttr "rig_r_metatarsal.r" "r_metatarsal_orientConstraint1.tg[0].tr";
connectAttr "rig_r_metatarsal.ro" "r_metatarsal_orientConstraint1.tg[0].tro";
connectAttr "rig_r_metatarsal.pm" "r_metatarsal_orientConstraint1.tg[0].tpm";
connectAttr "rig_r_metatarsal.jo" "r_metatarsal_orientConstraint1.tg[0].tjo";
connectAttr "r_metatarsal_orientConstraint1.w0" "r_metatarsal_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_r_metatarsal.t" "r_metatarsal_pointConstraint1.tg[0].tt";
connectAttr "rig_r_metatarsal.rp" "r_metatarsal_pointConstraint1.tg[0].trp";
connectAttr "rig_r_metatarsal.rpt" "r_metatarsal_pointConstraint1.tg[0].trt";
connectAttr "rig_r_metatarsal.pm" "r_metatarsal_pointConstraint1.tg[0].tpm";
connectAttr "r_metatarsal_pointConstraint1.w0" "r_metatarsal_pointConstraint1.tg[0].tw"
		;
connectAttr "rig_r_pastern.s" "r_pastern_scaleConstraint1.tg[0].ts";
connectAttr "rig_r_pastern.pm" "r_pastern_scaleConstraint1.tg[0].tpm";
connectAttr "r_pastern_scaleConstraint1.w0" "r_pastern_scaleConstraint1.tg[0].tw"
		;
connectAttr "rig_r_pastern.r" "r_pastern_orientConstraint1.tg[0].tr";
connectAttr "rig_r_pastern.ro" "r_pastern_orientConstraint1.tg[0].tro";
connectAttr "rig_r_pastern.pm" "r_pastern_orientConstraint1.tg[0].tpm";
connectAttr "rig_r_pastern.jo" "r_pastern_orientConstraint1.tg[0].tjo";
connectAttr "r_pastern_orientConstraint1.w0" "r_pastern_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_r_pastern.t" "r_pastern_pointConstraint1.tg[0].tt";
connectAttr "rig_r_pastern.rp" "r_pastern_pointConstraint1.tg[0].trp";
connectAttr "rig_r_pastern.rpt" "r_pastern_pointConstraint1.tg[0].trt";
connectAttr "rig_r_pastern.pm" "r_pastern_pointConstraint1.tg[0].tpm";
connectAttr "r_pastern_pointConstraint1.w0" "r_pastern_pointConstraint1.tg[0].tw"
		;
connectAttr "reindeer__flexi_twist_01.s" "spine_01_scaleConstraint1.tg[0].ts";
connectAttr "reindeer__flexi_twist_01.pm" "spine_01_scaleConstraint1.tg[0].tpm";
connectAttr "spine_01_scaleConstraint1.w0" "spine_01_scaleConstraint1.tg[0].tw";
connectAttr "reindeer__flexi_twist_01.r" "spine_01_orientConstraint1.tg[0].tr";
connectAttr "reindeer__flexi_twist_01.ro" "spine_01_orientConstraint1.tg[0].tro"
		;
connectAttr "reindeer__flexi_twist_01.pm" "spine_01_orientConstraint1.tg[0].tpm"
		;
connectAttr "reindeer__flexi_twist_01.jo" "spine_01_orientConstraint1.tg[0].tjo"
		;
connectAttr "spine_01_orientConstraint1.w0" "spine_01_orientConstraint1.tg[0].tw"
		;
connectAttr "reindeer__flexi_twist_01.t" "spine_01_pointConstraint1.tg[0].tt";
connectAttr "reindeer__flexi_twist_01.rp" "spine_01_pointConstraint1.tg[0].trp";
connectAttr "reindeer__flexi_twist_01.rpt" "spine_01_pointConstraint1.tg[0].trt"
		;
connectAttr "reindeer__flexi_twist_01.pm" "spine_01_pointConstraint1.tg[0].tpm";
connectAttr "spine_01_pointConstraint1.w0" "spine_01_pointConstraint1.tg[0].tw";
connectAttr "reindeer__flexi_twist_02.s" "spine_02_scaleConstraint1.tg[0].ts";
connectAttr "reindeer__flexi_twist_02.pm" "spine_02_scaleConstraint1.tg[0].tpm";
connectAttr "spine_02_scaleConstraint1.w0" "spine_02_scaleConstraint1.tg[0].tw";
connectAttr "reindeer__flexi_twist_02.r" "spine_02_orientConstraint1.tg[0].tr";
connectAttr "reindeer__flexi_twist_02.ro" "spine_02_orientConstraint1.tg[0].tro"
		;
connectAttr "reindeer__flexi_twist_02.pm" "spine_02_orientConstraint1.tg[0].tpm"
		;
connectAttr "reindeer__flexi_twist_02.jo" "spine_02_orientConstraint1.tg[0].tjo"
		;
connectAttr "spine_02_orientConstraint1.w0" "spine_02_orientConstraint1.tg[0].tw"
		;
connectAttr "reindeer__flexi_twist_02.t" "spine_02_pointConstraint1.tg[0].tt";
connectAttr "reindeer__flexi_twist_02.rp" "spine_02_pointConstraint1.tg[0].trp";
connectAttr "reindeer__flexi_twist_02.rpt" "spine_02_pointConstraint1.tg[0].trt"
		;
connectAttr "reindeer__flexi_twist_02.pm" "spine_02_pointConstraint1.tg[0].tpm";
connectAttr "spine_02_pointConstraint1.w0" "spine_02_pointConstraint1.tg[0].tw";
connectAttr "reindeer__flexi_twist_03.s" "spine_03_scaleConstraint1.tg[0].ts";
connectAttr "reindeer__flexi_twist_03.pm" "spine_03_scaleConstraint1.tg[0].tpm";
connectAttr "spine_03_scaleConstraint1.w0" "spine_03_scaleConstraint1.tg[0].tw";
connectAttr "reindeer__flexi_twist_03.r" "spine_03_orientConstraint1.tg[0].tr";
connectAttr "reindeer__flexi_twist_03.ro" "spine_03_orientConstraint1.tg[0].tro"
		;
connectAttr "reindeer__flexi_twist_03.pm" "spine_03_orientConstraint1.tg[0].tpm"
		;
connectAttr "reindeer__flexi_twist_03.jo" "spine_03_orientConstraint1.tg[0].tjo"
		;
connectAttr "spine_03_orientConstraint1.w0" "spine_03_orientConstraint1.tg[0].tw"
		;
connectAttr "reindeer__flexi_twist_03.t" "spine_03_pointConstraint1.tg[0].tt";
connectAttr "reindeer__flexi_twist_03.rp" "spine_03_pointConstraint1.tg[0].trp";
connectAttr "reindeer__flexi_twist_03.rpt" "spine_03_pointConstraint1.tg[0].trt"
		;
connectAttr "reindeer__flexi_twist_03.pm" "spine_03_pointConstraint1.tg[0].tpm";
connectAttr "spine_03_pointConstraint1.w0" "spine_03_pointConstraint1.tg[0].tw";
connectAttr "reindeer__flexi_twist_04.s" "spine_04_scaleConstraint1.tg[0].ts";
connectAttr "reindeer__flexi_twist_04.pm" "spine_04_scaleConstraint1.tg[0].tpm";
connectAttr "spine_04_scaleConstraint1.w0" "spine_04_scaleConstraint1.tg[0].tw";
connectAttr "reindeer__flexi_twist_04.r" "spine_04_orientConstraint1.tg[0].tr";
connectAttr "reindeer__flexi_twist_04.ro" "spine_04_orientConstraint1.tg[0].tro"
		;
connectAttr "reindeer__flexi_twist_04.pm" "spine_04_orientConstraint1.tg[0].tpm"
		;
connectAttr "reindeer__flexi_twist_04.jo" "spine_04_orientConstraint1.tg[0].tjo"
		;
connectAttr "spine_04_orientConstraint1.w0" "spine_04_orientConstraint1.tg[0].tw"
		;
connectAttr "reindeer__flexi_twist_04.t" "spine_04_pointConstraint1.tg[0].tt";
connectAttr "reindeer__flexi_twist_04.rp" "spine_04_pointConstraint1.tg[0].trp";
connectAttr "reindeer__flexi_twist_04.rpt" "spine_04_pointConstraint1.tg[0].trt"
		;
connectAttr "reindeer__flexi_twist_04.pm" "spine_04_pointConstraint1.tg[0].tpm";
connectAttr "spine_04_pointConstraint1.w0" "spine_04_pointConstraint1.tg[0].tw";
connectAttr "reindeer__flexi_twist_05.s" "spine_05_scaleConstraint1.tg[0].ts";
connectAttr "reindeer__flexi_twist_05.pm" "spine_05_scaleConstraint1.tg[0].tpm";
connectAttr "spine_05_scaleConstraint1.w0" "spine_05_scaleConstraint1.tg[0].tw";
connectAttr "reindeer__flexi_twist_05.r" "spine_05_orientConstraint1.tg[0].tr";
connectAttr "reindeer__flexi_twist_05.ro" "spine_05_orientConstraint1.tg[0].tro"
		;
connectAttr "reindeer__flexi_twist_05.pm" "spine_05_orientConstraint1.tg[0].tpm"
		;
connectAttr "reindeer__flexi_twist_05.jo" "spine_05_orientConstraint1.tg[0].tjo"
		;
connectAttr "spine_05_orientConstraint1.w0" "spine_05_orientConstraint1.tg[0].tw"
		;
connectAttr "reindeer__flexi_twist_05.t" "spine_05_pointConstraint1.tg[0].tt";
connectAttr "reindeer__flexi_twist_05.rp" "spine_05_pointConstraint1.tg[0].trp";
connectAttr "reindeer__flexi_twist_05.rpt" "spine_05_pointConstraint1.tg[0].trt"
		;
connectAttr "reindeer__flexi_twist_05.pm" "spine_05_pointConstraint1.tg[0].tpm";
connectAttr "spine_05_pointConstraint1.w0" "spine_05_pointConstraint1.tg[0].tw";
connectAttr "rig_thorax.s" "thorax_scaleConstraint1.tg[0].ts";
connectAttr "rig_thorax.pm" "thorax_scaleConstraint1.tg[0].tpm";
connectAttr "thorax_scaleConstraint1.w0" "thorax_scaleConstraint1.tg[0].tw";
connectAttr "rig_thorax.r" "thorax_orientConstraint1.tg[0].tr";
connectAttr "rig_thorax.ro" "thorax_orientConstraint1.tg[0].tro";
connectAttr "rig_thorax.pm" "thorax_orientConstraint1.tg[0].tpm";
connectAttr "rig_thorax.jo" "thorax_orientConstraint1.tg[0].tjo";
connectAttr "thorax_orientConstraint1.w0" "thorax_orientConstraint1.tg[0].tw";
connectAttr "rig_thorax.t" "thorax_pointConstraint1.tg[0].tt";
connectAttr "rig_thorax.rp" "thorax_pointConstraint1.tg[0].trp";
connectAttr "rig_thorax.rpt" "thorax_pointConstraint1.tg[0].trt";
connectAttr "rig_thorax.pm" "thorax_pointConstraint1.tg[0].tpm";
connectAttr "thorax_pointConstraint1.w0" "thorax_pointConstraint1.tg[0].tw";
connectAttr "rig_neckBase.s" "neckBase_scaleConstraint1.tg[0].ts";
connectAttr "rig_neckBase.pm" "neckBase_scaleConstraint1.tg[0].tpm";
connectAttr "neckBase_scaleConstraint1.w0" "neckBase_scaleConstraint1.tg[0].tw";
connectAttr "rig_neckBase.r" "neckBase_orientConstraint1.tg[0].tr";
connectAttr "rig_neckBase.ro" "neckBase_orientConstraint1.tg[0].tro";
connectAttr "rig_neckBase.pm" "neckBase_orientConstraint1.tg[0].tpm";
connectAttr "rig_neckBase.jo" "neckBase_orientConstraint1.tg[0].tjo";
connectAttr "neckBase_orientConstraint1.w0" "neckBase_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_neckBase.t" "neckBase_pointConstraint1.tg[0].tt";
connectAttr "rig_neckBase.rp" "neckBase_pointConstraint1.tg[0].trp";
connectAttr "rig_neckBase.rpt" "neckBase_pointConstraint1.tg[0].trt";
connectAttr "rig_neckBase.pm" "neckBase_pointConstraint1.tg[0].tpm";
connectAttr "neckBase_pointConstraint1.w0" "neckBase_pointConstraint1.tg[0].tw";
connectAttr "rig_neck.s" "neck_scaleConstraint1.tg[0].ts";
connectAttr "rig_neck.pm" "neck_scaleConstraint1.tg[0].tpm";
connectAttr "neck_scaleConstraint1.w0" "neck_scaleConstraint1.tg[0].tw";
connectAttr "rig_neck.r" "neck_orientConstraint1.tg[0].tr";
connectAttr "rig_neck.ro" "neck_orientConstraint1.tg[0].tro";
connectAttr "rig_neck.pm" "neck_orientConstraint1.tg[0].tpm";
connectAttr "rig_neck.jo" "neck_orientConstraint1.tg[0].tjo";
connectAttr "neck_orientConstraint1.w0" "neck_orientConstraint1.tg[0].tw";
connectAttr "rig_neck.t" "neck_pointConstraint1.tg[0].tt";
connectAttr "rig_neck.rp" "neck_pointConstraint1.tg[0].trp";
connectAttr "rig_neck.rpt" "neck_pointConstraint1.tg[0].trt";
connectAttr "rig_neck.pm" "neck_pointConstraint1.tg[0].tpm";
connectAttr "neck_pointConstraint1.w0" "neck_pointConstraint1.tg[0].tw";
connectAttr "rig_head.s" "head_scaleConstraint1.tg[0].ts";
connectAttr "rig_head.pm" "head_scaleConstraint1.tg[0].tpm";
connectAttr "head_scaleConstraint1.w0" "head_scaleConstraint1.tg[0].tw";
connectAttr "rig_head.r" "head_orientConstraint1.tg[0].tr";
connectAttr "rig_head.ro" "head_orientConstraint1.tg[0].tro";
connectAttr "rig_head.pm" "head_orientConstraint1.tg[0].tpm";
connectAttr "rig_head.jo" "head_orientConstraint1.tg[0].tjo";
connectAttr "head_orientConstraint1.w0" "head_orientConstraint1.tg[0].tw";
connectAttr "rig_head.t" "head_pointConstraint1.tg[0].tt";
connectAttr "rig_head.rp" "head_pointConstraint1.tg[0].trp";
connectAttr "rig_head.rpt" "head_pointConstraint1.tg[0].trt";
connectAttr "rig_head.pm" "head_pointConstraint1.tg[0].tpm";
connectAttr "head_pointConstraint1.w0" "head_pointConstraint1.tg[0].tw";
connectAttr "rig_r_fnt_humerus.s" "r_fnt_humerus_scaleConstraint1.tg[0].ts";
connectAttr "rig_r_fnt_humerus.pm" "r_fnt_humerus_scaleConstraint1.tg[0].tpm";
connectAttr "r_fnt_humerus_scaleConstraint1.w0" "r_fnt_humerus_scaleConstraint1.tg[0].tw"
		;
connectAttr "rig_r_fnt_humerus.r" "r_fnt_humerus_orientConstraint1.tg[0].tr";
connectAttr "rig_r_fnt_humerus.ro" "r_fnt_humerus_orientConstraint1.tg[0].tro";
connectAttr "rig_r_fnt_humerus.pm" "r_fnt_humerus_orientConstraint1.tg[0].tpm";
connectAttr "rig_r_fnt_humerus.jo" "r_fnt_humerus_orientConstraint1.tg[0].tjo";
connectAttr "r_fnt_humerus_orientConstraint1.w0" "r_fnt_humerus_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_r_fnt_humerus.t" "r_fnt_humerus_pointConstraint1.tg[0].tt";
connectAttr "rig_r_fnt_humerus.rp" "r_fnt_humerus_pointConstraint1.tg[0].trp";
connectAttr "rig_r_fnt_humerus.rpt" "r_fnt_humerus_pointConstraint1.tg[0].trt";
connectAttr "rig_r_fnt_humerus.pm" "r_fnt_humerus_pointConstraint1.tg[0].tpm";
connectAttr "r_fnt_humerus_pointConstraint1.w0" "r_fnt_humerus_pointConstraint1.tg[0].tw"
		;
connectAttr "rig_r_fnt_metacarpal.s" "r_fnt_metacarpal_scaleConstraint1.tg[0].ts"
		;
connectAttr "rig_r_fnt_metacarpal.pm" "r_fnt_metacarpal_scaleConstraint1.tg[0].tpm"
		;
connectAttr "r_fnt_metacarpal_scaleConstraint1.w0" "r_fnt_metacarpal_scaleConstraint1.tg[0].tw"
		;
connectAttr "rig_r_fnt_metacarpal.r" "r_fnt_metacarpal_orientConstraint1.tg[0].tr"
		;
connectAttr "rig_r_fnt_metacarpal.ro" "r_fnt_metacarpal_orientConstraint1.tg[0].tro"
		;
connectAttr "rig_r_fnt_metacarpal.pm" "r_fnt_metacarpal_orientConstraint1.tg[0].tpm"
		;
connectAttr "rig_r_fnt_metacarpal.jo" "r_fnt_metacarpal_orientConstraint1.tg[0].tjo"
		;
connectAttr "r_fnt_metacarpal_orientConstraint1.w0" "r_fnt_metacarpal_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_r_fnt_metacarpal.t" "r_fnt_metacarpal_pointConstraint1.tg[0].tt"
		;
connectAttr "rig_r_fnt_metacarpal.rp" "r_fnt_metacarpal_pointConstraint1.tg[0].trp"
		;
connectAttr "rig_r_fnt_metacarpal.rpt" "r_fnt_metacarpal_pointConstraint1.tg[0].trt"
		;
connectAttr "rig_r_fnt_metacarpal.pm" "r_fnt_metacarpal_pointConstraint1.tg[0].tpm"
		;
connectAttr "r_fnt_metacarpal_pointConstraint1.w0" "r_fnt_metacarpal_pointConstraint1.tg[0].tw"
		;
connectAttr "rig_r_fnt_pastern.s" "r_fnt_pastern_scaleConstraint1.tg[0].ts";
connectAttr "rig_r_fnt_pastern.pm" "r_fnt_pastern_scaleConstraint1.tg[0].tpm";
connectAttr "r_fnt_pastern_scaleConstraint1.w0" "r_fnt_pastern_scaleConstraint1.tg[0].tw"
		;
connectAttr "rig_r_fnt_pastern.r" "r_fnt_pastern_orientConstraint1.tg[0].tr";
connectAttr "rig_r_fnt_pastern.ro" "r_fnt_pastern_orientConstraint1.tg[0].tro";
connectAttr "rig_r_fnt_pastern.pm" "r_fnt_pastern_orientConstraint1.tg[0].tpm";
connectAttr "rig_r_fnt_pastern.jo" "r_fnt_pastern_orientConstraint1.tg[0].tjo";
connectAttr "r_fnt_pastern_orientConstraint1.w0" "r_fnt_pastern_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_r_fnt_pastern.t" "r_fnt_pastern_pointConstraint1.tg[0].tt";
connectAttr "rig_r_fnt_pastern.rp" "r_fnt_pastern_pointConstraint1.tg[0].trp";
connectAttr "rig_r_fnt_pastern.rpt" "r_fnt_pastern_pointConstraint1.tg[0].trt";
connectAttr "rig_r_fnt_pastern.pm" "r_fnt_pastern_pointConstraint1.tg[0].tpm";
connectAttr "r_fnt_pastern_pointConstraint1.w0" "r_fnt_pastern_pointConstraint1.tg[0].tw"
		;
connectAttr "rig_l_fnt_humerus.s" "l_fnt_humerus_scaleConstraint1.tg[0].ts";
connectAttr "rig_l_fnt_humerus.pm" "l_fnt_humerus_scaleConstraint1.tg[0].tpm";
connectAttr "l_fnt_humerus_scaleConstraint1.w0" "l_fnt_humerus_scaleConstraint1.tg[0].tw"
		;
connectAttr "rig_l_fnt_humerus.r" "l_fnt_humerus_orientConstraint1.tg[0].tr";
connectAttr "rig_l_fnt_humerus.ro" "l_fnt_humerus_orientConstraint1.tg[0].tro";
connectAttr "rig_l_fnt_humerus.pm" "l_fnt_humerus_orientConstraint1.tg[0].tpm";
connectAttr "rig_l_fnt_humerus.jo" "l_fnt_humerus_orientConstraint1.tg[0].tjo";
connectAttr "l_fnt_humerus_orientConstraint1.w0" "l_fnt_humerus_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_l_fnt_humerus.t" "l_fnt_humerus_pointConstraint1.tg[0].tt";
connectAttr "rig_l_fnt_humerus.rp" "l_fnt_humerus_pointConstraint1.tg[0].trp";
connectAttr "rig_l_fnt_humerus.rpt" "l_fnt_humerus_pointConstraint1.tg[0].trt";
connectAttr "rig_l_fnt_humerus.pm" "l_fnt_humerus_pointConstraint1.tg[0].tpm";
connectAttr "l_fnt_humerus_pointConstraint1.w0" "l_fnt_humerus_pointConstraint1.tg[0].tw"
		;
connectAttr "rig_l_fnt_metacarpal.s" "l_fnt_metacarpal_scaleConstraint1.tg[0].ts"
		;
connectAttr "rig_l_fnt_metacarpal.pm" "l_fnt_metacarpal_scaleConstraint1.tg[0].tpm"
		;
connectAttr "l_fnt_metacarpal_scaleConstraint1.w0" "l_fnt_metacarpal_scaleConstraint1.tg[0].tw"
		;
connectAttr "rig_l_fnt_metacarpal.r" "l_fnt_metacarpal_orientConstraint1.tg[0].tr"
		;
connectAttr "rig_l_fnt_metacarpal.ro" "l_fnt_metacarpal_orientConstraint1.tg[0].tro"
		;
connectAttr "rig_l_fnt_metacarpal.pm" "l_fnt_metacarpal_orientConstraint1.tg[0].tpm"
		;
connectAttr "rig_l_fnt_metacarpal.jo" "l_fnt_metacarpal_orientConstraint1.tg[0].tjo"
		;
connectAttr "l_fnt_metacarpal_orientConstraint1.w0" "l_fnt_metacarpal_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_l_fnt_metacarpal.t" "l_fnt_metacarpal_pointConstraint1.tg[0].tt"
		;
connectAttr "rig_l_fnt_metacarpal.rp" "l_fnt_metacarpal_pointConstraint1.tg[0].trp"
		;
connectAttr "rig_l_fnt_metacarpal.rpt" "l_fnt_metacarpal_pointConstraint1.tg[0].trt"
		;
connectAttr "rig_l_fnt_metacarpal.pm" "l_fnt_metacarpal_pointConstraint1.tg[0].tpm"
		;
connectAttr "l_fnt_metacarpal_pointConstraint1.w0" "l_fnt_metacarpal_pointConstraint1.tg[0].tw"
		;
connectAttr "rig_l_fnt_pastern.s" "l_fnt_pastern_scaleConstraint1.tg[0].ts";
connectAttr "rig_l_fnt_pastern.pm" "l_fnt_pastern_scaleConstraint1.tg[0].tpm";
connectAttr "l_fnt_pastern_scaleConstraint1.w0" "l_fnt_pastern_scaleConstraint1.tg[0].tw"
		;
connectAttr "rig_l_fnt_pastern.r" "l_fnt_pastern_orientConstraint1.tg[0].tr";
connectAttr "rig_l_fnt_pastern.ro" "l_fnt_pastern_orientConstraint1.tg[0].tro";
connectAttr "rig_l_fnt_pastern.pm" "l_fnt_pastern_orientConstraint1.tg[0].tpm";
connectAttr "rig_l_fnt_pastern.jo" "l_fnt_pastern_orientConstraint1.tg[0].tjo";
connectAttr "l_fnt_pastern_orientConstraint1.w0" "l_fnt_pastern_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_l_fnt_pastern.t" "l_fnt_pastern_pointConstraint1.tg[0].tt";
connectAttr "rig_l_fnt_pastern.rp" "l_fnt_pastern_pointConstraint1.tg[0].trp";
connectAttr "rig_l_fnt_pastern.rpt" "l_fnt_pastern_pointConstraint1.tg[0].trt";
connectAttr "rig_l_fnt_pastern.pm" "l_fnt_pastern_pointConstraint1.tg[0].tpm";
connectAttr "l_fnt_pastern_pointConstraint1.w0" "l_fnt_pastern_pointConstraint1.tg[0].tw"
		;
connectAttr "rig_l_femur.s" "l_femur_scaleConstraint1.tg[0].ts";
connectAttr "rig_l_femur.pm" "l_femur_scaleConstraint1.tg[0].tpm";
connectAttr "l_femur_scaleConstraint1.w0" "l_femur_scaleConstraint1.tg[0].tw";
connectAttr "rig_l_femur.r" "l_femur_orientConstraint1.tg[0].tr";
connectAttr "rig_l_femur.ro" "l_femur_orientConstraint1.tg[0].tro";
connectAttr "rig_l_femur.pm" "l_femur_orientConstraint1.tg[0].tpm";
connectAttr "rig_l_femur.jo" "l_femur_orientConstraint1.tg[0].tjo";
connectAttr "l_femur_orientConstraint1.w0" "l_femur_orientConstraint1.tg[0].tw";
connectAttr "rig_l_femur.t" "l_femur_pointConstraint1.tg[0].tt";
connectAttr "rig_l_femur.rp" "l_femur_pointConstraint1.tg[0].trp";
connectAttr "rig_l_femur.rpt" "l_femur_pointConstraint1.tg[0].trt";
connectAttr "rig_l_femur.pm" "l_femur_pointConstraint1.tg[0].tpm";
connectAttr "l_femur_pointConstraint1.w0" "l_femur_pointConstraint1.tg[0].tw";
connectAttr "rig_l_tibia.s" "l_tibia_scaleConstraint1.tg[0].ts";
connectAttr "rig_l_tibia.pm" "l_tibia_scaleConstraint1.tg[0].tpm";
connectAttr "l_tibia_scaleConstraint1.w0" "l_tibia_scaleConstraint1.tg[0].tw";
connectAttr "rig_l_tibia.r" "l_tibia_orientConstraint1.tg[0].tr";
connectAttr "rig_l_tibia.ro" "l_tibia_orientConstraint1.tg[0].tro";
connectAttr "rig_l_tibia.pm" "l_tibia_orientConstraint1.tg[0].tpm";
connectAttr "rig_l_tibia.jo" "l_tibia_orientConstraint1.tg[0].tjo";
connectAttr "l_tibia_orientConstraint1.w0" "l_tibia_orientConstraint1.tg[0].tw";
connectAttr "rig_l_tibia.t" "l_tibia_pointConstraint1.tg[0].tt";
connectAttr "rig_l_tibia.rp" "l_tibia_pointConstraint1.tg[0].trp";
connectAttr "rig_l_tibia.rpt" "l_tibia_pointConstraint1.tg[0].trt";
connectAttr "rig_l_tibia.pm" "l_tibia_pointConstraint1.tg[0].tpm";
connectAttr "l_tibia_pointConstraint1.w0" "l_tibia_pointConstraint1.tg[0].tw";
connectAttr "rig_l_metatarsal.s" "l_metatarsal_scaleConstraint1.tg[0].ts";
connectAttr "rig_l_metatarsal.pm" "l_metatarsal_scaleConstraint1.tg[0].tpm";
connectAttr "l_metatarsal_scaleConstraint1.w0" "l_metatarsal_scaleConstraint1.tg[0].tw"
		;
connectAttr "rig_l_metatarsal.r" "l_metatarsal_orientConstraint1.tg[0].tr";
connectAttr "rig_l_metatarsal.ro" "l_metatarsal_orientConstraint1.tg[0].tro";
connectAttr "rig_l_metatarsal.pm" "l_metatarsal_orientConstraint1.tg[0].tpm";
connectAttr "rig_l_metatarsal.jo" "l_metatarsal_orientConstraint1.tg[0].tjo";
connectAttr "l_metatarsal_orientConstraint1.w0" "l_metatarsal_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_l_metatarsal.t" "l_metatarsal_pointConstraint1.tg[0].tt";
connectAttr "rig_l_metatarsal.rp" "l_metatarsal_pointConstraint1.tg[0].trp";
connectAttr "rig_l_metatarsal.rpt" "l_metatarsal_pointConstraint1.tg[0].trt";
connectAttr "rig_l_metatarsal.pm" "l_metatarsal_pointConstraint1.tg[0].tpm";
connectAttr "l_metatarsal_pointConstraint1.w0" "l_metatarsal_pointConstraint1.tg[0].tw"
		;
connectAttr "rig_l_pastern.s" "l_pastern_scaleConstraint1.tg[0].ts";
connectAttr "rig_l_pastern.pm" "l_pastern_scaleConstraint1.tg[0].tpm";
connectAttr "l_pastern_scaleConstraint1.w0" "l_pastern_scaleConstraint1.tg[0].tw"
		;
connectAttr "rig_l_pastern.r" "l_pastern_orientConstraint1.tg[0].tr";
connectAttr "rig_l_pastern.ro" "l_pastern_orientConstraint1.tg[0].tro";
connectAttr "rig_l_pastern.pm" "l_pastern_orientConstraint1.tg[0].tpm";
connectAttr "rig_l_pastern.jo" "l_pastern_orientConstraint1.tg[0].tjo";
connectAttr "l_pastern_orientConstraint1.w0" "l_pastern_orientConstraint1.tg[0].tw"
		;
connectAttr "rig_l_pastern.t" "l_pastern_pointConstraint1.tg[0].tt";
connectAttr "rig_l_pastern.rp" "l_pastern_pointConstraint1.tg[0].trp";
connectAttr "rig_l_pastern.rpt" "l_pastern_pointConstraint1.tg[0].trt";
connectAttr "rig_l_pastern.pm" "l_pastern_pointConstraint1.tg[0].tpm";
connectAttr "l_pastern_pointConstraint1.w0" "l_pastern_pointConstraint1.tg[0].tw"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "Reindeer_SkinRNfosterParent1.msg" "Reindeer_SkinRN.fp";
connectAttr "layerManager.dli[1]" "ReindeerMeshLayer.id";
connectAttr "reindeer__flexi_bsShps_surface_01GroupParts.og" "reindeer__flexi_bsShps_surface_01.ip[0].ig"
		;
connectAttr "reindeer__flexi_bsShps_surface_01GroupId.id" "reindeer__flexi_bsShps_surface_01.ip[0].gi"
		;
connectAttr "reindeer__flexi_surface_bsShps_01Shape.ws" "reindeer__flexi_bsShps_surface_01.it[0].itg[0].iti[6000].igt"
		;
connectAttr "groupParts8.og" "tweak4.ip[0].ig";
connectAttr "groupId8.id" "tweak4.ip[0].gi";
connectAttr "reindeer__flexi_bsShps_surface_01GroupId.msg" "reindeer__flexi_bsShps_surface_01Set.gn"
		 -na;
connectAttr "reindeer__flexi_surface_01Shape.iog.og[0]" "reindeer__flexi_bsShps_surface_01Set.dsm"
		 -na;
connectAttr "reindeer__flexi_bsShps_surface_01.msg" "reindeer__flexi_bsShps_surface_01Set.ub[0]"
		;
connectAttr "tweak4.og[0]" "reindeer__flexi_bsShps_surface_01GroupParts.ig";
connectAttr "reindeer__flexi_bsShps_surface_01GroupId.id" "reindeer__flexi_bsShps_surface_01GroupParts.gi"
		;
connectAttr "groupId8.msg" "tweakSet4.gn" -na;
connectAttr "reindeer__flexi_surface_01Shape.iog.og[1]" "tweakSet4.dsm" -na;
connectAttr "tweak4.msg" "tweakSet4.ub[0]";
connectAttr "reindeer__flexi_surface_01ShapeOrig.ws" "groupParts8.ig";
connectAttr "groupId8.id" "groupParts8.gi";
connectAttr "reindeer__flexi_start_ctrl.rx" "reindeer__flexi_twist_06.sa";
connectAttr "unitConversion7.o" "reindeer__flexi_twist_06.ea";
connectAttr "twist1GroupParts.og" "reindeer__flexi_twist_06.ip[0].ig";
connectAttr "twist1GroupId.id" "reindeer__flexi_twist_06.ip[0].gi";
connectAttr "reindeer__flexi_twistHdl_01Shape.dd" "reindeer__flexi_twist_06.dd";
connectAttr "reindeer__flexi_twistHdl_01.wm" "reindeer__flexi_twist_06.ma";
connectAttr "groupParts10.og" "tweak5.ip[0].ig";
connectAttr "groupId10.id" "tweak5.ip[0].gi";
connectAttr "twist1GroupId.msg" "twist1Set.gn" -na;
connectAttr "reindeer__flexi_surface_bsShps_01Shape.iog.og[0]" "twist1Set.dsm" -na
		;
connectAttr "reindeer__flexi_twist_06.msg" "twist1Set.ub[0]";
connectAttr "tweak5.og[0]" "twist1GroupParts.ig";
connectAttr "twist1GroupId.id" "twist1GroupParts.gi";
connectAttr "groupId10.msg" "tweakSet5.gn" -na;
connectAttr "reindeer__flexi_surface_bsShps_01Shape.iog.og[1]" "tweakSet5.dsm" -na
		;
connectAttr "tweak5.msg" "tweakSet5.ub[0]";
connectAttr "reindeer__flexi_surface_bsShps_01ShapeOrig.ws" "groupParts10.ig";
connectAttr "groupId10.id" "groupParts10.gi";
connectAttr "reindeer__flexi_wire_surface_01GroupParts.og" "reindeer__flexi_wire_surface_01.ip[0].ig"
		;
connectAttr "reindeer__flexi_wire_surface_01GroupId.id" "reindeer__flexi_wire_surface_01.ip[0].gi"
		;
connectAttr "reindeer__flexi_cWire_surface_01BaseWireShape.ws" "reindeer__flexi_wire_surface_01.bw[0]"
		;
connectAttr "reindeer__flexi_cWire_surface_0Shape1.ws" "reindeer__flexi_wire_surface_01.dw[0]"
		;
connectAttr "reindeer__flexi_wire_surface_01GroupId.msg" "reindeer__flexi_wire_surface_01Set.gn"
		 -na;
connectAttr "reindeer__flexi_surface_bsShps_01Shape.iog.og[2]" "reindeer__flexi_wire_surface_01Set.dsm"
		 -na;
connectAttr "reindeer__flexi_wire_surface_01.msg" "reindeer__flexi_wire_surface_01Set.ub[0]"
		;
connectAttr "reindeer__flexi_twist_06.og[0]" "reindeer__flexi_wire_surface_01GroupParts.ig"
		;
connectAttr "reindeer__flexi_wire_surface_01GroupId.id" "reindeer__flexi_wire_surface_01GroupParts.gi"
		;
connectAttr "skinCluster4GroupParts.og" "skinCluster4.ip[0].ig";
connectAttr "skinCluster4GroupId.id" "skinCluster4.ip[0].gi";
connectAttr "bindPose2.msg" "skinCluster4.bp";
connectAttr "reindeer__flexi_jnt_mid_01.wm" "skinCluster4.ma[0]";
connectAttr "reindeer__flexi_jnt_start_01.wm" "skinCluster4.ma[1]";
connectAttr "reindeer__flexi_jnt_end_01.wm" "skinCluster4.ma[2]";
connectAttr "reindeer__flexi_jnt_mid_01.liw" "skinCluster4.lw[0]";
connectAttr "reindeer__flexi_jnt_start_01.liw" "skinCluster4.lw[1]";
connectAttr "reindeer__flexi_jnt_end_01.liw" "skinCluster4.lw[2]";
connectAttr "reindeer__flexi_jnt_mid_01.obcc" "skinCluster4.ifcl[0]";
connectAttr "reindeer__flexi_jnt_start_01.obcc" "skinCluster4.ifcl[1]";
connectAttr "reindeer__flexi_jnt_end_01.obcc" "skinCluster4.ifcl[2]";
connectAttr "groupParts12.og" "tweak6.ip[0].ig";
connectAttr "groupId12.id" "tweak6.ip[0].gi";
connectAttr "skinCluster4GroupId.msg" "skinCluster4Set.gn" -na;
connectAttr "reindeer__flexi_cWire_surface_0Shape1.iog.og[0]" "skinCluster4Set.dsm"
		 -na;
connectAttr "skinCluster4.msg" "skinCluster4Set.ub[0]";
connectAttr "tweak6.og[0]" "skinCluster4GroupParts.ig";
connectAttr "skinCluster4GroupId.id" "skinCluster4GroupParts.gi";
connectAttr "groupId12.msg" "tweakSet6.gn" -na;
connectAttr "reindeer__flexi_cWire_surface_0Shape1.iog.og[1]" "tweakSet6.dsm" -na
		;
connectAttr "tweak6.msg" "tweakSet6.ub[0]";
connectAttr "reindeer__flexi_cWire_surface_0Shape1Orig.ws" "groupParts12.ig";
connectAttr "groupId12.id" "groupParts12.gi";
connectAttr "reindeer__flexiPlane_grp_01.msg" "bindPose2.m[0]";
connectAttr "reindeer__flexi_hidden.msg" "bindPose2.m[1]";
connectAttr "reindeer__flexi_jnt_cWire_01.msg" "bindPose2.m[2]";
connectAttr "reindeer__flexi_grp_mid_01.msg" "bindPose2.m[3]";
connectAttr "reindeer__flexi_jnt_mid_01.msg" "bindPose2.m[4]";
connectAttr "reindeer__flexi_grp_start_01.msg" "bindPose2.m[5]";
connectAttr "reindeer__flexi_jnt_start_01.msg" "bindPose2.m[6]";
connectAttr "reindeer__flexi_grp_end_01.msg" "bindPose2.m[7]";
connectAttr "reindeer__flexi_jnt_end_01.msg" "bindPose2.m[8]";
connectAttr "bindPose2.w" "bindPose2.p[0]";
connectAttr "bindPose2.m[0]" "bindPose2.p[1]";
connectAttr "bindPose2.m[1]" "bindPose2.p[2]";
connectAttr "bindPose2.m[2]" "bindPose2.p[3]";
connectAttr "bindPose2.m[3]" "bindPose2.p[4]";
connectAttr "bindPose2.m[2]" "bindPose2.p[5]";
connectAttr "bindPose2.m[5]" "bindPose2.p[6]";
connectAttr "bindPose2.m[2]" "bindPose2.p[7]";
connectAttr "bindPose2.m[7]" "bindPose2.p[8]";
connectAttr "reindeer__flexi_jnt_mid_01.bps" "bindPose2.wm[4]";
connectAttr "reindeer__flexi_jnt_start_01.bps" "bindPose2.wm[6]";
connectAttr "reindeer__flexi_jnt_end_01.bps" "bindPose2.wm[8]";
connectAttr "unitConversion1.o" "reindeer__flexi_pma_twist_end_mid01.i1[0]";
connectAttr "unitConversion2.o" "reindeer__flexi_pma_twist_end_mid01.i1[1]";
connectAttr "unitConversion3.o" "reindeer__flexi_pma_twist_end_all01.i1[0]";
connectAttr "unitConversion4.o" "reindeer__flexi_pma_twist_end_all01.i1[1]";
connectAttr "unitConversion5.o" "reindeer__flexi_pma_twist_end_all01.i1[2]";
connectAttr "reindeer__flexi_end_ctrl.rx" "unitConversion1.i";
connectAttr "reindeer__flexi_mid_ctrl.rx" "unitConversion2.i";
connectAttr "reindeer__flexi_start_ctrl.rx" "unitConversion3.i";
connectAttr "reindeer__flexi_mid_ctrl.rx" "unitConversion4.i";
connectAttr "reindeer__flexi_end_ctrl.rx" "unitConversion5.i";
connectAttr "reindeer__flexi_end_ctrl.followMid" "reindeer__flexi_cond_twist_end01.ft"
		;
connectAttr "reindeer__flexi_cond_twist_mid01.ocr" "reindeer__flexi_cond_twist_end01.ctr"
		;
connectAttr "unitConversion6.o" "reindeer__flexi_cond_twist_end01.cfr";
connectAttr "reindeer__flexi_mid_ctrl.followStart" "reindeer__flexi_cond_twist_mid01.ft"
		;
connectAttr "reindeer__flexi_pma_twist_end_all01.o1" "reindeer__flexi_cond_twist_mid01.ctr"
		;
connectAttr "reindeer__flexi_pma_twist_end_mid01.o1" "reindeer__flexi_cond_twist_mid01.cfr"
		;
connectAttr "reindeer__flexi_end_ctrl.rx" "unitConversion6.i";
connectAttr "reindeer__flexi_cond_twist_end01.ocr" "unitConversion7.i";
connectAttr "reindeer__flexi_cWire_surface_0Shape1.ws" "reindeer__flexiArcLen_node_01.ic"
		;
connectAttr "reindeer__global_move_ctrl.autoVolume" "reindeer__flexi_cond_volume01.ft"
		;
connectAttr "reindeer__flexi_inverse_volume01.ox" "reindeer__flexi_cond_volume01.ctr"
		;
connectAttr "reindeer__flexiArcLen_node_01.al" "reindeer__flexi_div_volume01.i1x"
		;
connectAttr "reindeer__flexi_div_volume01.ox" "reindeer__flexi_inverse_volume01.i2x"
		;
connectAttr "reindeer__flexi_cond_volume01.ocr" "reindeer__flexi_pow_volume_a01.i1x"
		;
connectAttr "reindeer__midCtrl_01.magnitude" "reindeer__flexi_pow_volume_a01.i2x"
		;
connectAttr "reindeer__flexi_cond_volume01.ocr" "reindeer__flexi_pow_volume_b01.i1x"
		;
connectAttr "reindeer__midCtrl_02.magnitude" "reindeer__flexi_pow_volume_b01.i2x"
		;
connectAttr "reindeer__flexi_cond_volume01.ocr" "reindeer__flexi_pow_volume_c01.i1x"
		;
connectAttr "reindeer__midCtrl_03.magnitude" "reindeer__flexi_pow_volume_c01.i2x"
		;
connectAttr "reindeer__flexi_cond_volume01.ocr" "reindeer__flexi_pow_volume_d01.i1x"
		;
connectAttr "reindeer__midCtrl_04.magnitude" "reindeer__flexi_pow_volume_d01.i2x"
		;
connectAttr "reindeer__flexi_cond_volume01.ocr" "reindeer__flexi_pow_volume_e01.i1x"
		;
connectAttr "reindeer__midCtrl_05.magnitude" "reindeer__flexi_pow_volume_e01.i2x"
		;
connectAttr "reindeer__flexi_pma_twist_end_mid01.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "reindeer__flexi_pma_twist_end_all01.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "reindeer__flexi_cond_twist_end01.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "reindeer__flexi_cond_twist_mid01.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "reindeer__flexi_cond_volume01.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "reindeer__flexi_div_volume01.msg" ":defaultRenderUtilityList1.u" -na
		;
connectAttr "reindeer__flexi_inverse_volume01.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "reindeer__flexi_pow_volume_a01.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "reindeer__flexi_pow_volume_b01.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "reindeer__flexi_pow_volume_c01.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "reindeer__flexi_pow_volume_d01.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "reindeer__flexi_pow_volume_e01.msg" ":defaultRenderUtilityList1.u" 
		-na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "reindeer__flexi_surface_01Shape.iog" ":initialShadingGroup.dsm" -na
		;
connectAttr "reindeer__flexi_surface_bsShps_01Shape.iog" ":initialShadingGroup.dsm"
		 -na;
connectAttr "ikRPsolver.msg" ":ikSystem.sol" -na;
// End of Reindeer_Rig.ma
