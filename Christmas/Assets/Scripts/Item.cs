﻿using UnityEngine;
using System.Collections;

[System.Serializable]

// This gives us a list.  Something like an array, but we don't have to define the size.
public class Item
{
	public string itemName;
	public int toyIdItem;
	public string itemDesc;
	public Texture2D itemIcon;
	public int itemPower;
	public int itemSpeed;
	public ItemType itemType;

	public enum ItemType {
		house,
		appartment,
		townhouse
		}
	public Item(string kidsName, int toyId, string toyName, int score, int speed, ItemType type)
		{
			itemName = kidsName;
			toyIdItem = toyId;
			itemDesc = toyName;
			itemIcon = Resources.Load<Texture2D>("icons/" + toyName);
			itemPower = score;
			itemSpeed = speed;
			itemType = type;


		}
	public Item()
	{

	}


}
