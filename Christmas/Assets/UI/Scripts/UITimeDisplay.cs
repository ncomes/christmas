﻿using UnityEngine;
using System.Collections;

public class UITimeDisplay : MonoBehaviour
{
    public static UITimeDisplay instance;
    public GameObject ScreenParent;
    public UISlider slider;

    void Awake()
    {
        if (!instance)
            instance = this;
        else
            Debug.LogWarning(gameObject.name + " is trying to create a new UITimeDisplay UI script and there can be only one! Current: " + instance.gameObject.name);

        UpdateUITime(1.0f);
        HideUI();
    }

    public static void Show() { UITimeDisplay.instance.ShowUI(); }
    public void ShowUI()
    {
        ScreenParent.SetActive(true);
    }

    public static void Hide() { UITimeDisplay.instance.HideUI(); }
    public void HideUI()
    {
        ScreenParent.SetActive(false);
    }

    public static void UpdateTime(float pctLeft) { UITimeDisplay.instance.UpdateUITime(pctLeft); }
    public void UpdateUITime(float pctLeft)
    {
        slider.value = pctLeft;
    }
}
