﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum eChimneyTargetScore
{
    CTS_PERFECT,
    CTS_GREAT,
    CTS_GOOD,
    CTS_BAD
}

public class ChimneyTarget : MonoBehaviour
{
    public eChimneyTargetScore Score;
    
}
