﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class UIScoreLinePrefabs
{
    public eChimneyTargetScore Score;
    public GameObject UIPrefab;
}

public class UIScoreLine : MonoBehaviour
{
    public static UIScoreLine instance;
    public List<UIScoreLinePrefabs> ScoreResponsePrefabs = new List<UIScoreLinePrefabs>();

    void Awake()
    {
        if (!instance)
            instance = this;
        else
            Debug.LogWarning(gameObject.name + " is trying to create a new ScoreLine UI script and there can be only one! Current: " + instance.gameObject.name);
    }

    public static void ShowScore(eChimneyTargetScore score) { UIScoreLine.instance.ShowChimneyScore(score); }
    public void ShowChimneyScore(eChimneyTargetScore score)
    {
        foreach(UIScoreLinePrefabs pref in ScoreResponsePrefabs)
        {
            if (pref.Score == score)
            {
                NGUITools.AddChild(gameObject, pref.UIPrefab);
            }
        }
    }


}
