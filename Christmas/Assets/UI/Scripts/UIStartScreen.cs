﻿using UnityEngine;
using System.Collections;

public class UIStartScreen : MonoBehaviour {

    public static UIStartScreen instance;
    public GameObject ScreenParent;
    public UILabel CountdownText;

    void Awake()
    {
        if (!instance)
            instance = this;
        else
            Debug.LogWarning(gameObject.name + " is trying to create a new StartScreen UI script and there can be only one! Current: " + instance.gameObject.name);

        HideUI();
    }

    public static void Show() { UIStartScreen.instance.ShowUI(); }
    public void ShowUI()
    {
        ScreenParent.SetActive(true);
    }

    public static void Hide() { UIStartScreen.instance.HideUI(); }
    public void HideUI()
    {
        ScreenParent.SetActive(false);
        CountdownText.text = "";
    }

    public static void UpdateCountdown(int val) { UIStartScreen.instance.UpdateUICountdown(val); }
    public void UpdateUICountdown(int val)
    {
        CountdownText.text = val == 0 ? "START" : val.ToString();
    }
}
