//Maya ASCII 2016 scene
//Name: Reindeer_Skin.ma
//Last modified: Sat, Nov 21, 2015 08:52:17 PM
//Codeset: 1252
file -rdi 1 -dns -rpr "Reindeer_Skeleton" -rfn "Reindeer_SkeletonRN" -typ "mayaAscii"
		 "C:/Users/Nathan/Documents/christmas/SourceContent/Reindeer/Rig/Reindeer_Skeleton.ma";
file -rdi 1 -dns -rpr "Reindeer" -rfn "ReindeerRN" -typ "mayaAscii" "C:/Users/Nathan/Documents/christmas/SourceContent/Reindeer/Model/Reindeer.ma";
file -r -dns -rpr "Reindeer_Skeleton" -dr 1 -rfn "Reindeer_SkeletonRN" -typ "mayaAscii"
		 "C:/Users/Nathan/Documents/christmas/SourceContent/Reindeer/Rig/Reindeer_Skeleton.ma";
file -r -dns -rpr "Reindeer" -dr 1 -rfn "ReindeerRN" -typ "mayaAscii" "C:/Users/Nathan/Documents/christmas/SourceContent/Reindeer/Model/Reindeer.ma";
requires maya "2016";
requires -nodeType "ikSpringSolver" "ikSpringSolver" "1.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "6C7DDABF-403F-A5CF-B070-3BBEDA01AC63";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1138.0782293866716 573.791785395994 810.27812957784272 ;
	setAttr ".r" -type "double3" -11.138352729194789 -663.00000000000716 1.4599369843988248e-015 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "C4B086D7-4605-CC01-5C5B-3795DAC26A80";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".ncp" 1;
	setAttr ".fcp" 1000000;
	setAttr ".coi" 1398.2905792495337;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "006D6300-48E2-A405-4FAB-B7AC5E2E6FBD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "6D2909CE-4261-2E2B-2512-BF92528EB92F";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "EF176B26-42BA-7351-9512-D798B4D54A1F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "CC1622AD-47C1-FBD5-393F-D68257341921";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "849AB5FC-460F-0D8C-7E5F-2CA290F17EBA";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "98877F94-42A7-EFD3-4173-C28B8BB4F964";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode fosterParent -n "ReindeerRNfosterParent1";
	rename -uid "5885EBFC-413D-7545-6BCD-57ACAF3960D5";
createNode mesh -n "SatleShapeDeformed" -p "ReindeerRNfosterParent1";
	rename -uid "DE40B27B-47B8-02F1-4C0C-CE86449029C4";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.93619167804718018 0.11351795122027397 ;
	setAttr ".uvst[0].uvsn" -type "string" "UVChannel_1";
	setAttr ".cuvs" -type "string" "UVChannel_1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vs" 10;
createNode mesh -n "RudolfShapeDeformed" -p "ReindeerRNfosterParent1";
	rename -uid "F2DD2FC6-43B3-A14E-0E86-28A8D8A7C243";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.42019905149936676 0.40572395920753479 ;
	setAttr ".uvst[0].uvsn" -type "string" "UVChannel_1";
	setAttr ".cuvs" -type "string" "UVChannel_1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vs" 10;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "C439F405-4138-E15B-9D23-2DA046CA6094";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "B7AA0595-4465-77EC-2FEC-83B158313CBF";
createNode displayLayer -n "defaultLayer";
	rename -uid "00025052-4CBD-FEC9-947F-8B9990AB13B0";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "2FCB2098-4CE0-6569-7010-8DBA24687627";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "C7B52647-46E7-36B5-2EEF-1CAA4CAB2608";
	setAttr ".g" yes;
createNode reference -n "Reindeer_SkeletonRN";
	rename -uid "453FD27A-4D48-DB58-38D0-C98F015AF9CD";
	setAttr -s 184 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".phl[81]" 0;
	setAttr ".phl[82]" 0;
	setAttr ".phl[83]" 0;
	setAttr ".phl[84]" 0;
	setAttr ".phl[85]" 0;
	setAttr ".phl[86]" 0;
	setAttr ".phl[87]" 0;
	setAttr ".phl[88]" 0;
	setAttr ".phl[89]" 0;
	setAttr ".phl[90]" 0;
	setAttr ".phl[91]" 0;
	setAttr ".phl[92]" 0;
	setAttr ".phl[93]" 0;
	setAttr ".phl[94]" 0;
	setAttr ".phl[95]" 0;
	setAttr ".phl[96]" 0;
	setAttr ".phl[97]" 0;
	setAttr ".phl[98]" 0;
	setAttr ".phl[99]" 0;
	setAttr ".phl[100]" 0;
	setAttr ".phl[101]" 0;
	setAttr ".phl[102]" 0;
	setAttr ".phl[103]" 0;
	setAttr ".phl[104]" 0;
	setAttr ".phl[105]" 0;
	setAttr ".phl[106]" 0;
	setAttr ".phl[107]" 0;
	setAttr ".phl[108]" 0;
	setAttr ".phl[109]" 0;
	setAttr ".phl[110]" 0;
	setAttr ".phl[111]" 0;
	setAttr ".phl[112]" 0;
	setAttr ".phl[113]" 0;
	setAttr ".phl[114]" 0;
	setAttr ".phl[115]" 0;
	setAttr ".phl[116]" 0;
	setAttr ".phl[117]" 0;
	setAttr ".phl[118]" 0;
	setAttr ".phl[119]" 0;
	setAttr ".phl[120]" 0;
	setAttr ".phl[121]" 0;
	setAttr ".phl[122]" 0;
	setAttr ".phl[123]" 0;
	setAttr ".phl[124]" 0;
	setAttr ".phl[125]" 0;
	setAttr ".phl[126]" 0;
	setAttr ".phl[127]" 0;
	setAttr ".phl[128]" 0;
	setAttr ".phl[129]" 0;
	setAttr ".phl[130]" 0;
	setAttr ".phl[131]" 0;
	setAttr ".phl[132]" 0;
	setAttr ".phl[133]" 0;
	setAttr ".phl[134]" 0;
	setAttr ".phl[135]" 0;
	setAttr ".phl[136]" 0;
	setAttr ".phl[137]" 0;
	setAttr ".phl[138]" 0;
	setAttr ".phl[139]" 0;
	setAttr ".phl[140]" 0;
	setAttr ".phl[141]" 0;
	setAttr ".phl[142]" 0;
	setAttr ".phl[143]" 0;
	setAttr ".phl[144]" 0;
	setAttr ".phl[145]" 0;
	setAttr ".phl[146]" 0;
	setAttr ".phl[147]" 0;
	setAttr ".phl[148]" 0;
	setAttr ".phl[149]" 0;
	setAttr ".phl[150]" 0;
	setAttr ".phl[151]" 0;
	setAttr ".phl[152]" 0;
	setAttr ".phl[153]" 0;
	setAttr ".phl[154]" 0;
	setAttr ".phl[155]" 0;
	setAttr ".phl[156]" 0;
	setAttr ".phl[157]" 0;
	setAttr ".phl[158]" 0;
	setAttr ".phl[159]" 0;
	setAttr ".phl[160]" 0;
	setAttr ".phl[161]" 0;
	setAttr ".phl[162]" 0;
	setAttr ".phl[163]" 0;
	setAttr ".phl[164]" 0;
	setAttr ".phl[165]" 0;
	setAttr ".phl[166]" 0;
	setAttr ".phl[167]" 0;
	setAttr ".phl[168]" 0;
	setAttr ".phl[169]" 0;
	setAttr ".phl[170]" 0;
	setAttr ".phl[171]" 0;
	setAttr ".phl[172]" 0;
	setAttr ".phl[173]" 0;
	setAttr ".phl[174]" 0;
	setAttr ".phl[175]" 0;
	setAttr ".phl[176]" 0;
	setAttr ".phl[177]" 0;
	setAttr ".phl[178]" 0;
	setAttr ".phl[179]" 0;
	setAttr ".phl[180]" 0;
	setAttr ".phl[181]" 0;
	setAttr ".phl[182]" 0;
	setAttr ".phl[183]" 0;
	setAttr ".phl[184]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"Reindeer_SkeletonRN"
		"Reindeer_SkeletonRN" 0
		"Reindeer_SkeletonRN" 368
		1 |ref_frame "lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		
		1 |ref_frame|pelvis "lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		
		1 |ref_frame|pelvis|hips "lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		
		1 |ref_frame|pelvis|hips|l_femur "lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		
		1 |ref_frame|pelvis|hips|l_femur|l_tibia "lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		
		1 |ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal "lockInfluenceWeights" 
		"liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		1 |ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern "lockInfluenceWeights" 
		"liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		1 |ref_frame|pelvis|hips|spine_01 "lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		
		1 |ref_frame|pelvis|hips|spine_01|spine_02 "lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		
		1 |ref_frame|pelvis|hips|spine_01|spine_02|spine_03 "lockInfluenceWeights" 
		"liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		1 |ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04 "lockInfluenceWeights" 
		"liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		1 |ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05 "lockInfluenceWeights" 
		"liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		1 |ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax 
		"lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		1 |ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus 
		"lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		1 |ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal 
		"lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		1 |ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern 
		"lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		1 |ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus 
		"lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		1 |ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal 
		"lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		1 |ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern 
		"lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		1 |ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase 
		"lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		1 |ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck 
		"lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		1 |ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head 
		"lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		1 |ref_frame|pelvis|hips|r_femur "lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		
		1 |ref_frame|pelvis|hips|r_femur|r_tibia "lockInfluenceWeights" "liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		
		1 |ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal "lockInfluenceWeights" 
		"liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		1 |ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern "lockInfluenceWeights" 
		"liw" " -ci 1 -min 0 -max 1 -at \"bool\""
		2 "|ref_frame" "useObjectColor" " 1"
		2 "|ref_frame" "objectColor" " 0"
		2 "|ref_frame" "segmentScaleCompensate" " 1"
		2 "|ref_frame" "bindPose" " -type \"matrix\" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 10.322264773308 1"
		
		2 "|ref_frame" "lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis" "useObjectColor" " 1"
		2 "|ref_frame|pelvis" "objectColor" " 1"
		2 "|ref_frame|pelvis" "bindPose" " -type \"matrix\" 0 0 1.0000000000000002 0 0 1 0 0 -1.0000000000000002 0 0 0 0 232.42308711591741 -72.195067408442185 1"
		
		2 "|ref_frame|pelvis" "lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips" "useObjectColor" " 1"
		2 "|ref_frame|pelvis|hips" "objectColor" " 2"
		2 "|ref_frame|pelvis|hips" "rotate" " -type \"double3\" 0 0 0"
		2 "|ref_frame|pelvis|hips" "segmentScaleCompensate" " 1"
		2 "|ref_frame|pelvis|hips" "bindPose" " -type \"matrix\" 0 0 1.0000000000000002 0 0 1 0 0 -1.0000000000000002 0 0 0 0 232.42308711591741 -72.195067408442185 1"
		
		2 "|ref_frame|pelvis|hips" "lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips|l_femur" "visibility" " 1"
		2 "|ref_frame|pelvis|hips|l_femur" "useObjectColor" " 1"
		2 "|ref_frame|pelvis|hips|l_femur" "objectColor" " 3"
		2 "|ref_frame|pelvis|hips|l_femur" "translate" " -type \"double3\" -3.4364080829390105 -11.426279360077302 -48.218898531538571"
		
		2 "|ref_frame|pelvis|hips|l_femur" "rotate" " -type \"double3\" 0 0 0"
		2 "|ref_frame|pelvis|hips|l_femur" "rotateZ" " -av"
		2 "|ref_frame|pelvis|hips|l_femur" "scale" " -type \"double3\" 1 1 1"
		2 "|ref_frame|pelvis|hips|l_femur" "segmentScaleCompensate" " 1"
		2 "|ref_frame|pelvis|hips|l_femur" "bindPose" " -type \"matrix\" -0.082713528313459739 -0.95872590144880609 0.27203514134227674 0 0.023382815039709078 0.27102834184994606 0.96228731773565834 0 -0.99629900943178584 0.085955126695519135 0 0 48.218898531538585 220.99680775584011 -75.63147549138121 1"
		
		2 "|ref_frame|pelvis|hips|l_femur" "lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips|l_femur|l_tibia" "useObjectColor" " 1"
		2 "|ref_frame|pelvis|hips|l_femur|l_tibia" "objectColor" " 4"
		2 "|ref_frame|pelvis|hips|l_femur|l_tibia" "bindPose" " -type \"matrix\" -0.073166617250642441 -0.58220963005696413 -0.80973983030911301 0 -0.10096630584242022 -0.80342043655458839 0.58678906534713904 0 -0.99219577261981584 0.12468981031089565 0 0 42.638111194415792 156.31034739471806 -57.276917258754736 1"
		
		2 "|ref_frame|pelvis|hips|l_femur|l_tibia" "lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal" "useObjectColor" 
		" 1"
		2 "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal" "objectColor" " 5"
		
		2 "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal" "bindPose" " -type \"matrix\" -0.064285018712655365 -0.97298579392550089 0.22173425803938313 0 0.014618076783577729 0.22125187687209616 0.97510713196639287 0 -0.99782450771679454 0.065926108634872596 0 0 38.702444204291957 124.99301568730917 -100.83320618515103 1"
		
		2 "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal" "lockInfluenceWeights" 
		" 0"
		2 "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern" "useObjectColor" 
		" 1"
		2 "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern" "objectColor" 
		" 6"
		2 "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern" "bindPose" 
		" -type \"matrix\" -0.064285018712655365 -0.97298579392550089 0.22173425803938313 0 0.014618076783577729 0.22125187687209616 0.97510713196639287 0 -0.99782450771679454 0.065926108634872596 0 0 33.136186047791803 40.744924607101893 -81.633863210638268 1"
		
		2 "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern" "lockInfluenceWeights" 
		" 0"
		2 "|ref_frame|pelvis|hips|spine_01" "useObjectColor" " 1"
		2 "|ref_frame|pelvis|hips|spine_01" "objectColor" " 3"
		2 "|ref_frame|pelvis|hips|spine_01" "rotate" " -type \"double3\" 0 0 0"
		2 "|ref_frame|pelvis|hips|spine_01" "segmentScaleCompensate" " 1"
		2 "|ref_frame|pelvis|hips|spine_01" "bindPose" " -type \"matrix\" 0 0 1.0000000000000002 0 0 1 0 0 -1.0000000000000002 0 0 0 0 232.42308711591741 -55.69160082669201 1"
		
		2 "|ref_frame|pelvis|hips|spine_01" "lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02" "useObjectColor" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02" "objectColor" " 4"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02" "segmentScaleCompensate" " 1"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02" "bindPose" " -type \"matrix\" 0 0 1.0000000000000002 0 0 1 0 0 -1.0000000000000002 0 0 0 0 232.42308711591741 -22.684668026691995 1"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02" "lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03" "useObjectColor" " 1"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03" "objectColor" " 5"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03" "segmentScaleCompensate" 
		" 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03" "bindPose" " -type \"matrix\" 0 0 1.0000000000000002 0 0 1 0 0 -1.0000000000000002 0 0 0 0 232.42308711591741 10.322264773308014 1"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03" "lockInfluenceWeights" 
		" 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04" "useObjectColor" 
		" 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04" "objectColor" 
		" 6"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04" "segmentScaleCompensate" 
		" 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04" "bindPose" 
		" -type \"matrix\" 0 0 1.0000000000000002 0 0 1 0 0 -1.0000000000000002 0 0 0 0 232.42308711591741 43.329197573308008 1"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04" "lockInfluenceWeights" 
		" 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05" "useObjectColor" 
		" 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05" "objectColor" 
		" 7"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05" "rotate" 
		" -type \"double3\" 0 0 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05" "segmentScaleCompensate" 
		" 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05" "bindPose" 
		" -type \"matrix\" 0 0 1.0000000000000002 0 0 1 0 0 -1.0000000000000002 0 0 0 0 232.42308711591741 76.336130373308038 1"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05" "lockInfluenceWeights" 
		" 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax" 
		"useObjectColor" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax" 
		"objectColor" " 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax" 
		"bindPose" " -type \"matrix\" 0 0 1.0000000000000002 0 0 1 0 0 -1.0000000000000002 0 0 0 0 232.42308711591741 92.839596955058198 1"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax" 
		"lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"visibility" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"useObjectColor" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"objectColor" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"translate" " -type \"double3\" 5.8593266959954633 -18.341454555869063 -45.988075806042318"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"rotateY" " -av"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"rotateZ" " -av"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"segmentScaleCompensate" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"bindPose" " -type \"matrix\" -0.087860666602631421 -0.99143411462521025 0.096637982291988098 0 0.0085306042079501057 0.096260731418963852 0.99531959710363072 0 -0.9960962463818388 0.088273823662575329 0 0 45.988075806042325 214.08163256004835 98.698923651053647 1"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus" 
		"lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal" 
		"useObjectColor" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal" 
		"objectColor" " 2"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal" 
		"segmentScaleCompensate" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal" 
		"bindPose" " -type \"matrix\" -0.052083927011659203 -0.9780246901410814 -0.20187860218826026 0 -0.010735671937347103 -0.20159294474147518 0.97941055231119212 0 -0.99858500384048343 0.053178850165288311 0 0 36.866473993273623 111.15198762453849 108.73177712852217 1"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal" 
		"lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern" 
		"useObjectColor" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern" 
		"objectColor" " 3"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern" 
		"bindPose" " -type \"matrix\" -0.052083927011659203 -0.9780246901410814 -0.20187860218826026 0 -0.010735671937347103 -0.20159294474147518 0.97941055231119212 0 -0.99858500384048343 0.053178850165288311 0 0 33.445873313485365 46.920426912969873 95.473443444787264 1"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern" 
		"lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus" 
		"useObjectColor" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus" 
		"objectColor" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus" 
		"segmentScaleCompensate" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus" 
		"bindPose" " -type \"matrix\" -0.087860666602631393 0.99143411462521025 -0.096637982291988958 0 0.0085306042079492782 -0.096260731418964962 -0.99531959710363072 0 -0.9960962463818388 -0.088273823662575232 0 0 -45.98810000000001 214.082 98.698900000000023 1"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus" 
		"lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal" 
		"useObjectColor" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal" 
		"objectColor" " 2"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal" 
		"segmentScaleCompensate" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal" 
		"bindPose" " -type \"matrix\" -0.052083927011659231 0.97802469014108162 0.20187860218825962 0 -0.010735671937331083 0.20159294474147529 -0.97941055231119234 0 -0.99858500384048354 -0.053178850165285105 0 0 -36.8665 111.152 108.73200000000003 1"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal" 
		"lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern" 
		"useObjectColor" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern" 
		"objectColor" " 3"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern" 
		"bindPose" " -type \"matrix\" -0.052083927011659231 0.97802469014108162 0.20187860218825962 0 -0.010735671937331083 0.20159294474147529 -0.97941055231119234 0 -0.99858500384048354 -0.053178850165285105 0 0 -33.4459 46.920399999999987 95.473400000000041 1"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern" 
		"lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"visibility" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"useObjectColor" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"objectColor" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"translate" " -type \"double3\" 7.324568659949108 51.572528130797906 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"rotateX" " -av"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"rotateY" " -av"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"rotateZ" " -av"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"segmentScaleCompensate" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"bindPose" " -type \"matrix\" 0 0.97758886526498945 0.21052318283246224 0 0 -0.2105231828324623 0.97758886526498967 0 1.0000000000000002 0 0 0 0 283.99561524671532 100.16416561500731 1"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase" 
		"lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"visibility" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"useObjectColor" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"objectColor" " 2"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"translate" " -type \"double3\" 56.587329315653449 0 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"rotateX" " -av"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"rotateY" " -av"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"rotateZ" " -av"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"segmentScaleCompensate" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"bindPose" " -type \"matrix\" 0 0.59590313857010757 0.80305631772765196 0 1.0000000000000004 0 0 0 0 0.80305631772765185 -0.59590313857010802 0 0 339.31475830078125 112.07711029052739 1"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck" 
		"lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head" 
		"useObjectColor" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head" 
		"objectColor" " 3"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head" 
		"segmentScaleCompensate" " 1"
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head" 
		"bindPose" " -type \"matrix\" 1.0000000000000004 0 0 0 0 1.0000000000000002 0 0 0 0 1.0000000000000004 0 0 386.21650480897489 175.28326143825211 1"
		
		2 "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head" 
		"lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips|r_femur" "useObjectColor" " 1"
		2 "|ref_frame|pelvis|hips|r_femur" "objectColor" " 3"
		2 "|ref_frame|pelvis|hips|r_femur" "bindPose" " -type \"matrix\" -0.082713528313459753 0.95872590144880598 -0.27203514134227708 0 0.023382815039712446 -0.27102834184994617 -0.96228731773565823 0 -0.99629900943178573 -0.085955126695520079 0 0 -48.218900000000012 220.997 -75.6315 1"
		
		2 "|ref_frame|pelvis|hips|r_femur" "lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips|r_femur|r_tibia" "useObjectColor" " 1"
		2 "|ref_frame|pelvis|hips|r_femur|r_tibia" "objectColor" " 4"
		2 "|ref_frame|pelvis|hips|r_femur|r_tibia" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|ref_frame|pelvis|hips|r_femur|r_tibia" "segmentScaleCompensate" " 1"
		2 "|ref_frame|pelvis|hips|r_femur|r_tibia" "bindPose" " -type \"matrix\" -0.073166617250642468 0.58220963005696436 0.80973983030911267 0 -0.10096630584243044 0.80342043655458684 -0.58678906534713937 0 -0.99219577261981473 -0.12468981031090395 0 0 -42.638100000000016 156.31000000000003 -57.276900000000012 1"
		
		2 "|ref_frame|pelvis|hips|r_femur|r_tibia" "lockInfluenceWeights" " 0"
		2 "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal" "useObjectColor" 
		" 1"
		2 "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal" "objectColor" " 5"
		
		2 "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal" "rotate" " -type \"double3\" 0 0 0"
		
		2 "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal" "segmentScaleCompensate" 
		" 1"
		2 "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal" "bindPose" " -type \"matrix\" -0.064285018712655365 0.97298579392550077 -0.22173425803938399 0 0.01461807678356106 -0.22125187687209802 -0.97510713196639276 0 -0.99782450771679465 -0.065926108634868891 0 0 -38.702400000000019 124.99300000000007 -100.833 1"
		
		2 "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal" "lockInfluenceWeights" 
		" 0"
		2 "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern" "useObjectColor" 
		" 1"
		2 "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern" "objectColor" 
		" 6"
		2 "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern" "bindPose" 
		" -type \"matrix\" -0.064285018712655365 0.97298579392550077 -0.22173425803938399 0 0.01461807678356106 -0.22125187687209802 -0.97510713196639276 0 -0.99782450771679465 -0.065926108634868891 0 0 -33.136200000000009 40.744900000000044 -81.6339 1"
		
		2 "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern" "lockInfluenceWeights" 
		" 0"
		5 3 "Reindeer_SkeletonRN" "|ref_frame.bindPose" "Reindeer_SkeletonRN.placeHolderList[1]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame.message" "Reindeer_SkeletonRN.placeHolderList[2]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis.lockInfluenceWeights" "Reindeer_SkeletonRN.placeHolderList[3]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis.lockInfluenceWeights" "Reindeer_SkeletonRN.placeHolderList[4]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis.bindPose" "Reindeer_SkeletonRN.placeHolderList[5]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis.message" "Reindeer_SkeletonRN.placeHolderList[6]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis.worldMatrix" "Reindeer_SkeletonRN.placeHolderList[7]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis.worldMatrix" "Reindeer_SkeletonRN.placeHolderList[8]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis.objectColorRGB" "Reindeer_SkeletonRN.placeHolderList[9]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis.objectColorRGB" "Reindeer_SkeletonRN.placeHolderList[10]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[11]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[12]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips.bindPose" "Reindeer_SkeletonRN.placeHolderList[13]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips.message" "Reindeer_SkeletonRN.placeHolderList[14]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips.worldMatrix" "Reindeer_SkeletonRN.placeHolderList[15]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips.worldMatrix" "Reindeer_SkeletonRN.placeHolderList[16]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips.objectColorRGB" "Reindeer_SkeletonRN.placeHolderList[17]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips.objectColorRGB" "Reindeer_SkeletonRN.placeHolderList[18]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[19]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[20]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[21]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur.message" "Reindeer_SkeletonRN.placeHolderList[22]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[23]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[24]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[25]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[26]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[27]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[28]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[29]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.message" 
		"Reindeer_SkeletonRN.placeHolderList[30]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[31]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[32]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[33]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[34]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[35]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[36]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.message" 
		"Reindeer_SkeletonRN.placeHolderList[37]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[38]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[39]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[40]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[41]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.message" 
		"Reindeer_SkeletonRN.placeHolderList[42]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[43]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|l_femur|l_tibia|l_metatarsal|l_pastern.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[44]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[45]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[46]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[47]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01.message" 
		"Reindeer_SkeletonRN.placeHolderList[48]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[49]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[50]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[51]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[52]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[53]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[54]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[55]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02.message" 
		"Reindeer_SkeletonRN.placeHolderList[56]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[57]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[58]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[59]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[60]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[61]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[62]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[63]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.message" 
		"Reindeer_SkeletonRN.placeHolderList[64]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[65]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[66]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[67]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[68]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[69]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[70]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[71]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.message" 
		"Reindeer_SkeletonRN.placeHolderList[72]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[73]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[74]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[75]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[76]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[77]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[78]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[79]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.message" 
		"Reindeer_SkeletonRN.placeHolderList[80]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[81]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[82]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[83]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[84]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[85]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[86]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[87]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.message" 
		"Reindeer_SkeletonRN.placeHolderList[88]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[89]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[90]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[91]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[92]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[93]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[94]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[95]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.message" 
		"Reindeer_SkeletonRN.placeHolderList[96]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[97]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[98]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[99]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[100]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[101]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[102]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[103]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.message" 
		"Reindeer_SkeletonRN.placeHolderList[104]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[105]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[106]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[107]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[108]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[109]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[110]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.message" 
		"Reindeer_SkeletonRN.placeHolderList[111]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[112]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|l_fnt_humerus|l_fnt_metacarpal|l_fnt_pastern.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[113]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[114]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[115]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[116]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.message" 
		"Reindeer_SkeletonRN.placeHolderList[117]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[118]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[119]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[120]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[121]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[122]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[123]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[124]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.message" 
		"Reindeer_SkeletonRN.placeHolderList[125]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[126]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[127]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[128]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[129]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[130]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[131]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.message" 
		"Reindeer_SkeletonRN.placeHolderList[132]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[133]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|r_fnt_humerus|r_fnt_metacarpal|r_fnt_pastern.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[134]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[135]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[136]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[137]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.message" 
		"Reindeer_SkeletonRN.placeHolderList[138]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[139]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[140]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[141]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[142]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[143]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[144]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[145]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.message" 
		"Reindeer_SkeletonRN.placeHolderList[146]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[147]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[148]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[149]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[150]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[151]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[152]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[153]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.message" 
		"Reindeer_SkeletonRN.placeHolderList[154]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[155]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[156]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[157]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|spine_01|spine_02|spine_03|spine_04|spine_05|thorax|neckBase|neck|head.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[158]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[159]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[160]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[161]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur.message" "Reindeer_SkeletonRN.placeHolderList[162]" 
		""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[163]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[164]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[165]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[166]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[167]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[168]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[169]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.message" 
		"Reindeer_SkeletonRN.placeHolderList[170]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[171]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[172]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[173]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[174]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[175]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[176]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.message" 
		"Reindeer_SkeletonRN.placeHolderList[177]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[178]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[179]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.lockInfluenceWeights" 
		"Reindeer_SkeletonRN.placeHolderList[180]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.bindPose" 
		"Reindeer_SkeletonRN.placeHolderList[181]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.message" 
		"Reindeer_SkeletonRN.placeHolderList[182]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.worldMatrix" 
		"Reindeer_SkeletonRN.placeHolderList[183]" ""
		5 3 "Reindeer_SkeletonRN" "|ref_frame|pelvis|hips|r_femur|r_tibia|r_metatarsal|r_pastern.objectColorRGB" 
		"Reindeer_SkeletonRN.placeHolderList[184]" "";
lockNode -l 1 ;
createNode reference -n "ReindeerRN";
	rename -uid "EA8084F3-44A8-3C70-6A16-EA866CFEA110";
	setAttr -s 5 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"ReindeerRN"
		"ReindeerRN" 32
		0 "|ReindeerRNfosterParent1|RibonShapeDeformed" "|Ribon" "-s -r "
		2 "|Ribon" "visibility" " 1"
		2 "|Ribon|RibonShape" "intermediateObject" " 1"
		2 "|Ribon|RibonShape" "vertexColorSource" " 2"
		5 3 "ReindeerRN" "|Ribon|RibonShape.worldMesh" "ReindeerRN.placeHolderList[1]" 
		""
		8 "|Ribon" "translateX"
		8 "|Ribon" "translateY"
		8 "|Ribon" "translateZ"
		8 "|Ribon" "rotateX"
		8 "|Ribon" "rotateY"
		8 "|Ribon" "rotateZ"
		8 "|Ribon" "scaleX"
		8 "|Ribon" "scaleY"
		8 "|Ribon" "scaleZ"
		8 "|Rudolf" "translateX"
		8 "|Rudolf" "translateY"
		8 "|Rudolf" "translateZ"
		8 "|Rudolf" "rotateX"
		8 "|Rudolf" "rotateY"
		8 "|Rudolf" "rotateZ"
		8 "|Rudolf" "scaleX"
		8 "|Rudolf" "scaleY"
		8 "|Rudolf" "scaleZ"
		8 "|Satle" "translateX"
		8 "|Satle" "translateY"
		8 "|Satle" "translateZ"
		8 "|Satle" "rotateX"
		8 "|Satle" "rotateY"
		8 "|Satle" "rotateZ"
		8 "|Satle" "scaleX"
		8 "|Satle" "scaleY"
		8 "|Satle" "scaleZ"
		"ReindeerRN" 11
		0 "|ReindeerRNfosterParent1|RudolfShapeDeformed" "|Rudolf" "-s -r "
		0 "|ReindeerRNfosterParent1|SatleShapeDeformed" "|Satle" "-s -r "
		2 "|Rudolf|RudolfShape" "intermediateObject" " 1"
		2 "|Rudolf|RudolfShape" "vertexColorSource" " 2"
		2 "|Satle" "visibility" " 1"
		2 "|Satle|SatleShape" "intermediateObject" " 1"
		2 "|Satle|SatleShape" "vertexColorSource" " 2"
		5 3 "ReindeerRN" "|Rudolf|RudolfShape.worldMesh" "ReindeerRN.placeHolderList[2]" 
		""
		5 3 "ReindeerRN" "|Satle|SatleShape.worldMesh" "ReindeerRN.placeHolderList[3]" 
		""
		5 4 "ReindeerRN" "RudolfSG.dagSetMembers" "ReindeerRN.placeHolderList[4]" 
		""
		5 4 "ReindeerRN" "RudolfSG.dagSetMembers" "ReindeerRN.placeHolderList[5]" 
		"";
lockNode -l 1 ;
createNode dagPose -n "bindPose1";
	rename -uid "239E4FA6-4A14-6B2C-E6F2-0FA9E1618841";
	setAttr -s 26 ".wm";
	setAttr -s 31 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 9.1612641917038103e-016
		 0 10.322264773308 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 -9.1612641917038103e-016
		 232.42308711591741 -82.517332181750191 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 -0.70710678118654746 0 0.70710678118654768 1
		 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 2.8421709430404007e-014
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 0 0 -3.4364080829390105 -11.426279360077274
		 -48.218898531538571 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.034306668668587642 -0.025952798865843677 -0.60275109420049233 0.79676886432390304 1
		 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 0 0 0 67.471276475757293 0 -2.4868995751603507e-014 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.006386466825291255 -0.018398807251261161 -0.5724374793355046 0.81971704214639463 1
		 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 0 0 0 0 53.790473552188978 5.6843418860808015e-014
		 1.7763568394002505e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.01039944464768606 0.027623637964338851 0.55083378311365883 0.83409287825597067 1
		 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 0 0 0 0 86.58717486543074 1.5631940186722204e-013
		 -2.1316282072803006e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 0 0 0 0 16.503466581750175 0 -2.6386349428456973e-014 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 0 0 0 0 33.006932800000016 2.8421709430404007e-014
		 2.1815260356494738e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 0 0 0 0 33.006932800000001 2.8421709430404007e-014
		 2.1977491076556148e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 0 0 0 0 33.006932799999987 2.8421709430404007e-014
		 2.1977491076556138e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[11]" -type "matrix" "xform" 1 1 1 0 0 0 0 33.006932800000023 2.8421709430404007e-014
		 2.1815260356494741e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[12]" -type "matrix" "xform" 1 1 1 0 0 0 0 16.50346658175016 2.8421709430404007e-014
		 -2.4554096590116208e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[13]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.8593266959954633 -18.341454555869035
		 -45.988075806042318 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.032714692766345553 -0.029692182241868089 -0.67141595391211972 0.73976262410532578 1
		 1 1 yes;
	setAttr ".xm[14]" -type "matrix" "xform" 1 1 1 0 0 0 0 103.81894612777185 4.2632564145606011e-014
		 -1.0658141036401503e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.00093618994845020816 0.017566625116858109 -0.14944697529435319 0.98861324025412145 1
		 1 1 yes;
	setAttr ".xm[15]" -type "matrix" "xform" 1 1 1 0 0 0 0 65.674784449770016 -4.2632564145606011e-014
		 -1.0658141036401503e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[16]" -type "matrix" "xform" 1 1 1 0 0 0 0 5.8593030449418109 -18.341087115917418
		 45.988100000000003 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.029692182241868204 -0.032714692766345366 0.73976262410532612 0.67141595391211928 1
		 1 1 yes;
	setAttr ".xm[17]" -type "matrix" "xform" 1 1 1 0 0 0 0 -103.81932181498918 -0.00021120540316132974
		 3.3148592287091105e-005 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.00093618994845855652 0.017566625116859219 -0.14944697529435316 0.98861324025412145 1
		 1 1 yes;
	setAttr ".xm[18]" -type "matrix" "xform" 1 1 1 0 0 0 0 -65.674876602975232 0.00025291998777277058
		 2.7681399785706162e-006 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[19]" -type "matrix" "xform" 1 1 1 0 0 0 0 7.324568659949108 51.572528130797906
		 1.6263809543446657e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.77798559846325632 0.62828210907503079 1.7797783638304487e-016 2.2038538349517605e-016 1
		 1 1 yes;
	setAttr ".xm[20]" -type "matrix" "xform" 1 1 1 0 0 0 0 56.587329315653506 4.2632564145606011e-014
		 -6.2824555908284396e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.66174206550670844 0.2491935768412081 0.24919357684120813 0.66174206550670855 1
		 1 1 yes;
	setAttr ".xm[21]" -type "matrix" "xform" 1 1 1 0 0 0 0 78.706996947081393 -9.6654688499407832e-015
		 -2.8421709430404007e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.63164529970746008 0.63164529970745986 0.31784306718485 0.31784306718485011 1
		 1 1 yes;
	setAttr ".xm[22]" -type "matrix" "xform" 1 1 1 0 0 0 0 -3.436432591557832 -11.426087115917397
		 48.218899999999998 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.025952798865842498 -0.034306668668589127 0.79676886432390304 0.60275109420049211 1
		 1 1 yes;
	setAttr ".xm[23]" -type "matrix" "xform" 1 1 1 0 0 0 0 -67.471806251111644 0.00010636131019126083
		 3.3768716139803701e-005 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.0063864668252968495 -0.018398807251263145 -0.5724374793355046 0.81971704214639463 1
		 1 1 yes;
	setAttr ".xm[24]" -type "matrix" "xform" 1 1 1 0 0 0 0 -53.790129862934023 0.00015230768249807625
		 -7.4112793495118012e-005 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.010399444647683359 0.027623637964346314 0.5508337831136586 0.83409287825597067 1
		 1 1 yes;
	setAttr ".xm[25]" -type "matrix" "xform" 1 1 1 0 0 0 0 -86.587125929897354 0.00023804956489925644
		 5.8618028372592335e-005 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[26]" -type "matrix" "xform" 1 1 1 0 0 0 0 86.58717486543074 1.5631940186722204e-013
		 -2.1316282072803006e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[27]" -type "matrix" "xform" 1 1 1 0 0 0 0 65.674784449770016 -5.6843418860808015e-014
		 -1.7763568394002505e-014 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[28]" -type "matrix" "xform" 1 1 1 0 0 0 0 -65.674876602975232 0.00025291998775855973
		 2.7681399927814709e-006 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[29]" -type "matrix" "xform" 1 1 1 0 0 0 0 78.706996947081393 -9.6654688499407895e-015
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.63164529970746008 0.63164529970745986 0.31784306718485 0.31784306718485011 1
		 1 1 yes;
	setAttr ".xm[30]" -type "matrix" "xform" 1 1 1 0 0 0 0 -86.58712592989734 0.00023804956489925644
		 5.8618028376145048e-005 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr -s 26 ".m";
	setAttr -s 26 ".p";
	setAttr -s 31 ".g[0:30]" yes yes yes yes yes yes no yes yes yes no 
		no no no yes no no yes no no no no yes yes yes no no no no no no;
	setAttr ".bp" yes;
createNode skinCluster -n "skinCluster2";
	rename -uid "5F40C3F4-453B-470D-6E91-529B36B49BEF";
	setAttr -s 407 ".wl";
	setAttr ".wl[0].w[21]"  1;
	setAttr ".wl[1].w[21]"  1.0000000000000284;
	setAttr -s 4 ".wl[2].w";
	setAttr ".wl[2].w[12]" 0.088279059462239101;
	setAttr ".wl[2].w[13]" 0.41803161914340292;
	setAttr ".wl[2].w[16]" 0.41803312653246588;
	setAttr ".wl[2].w[17]" 0.075656194861892165;
	setAttr -s 4 ".wl[3].w";
	setAttr ".wl[3].w[11]" 0.22815023951918606;
	setAttr ".wl[3].w[12]" 0.26705259774794438;
	setAttr ".wl[3].w[13]" 0.25239816396327497;
	setAttr ".wl[3].w[16]" 0.25239899876959454;
	setAttr -s 4 ".wl[4].w";
	setAttr ".wl[4].w[10]" 0.20667087352677341;
	setAttr ".wl[4].w[11]" 0.18392993590965373;
	setAttr ".wl[4].w[13]" 0.30470040685682104;
	setAttr ".wl[4].w[16]" 0.3046987837067518;
	setAttr -s 3 ".wl[5].w[8:10]"  0.1 0.36 0.54;
	setAttr -s 3 ".wl[6].w[8:10]"  0.54 0.36 0.1;
	setAttr -s 3 ".wl[7].w[8:10]"  0.2 0.6 0.2;
	setAttr -s 3 ".wl[8].w";
	setAttr ".wl[8].w[3]" 0.33583339225229009;
	setAttr ".wl[8].w[4]" 0.32833395922192649;
	setAttr ".wl[8].w[22]" 0.33583264852578343;
	setAttr -s 3 ".wl[9].w";
	setAttr ".wl[9].w[4]" 0.33511176789668301;
	setAttr ".wl[9].w[22]" 0.32977968192036072;
	setAttr ".wl[9].w[23]" 0.33510855018295621;
	setAttr -s 3 ".wl[10].w[1:3]"  0.38403585757181641 0.40675378517514432 
		0.20921035725303938;
	setAttr -s 2 ".wl[11].w[20:21]"  0.5 0.5;
	setAttr ".wl[12].w[21]"  1;
	setAttr -s 5 ".wl[13].w";
	setAttr ".wl[13].w[11]" 5.1912844121720624e-009;
	setAttr ".wl[13].w[12]" 2.4525966733592465e-008;
	setAttr ".wl[13].w[16]" 5.3450070556142012e-009;
	setAttr ".wl[13].w[19]" 0.69999998281913522;
	setAttr ".wl[13].w[20]" 0.29999998211860662;
	setAttr -s 5 ".wl[14].w";
	setAttr ".wl[14].w[10]" 2.7880277658592063e-008;
	setAttr ".wl[14].w[11]" 0.087095312224737112;
	setAttr ".wl[14].w[12]" 0.41147743236849077;
	setAttr ".wl[14].w[16]" 0.089674380445956589;
	setAttr ".wl[14].w[19]" 0.41175284708053794;
	setAttr -s 2 ".wl[15].w[19:20]"  0.5 0.5;
	setAttr -s 2 ".wl[16].w[19:20]"  0.4 0.6;
	setAttr -s 4 ".wl[17].w";
	setAttr ".wl[17].w[11]" 0.14386691469917462;
	setAttr ".wl[17].w[12]" 0.36607362753463407;
	setAttr ".wl[17].w[16]" 0.15292014655586839;
	setAttr ".wl[17].w[19]" 0.33713931121032281;
	setAttr -s 5 ".wl[18].w";
	setAttr ".wl[18].w[11]" 0.030557306432816286;
	setAttr ".wl[18].w[12]" 0.31893745246923216;
	setAttr ".wl[18].w[16]" 1.0690013837570007e-008;
	setAttr ".wl[18].w[19]" 0.49647584219946472;
	setAttr ".wl[18].w[20]" 0.15402938820847301;
	setAttr ".wl[19].w[21]"  1;
	setAttr -s 2 ".wl[20].w[20:21]"  0.49999982248776531 0.50000017751223469;
	setAttr -s 2 ".wl[21].w[20:21]"  0.4999997615814209 0.5000002384185791;
	setAttr -s 2 ".wl[22].w[20:21]"  0.8 0.19999999999999996;
	setAttr -s 2 ".wl[23].w[20:21]"  0.7999999679850075 0.20000003201499264;
	setAttr ".wl[24].w[21]"  1;
	setAttr ".wl[25].w[21]"  1;
	setAttr ".wl[26].w[21]"  1;
	setAttr ".wl[27].w[21]"  1;
	setAttr ".wl[28].w[21]"  0.99999999999997158;
	setAttr ".wl[29].w[21]"  1;
	setAttr ".wl[30].w[21]"  1;
	setAttr ".wl[31].w[21]"  1;
	setAttr ".wl[32].w[21]"  1;
	setAttr -s 2 ".wl[33].w[16:17]"  0.6 0.4;
	setAttr -s 2 ".wl[34].w[16:17]"  0.6 0.4;
	setAttr -s 2 ".wl[35].w";
	setAttr ".wl[35].w[12]" 2.3516463443229444e-009;
	setAttr ".wl[35].w[16]" 0.99999999764835379;
	setAttr ".wl[36].w[16]"  1;
	setAttr ".wl[37].w[16]"  1;
	setAttr -s 2 ".wl[38].w[16:17]"  0.6 0.4;
	setAttr -s 2 ".wl[39].w[16:17]"  0.59999999816348015 0.40000000183651996;
	setAttr -s 2 ".wl[40].w[16:17]"  0.99999997615814218 2.3841857910156251e-008;
	setAttr -s 2 ".wl[41].w[16:17]"  0.6 0.4;
	setAttr -s 2 ".wl[42].w";
	setAttr ".wl[42].w[12]" 0.1999999143662734;
	setAttr ".wl[42].w[16]" 0.8000000856337266;
	setAttr -s 2 ".wl[43].w";
	setAttr ".wl[43].w[12]" 0.10000005960464475;
	setAttr ".wl[43].w[16]" 0.89999994039535525;
	setAttr -s 2 ".wl[44].w";
	setAttr ".wl[44].w[12]" 0.30000000000000004;
	setAttr ".wl[44].w[16]" 0.7;
	setAttr -s 2 ".wl[45].w";
	setAttr ".wl[45].w[12]" 0.30000000000000004;
	setAttr ".wl[45].w[16]" 0.7;
	setAttr -s 2 ".wl[46].w";
	setAttr ".wl[46].w[12]" 0.39999996870485588;
	setAttr ".wl[46].w[16]" 0.60000003129514412;
	setAttr -s 2 ".wl[47].w";
	setAttr ".wl[47].w[12]" 0.8;
	setAttr ".wl[47].w[16]" 0.2;
	setAttr -s 2 ".wl[48].w";
	setAttr ".wl[48].w[12]" 0.8;
	setAttr ".wl[48].w[16]" 0.2;
	setAttr ".wl[49].w[18]"  1;
	setAttr ".wl[50].w[18]"  1;
	setAttr ".wl[51].w[18]"  1;
	setAttr ".wl[52].w[18]"  1;
	setAttr ".wl[53].w[18]"  1;
	setAttr ".wl[54].w[18]"  1;
	setAttr ".wl[55].w[18]"  0.99999999999999989;
	setAttr ".wl[56].w[18]"  1;
	setAttr ".wl[57].w[18]"  1;
	setAttr ".wl[58].w[18]"  1;
	setAttr ".wl[59].w[18]"  1;
	setAttr ".wl[60].w[18]"  1;
	setAttr ".wl[61].w[18]"  1;
	setAttr ".wl[62].w[18]"  1;
	setAttr ".wl[63].w[18]"  1;
	setAttr ".wl[64].w[18]"  1;
	setAttr ".wl[65].w[21]"  1;
	setAttr -s 4 ".wl[66].w";
	setAttr ".wl[66].w[10]" 0.033802980062886595;
	setAttr ".wl[66].w[11]" 0.058665403027162201;
	setAttr ".wl[66].w[12]" 0.4537658113853163;
	setAttr ".wl[66].w[16]" 0.45376580552463486;
	setAttr -s 6 ".wl[67].w";
	setAttr ".wl[67].w[8]" 2.3841857910156251e-008;
	setAttr ".wl[67].w[9]" 7.1525573730468747e-008;
	setAttr ".wl[67].w[10]" 0.15591781872773056;
	setAttr ".wl[67].w[11]" 0.15601772535038663;
	setAttr ".wl[67].w[12]" 0.36041983141716849;
	setAttr ".wl[67].w[16]" 0.3276445291372827;
	setAttr -s 5 ".wl[68].w";
	setAttr ".wl[68].w[8]" 4.2213654882696911e-008;
	setAttr ".wl[68].w[9]" 0.17762557318778532;
	setAttr ".wl[68].w[10]" 0.18385450966259431;
	setAttr ".wl[68].w[12]" 0.29316350455499757;
	setAttr ".wl[68].w[16]" 0.34535637038096784;
	setAttr -s 4 ".wl[69].w";
	setAttr ".wl[69].w[10]" 0.14072592334173087;
	setAttr ".wl[69].w[11]" 0.12299861892762273;
	setAttr ".wl[69].w[12]" 0.36852686455867484;
	setAttr ".wl[69].w[19]" 0.36774859317197162;
	setAttr -s 2 ".wl[70].w[23:24]"  0.2 0.8;
	setAttr -s 3 ".wl[71].w[23:25]"  0.19999998807907138 0.79999995231628551 
		5.9604643116950983e-008;
	setAttr -s 2 ".wl[72].w[23:24]"  0.49999996423721355 0.50000003576278651;
	setAttr -s 2 ".wl[73].w[23:24]"  0.49999999837501696 0.50000000162498304;
	setAttr -s 2 ".wl[74].w[23:24]"  0.5 0.5;
	setAttr -s 2 ".wl[75].w[23:24]"  0.2 0.8;
	setAttr -s 2 ".wl[76].w[23:24]"  0.19999999686860975 0.8000000031313903;
	setAttr -s 2 ".wl[77].w[23:24]"  0.20000004768371582 0.79999995231628418;
	setAttr -s 2 ".wl[78].w[23:24]"  0.49999974966049193 0.50000025033950801;
	setAttr -s 2 ".wl[79].w[23:24]"  0.20000023245810941 0.79999976754189062;
	setAttr ".wl[80].w[23]"  1;
	setAttr -s 2 ".wl[81].w[22:23]"  0.79999990463256843 0.20000009536743166;
	setAttr ".wl[82].w[23]"  1;
	setAttr ".wl[83].w[23]"  1;
	setAttr ".wl[84].w[25]"  1;
	setAttr ".wl[85].w[25]"  1;
	setAttr ".wl[86].w[25]"  1;
	setAttr ".wl[87].w[25]"  1;
	setAttr ".wl[88].w[25]"  0.99999999999999911;
	setAttr ".wl[89].w[25]"  1;
	setAttr -s 3 ".wl[90].w[23:25]"  3.8004849267636587e-010 3.8004849267636587e-010 
		0.9999999992398898;
	setAttr ".wl[91].w[25]"  1;
	setAttr -s 3 ".wl[92].w[23:25]"  8.7417200202587913e-009 3.4966880081035165e-008 
		0.99999995629139982;
	setAttr ".wl[93].w[25]"  1;
	setAttr ".wl[94].w[25]"  1;
	setAttr ".wl[95].w[25]"  1;
	setAttr ".wl[96].w[25]"  1;
	setAttr ".wl[97].w[25]"  1;
	setAttr ".wl[98].w[25]"  1;
	setAttr ".wl[99].w[25]"  1;
	setAttr -s 2 ".wl[100].w[22:23]"  0.8 0.2;
	setAttr -s 2 ".wl[101].w[22:23]"  0.79999999999999993 0.19999999999999998;
	setAttr ".wl[102].w[23]"  1;
	setAttr -s 6 ".wl[103].w";
	setAttr ".wl[103].w[1]" 5.3239551260796862e-009;
	setAttr ".wl[103].w[2]" 5.6388976626500554e-009;
	setAttr ".wl[103].w[3]" 9.3710211541008249e-008;
	setAttr ".wl[103].w[4]" 9.5224463023252634e-008;
	setAttr ".wl[103].w[22]" 0.79999976448174015;
	setAttr ".wl[103].w[23]" 0.2000000356207326;
	setAttr -s 2 ".wl[104].w";
	setAttr ".wl[104].w[2]" 0.9;
	setAttr ".wl[104].w[22]" 0.1;
	setAttr -s 5 ".wl[105].w";
	setAttr ".wl[105].w[2]" 0.90000000200721586;
	setAttr ".wl[105].w[8]" -4.4604795512274219e-010;
	setAttr ".wl[105].w[9]" -1.3381438653682265e-009;
	setAttr ".wl[105].w[10]" -4.4604795512274219e-010;
	setAttr ".wl[105].w[22]" 0.10000000022302398;
	setAttr -s 3 ".wl[106].w";
	setAttr ".wl[106].w[2]" 0.90000002453758732;
	setAttr ".wl[106].w[7]" -6.9572934080497349e-010;
	setAttr ".wl[106].w[22]" 0.099999976158142101;
	setAttr -s 2 ".wl[107].w";
	setAttr ".wl[107].w[2]" 0.40000005206875716;
	setAttr ".wl[107].w[22]" 0.59999994793124289;
	setAttr -s 3 ".wl[108].w[8:10]"  0.1 0.36 0.54;
	setAttr -s 3 ".wl[109].w[8:10]"  0.54 0.36 0.1;
	setAttr -s 3 ".wl[110].w[8:10]"  0.2 0.6 0.2;
	setAttr -s 3 ".wl[111].w[8:10]"  0.2 0.6 0.2;
	setAttr -s 3 ".wl[112].w";
	setAttr ".wl[112].w[3]" 0.062247857428475849;
	setAttr ".wl[112].w[22]" 0.54283073985283503;
	setAttr ".wl[112].w[23]" 0.3949214027186892;
	setAttr -s 2 ".wl[113].w";
	setAttr ".wl[113].w[2]" 0.50000003576278684;
	setAttr ".wl[113].w[7]" 0.49999996423721316;
	setAttr -s 2 ".wl[114].w";
	setAttr ".wl[114].w[2]" 0.80000006788059863;
	setAttr ".wl[114].w[7]" 0.19999993211940117;
	setAttr ".wl[115].w[2]"  1;
	setAttr -s 2 ".wl[116].w";
	setAttr ".wl[116].w[2]" 0.40000002980232241;
	setAttr ".wl[116].w[22]" 0.59999997019767759;
	setAttr ".wl[117].w[23]"  1;
	setAttr -s 2 ".wl[118].w[22:23]"  0.79999985694885256 0.20000014305114747;
	setAttr -s 2 ".wl[119].w[23:24]"  0.5 0.5;
	setAttr -s 3 ".wl[120].w[16:18]"  0.19999999671331353 0.79999998685325413 
		1.6433432392659597e-008;
	setAttr -s 2 ".wl[121].w[16:17]"  0.2 0.8;
	setAttr -s 3 ".wl[122].w[16:18]"  0.1999999983240546 0.7999999932962184 
		8.3797270641912159e-009;
	setAttr -s 3 ".wl[123].w[16:18]"  0.19999999848146219 0.79999999392584875 
		7.5926891440758482e-009;
	setAttr -s 2 ".wl[124].w[16:17]"  0.2 0.8;
	setAttr -s 4 ".wl[125].w";
	setAttr ".wl[125].w[1]" 1.1329467202340222e-007;
	setAttr ".wl[125].w[2]" 0.89999988283337717;
	setAttr ".wl[125].w[3]" 4.5595202133013406e-008;
	setAttr ".wl[125].w[22]" 0.099999958276748666;
	setAttr ".wl[126].w[21]"  1;
	setAttr ".wl[127].w[21]"  1;
	setAttr ".wl[128].w[21]"  1;
	setAttr ".wl[129].w[21]"  1;
	setAttr ".wl[130].w[21]"  1;
	setAttr ".wl[131].w[21]"  1;
	setAttr ".wl[132].w[21]"  1;
	setAttr ".wl[133].w[21]"  1;
	setAttr ".wl[134].w[21]"  1;
	setAttr ".wl[135].w[21]"  1;
	setAttr ".wl[136].w[21]"  1;
	setAttr -s 3 ".wl[137].w[8:10]"  0.2 0.6 0.2;
	setAttr ".wl[138].w[21]"  1;
	setAttr -s 2 ".wl[139].w[20:21]"  0.49999979138374329 0.50000020861625671;
	setAttr ".wl[140].w[21]"  1;
	setAttr ".wl[141].w[21]"  1;
	setAttr ".wl[142].w[21]"  1;
	setAttr ".wl[143].w[21]"  1;
	setAttr ".wl[144].w[21]"  1;
	setAttr ".wl[145].w[21]"  1;
	setAttr ".wl[146].w[21]"  1;
	setAttr ".wl[147].w[21]"  1;
	setAttr ".wl[148].w[21]"  1;
	setAttr ".wl[149].w[21]"  1;
	setAttr ".wl[150].w[2]"  1;
	setAttr ".wl[151].w[2]"  1;
	setAttr ".wl[152].w[2]"  1;
	setAttr ".wl[153].w[2]"  1;
	setAttr -s 2 ".wl[154].w[22:23]"  0.8 0.2;
	setAttr -s 2 ".wl[155].w";
	setAttr ".wl[155].w[2]" 0.4;
	setAttr ".wl[155].w[22]" 0.6;
	setAttr -s 4 ".wl[156].w";
	setAttr ".wl[156].w[11]" 1.8213570234854512e-009;
	setAttr ".wl[156].w[12]" 1.9010153215490501e-008;
	setAttr ".wl[156].w[19]" 0.49999999978994447;
	setAttr ".wl[156].w[20]" 0.49999997937854535;
	setAttr ".wl[157].w[21]"  1;
	setAttr ".wl[158].w[21]"  1;
	setAttr ".wl[159].w[21]"  1;
	setAttr ".wl[160].w[21]"  1;
	setAttr ".wl[161].w[21]"  1;
	setAttr ".wl[162].w[21]"  1;
	setAttr ".wl[163].w[21]"  0.99999999999997158;
	setAttr ".wl[164].w[21]"  1;
	setAttr ".wl[165].w[21]"  1;
	setAttr ".wl[166].w[21]"  1;
	setAttr ".wl[167].w[21]"  1;
	setAttr ".wl[168].w[21]"  1;
	setAttr ".wl[169].w[21]"  1;
	setAttr ".wl[170].w[21]"  1;
	setAttr ".wl[171].w[21]"  1;
	setAttr ".wl[172].w[21]"  1;
	setAttr ".wl[173].w[21]"  1;
	setAttr ".wl[174].w[21]"  1;
	setAttr ".wl[175].w[21]"  1;
	setAttr ".wl[176].w[21]"  1;
	setAttr ".wl[177].w[21]"  1;
	setAttr ".wl[178].w[21]"  1;
	setAttr ".wl[179].w[21]"  1;
	setAttr ".wl[180].w[21]"  1;
	setAttr ".wl[181].w[21]"  1;
	setAttr ".wl[182].w[21]"  1;
	setAttr ".wl[183].w[21]"  1;
	setAttr ".wl[184].w[21]"  1;
	setAttr ".wl[185].w[21]"  1;
	setAttr ".wl[186].w[21]"  1;
	setAttr ".wl[187].w[21]"  1;
	setAttr ".wl[188].w[21]"  1;
	setAttr ".wl[189].w[21]"  1;
	setAttr ".wl[190].w[21]"  1;
	setAttr ".wl[191].w[21]"  1;
	setAttr ".wl[192].w[21]"  1;
	setAttr ".wl[193].w[21]"  1;
	setAttr ".wl[194].w[21]"  1;
	setAttr -s 2 ".wl[195].w[19:20]"  0.7 0.30000000000000004;
	setAttr ".wl[196].w[2]"  1;
	setAttr -s 4 ".wl[197].w";
	setAttr ".wl[197].w[12]" 0.0068628155876099854;
	setAttr ".wl[197].w[19]" 0.028712738423536544;
	setAttr ".wl[197].w[20]" 0.48221222299442679;
	setAttr ".wl[197].w[21]" 0.48221222299442679;
	setAttr ".wl[198].w[21]"  0.99999999999995737;
	setAttr -s 2 ".wl[199].w[19:20]"  0.4 0.6;
	setAttr -s 2 ".wl[200].w[20:21]"  0.8 0.19999999999999996;
	setAttr -s 2 ".wl[201].w[20:21]"  0.5 0.5;
	setAttr ".wl[202].w[21]"  1;
	setAttr ".wl[203].w[21]"  1;
	setAttr ".wl[204].w[2]"  1;
	setAttr ".wl[205].w[2]"  1;
	setAttr ".wl[206].w[2]"  1;
	setAttr ".wl[207].w[21]"  1;
	setAttr ".wl[208].w[21]"  1;
	setAttr ".wl[209].w[21]"  1;
	setAttr ".wl[210].w[21]"  0.99999999999998579;
	setAttr -s 2 ".wl[211].w[19:20]"  0.7 0.30000000000000004;
	setAttr -s 2 ".wl[212].w[19:20]"  0.5 0.5;
	setAttr -s 2 ".wl[213].w[19:20]"  0.4 0.6;
	setAttr -s 4 ".wl[214].w";
	setAttr ".wl[214].w[11]" 0.087095299900443721;
	setAttr ".wl[214].w[12]" 0.41147744149829524;
	setAttr ".wl[214].w[13]" 0.089674337893563466;
	setAttr ".wl[214].w[19]" 0.41175292070769764;
	setAttr -s 4 ".wl[215].w";
	setAttr ".wl[215].w[11]" 0.030557300475211553;
	setAttr ".wl[215].w[12]" 0.31893744960222647;
	setAttr ".wl[215].w[19]" 0.49647585220900869;
	setAttr ".wl[215].w[20]" 0.15402939771355328;
	setAttr -s 4 ".wl[216].w";
	setAttr ".wl[216].w[11]" 0.14386693500229347;
	setAttr ".wl[216].w[12]" 0.36607361129695909;
	setAttr ".wl[216].w[13]" 0.15292016917434023;
	setAttr ".wl[216].w[19]" 0.33713928452640712;
	setAttr -s 4 ".wl[217].w";
	setAttr ".wl[217].w[11]" 0.070152725621681983;
	setAttr ".wl[217].w[12]" 0.38441073064101711;
	setAttr ".wl[217].w[19]" 0.42178650230171294;
	setAttr ".wl[217].w[20]" 0.12365004143558797;
	setAttr ".wl[218].w[21]"  1;
	setAttr -s 2 ".wl[219].w[20:21]"  0.5 0.5;
	setAttr -s 2 ".wl[220].w[20:21]"  0.5 0.5;
	setAttr -s 2 ".wl[221].w[20:21]"  0.8 0.19999999999999996;
	setAttr -s 2 ".wl[222].w[20:21]"  0.8 0.19999999999999996;
	setAttr ".wl[223].w[21]"  1;
	setAttr ".wl[224].w[21]"  1;
	setAttr ".wl[225].w[21]"  1;
	setAttr ".wl[226].w[21]"  1;
	setAttr ".wl[227].w[21]"  1;
	setAttr ".wl[228].w[21]"  1;
	setAttr ".wl[229].w[21]"  1;
	setAttr ".wl[230].w[21]"  1;
	setAttr ".wl[231].w[21]"  1;
	setAttr ".wl[232].w[21]"  1;
	setAttr ".wl[233].w[21]"  1;
	setAttr -s 2 ".wl[234].w[13:14]"  0.6 0.4;
	setAttr -s 2 ".wl[235].w[13:14]"  0.6 0.4;
	setAttr -s 2 ".wl[236].w[13:14]"  0.2 0.8;
	setAttr -s 2 ".wl[237].w[13:14]"  0.2 0.8;
	setAttr ".wl[238].w[13]"  1;
	setAttr ".wl[239].w[13]"  1;
	setAttr ".wl[240].w[13]"  1;
	setAttr -s 2 ".wl[241].w[13:14]"  0.6 0.4;
	setAttr -s 2 ".wl[242].w[13:14]"  0.6 0.4;
	setAttr -s 2 ".wl[243].w[13:14]"  0.2 0.8;
	setAttr -s 2 ".wl[244].w[13:14]"  0.2 0.8;
	setAttr ".wl[245].w[13]"  1;
	setAttr -s 2 ".wl[246].w[13:14]"  0.6 0.4;
	setAttr -s 2 ".wl[247].w[12:13]"  0.2 0.8;
	setAttr -s 2 ".wl[248].w[13:14]"  0.2 0.8;
	setAttr -s 2 ".wl[249].w[12:13]"  0.099999999999999978 0.9;
	setAttr -s 2 ".wl[250].w[12:13]"  0.30000000000000004 0.7;
	setAttr -s 2 ".wl[251].w[12:13]"  0.30000000000000004 0.7;
	setAttr -s 2 ".wl[252].w[12:13]"  0.4 0.6;
	setAttr -s 2 ".wl[253].w[12:13]"  0.8 0.2;
	setAttr -s 2 ".wl[254].w[12:13]"  0.8 0.2;
	setAttr ".wl[255].w[15]"  1;
	setAttr ".wl[256].w[15]"  1;
	setAttr ".wl[257].w[15]"  1;
	setAttr ".wl[258].w[15]"  1;
	setAttr ".wl[259].w[15]"  1;
	setAttr ".wl[260].w[15]"  1;
	setAttr ".wl[261].w[15]"  1;
	setAttr ".wl[262].w[15]"  1;
	setAttr ".wl[263].w[15]"  1;
	setAttr ".wl[264].w[15]"  1;
	setAttr ".wl[265].w[15]"  1;
	setAttr ".wl[266].w[15]"  1;
	setAttr ".wl[267].w[15]"  1;
	setAttr ".wl[268].w[15]"  1;
	setAttr ".wl[269].w[15]"  1;
	setAttr ".wl[270].w[15]"  1;
	setAttr -s 4 ".wl[271].w[10:13]"  0.033802958227048895 0.058665385619203994 
		0.45376582807687355 0.45376582807687355;
	setAttr -s 4 ".wl[272].w";
	setAttr ".wl[272].w[11]" 0.29781362669475892;
	setAttr ".wl[272].w[12]" 0.3260522983722447;
	setAttr ".wl[272].w[16]" 0.16852486311728224;
	setAttr ".wl[272].w[19]" 0.20760921181571415;
	setAttr -s 4 ".wl[273].w[10:13]"  0.15591781347272443 0.15601774394915105 
		0.36041987438256567 0.32764456819555893;
	setAttr -s 4 ".wl[274].w";
	setAttr ".wl[274].w[9]" 0.17762548403792511;
	setAttr ".wl[274].w[10]" 0.18385450625479283;
	setAttr ".wl[274].w[12]" 0.29316356643252567;
	setAttr ".wl[274].w[13]" 0.3453564432747564;
	setAttr -s 4 ".wl[275].w";
	setAttr ".wl[275].w[10]" 0.14072592334173087;
	setAttr ".wl[275].w[11]" 0.12299861892762273;
	setAttr ".wl[275].w[12]" 0.36852686455867484;
	setAttr ".wl[275].w[19]" 0.36774859317197162;
	setAttr -s 4 ".wl[276].w";
	setAttr ".wl[276].w[9]" 0.13083259928350108;
	setAttr ".wl[276].w[10]" 0.13435623435430216;
	setAttr ".wl[276].w[12]" 0.36562784337410215;
	setAttr ".wl[276].w[19]" 0.36918332298809459;
	setAttr -s 2 ".wl[277].w[4:5]"  0.2 0.8;
	setAttr -s 2 ".wl[278].w[4:5]"  0.2 0.8;
	setAttr -s 2 ".wl[279].w[4:5]"  0.5 0.5;
	setAttr -s 2 ".wl[280].w[4:5]"  0.5 0.5;
	setAttr -s 2 ".wl[281].w[4:5]"  0.5 0.5;
	setAttr -s 2 ".wl[282].w[4:5]"  0.2 0.8;
	setAttr -s 2 ".wl[283].w[4:5]"  0.5 0.5;
	setAttr -s 2 ".wl[284].w[4:5]"  0.2 0.8;
	setAttr -s 2 ".wl[285].w[4:5]"  0.2 0.8;
	setAttr -s 2 ".wl[286].w[4:5]"  0.2 0.8;
	setAttr ".wl[287].w[4]"  1;
	setAttr -s 2 ".wl[288].w[4:5]"  0.5 0.5;
	setAttr ".wl[289].w[4]"  1;
	setAttr ".wl[290].w[4]"  1;
	setAttr ".wl[291].w[4]"  1;
	setAttr -s 2 ".wl[292].w[3:4]"  0.8 0.2;
	setAttr -s 2 ".wl[293].w[3:4]"  0.8 0.2;
	setAttr ".wl[294].w[4]"  1;
	setAttr ".wl[295].w[6]"  1;
	setAttr ".wl[296].w[6]"  1;
	setAttr ".wl[297].w[6]"  1;
	setAttr ".wl[298].w[6]"  1;
	setAttr ".wl[299].w[6]"  1;
	setAttr ".wl[300].w[6]"  1;
	setAttr ".wl[301].w[6]"  1;
	setAttr ".wl[302].w[6]"  1;
	setAttr ".wl[303].w[6]"  1;
	setAttr ".wl[304].w[6]"  1;
	setAttr ".wl[305].w[6]"  1;
	setAttr ".wl[306].w[6]"  1;
	setAttr ".wl[307].w[6]"  1;
	setAttr ".wl[308].w[6]"  1;
	setAttr ".wl[309].w[6]"  1;
	setAttr -s 2 ".wl[310].w[2:3]"  0.4 0.6;
	setAttr -s 2 ".wl[311].w[3:4]"  0.8 0.2;
	setAttr -s 2 ".wl[312].w[2:3]"  0.9 0.1;
	setAttr -s 2 ".wl[313].w[3:4]"  0.8 0.2;
	setAttr -s 2 ".wl[314].w[3:4]"  0.8 0.2;
	setAttr -s 2 ".wl[315].w[2:3]"  0.9 0.1;
	setAttr -s 2 ".wl[316].w[2:3]"  0.9 0.1;
	setAttr -s 2 ".wl[317].w[2:3]"  0.4 0.6;
	setAttr -s 3 ".wl[318].w[8:10]"  0.1 0.36 0.54;
	setAttr -s 3 ".wl[319].w[8:10]"  0.2 0.6 0.2;
	setAttr -s 3 ".wl[320].w[8:10]"  0.54 0.36 0.1;
	setAttr -s 3 ".wl[321].w[8:10]"  0.2 0.6 0.2;
	setAttr -s 2 ".wl[322].w";
	setAttr ".wl[322].w[2]" 0.5;
	setAttr ".wl[322].w[7]" 0.5;
	setAttr ".wl[323].w[2]"  1;
	setAttr -s 2 ".wl[324].w[2:3]"  0.9 0.1;
	setAttr -s 2 ".wl[325].w";
	setAttr ".wl[325].w[2]" 0.8;
	setAttr ".wl[325].w[7]" 0.19999999999999996;
	setAttr -s 2 ".wl[326].w";
	setAttr ".wl[326].w[2]" 0.8;
	setAttr ".wl[326].w[7]" 0.19999999999999996;
	setAttr -s 3 ".wl[327].w";
	setAttr ".wl[327].w[3]" 0.54283073985283503;
	setAttr ".wl[327].w[4]" 0.3949214027186892;
	setAttr ".wl[327].w[22]" 0.062247857428475849;
	setAttr -s 2 ".wl[328].w[3:4]"  0.8 0.2;
	setAttr ".wl[329].w[6]"  1;
	setAttr -s 2 ".wl[330].w[2:3]"  0.4 0.6;
	setAttr -s 3 ".wl[331].w";
	setAttr ".wl[331].w[1]" 0.4155340025482816;
	setAttr ".wl[331].w[2]" 0.41723521332962443;
	setAttr ".wl[331].w[22]" 0.16723078412209391;
	setAttr ".wl[332].w[21]"  1;
	setAttr -s 2 ".wl[333].w[20:21]"  0.5 0.5;
	setAttr ".wl[334].w[21]"  1;
	setAttr ".wl[335].w[21]"  1;
	setAttr ".wl[336].w[21]"  1;
	setAttr ".wl[337].w[21]"  1;
	setAttr ".wl[338].w[21]"  1;
	setAttr ".wl[339].w[21]"  1;
	setAttr ".wl[340].w[21]"  1;
	setAttr -s 2 ".wl[341].w[19:20]"  0.5 0.5;
	setAttr -s 2 ".wl[342].w[19:20]"  0.5 0.5;
	setAttr ".wl[343].w[21]"  1;
	setAttr ".wl[344].w[21]"  1;
	setAttr ".wl[345].w[21]"  1;
	setAttr ".wl[346].w[21]"  1;
	setAttr ".wl[347].w[21]"  1;
	setAttr -s 3 ".wl[348].w[8:10]"  0.2 0.6 0.2;
	setAttr ".wl[349].w[21]"  1;
	setAttr ".wl[350].w[21]"  1;
	setAttr ".wl[351].w[21]"  1;
	setAttr ".wl[352].w[21]"  1;
	setAttr ".wl[353].w[21]"  1;
	setAttr ".wl[354].w[21]"  1;
	setAttr ".wl[355].w[21]"  1;
	setAttr ".wl[356].w[2]"  1;
	setAttr ".wl[357].w[2]"  1;
	setAttr ".wl[358].w[2]"  1;
	setAttr ".wl[359].w[2]"  1;
	setAttr ".wl[360].w[2]"  1;
	setAttr ".wl[361].w[2]"  1;
	setAttr ".wl[362].w[2]"  1;
	setAttr ".wl[363].w[2]"  1;
	setAttr ".wl[364].w[2]"  1;
	setAttr ".wl[365].w[2]"  1;
	setAttr ".wl[366].w[21]"  1;
	setAttr ".wl[367].w[21]"  1;
	setAttr ".wl[368].w[21]"  1;
	setAttr ".wl[369].w[21]"  1;
	setAttr ".wl[370].w[21]"  1;
	setAttr ".wl[371].w[21]"  1;
	setAttr ".wl[372].w[21]"  1;
	setAttr ".wl[373].w[21]"  1;
	setAttr ".wl[374].w[21]"  1;
	setAttr ".wl[375].w[21]"  1;
	setAttr ".wl[376].w[21]"  1;
	setAttr ".wl[377].w[21]"  1;
	setAttr ".wl[378].w[21]"  1;
	setAttr ".wl[379].w[21]"  1;
	setAttr ".wl[380].w[21]"  1;
	setAttr ".wl[381].w[21]"  1;
	setAttr ".wl[382].w[21]"  1;
	setAttr ".wl[383].w[21]"  1;
	setAttr ".wl[384].w[21]"  1;
	setAttr ".wl[385].w[21]"  1;
	setAttr ".wl[386].w[21]"  1;
	setAttr ".wl[387].w[21]"  1;
	setAttr ".wl[388].w[21]"  1;
	setAttr ".wl[389].w[21]"  1;
	setAttr ".wl[390].w[21]"  1;
	setAttr ".wl[391].w[21]"  1;
	setAttr ".wl[392].w[21]"  1;
	setAttr ".wl[393].w[21]"  1;
	setAttr ".wl[394].w[21]"  1;
	setAttr ".wl[395].w[21]"  1;
	setAttr ".wl[396].w[21]"  1;
	setAttr ".wl[397].w[21]"  1;
	setAttr ".wl[398].w[21]"  1;
	setAttr ".wl[399].w[21]"  1;
	setAttr ".wl[400].w[21]"  1;
	setAttr ".wl[401].w[21]"  1;
	setAttr ".wl[402].w[21]"  1;
	setAttr ".wl[403].w[21]"  1;
	setAttr ".wl[404].w[2]"  1;
	setAttr ".wl[405].w[2]"  1;
	setAttr ".wl[406].w[21]"  1;
	setAttr -s 26 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -9.1612641917038103e-016 0 -10.322264773308 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503121e-016 0 -0.99999999999999978 0
		 0 1 0 0 0.99999999999999978 0 2.2204460492503121e-016 0 72.195067408442171 -232.42308711591741 1.6030525220243543e-014 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503121e-016 0 -0.99999999999999978 0
		 0 1 0 0 0.99999999999999978 0 2.2204460492503121e-016 0 72.195067408442171 -232.42308711591741 1.6030525220243543e-014 1;
	setAttr ".pm[3]" -type "matrix" -0.082713528313459725 0.023382815039709067 -0.9962990094317854 0
		 -0.95872590144880632 0.27102834184994612 0.085955126695519163 0 0.27203514134227674 0.96228731773565812 2.2898349882893849e-016 0
		 236.43813808718144 11.755317741013263 29.044632232905155 1;
	setAttr ".pm[4]" -type "matrix" -0.073166617250642413 -0.10096630584242022 -0.99219577261981551 0
		 -0.58220963005696424 -0.80342043655458895 0.1246898103108957 0 -0.80973983030911278 0.58678906534713915 -1.457167719820518e-016 0
		 47.745674631073378 163.49740886149249 22.815046113315365 1;
	setAttr ".pm[5]" -type "matrix" -0.064285018712655351 0.014618076783577714 -0.9978245077167941 0
		 -0.97298579392550133 0.22125187687209613 0.06592610863487261 0 0.22173425803938324 0.97510713196639276 4.8745729674948279e-016 0
		 146.46259211275418 70.102483872367571 30.377944204782487 1;
	setAttr ".pm[6]" -type "matrix" -0.064285018712655351 0.014618076783577714 -0.9978245077167941 0
		 -0.97298579392550133 0.22125187687209613 0.06592610863487261 0 0.22173425803938324 0.97510713196639276 4.8745729674948279e-016 0
		 59.875417247323441 70.102483872367415 30.377944204782498 1;
	setAttr ".pm[7]" -type "matrix" 2.2204460492503121e-016 0 -0.99999999999999978 0
		 0 1 0 0 0.99999999999999978 0 2.2204460492503121e-016 0 55.691600826692003 -232.42308711591741 4.2416874648700516e-014 1;
	setAttr ".pm[8]" -type "matrix" 2.2204460492503121e-016 0 -0.99999999999999978 0
		 0 1 0 0 0.99999999999999978 0 2.2204460492503121e-016 0 22.684668026691988 -232.42308711591741 2.0601614292205784e-014 1;
	setAttr ".pm[9]" -type "matrix" 2.2204460492503121e-016 0 -0.99999999999999978 0
		 0 1 0 0 0.99999999999999978 0 2.2204460492503121e-016 0 -10.32226477330801 -232.42308711591741 -1.3758767843503639e-015 1;
	setAttr ".pm[10]" -type "matrix" 2.2204460492503121e-016 0 -0.99999999999999978 0
		 0 1 0 0 0.99999999999999978 0 2.2204460492503121e-016 0 -43.329197573307994 -232.42308711591741 -2.3353367860906504e-014 1;
	setAttr ".pm[11]" -type "matrix" 2.2204460492503121e-016 0 -0.99999999999999978 0
		 0 1 0 0 0.99999999999999978 0 2.2204460492503121e-016 0 -76.336130373308023 -232.42308711591741 -4.5168628217401248e-014 1;
	setAttr ".pm[12]" -type "matrix" 2.2204460492503121e-016 0 -0.99999999999999978 0
		 0 1 0 0 0.99999999999999978 0 2.2204460492503121e-016 0 -92.83959695505817 -232.42308711591741 -2.0614531627285034e-014 1;
	setAttr ".pm[13]" -type "matrix" -0.087860666602631393 0.0085306042079501144 -0.99609624638183847 0
		 -0.99143411462521047 0.096260731418963907 0.088273823662575343 0 0.096637982291988 0.99531959710363049 7.63278329429795e-017 0
		 206.75031199475356 -119.23693352951138 26.910745406720231 1;
	setAttr ".pm[14]" -type "matrix" -0.05208392701165919 -0.010735671937347093 -0.99858500384048299 0
		 -0.97802469014108151 -0.20159294474147524 0.053178850165288311 0 -0.20187860218826018 0.97941055231119201 2.3765711620882252e-016 0
		 132.58015817584899 -83.689807021846079 30.903373178698871 1;
	setAttr ".pm[15]" -type "matrix" -0.05208392701165919 -0.010735671937347093 -0.99858500384048299 0
		 -0.97802469014108151 -0.20159294474147524 0.053178850165288311 0 -0.20187860218826018 0.97941055231119201 2.3765711620882252e-016 0
		 66.905373726078963 -83.689807021846036 30.903373178698889 1;
	setAttr ".pm[16]" -type "matrix" -0.087860666602631365 0.008530604207949273 -0.99609624638183847 0
		 0.99143411462521025 -0.096260731418964837 -0.088273823662575218 0 -0.096637982291989027 -0.99531959710363016 8.3960616237277424e-016 0
		 -206.75068069854393 119.23694556558192 -26.910737070901288 1;
	setAttr ".pm[17]" -type "matrix" -0.052083927011659217 -0.010735671937331085 -0.99858500384048332 0
		 0.97802469014108162 0.20159294474147546 -0.053178850165285105 0 0.20187860218825943 -0.97941055231119212 -1.5914353168611225e-014 0
		 -132.58021662687068 83.690022530518462 -30.90339849051168 1;
	setAttr ".pm[18]" -type "matrix" -0.052083927011659217 -0.010735671937331085 -0.99858500384048332 0
		 0.97802469014108162 0.20159294474147546 -0.053178850165285105 0 0.20187860218825943 -0.97941055231119212 -1.5914353168611225e-014 0
		 -66.90534002389542 83.689769610530718 -30.903401258651662 1;
	setAttr ".pm[19]" -type "matrix" 4.6745536959594181e-017 -3.4948555639792369e-016 0.99999999999999978 0
		 0.97758886526498945 -0.21052318283246221 -1.1927272811939723e-016 0 0.21052318283246227 0.97758886526498923 3.3181216928160342e-016 0
		 -298.71783020029818 -38.131712171587608 6.3724272742506342e-016 1;
	setAttr ".pm[20]" -type "matrix" -1.1194336584910253e-016 0.99999999999999956 3.6674177308981428e-016 0
		 0.59590313857010768 -1.1927272811939718e-016 0.80305631772765163 0 0.80305631772765151 3.3181216928160327e-016 -0.59590313857010724 0
		 -292.2029609260598 6.9196983182535072e-015 -205.70175856768014 1;
	setAttr ".pm[21]" -type "matrix" 0.99999999999999956 9.5489817825448543e-017 -4.8675372360419306e-016 0
		 3.2481648173066539e-016 0.99999999999999978 1.1102230246251542e-016 0 6.0936792543789231e-016 1.1102230246251563e-016 0.99999999999999956 0
		 -2.0358029345351083e-013 -386.21650480897478 -175.28326143825208 1;
	setAttr ".pm[22]" -type "matrix" -0.082713528313459739 0.023382815039712432 -0.9962990094317854 0
		 0.95872590144880621 -0.27102834184994612 -0.085955126695520093 0 -0.27203514134227708 -0.96228731773565801 -3.4416913763379845e-015 0
		 -236.43832918530416 -11.755289187393471 -29.044617171560738 1;
	setAttr ".pm[23]" -type "matrix" -0.073166617250642468 -0.10096630584243045 -0.99219577261981451 0
		 0.58220963005696469 0.8034204365545875 -0.124689810310904 0 0.80973983030911267 -0.58678906534713948 6.210310043996971e-015 0
		 -47.745485530566768 -163.49711849996933 -22.81507832284316 1;
	setAttr ".pm[24]" -type "matrix" -0.064285018712655351 0.014618076783561031 -0.99782450771679443 0
		 0.97298579392550111 -0.22125187687209807 -0.065926108634868918 0 -0.22173425803938399 -0.97510713196639254 1.585363784695204e-014 0
		 -146.46252828924011 -70.102286936784978 -30.377901130858714 1;
	setAttr ".pm[25]" -type "matrix" -0.064285018712655351 0.014618076783561031 -0.99782450771679443 0
		 0.97298579392550111 -0.22125187687209807 -0.065926108634868918 0 -0.22173425803938399 -0.97510713196639254 1.585363784695204e-014 0
		 -59.875402359342758 -70.102524986349891 -30.377959748887093 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 25 ".ma";
	setAttr -s 26 ".dpf[0:25]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 
		4 4 4 4 4 4;
	setAttr -s 25 ".lw";
	setAttr -s 25 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 4;
	setAttr ".ucm" yes;
	setAttr -s 25 ".ifcl";
	setAttr -s 25 ".ifcl";
createNode tweak -n "tweak2";
	rename -uid "681923BC-46BB-F965-E00A-F9A267E38B4A";
createNode objectSet -n "skinCluster2Set";
	rename -uid "D276F4AA-470A-D74A-8147-829278E74785";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster2GroupId";
	rename -uid "86925C54-4C61-0219-4284-DD88CC96EFC2";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster2GroupParts";
	rename -uid "D2D3C4A0-4EB1-C112-E45F-699D042C60D4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet2";
	rename -uid "425C6C43-4561-DDCA-522C-F484F0E058C9";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId4";
	rename -uid "2667A72D-487C-48A4-ED4A-05BDCC4EC97C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "F2211BE7-4842-2959-6BBD-EB9500DBD63C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode skinCluster -n "skinCluster3";
	rename -uid "1C67A092-4C15-8ECB-6A40-7298F12B339E";
	setAttr -s 163 ".wl";
	setAttr -s 6 ".wl[0].w";
	setAttr ".wl[0].w[8]" 0.061487822145828583;
	setAttr ".wl[0].w[9]" 0.19572610401047499;
	setAttr ".wl[0].w[10]" 0.1712417739053006;
	setAttr ".wl[0].w[11]" 0.098159224413742557;
	setAttr ".wl[0].w[12]" 0.24534821127902176;
	setAttr ".wl[0].w[16]" 0.22803686424563152;
	setAttr -s 3 ".wl[1].w[8:10]"  0.15281211367113021 0.36 0.48718788632886978;
	setAttr -s 6 ".wl[2].w";
	setAttr ".wl[2].w[8]" 0.17240371415971931;
	setAttr ".wl[2].w[9]" 0.52370123276723213;
	setAttr ".wl[2].w[10]" 0.23307879995521186;
	setAttr ".wl[2].w[11]" 0.013089474251118909;
	setAttr ".wl[2].w[12]" 0.030238269993570023;
	setAttr ".wl[2].w[16]" 0.027488508873147813;
	setAttr -s 7 ".wl[3].w";
	setAttr ".wl[3].w[8]" 2.5086492135383941e-009;
	setAttr ".wl[3].w[9]" 7.5259476406151824e-009;
	setAttr ".wl[3].w[10]" 0.13718525689892364;
	setAttr ".wl[3].w[11]" 0.12516175023974591;
	setAttr ".wl[3].w[12]" 0.36924234956922714;
	setAttr ".wl[3].w[16]" 0.03774969511800675;
	setAttr ".wl[3].w[19]" 0.33066093813949959;
	setAttr -s 3 ".wl[4].w[8:10]"  0.52619145145178503 0.36 0.11380854854821504;
	setAttr -s 5 ".wl[5].w";
	setAttr ".wl[5].w[2]" 0.4602909417360479;
	setAttr ".wl[5].w[7]" 0.46029087589090523;
	setAttr ".wl[5].w[8]" 0.042885818481445313;
	setAttr ".wl[5].w[9]" 0.028590545654296876;
	setAttr ".wl[5].w[10]" 0.0079418182373046878;
	setAttr -s 6 ".wl[6].w";
	setAttr ".wl[6].w[2]" 0.59389168904629475;
	setAttr ".wl[6].w[7]" 0.073380120019068323;
	setAttr ".wl[6].w[8]" 0.054978714664035887;
	setAttr ".wl[6].w[9]" 0.16493614399210765;
	setAttr ".wl[6].w[10]" 0.054978714664035887;
	setAttr ".wl[6].w[22]" 0.057834617614457261;
	setAttr -s 5 ".wl[7].w";
	setAttr ".wl[7].w[2]" 0.61691966752881722;
	setAttr ".wl[7].w[7]" 0.15422985144959042;
	setAttr ".wl[7].w[8]" 0.1235792597516599;
	setAttr ".wl[7].w[9]" 0.082386173167773247;
	setAttr ".wl[7].w[10]" 0.022885048102159238;
	setAttr -s 5 ".wl[8].w";
	setAttr ".wl[8].w[2]" 0.72705650383936993;
	setAttr ".wl[8].w[7]" 0.18176412595984243;
	setAttr ".wl[8].w[8]" 0.049236859908425364;
	setAttr ".wl[8].w[9]" 0.032824573272283573;
	setAttr ".wl[8].w[10]" 0.0091179370200787698;
	setAttr -s 6 ".wl[9].w";
	setAttr ".wl[9].w[8]" 0.10475504895400496;
	setAttr ".wl[9].w[9]" 0.36780271207979209;
	setAttr ".wl[9].w[10]" 0.18742728642188203;
	setAttr ".wl[9].w[11]" 0.027274670938486213;
	setAttr ".wl[9].w[12]" 0.1513693440877133;
	setAttr ".wl[9].w[16]" 0.16137093751812137;
	setAttr -s 5 ".wl[10].w";
	setAttr ".wl[10].w[2]" 0.065084070101926142;
	setAttr ".wl[10].w[8]" 0.18553687331068308;
	setAttr ".wl[10].w[9]" 0.55661061993204919;
	setAttr ".wl[10].w[10]" 0.18553687331068308;
	setAttr ".wl[10].w[22]" 0.0072315633446584605;
	setAttr -s 5 ".wl[11].w";
	setAttr ".wl[11].w[2]" 0.31466101751669445;
	setAttr ".wl[11].w[8]" 0.13007532944073461;
	setAttr ".wl[11].w[9]" 0.39022598832220373;
	setAttr ".wl[11].w[10]" 0.13007532944073461;
	setAttr ".wl[11].w[22]" 0.03496233527963271;
	setAttr -s 5 ".wl[12].w";
	setAttr ".wl[12].w[2]" 0.36763424512934739;
	setAttr ".wl[12].w[7]" 0.09190852228978906;
	setAttr ".wl[12].w[8]" 0.29184690559366633;
	setAttr ".wl[12].w[9]" 0.19456460372911083;
	setAttr ".wl[12].w[10]" 0.054045723258086348;
	setAttr -s 5 ".wl[13].w";
	setAttr ".wl[13].w[2]" 0.26068478161914421;
	setAttr ".wl[13].w[7]" 0.17240113680736946;
	setAttr ".wl[13].w[8]" 0.30613360404968265;
	setAttr ".wl[13].w[9]" 0.20408906936645507;
	setAttr ".wl[13].w[10]" 0.056691408157348633;
	setAttr -s 6 ".wl[14].w";
	setAttr ".wl[14].w[2]" 0.24407676371870118;
	setAttr ".wl[14].w[7]" 0.067222351958686616;
	setAttr ".wl[14].w[8]" 0.13381007903910513;
	setAttr ".wl[14].w[9]" 0.4014302371173154;
	setAttr ".wl[14].w[10]" 0.13381007903910513;
	setAttr ".wl[14].w[22]" 0.019650489127086588;
	setAttr -s 5 ".wl[15].w";
	setAttr ".wl[15].w[8]" 0.13256608579464738;
	setAttr ".wl[15].w[9]" 0.45758816564043048;
	setAttr ".wl[15].w[10]" 0.19455623079991322;
	setAttr ".wl[15].w[12]" 0.098845833934730648;
	setAttr ".wl[15].w[16]" 0.11644368383027827;
	setAttr -s 6 ".wl[16].w";
	setAttr ".wl[16].w[8]" 0.11958505105389322;
	setAttr ".wl[16].w[9]" 0.36743346903959401;
	setAttr ".wl[16].w[10]" 0.23464078224033735;
	setAttr ".wl[16].w[11]" 0.05144770070053617;
	setAttr ".wl[16].w[12]" 0.11885041633343001;
	setAttr ".wl[16].w[16]" 0.10804258063220924;
	setAttr -s 6 ".wl[17].w";
	setAttr ".wl[17].w[8]" 0.047972959211423284;
	setAttr ".wl[17].w[9]" 0.1780180238413899;
	setAttr ".wl[17].w[10]" 0.33201073023640071;
	setAttr ".wl[17].w[11]" 0.058995444154429678;
	setAttr ".wl[17].w[12]" 0.19161584283893079;
	setAttr ".wl[17].w[19]" 0.1913869997174257;
	setAttr -s 6 ".wl[18].w";
	setAttr ".wl[18].w[2]" 0.36763827492698831;
	setAttr ".wl[18].w[7]" 0.10527480065134302;
	setAttr ".wl[18].w[8]" 0.09958708579064568;
	setAttr ".wl[18].w[9]" 0.29876125737193704;
	setAttr ".wl[18].w[10]" 0.09958708579064568;
	setAttr ".wl[18].w[22]" 0.029151495468440352;
	setAttr -s 5 ".wl[19].w";
	setAttr ".wl[19].w[2]" 0.6217954158782959;
	setAttr ".wl[19].w[7]" 0.15544885396957395;
	setAttr ".wl[19].w[8]" 0.12028809428215027;
	setAttr ".wl[19].w[9]" 0.080192062854766849;
	setAttr ".wl[19].w[10]" 0.022275573015213015;
	setAttr -s 5 ".wl[20].w";
	setAttr ".wl[20].w[2]" 0.56831030899425594;
	setAttr ".wl[20].w[7]" 0.14207751697162524;
	setAttr ".wl[20].w[8]" 0.1563905739784241;
	setAttr ".wl[20].w[9]" 0.10426038265228271;
	setAttr ".wl[20].w[10]" 0.028961217403411864;
	setAttr -s 5 ".wl[21].w";
	setAttr ".wl[21].w[2]" 0.38916714950594206;
	setAttr ".wl[21].w[7]" 0.34076628514287188;
	setAttr ".wl[21].w[8]" 0.14583594528964039;
	setAttr ".wl[21].w[9]" 0.097223963526426926;
	setAttr ".wl[21].w[10]" 0.027006656535118589;
	setAttr -s 6 ".wl[22].w";
	setAttr ".wl[22].w[8]" 0.093828082820407138;
	setAttr ".wl[22].w[9]" 0.28148424846122139;
	setAttr ".wl[22].w[10]" 0.17659854871465366;
	setAttr ".wl[22].w[11]" 0.082823514945580962;
	setAttr ".wl[22].w[12]" 0.19133234526412513;
	setAttr ".wl[22].w[16]" 0.17393325979401172;
	setAttr -s 6 ".wl[23].w";
	setAttr ".wl[23].w[8]" 0.025460523366928101;
	setAttr ".wl[23].w[9]" 0.091657884120941155;
	setAttr ".wl[23].w[10]" 0.24238319292739591;
	setAttr ".wl[23].w[11]" 0.091682526814556492;
	setAttr ".wl[23].w[12]" 0.27469799609430595;
	setAttr ".wl[23].w[19]" 0.27411787667587245;
	setAttr ".wl[24].w[9]"  1;
	setAttr ".wl[25].w[9]"  1;
	setAttr -s 5 ".wl[26].w";
	setAttr ".wl[26].w[2]" 0.065084070101926142;
	setAttr ".wl[26].w[8]" 0.18553687331068308;
	setAttr ".wl[26].w[9]" 0.55661061993204919;
	setAttr ".wl[26].w[10]" 0.18553687331068308;
	setAttr ".wl[26].w[22]" 0.0072315633446584605;
	setAttr -s 5 ".wl[27].w";
	setAttr ".wl[27].w[8]" 0.13256608579464738;
	setAttr ".wl[27].w[9]" 0.45758816564043048;
	setAttr ".wl[27].w[10]" 0.19455623079991322;
	setAttr ".wl[27].w[12]" 0.098845833934730648;
	setAttr ".wl[27].w[16]" 0.11644368383027827;
	setAttr ".wl[28].w[9]"  1;
	setAttr -s 5 ".wl[29].w";
	setAttr ".wl[29].w[8]" 0.13055675355127933;
	setAttr ".wl[29].w[9]" 0.4533447119719326;
	setAttr ".wl[29].w[10]" 0.1943940224940765;
	setAttr ".wl[29].w[12]" 0.10179114896779885;
	setAttr ".wl[29].w[16]" 0.11991336301491262;
	setAttr ".wl[30].w[9]"  1;
	setAttr ".wl[31].w[9]"  1;
	setAttr ".wl[32].w[9]"  1;
	setAttr ".wl[33].w[9]"  1;
	setAttr ".wl[34].w[9]"  1;
	setAttr ".wl[35].w[9]"  1;
	setAttr ".wl[36].w[9]"  1;
	setAttr ".wl[37].w[9]"  1;
	setAttr ".wl[38].w[9]"  1;
	setAttr ".wl[39].w[9]"  1;
	setAttr ".wl[40].w[9]"  1;
	setAttr ".wl[41].w[9]"  1;
	setAttr ".wl[42].w[9]"  1;
	setAttr ".wl[43].w[9]"  1;
	setAttr ".wl[44].w[9]"  1;
	setAttr ".wl[45].w[9]"  1;
	setAttr ".wl[46].w[9]"  1;
	setAttr ".wl[47].w[9]"  1;
	setAttr ".wl[48].w[9]"  1;
	setAttr ".wl[49].w[21]"  1;
	setAttr ".wl[50].w[21]"  1;
	setAttr ".wl[51].w[21]"  1;
	setAttr ".wl[52].w[21]"  1;
	setAttr ".wl[53].w[21]"  1;
	setAttr ".wl[54].w[21]"  0.99999999999996247;
	setAttr ".wl[55].w[21]"  0.9999999999999829;
	setAttr -s 7 ".wl[56].w";
	setAttr ".wl[56].w[8]" 6.7648230128725123e-009;
	setAttr ".wl[56].w[9]" 2.0294469038617536e-008;
	setAttr ".wl[56].w[10]" 0.051460543611214715;
	setAttr ".wl[56].w[11]" 0.10057810073011093;
	setAttr ".wl[56].w[12]" 0.40602393921835128;
	setAttr ".wl[56].w[16]" 0.23497091593599351;
	setAttr ".wl[56].w[19]" 0.20696647344503757;
	setAttr -s 2 ".wl[57].w[20:21]"  0.5 0.5;
	setAttr -s 6 ".wl[58].w";
	setAttr ".wl[58].w[10]" 0.067687979686619465;
	setAttr ".wl[58].w[11]" 0.090924840575998256;
	setAttr ".wl[58].w[12]" 0.32732359442426651;
	setAttr ".wl[58].w[16]" 0.032704136376955219;
	setAttr ".wl[58].w[19]" 0.43506652303392257;
	setAttr ".wl[58].w[20]" 0.046292925902237839;
	setAttr -s 7 ".wl[59].w";
	setAttr ".wl[59].w[8]" 0.010605395593335938;
	setAttr ".wl[59].w[9]" 0.038179419708248348;
	setAttr ".wl[59].w[10]" 0.1877727291668041;
	setAttr ".wl[59].w[11]" 0.12017430842950196;
	setAttr ".wl[59].w[12]" 0.32693384725594526;
	setAttr ".wl[59].w[16]" 0.10141349436659478;
	setAttr ".wl[59].w[19]" 0.21492080547956963;
	setAttr -s 2 ".wl[60].w[20:21]"  0.5 0.5;
	setAttr -s 2 ".wl[61].w[19:20]"  0.5 0.5;
	setAttr -s 5 ".wl[62].w";
	setAttr ".wl[62].w[9]" 0.054603553820696059;
	setAttr ".wl[62].w[10]" 0.056074158228822668;
	setAttr ".wl[62].w[12]" 0.15259636920276712;
	setAttr ".wl[62].w[19]" 0.5619322223442107;
	setAttr ".wl[62].w[20]" 0.17479369640350345;
	setAttr -s 5 ".wl[63].w";
	setAttr ".wl[63].w[9]" 0.11352568197008948;
	setAttr ".wl[63].w[10]" 0.11658320033032329;
	setAttr ".wl[63].w[12]" 0.31726152727695967;
	setAttr ".wl[63].w[19]" 0.41294471660327631;
	setAttr ".wl[63].w[20]" 0.0396848738193512;
	setAttr -s 2 ".wl[64].w[19:20]"  0.5 0.5;
	setAttr -s 6 ".wl[65].w";
	setAttr ".wl[65].w[8]" 1.7794956173977481e-008;
	setAttr ".wl[65].w[9]" 5.3384868521932446e-008;
	setAttr ".wl[65].w[10]" 0.1179566261269336;
	setAttr ".wl[65].w[11]" 0.119195897918609;
	setAttr ".wl[65].w[12]" 0.35229913179836747;
	setAttr ".wl[65].w[16]" 0.41054827297626523;
	setAttr ".wl[66].w[21]"  1;
	setAttr ".wl[67].w[21]"  1;
	setAttr ".wl[68].w[21]"  1;
	setAttr ".wl[69].w[21]"  1;
	setAttr ".wl[70].w[21]"  1;
	setAttr ".wl[71].w[21]"  1;
	setAttr ".wl[72].w[21]"  1;
	setAttr -s 6 ".wl[73].w";
	setAttr ".wl[73].w[8]" 1.7824944809905086e-008;
	setAttr ".wl[73].w[9]" 5.3474834429715253e-008;
	setAttr ".wl[73].w[10]" 0.11842336462545623;
	setAttr ".wl[73].w[11]" 0.11986180779896941;
	setAttr ".wl[73].w[12]" 0.35360616026071839;
	setAttr ".wl[73].w[16]" 0.40810859601507671;
	setAttr -s 7 ".wl[74].w";
	setAttr ".wl[74].w[8]" 6.7055815122785139e-009;
	setAttr ".wl[74].w[9]" 2.0116744536835541e-008;
	setAttr ".wl[74].w[10]" 0.05104807884333791;
	setAttr ".wl[74].w[11]" 0.10042790864157282;
	setAttr ".wl[74].w[12]" 0.40611947342642779;
	setAttr ".wl[74].w[16]" 0.23410985418218344;
	setAttr ".wl[74].w[19]" 0.20829465808415198;
	setAttr -s 6 ".wl[75].w";
	setAttr ".wl[75].w[10]" 0.068339628486337733;
	setAttr ".wl[75].w[11]" 0.090856376582886783;
	setAttr ".wl[75].w[12]" 0.3260157894908487;
	setAttr ".wl[75].w[16]" 0.032047218687271145;
	setAttr ".wl[75].w[19]" 0.4356395715879926;
	setAttr ".wl[75].w[20]" 0.047101415164663055;
	setAttr -s 7 ".wl[76].w";
	setAttr ".wl[76].w[8]" 0.010549915403708981;
	setAttr ".wl[76].w[9]" 0.037979691137105309;
	setAttr ".wl[76].w[10]" 0.18743278509109534;
	setAttr ".wl[76].w[11]" 0.11998515113951012;
	setAttr ".wl[76].w[12]" 0.32720150348795546;
	setAttr ".wl[76].w[16]" 0.098859376325646592;
	setAttr ".wl[76].w[19]" 0.21799157741497818;
	setAttr -s 2 ".wl[77].w[20:21]"  0.5 0.5;
	setAttr -s 2 ".wl[78].w[19:20]"  0.5 0.5;
	setAttr -s 2 ".wl[79].w[19:20]"  0.5 0.5;
	setAttr -s 2 ".wl[80].w[20:21]"  0.5 0.5;
	setAttr -s 5 ".wl[81].w";
	setAttr ".wl[81].w[9]" 0.12491615190730954;
	setAttr ".wl[81].w[10]" 0.12828044288815546;
	setAttr ".wl[81].w[12]" 0.34909360109473109;
	setAttr ".wl[81].w[19]" 0.38414335184501813;
	setAttr ".wl[81].w[20]" 0.013566452264785769;
	setAttr -s 5 ".wl[82].w";
	setAttr ".wl[82].w[8]" 0.030004805326461794;
	setAttr ".wl[82].w[9]" 0.19959383174019918;
	setAttr ".wl[82].w[10]" 0.25606885655522271;
	setAttr ".wl[82].w[12]" 0.25592192075036219;
	setAttr ".wl[82].w[19]" 0.25841058562775415;
	setAttr -s 3 ".wl[83].w[8:10]"  0.15281215202386 0.36 0.48718784797614006;
	setAttr -s 6 ".wl[84].w[8:13]"  0.17240370722702805 0.52370121262145308 
		0.23307880236313058 0.013089478811153311 0.030238280527801482 0.027488518449433591;
	setAttr -s 6 ".wl[85].w[8:13]"  0.11958503723144531 0.3674334168434143 
		0.23464071445572643 0.051447725432345166 0.11885047346690256 0.10804263257016626;
	setAttr -s 3 ".wl[86].w[8:10]"  0.14703475713729861 0.36 0.49296524286270144;
	setAttr -s 3 ".wl[87].w[8:10]"  0.4966152787208557 0.36 0.14338472127914428;
	setAttr -s 3 ".wl[88].w[8:10]"  0.52619142900552907 0.36 0.11380857099447102;
	setAttr -s 6 ".wl[89].w";
	setAttr ".wl[89].w[8]" 0.047972952607637093;
	setAttr ".wl[89].w[9]" 0.17801799773442856;
	setAttr ".wl[89].w[10]" 0.33201070398279442;
	setAttr ".wl[89].w[11]" 0.05899545447061158;
	setAttr ".wl[89].w[12]" 0.19161586722735954;
	setAttr ".wl[89].w[19]" 0.19138702397716886;
	setAttr -s 6 ".wl[90].w";
	setAttr ".wl[90].w[2]" 0.24407675862312317;
	setAttr ".wl[90].w[3]" 0.019650489091873169;
	setAttr ".wl[90].w[7]" 0.067222356796264648;
	setAttr ".wl[90].w[8]" 0.13381007909774781;
	setAttr ".wl[90].w[9]" 0.4014302372932434;
	setAttr ".wl[90].w[10]" 0.13381007909774781;
	setAttr -s 5 ".wl[91].w";
	setAttr ".wl[91].w[2]" 0.36763430930273805;
	setAttr ".wl[91].w[7]" 0.091908577325684485;
	setAttr ".wl[91].w[8]" 0.29184684122065185;
	setAttr ".wl[91].w[9]" 0.19456456081376788;
	setAttr ".wl[91].w[10]" 0.054045711337157749;
	setAttr -s 5 ".wl[92].w";
	setAttr ".wl[92].w[2]" 0.26068490743637085;
	setAttr ".wl[92].w[7]" 0.17240124940872192;
	setAttr ".wl[92].w[8]" 0.30613347530364993;
	setAttr ".wl[92].w[9]" 0.2040889835357666;
	setAttr ".wl[92].w[10]" 0.056691384315490728;
	setAttr -s 5 ".wl[93].w";
	setAttr ".wl[93].w[2]" 0.37040390968322756;
	setAttr ".wl[93].w[7]" 0.092600977420806863;
	setAttr ".wl[93].w[8]" 0.28997736096382143;
	setAttr ".wl[93].w[9]" 0.1933182406425476;
	setAttr ".wl[93].w[10]" 0.05369951128959656;
	setAttr -s 5 ".wl[94].w";
	setAttr ".wl[94].w[8]" 0.1325660467147827;
	setAttr ".wl[94].w[9]" 0.45758808310873594;
	setAttr ".wl[94].w[10]" 0.19455622764509464;
	setAttr ".wl[94].w[12]" 0.098845891218693152;
	setAttr ".wl[94].w[13]" 0.11644375131269358;
	setAttr -s 5 ".wl[95].w";
	setAttr ".wl[95].w[2]" 0.065083783084536717;
	setAttr ".wl[95].w[3]" 0.0072315314538374123;
	setAttr ".wl[95].w[8]" 0.18553693709232519;
	setAttr ".wl[95].w[9]" 0.55661081127697543;
	setAttr ".wl[95].w[10]" 0.18553693709232519;
	setAttr -s 5 ".wl[96].w";
	setAttr ".wl[96].w[2]" 0.56831045150756843;
	setAttr ".wl[96].w[7]" 0.14207761287689205;
	setAttr ".wl[96].w[8]" 0.15639044523239137;
	setAttr ".wl[96].w[9]" 0.10426029682159424;
	setAttr ".wl[96].w[10]" 0.028961193561553959;
	setAttr -s 5 ".wl[97].w";
	setAttr ".wl[97].w[2]" 0.38916693329811097;
	setAttr ".wl[97].w[7]" 0.34076614975929259;
	setAttr ".wl[97].w[8]" 0.14583613514900209;
	setAttr ".wl[97].w[9]" 0.09722409009933472;
	setAttr ".wl[97].w[10]" 0.027006691694259646;
	setAttr -s 6 ".wl[98].w";
	setAttr ".wl[98].w[2]" 0.36763807535171511;
	setAttr ".wl[98].w[3]" 0.029151475429534914;
	setAttr ".wl[98].w[7]" 0.10527479648590088;
	setAttr ".wl[98].w[8]" 0.099587130546569827;
	setAttr ".wl[98].w[9]" 0.29876139163970944;
	setAttr ".wl[98].w[10]" 0.099587130546569827;
	setAttr -s 6 ".wl[99].w[8:13]"  0.10475504398345947 0.36780270317458763 
		0.18742728627100286 0.027274669540189644 0.15136935077084823 0.16137094625991216;
	setAttr -s 6 ".wl[100].w[8:13]"  0.093828046321868905 0.2814841389656067 
		0.17659854066997682 0.082823543417678919 0.19133241103811796 0.17393331958675076;
	setAttr -s 6 ".wl[101].w";
	setAttr ".wl[101].w[8]" 0.025460517406463625;
	setAttr ".wl[101].w[9]" 0.091657862663269044;
	setAttr ".wl[101].w[10]" 0.24238316912880639;
	setAttr ".wl[101].w[11]" 0.091682534145845485;
	setAttr ".wl[101].w[12]" 0.27469801806021882;
	setAttr ".wl[101].w[19]" 0.2741178985953967;
	setAttr -s 5 ".wl[102].w";
	setAttr ".wl[102].w[8]" 0.054819625616073613;
	setAttr ".wl[102].w[9]" 0.25646131039037295;
	setAttr ".wl[102].w[10]" 0.35672862801621674;
	setAttr ".wl[102].w[12]" 0.16519202848829537;
	setAttr ".wl[102].w[19]" 0.16679840748904132;
	setAttr -s 5 ".wl[103].w";
	setAttr ".wl[103].w[2]" 0.61691975593566895;
	setAttr ".wl[103].w[7]" 0.15422993898391721;
	setAttr ".wl[103].w[8]" 0.12357916474342348;
	setAttr ".wl[103].w[9]" 0.082386109828948978;
	setAttr ".wl[103].w[10]" 0.022885030508041384;
	setAttr -s 5 ".wl[104].w";
	setAttr ".wl[104].w[2]" 0.46029075980186462;
	setAttr ".wl[104].w[7]" 0.46029075980186462;
	setAttr ".wl[104].w[8]" 0.042885979413986211;
	setAttr ".wl[104].w[9]" 0.028590652942657471;
	setAttr ".wl[104].w[10]" 0.0079418480396270755;
	setAttr -s 6 ".wl[105].w";
	setAttr ".wl[105].w[2]" 0.59389168005858906;
	setAttr ".wl[105].w[3]" 0.057834615046817514;
	setAttr ".wl[105].w[7]" 0.073380144637231373;
	setAttr ".wl[105].w[8]" 0.05497871205147243;
	setAttr ".wl[105].w[9]" 0.1649361361544173;
	setAttr ".wl[105].w[10]" 0.05497871205147243;
	setAttr -s 5 ".wl[106].w";
	setAttr ".wl[106].w[2]" 0.31466099961581828;
	setAttr ".wl[106].w[3]" 0.03496233329064647;
	setAttr ".wl[106].w[8]" 0.13007533341870708;
	setAttr ".wl[106].w[9]" 0.39022600025612114;
	setAttr ".wl[106].w[10]" 0.13007533341870708;
	setAttr -s 6 ".wl[107].w[8:13]"  0.061487789794438773 0.1957260059506449 
		0.17124176661653168 0.098159250534021095 0.24534826996022549 0.2280369171441381;
	setAttr -s 5 ".wl[108].w";
	setAttr ".wl[108].w[10]" 0.13718525823162636;
	setAttr ".wl[108].w[11]" 0.1251617504532298;
	setAttr ".wl[108].w[12]" 0.36924235377944914;
	setAttr ".wl[108].w[13]" 0.037749672277380512;
	setAttr ".wl[108].w[19]" 0.33066096525831423;
	setAttr -s 5 ".wl[109].w";
	setAttr ".wl[109].w[8]" 0.1325660467147827;
	setAttr ".wl[109].w[9]" 0.45758808310873594;
	setAttr ".wl[109].w[10]" 0.19455622764509464;
	setAttr ".wl[109].w[12]" 0.098845891218693152;
	setAttr ".wl[109].w[13]" 0.11644375131269358;
	setAttr -s 5 ".wl[110].w";
	setAttr ".wl[110].w[2]" 0.065083783084536717;
	setAttr ".wl[110].w[3]" 0.0072315314538374123;
	setAttr ".wl[110].w[8]" 0.18553693709232519;
	setAttr ".wl[110].w[9]" 0.55661081127697543;
	setAttr ".wl[110].w[10]" 0.18553693709232519;
	setAttr ".wl[111].w[9]"  1;
	setAttr ".wl[112].w[9]"  1;
	setAttr ".wl[113].w[9]"  1;
	setAttr -s 5 ".wl[114].w";
	setAttr ".wl[114].w[8]" 0.13055671453475953;
	setAttr ".wl[114].w[9]" 0.45334462957401428;
	setAttr ".wl[114].w[10]" 0.19439401934437162;
	setAttr ".wl[114].w[12]" 0.10179120615890935;
	setAttr ".wl[114].w[13]" 0.11991343038794519;
	setAttr ".wl[115].w[9]"  1;
	setAttr ".wl[116].w[9]"  1;
	setAttr ".wl[117].w[9]"  1;
	setAttr ".wl[118].w[9]"  1;
	setAttr ".wl[119].w[9]"  1;
	setAttr ".wl[120].w[9]"  1;
	setAttr ".wl[121].w[9]"  1;
	setAttr ".wl[122].w[9]"  1;
	setAttr ".wl[123].w[9]"  1;
	setAttr ".wl[124].w[9]"  1;
	setAttr ".wl[125].w[9]"  1;
	setAttr ".wl[126].w[9]"  1;
	setAttr ".wl[127].w[9]"  1;
	setAttr ".wl[128].w[9]"  1;
	setAttr ".wl[129].w[9]"  1;
	setAttr ".wl[130].w[9]"  1;
	setAttr ".wl[131].w[9]"  1;
	setAttr ".wl[132].w[9]"  1;
	setAttr ".wl[133].w[9]"  1;
	setAttr ".wl[134].w[21]"  1;
	setAttr ".wl[135].w[21]"  1;
	setAttr ".wl[136].w[21]"  1;
	setAttr ".wl[137].w[21]"  1;
	setAttr ".wl[138].w[21]"  1;
	setAttr ".wl[139].w[21]"  1;
	setAttr ".wl[140].w[21]"  1;
	setAttr ".wl[141].w[21]"  1;
	setAttr -s 4 ".wl[142].w[10:13]"  0.11795663299375267 0.11919592465505535 
		0.35229917970633118 0.41054826264486083;
	setAttr -s 5 ".wl[143].w";
	setAttr ".wl[143].w[10]" 0.051460523704872994;
	setAttr ".wl[143].w[11]" 0.10057809671843955;
	setAttr ".wl[143].w[12]" 0.40602395885247111;
	setAttr ".wl[143].w[13]" 0.23497090718664129;
	setAttr ".wl[143].w[19]" 0.20696651353757489;
	setAttr -s 6 ".wl[144].w";
	setAttr ".wl[144].w[10]" 0.067687976738564304;
	setAttr ".wl[144].w[11]" 0.090924840414937202;
	setAttr ".wl[144].w[12]" 0.32732360732177723;
	setAttr ".wl[144].w[13]" 0.032704118822847884;
	setAttr ".wl[144].w[19]" 0.43506653938195228;
	setAttr ".wl[144].w[20]" 0.046292917319921183;
	setAttr -s 7 ".wl[145].w";
	setAttr ".wl[145].w[8]" 0.010605373978614809;
	setAttr ".wl[145].w[9]" 0.038179346323013306;
	setAttr ".wl[145].w[10]" 0.18777267021532951;
	setAttr ".wl[145].w[11]" 0.12017433063505345;
	setAttr ".wl[145].w[12]" 0.32693391327526306;
	setAttr ".wl[145].w[13]" 0.10141349593586828;
	setAttr ".wl[145].w[19]" 0.21492086963685764;
	setAttr -s 2 ".wl[146].w[20:21]"  0.5 0.5;
	setAttr -s 2 ".wl[147].w[19:20]"  0.5 0.5;
	setAttr -s 2 ".wl[148].w[19:20]"  0.5 0.5;
	setAttr -s 2 ".wl[149].w[20:21]"  0.5 0.5;
	setAttr ".wl[150].w[21]"  1;
	setAttr ".wl[151].w[21]"  1;
	setAttr ".wl[152].w[21]"  1;
	setAttr ".wl[153].w[21]"  1;
	setAttr -s 4 ".wl[154].w[10:13]"  0.11842337206808902 0.11986183445715878 
		0.35360620457257086 0.40810858890218138;
	setAttr -s 7 ".wl[155].w";
	setAttr ".wl[155].w[8]" 0.010549896955490114;
	setAttr ".wl[155].w[9]" 0.037979629039764401;
	setAttr ".wl[155].w[10]" 0.18743273853516673;
	setAttr ".wl[155].w[11]" 0.11998517051988838;
	setAttr ".wl[155].w[12]" 0.32720155794578332;
	setAttr ".wl[155].w[13]" 0.098859387374044191;
	setAttr ".wl[155].w[19]" 0.21799161962986291;
	setAttr -s 6 ".wl[156].w";
	setAttr ".wl[156].w[10]" 0.068339619541014113;
	setAttr ".wl[156].w[11]" 0.090856367526039788;
	setAttr ".wl[156].w[12]" 0.32601576923495029;
	setAttr ".wl[156].w[13]" 0.032047197773529511;
	setAttr ".wl[156].w[19]" 0.43563961383942906;
	setAttr ".wl[156].w[20]" 0.04710143208503724;
	setAttr -s 5 ".wl[157].w";
	setAttr ".wl[157].w[10]" 0.051048056598367662;
	setAttr ".wl[157].w[11]" 0.10042790562683826;
	setAttr ".wl[157].w[12]" 0.40611949116169654;
	setAttr ".wl[157].w[13]" 0.23410982674056507;
	setAttr ".wl[157].w[19]" 0.20829471987253251;
	setAttr -s 2 ".wl[158].w[20:21]"  0.5 0.5;
	setAttr -s 2 ".wl[159].w[20:21]"  0.5 0.5;
	setAttr -s 2 ".wl[160].w[19:20]"  0.5 0.5;
	setAttr -s 2 ".wl[161].w[19:20]"  0.5 0.5;
	setAttr -s 5 ".wl[162].w";
	setAttr ".wl[162].w[9]" 0.054603553820696059;
	setAttr ".wl[162].w[10]" 0.056074158228822668;
	setAttr ".wl[162].w[12]" 0.15259636920276712;
	setAttr ".wl[162].w[19]" 0.5619322223442107;
	setAttr ".wl[162].w[20]" 0.17479369640350345;
	setAttr -s 26 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -9.1612641917038103e-016 0 -10.322264773308 1;
	setAttr ".pm[1]" -type "matrix" 2.2204460492503121e-016 0 -0.99999999999999978 0
		 0 1 0 0 0.99999999999999978 0 2.2204460492503121e-016 0 72.195067408442171 -232.42308711591741 1.6030525220243543e-014 1;
	setAttr ".pm[2]" -type "matrix" 2.2204460492503121e-016 0 -0.99999999999999978 0
		 0 1 0 0 0.99999999999999978 0 2.2204460492503121e-016 0 72.195067408442171 -232.42308711591741 1.6030525220243543e-014 1;
	setAttr ".pm[3]" -type "matrix" -0.082713528313459725 0.023382815039709067 -0.9962990094317854 0
		 -0.95872590144880632 0.27102834184994612 0.085955126695519163 0 0.27203514134227674 0.96228731773565812 2.2898349882893849e-016 0
		 236.43813808718144 11.755317741013263 29.044632232905155 1;
	setAttr ".pm[4]" -type "matrix" -0.073166617250642413 -0.10096630584242022 -0.99219577261981551 0
		 -0.58220963005696424 -0.80342043655458895 0.1246898103108957 0 -0.80973983030911278 0.58678906534713915 -1.457167719820518e-016 0
		 47.745674631073378 163.49740886149249 22.815046113315365 1;
	setAttr ".pm[5]" -type "matrix" -0.064285018712655351 0.014618076783577714 -0.9978245077167941 0
		 -0.97298579392550133 0.22125187687209613 0.06592610863487261 0 0.22173425803938324 0.97510713196639276 4.8745729674948279e-016 0
		 146.46259211275418 70.102483872367571 30.377944204782487 1;
	setAttr ".pm[6]" -type "matrix" -0.064285018712655351 0.014618076783577714 -0.9978245077167941 0
		 -0.97298579392550133 0.22125187687209613 0.06592610863487261 0 0.22173425803938324 0.97510713196639276 4.8745729674948279e-016 0
		 59.875417247323441 70.102483872367415 30.377944204782498 1;
	setAttr ".pm[7]" -type "matrix" 2.2204460492503121e-016 0 -0.99999999999999978 0
		 0 1 0 0 0.99999999999999978 0 2.2204460492503121e-016 0 55.691600826692003 -232.42308711591741 4.2416874648700516e-014 1;
	setAttr ".pm[8]" -type "matrix" 2.2204460492503121e-016 0 -0.99999999999999978 0
		 0 1 0 0 0.99999999999999978 0 2.2204460492503121e-016 0 22.684668026691988 -232.42308711591741 2.0601614292205784e-014 1;
	setAttr ".pm[9]" -type "matrix" 2.2204460492503121e-016 0 -0.99999999999999978 0
		 0 1 0 0 0.99999999999999978 0 2.2204460492503121e-016 0 -10.32226477330801 -232.42308711591741 -1.3758767843503639e-015 1;
	setAttr ".pm[10]" -type "matrix" 2.2204460492503121e-016 0 -0.99999999999999978 0
		 0 1 0 0 0.99999999999999978 0 2.2204460492503121e-016 0 -43.329197573307994 -232.42308711591741 -2.3353367860906504e-014 1;
	setAttr ".pm[11]" -type "matrix" 2.2204460492503121e-016 0 -0.99999999999999978 0
		 0 1 0 0 0.99999999999999978 0 2.2204460492503121e-016 0 -76.336130373308023 -232.42308711591741 -4.5168628217401248e-014 1;
	setAttr ".pm[12]" -type "matrix" 2.2204460492503121e-016 0 -0.99999999999999978 0
		 0 1 0 0 0.99999999999999978 0 2.2204460492503121e-016 0 -92.83959695505817 -232.42308711591741 -2.0614531627285034e-014 1;
	setAttr ".pm[13]" -type "matrix" -0.087860666602631393 0.0085306042079501144 -0.99609624638183847 0
		 -0.99143411462521047 0.096260731418963907 0.088273823662575343 0 0.096637982291988 0.99531959710363049 7.63278329429795e-017 0
		 206.75031199475356 -119.23693352951138 26.910745406720231 1;
	setAttr ".pm[14]" -type "matrix" -0.05208392701165919 -0.010735671937347093 -0.99858500384048299 0
		 -0.97802469014108151 -0.20159294474147524 0.053178850165288311 0 -0.20187860218826018 0.97941055231119201 2.3765711620882252e-016 0
		 132.58015817584899 -83.689807021846079 30.903373178698871 1;
	setAttr ".pm[15]" -type "matrix" -0.05208392701165919 -0.010735671937347093 -0.99858500384048299 0
		 -0.97802469014108151 -0.20159294474147524 0.053178850165288311 0 -0.20187860218826018 0.97941055231119201 2.3765711620882252e-016 0
		 66.905373726078963 -83.689807021846036 30.903373178698889 1;
	setAttr ".pm[16]" -type "matrix" -0.087860666602631365 0.008530604207949273 -0.99609624638183847 0
		 0.99143411462521025 -0.096260731418964837 -0.088273823662575218 0 -0.096637982291989027 -0.99531959710363016 8.3960616237277424e-016 0
		 -206.75068069854393 119.23694556558192 -26.910737070901288 1;
	setAttr ".pm[17]" -type "matrix" -0.052083927011659217 -0.010735671937331085 -0.99858500384048332 0
		 0.97802469014108162 0.20159294474147546 -0.053178850165285105 0 0.20187860218825943 -0.97941055231119212 -1.5914353168611225e-014 0
		 -132.58021662687068 83.690022530518462 -30.90339849051168 1;
	setAttr ".pm[18]" -type "matrix" -0.052083927011659217 -0.010735671937331085 -0.99858500384048332 0
		 0.97802469014108162 0.20159294474147546 -0.053178850165285105 0 0.20187860218825943 -0.97941055231119212 -1.5914353168611225e-014 0
		 -66.90534002389542 83.689769610530718 -30.903401258651662 1;
	setAttr ".pm[19]" -type "matrix" 4.6745536959594181e-017 -3.4948555639792369e-016 0.99999999999999978 0
		 0.97758886526498945 -0.21052318283246221 -1.1927272811939723e-016 0 0.21052318283246227 0.97758886526498923 3.3181216928160342e-016 0
		 -298.71783020029818 -38.131712171587608 6.3724272742506342e-016 1;
	setAttr ".pm[20]" -type "matrix" -1.1194336584910253e-016 0.99999999999999956 3.6674177308981428e-016 0
		 0.59590313857010768 -1.1927272811939718e-016 0.80305631772765163 0 0.80305631772765151 3.3181216928160327e-016 -0.59590313857010724 0
		 -292.2029609260598 6.9196983182535072e-015 -205.70175856768014 1;
	setAttr ".pm[21]" -type "matrix" 0.99999999999999956 9.5489817825448543e-017 -4.8675372360419306e-016 0
		 3.2481648173066539e-016 0.99999999999999978 1.1102230246251542e-016 0 6.0936792543789231e-016 1.1102230246251563e-016 0.99999999999999956 0
		 -2.0358029345351083e-013 -386.21650480897478 -175.28326143825208 1;
	setAttr ".pm[22]" -type "matrix" -0.082713528313459739 0.023382815039712432 -0.9962990094317854 0
		 0.95872590144880621 -0.27102834184994612 -0.085955126695520093 0 -0.27203514134227708 -0.96228731773565801 -3.4416913763379845e-015 0
		 -236.43832918530416 -11.755289187393471 -29.044617171560738 1;
	setAttr ".pm[23]" -type "matrix" -0.073166617250642468 -0.10096630584243045 -0.99219577261981451 0
		 0.58220963005696469 0.8034204365545875 -0.124689810310904 0 0.80973983030911267 -0.58678906534713948 6.210310043996971e-015 0
		 -47.745485530566768 -163.49711849996933 -22.81507832284316 1;
	setAttr ".pm[24]" -type "matrix" -0.064285018712655351 0.014618076783561031 -0.99782450771679443 0
		 0.97298579392550111 -0.22125187687209807 -0.065926108634868918 0 -0.22173425803938399 -0.97510713196639254 1.585363784695204e-014 0
		 -146.46252828924011 -70.102286936784978 -30.377901130858714 1;
	setAttr ".pm[25]" -type "matrix" -0.064285018712655351 0.014618076783561031 -0.99782450771679443 0
		 0.97298579392550111 -0.22125187687209807 -0.065926108634868918 0 -0.22173425803938399 -0.97510713196639254 1.585363784695204e-014 0
		 -59.875402359342758 -70.102524986349891 -30.377959748887093 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 19 ".ma";
	setAttr -s 26 ".dpf[0:25]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 
		4 4 4 4 4 4;
	setAttr -s 19 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 4;
	setAttr ".ucm" yes;
	setAttr -s 19 ".ifcl";
createNode tweak -n "tweak3";
	rename -uid "986981F2-429E-B87D-10E4-5B8B134DFB98";
createNode objectSet -n "skinCluster3Set";
	rename -uid "6DC09C27-4534-6CB3-6C85-BF8AFE1D0CBC";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster3GroupId";
	rename -uid "AC8457FA-4922-35CA-2553-D5A9E6096D07";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster3GroupParts";
	rename -uid "CDEBEBAD-4BEC-0C67-AC25-44B2D34463F3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet3";
	rename -uid "CE779E75-41DD-C77F-5039-1D921A59333B";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId6";
	rename -uid "DDA57B3B-4F02-DA49-379B-238465F49540";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	rename -uid "1ACE89C4-43C0-7E49-E8F3-89BE464112D6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode script -n "uiConfigurationScriptNode2";
	rename -uid "CE794936-4496-DF64-19FB-81BE55701B98";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 0\n                -hulls 0\n                -grid 1\n                -imagePlane 0\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 0\n                -fluids 0\n                -hairSystems 0\n                -follicles 0\n                -nCloths 0\n                -nParticles 0\n                -nRigids 0\n                -dynamicConstraints 0\n                -locators 1\n                -manipulators 1\n                -pluginShapes 0\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 0\n                -clipGhosts 0\n                -greasePencils 0\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1226\n                -height 735\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 0\n            -hulls 0\n            -grid 1\n            -imagePlane 0\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 0\n            -fluids 0\n            -hairSystems 0\n            -follicles 0\n            -nCloths 0\n            -nParticles 0\n            -nRigids 0\n            -dynamicConstraints 0\n            -locators 1\n            -manipulators 1\n            -pluginShapes 0\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 0\n            -clipGhosts 0\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1226\n            -height 735\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n"
		+ "                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n"
		+ "                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n"
		+ "                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n"
		+ "                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n"
		+ "                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 19 100 -ps 2 81 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Outliner\")) \n\t\t\t\t\t\"outlinerPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t\t\"outlinerPanel -edit -l (localizedPanelLabel(\\\"Outliner\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\noutlinerEditor -e \\n    -docTag \\\"isolOutln_fromSeln\\\" \\n    -showShapes 0\\n    -showReferenceNodes 1\\n    -showReferenceMembers 1\\n    -showAttributes 0\\n    -showConnected 0\\n    -showAnimCurvesOnly 0\\n    -showMuteInfo 0\\n    -organizeByLayer 1\\n    -showAnimLayerWeight 1\\n    -autoExpandLayers 1\\n    -autoExpand 0\\n    -showDagOnly 1\\n    -showAssets 1\\n    -showContainedOnly 1\\n    -showPublishedAsConnected 0\\n    -showContainerContents 1\\n    -ignoreDagHierarchy 0\\n    -expandConnections 0\\n    -showUpstreamCurves 1\\n    -showUnitlessCurves 1\\n    -showCompounds 1\\n    -showLeafs 1\\n    -showNumericAttrsOnly 0\\n    -highlightActive 1\\n    -autoSelectNewObjects 0\\n    -doNotSelectNewObjects 0\\n    -dropIsParent 1\\n    -transmitFilters 0\\n    -setFilter \\\"defaultSetFilter\\\" \\n    -showSetMembers 1\\n    -allowMultiSelection 1\\n    -alwaysToggleSelect 0\\n    -directSelect 0\\n    -displayMode \\\"DAG\\\" \\n    -expandObjects 0\\n    -setsIgnoreFilters 1\\n    -containersIgnoreFilters 0\\n    -editAttrName 0\\n    -showAttrValues 0\\n    -highlightSecondary 0\\n    -showUVAttrsOnly 0\\n    -showTextureNodesOnly 0\\n    -attrAlphaOrder \\\"default\\\" \\n    -animLayerFilterOptions \\\"allAffecting\\\" \\n    -sortOrder \\\"none\\\" \\n    -longNames 0\\n    -niceNames 1\\n    -showNamespace 1\\n    -showPinIcons 0\\n    -mapMotionTrails 0\\n    -ignoreHiddenAttribute 0\\n    -ignoreOutlinerColor 0\\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1226\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 0\\n    -hulls 0\\n    -grid 1\\n    -imagePlane 0\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 0\\n    -fluids 0\\n    -hairSystems 0\\n    -follicles 0\\n    -nCloths 0\\n    -nParticles 0\\n    -nRigids 0\\n    -dynamicConstraints 0\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 0\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 0\\n    -clipGhosts 0\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1226\\n    -height 735\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode2";
	rename -uid "BEDB906B-4C61-4050-227D-97A2F0036812";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 30 -ast 0 -aet 250 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 0;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 9 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 3 ".r";
select -ne :defaultTextureList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "Reindeer_SkeletonRN.phl[1]" "bindPose1.wm[0]";
connectAttr "Reindeer_SkeletonRN.phl[2]" "bindPose1.m[0]";
connectAttr "Reindeer_SkeletonRN.phl[3]" "skinCluster3.lw[1]";
connectAttr "Reindeer_SkeletonRN.phl[4]" "skinCluster2.lw[1]";
connectAttr "Reindeer_SkeletonRN.phl[5]" "bindPose1.wm[1]";
connectAttr "Reindeer_SkeletonRN.phl[6]" "bindPose1.m[1]";
connectAttr "Reindeer_SkeletonRN.phl[7]" "skinCluster3.ma[1]";
connectAttr "Reindeer_SkeletonRN.phl[8]" "skinCluster2.ma[1]";
connectAttr "Reindeer_SkeletonRN.phl[9]" "skinCluster3.ifcl[1]";
connectAttr "Reindeer_SkeletonRN.phl[10]" "skinCluster2.ifcl[1]";
connectAttr "Reindeer_SkeletonRN.phl[11]" "skinCluster3.lw[2]";
connectAttr "Reindeer_SkeletonRN.phl[12]" "skinCluster2.lw[2]";
connectAttr "Reindeer_SkeletonRN.phl[13]" "bindPose1.wm[2]";
connectAttr "Reindeer_SkeletonRN.phl[14]" "bindPose1.m[2]";
connectAttr "Reindeer_SkeletonRN.phl[15]" "skinCluster3.ma[2]";
connectAttr "Reindeer_SkeletonRN.phl[16]" "skinCluster2.ma[2]";
connectAttr "Reindeer_SkeletonRN.phl[17]" "skinCluster3.ifcl[2]";
connectAttr "Reindeer_SkeletonRN.phl[18]" "skinCluster2.ifcl[2]";
connectAttr "Reindeer_SkeletonRN.phl[19]" "skinCluster3.lw[3]";
connectAttr "Reindeer_SkeletonRN.phl[20]" "skinCluster2.lw[3]";
connectAttr "Reindeer_SkeletonRN.phl[21]" "bindPose1.wm[3]";
connectAttr "Reindeer_SkeletonRN.phl[22]" "bindPose1.m[3]";
connectAttr "Reindeer_SkeletonRN.phl[23]" "skinCluster3.ma[3]";
connectAttr "Reindeer_SkeletonRN.phl[24]" "skinCluster2.ma[3]";
connectAttr "Reindeer_SkeletonRN.phl[25]" "skinCluster3.ifcl[3]";
connectAttr "Reindeer_SkeletonRN.phl[26]" "skinCluster2.ifcl[3]";
connectAttr "Reindeer_SkeletonRN.phl[27]" "skinCluster3.lw[4]";
connectAttr "Reindeer_SkeletonRN.phl[28]" "skinCluster2.lw[4]";
connectAttr "Reindeer_SkeletonRN.phl[29]" "bindPose1.wm[4]";
connectAttr "Reindeer_SkeletonRN.phl[30]" "bindPose1.m[4]";
connectAttr "Reindeer_SkeletonRN.phl[31]" "skinCluster3.ma[4]";
connectAttr "Reindeer_SkeletonRN.phl[32]" "skinCluster2.ma[4]";
connectAttr "Reindeer_SkeletonRN.phl[33]" "skinCluster3.ifcl[4]";
connectAttr "Reindeer_SkeletonRN.phl[34]" "skinCluster2.ifcl[4]";
connectAttr "Reindeer_SkeletonRN.phl[35]" "skinCluster2.lw[5]";
connectAttr "Reindeer_SkeletonRN.phl[36]" "bindPose1.wm[5]";
connectAttr "Reindeer_SkeletonRN.phl[37]" "bindPose1.m[5]";
connectAttr "Reindeer_SkeletonRN.phl[38]" "skinCluster2.ma[5]";
connectAttr "Reindeer_SkeletonRN.phl[39]" "skinCluster2.ifcl[5]";
connectAttr "Reindeer_SkeletonRN.phl[40]" "skinCluster2.lw[6]";
connectAttr "Reindeer_SkeletonRN.phl[41]" "bindPose1.wm[26]";
connectAttr "Reindeer_SkeletonRN.phl[42]" "bindPose1.m[26]";
connectAttr "Reindeer_SkeletonRN.phl[43]" "skinCluster2.ma[6]";
connectAttr "Reindeer_SkeletonRN.phl[44]" "skinCluster2.ifcl[6]";
connectAttr "Reindeer_SkeletonRN.phl[45]" "skinCluster3.lw[7]";
connectAttr "Reindeer_SkeletonRN.phl[46]" "skinCluster2.lw[7]";
connectAttr "Reindeer_SkeletonRN.phl[47]" "bindPose1.wm[7]";
connectAttr "Reindeer_SkeletonRN.phl[48]" "bindPose1.m[7]";
connectAttr "Reindeer_SkeletonRN.phl[49]" "skinCluster3.ma[7]";
connectAttr "Reindeer_SkeletonRN.phl[50]" "skinCluster2.ma[7]";
connectAttr "Reindeer_SkeletonRN.phl[51]" "skinCluster3.ifcl[7]";
connectAttr "Reindeer_SkeletonRN.phl[52]" "skinCluster2.ifcl[7]";
connectAttr "Reindeer_SkeletonRN.phl[53]" "skinCluster3.lw[8]";
connectAttr "Reindeer_SkeletonRN.phl[54]" "skinCluster2.lw[8]";
connectAttr "Reindeer_SkeletonRN.phl[55]" "bindPose1.wm[8]";
connectAttr "Reindeer_SkeletonRN.phl[56]" "bindPose1.m[8]";
connectAttr "Reindeer_SkeletonRN.phl[57]" "skinCluster3.ma[8]";
connectAttr "Reindeer_SkeletonRN.phl[58]" "skinCluster2.ma[8]";
connectAttr "Reindeer_SkeletonRN.phl[59]" "skinCluster3.ifcl[8]";
connectAttr "Reindeer_SkeletonRN.phl[60]" "skinCluster2.ifcl[8]";
connectAttr "Reindeer_SkeletonRN.phl[61]" "skinCluster3.lw[9]";
connectAttr "Reindeer_SkeletonRN.phl[62]" "skinCluster2.lw[9]";
connectAttr "Reindeer_SkeletonRN.phl[63]" "bindPose1.wm[9]";
connectAttr "Reindeer_SkeletonRN.phl[64]" "bindPose1.m[9]";
connectAttr "Reindeer_SkeletonRN.phl[65]" "skinCluster3.ma[9]";
connectAttr "Reindeer_SkeletonRN.phl[66]" "skinCluster2.ma[9]";
connectAttr "Reindeer_SkeletonRN.phl[67]" "skinCluster3.ifcl[9]";
connectAttr "Reindeer_SkeletonRN.phl[68]" "skinCluster2.ifcl[9]";
connectAttr "Reindeer_SkeletonRN.phl[69]" "skinCluster2.lw[10]";
connectAttr "Reindeer_SkeletonRN.phl[70]" "skinCluster3.lw[10]";
connectAttr "Reindeer_SkeletonRN.phl[71]" "bindPose1.wm[10]";
connectAttr "Reindeer_SkeletonRN.phl[72]" "bindPose1.m[10]";
connectAttr "Reindeer_SkeletonRN.phl[73]" "skinCluster2.ma[10]";
connectAttr "Reindeer_SkeletonRN.phl[74]" "skinCluster3.ma[10]";
connectAttr "Reindeer_SkeletonRN.phl[75]" "skinCluster2.ifcl[10]";
connectAttr "Reindeer_SkeletonRN.phl[76]" "skinCluster3.ifcl[10]";
connectAttr "Reindeer_SkeletonRN.phl[77]" "skinCluster2.lw[11]";
connectAttr "Reindeer_SkeletonRN.phl[78]" "skinCluster3.lw[11]";
connectAttr "Reindeer_SkeletonRN.phl[79]" "bindPose1.wm[11]";
connectAttr "Reindeer_SkeletonRN.phl[80]" "bindPose1.m[11]";
connectAttr "Reindeer_SkeletonRN.phl[81]" "skinCluster2.ma[11]";
connectAttr "Reindeer_SkeletonRN.phl[82]" "skinCluster3.ma[11]";
connectAttr "Reindeer_SkeletonRN.phl[83]" "skinCluster2.ifcl[11]";
connectAttr "Reindeer_SkeletonRN.phl[84]" "skinCluster3.ifcl[11]";
connectAttr "Reindeer_SkeletonRN.phl[85]" "skinCluster2.lw[12]";
connectAttr "Reindeer_SkeletonRN.phl[86]" "skinCluster3.lw[12]";
connectAttr "Reindeer_SkeletonRN.phl[87]" "bindPose1.wm[12]";
connectAttr "Reindeer_SkeletonRN.phl[88]" "bindPose1.m[12]";
connectAttr "Reindeer_SkeletonRN.phl[89]" "skinCluster2.ma[12]";
connectAttr "Reindeer_SkeletonRN.phl[90]" "skinCluster3.ma[12]";
connectAttr "Reindeer_SkeletonRN.phl[91]" "skinCluster2.ifcl[12]";
connectAttr "Reindeer_SkeletonRN.phl[92]" "skinCluster3.ifcl[12]";
connectAttr "Reindeer_SkeletonRN.phl[93]" "skinCluster2.lw[13]";
connectAttr "Reindeer_SkeletonRN.phl[94]" "skinCluster3.lw[13]";
connectAttr "Reindeer_SkeletonRN.phl[95]" "bindPose1.wm[13]";
connectAttr "Reindeer_SkeletonRN.phl[96]" "bindPose1.m[13]";
connectAttr "Reindeer_SkeletonRN.phl[97]" "skinCluster2.ma[13]";
connectAttr "Reindeer_SkeletonRN.phl[98]" "skinCluster3.ma[13]";
connectAttr "Reindeer_SkeletonRN.phl[99]" "skinCluster2.ifcl[13]";
connectAttr "Reindeer_SkeletonRN.phl[100]" "skinCluster3.ifcl[13]";
connectAttr "Reindeer_SkeletonRN.phl[101]" "skinCluster3.lw[14]";
connectAttr "Reindeer_SkeletonRN.phl[102]" "skinCluster2.lw[14]";
connectAttr "Reindeer_SkeletonRN.phl[103]" "bindPose1.wm[14]";
connectAttr "Reindeer_SkeletonRN.phl[104]" "bindPose1.m[14]";
connectAttr "Reindeer_SkeletonRN.phl[105]" "skinCluster3.ma[14]";
connectAttr "Reindeer_SkeletonRN.phl[106]" "skinCluster2.ma[14]";
connectAttr "Reindeer_SkeletonRN.phl[107]" "skinCluster3.ifcl[14]";
connectAttr "Reindeer_SkeletonRN.phl[108]" "skinCluster2.ifcl[14]";
connectAttr "Reindeer_SkeletonRN.phl[109]" "skinCluster2.lw[15]";
connectAttr "Reindeer_SkeletonRN.phl[110]" "bindPose1.wm[27]";
connectAttr "Reindeer_SkeletonRN.phl[111]" "bindPose1.m[27]";
connectAttr "Reindeer_SkeletonRN.phl[112]" "skinCluster2.ma[15]";
connectAttr "Reindeer_SkeletonRN.phl[113]" "skinCluster2.ifcl[15]";
connectAttr "Reindeer_SkeletonRN.phl[114]" "skinCluster2.lw[16]";
connectAttr "Reindeer_SkeletonRN.phl[115]" "skinCluster3.lw[16]";
connectAttr "Reindeer_SkeletonRN.phl[116]" "bindPose1.wm[16]";
connectAttr "Reindeer_SkeletonRN.phl[117]" "bindPose1.m[16]";
connectAttr "Reindeer_SkeletonRN.phl[118]" "skinCluster2.ma[16]";
connectAttr "Reindeer_SkeletonRN.phl[119]" "skinCluster3.ma[16]";
connectAttr "Reindeer_SkeletonRN.phl[120]" "skinCluster2.ifcl[16]";
connectAttr "Reindeer_SkeletonRN.phl[121]" "skinCluster3.ifcl[16]";
connectAttr "Reindeer_SkeletonRN.phl[122]" "skinCluster3.lw[17]";
connectAttr "Reindeer_SkeletonRN.phl[123]" "skinCluster2.lw[17]";
connectAttr "Reindeer_SkeletonRN.phl[124]" "bindPose1.wm[17]";
connectAttr "Reindeer_SkeletonRN.phl[125]" "bindPose1.m[17]";
connectAttr "Reindeer_SkeletonRN.phl[126]" "skinCluster3.ma[17]";
connectAttr "Reindeer_SkeletonRN.phl[127]" "skinCluster2.ma[17]";
connectAttr "Reindeer_SkeletonRN.phl[128]" "skinCluster3.ifcl[17]";
connectAttr "Reindeer_SkeletonRN.phl[129]" "skinCluster2.ifcl[17]";
connectAttr "Reindeer_SkeletonRN.phl[130]" "skinCluster2.lw[18]";
connectAttr "Reindeer_SkeletonRN.phl[131]" "bindPose1.wm[28]";
connectAttr "Reindeer_SkeletonRN.phl[132]" "bindPose1.m[28]";
connectAttr "Reindeer_SkeletonRN.phl[133]" "skinCluster2.ma[18]";
connectAttr "Reindeer_SkeletonRN.phl[134]" "skinCluster2.ifcl[18]";
connectAttr "Reindeer_SkeletonRN.phl[135]" "skinCluster2.lw[19]";
connectAttr "Reindeer_SkeletonRN.phl[136]" "skinCluster3.lw[19]";
connectAttr "Reindeer_SkeletonRN.phl[137]" "bindPose1.wm[19]";
connectAttr "Reindeer_SkeletonRN.phl[138]" "bindPose1.m[19]";
connectAttr "Reindeer_SkeletonRN.phl[139]" "skinCluster2.ma[19]";
connectAttr "Reindeer_SkeletonRN.phl[140]" "skinCluster3.ma[19]";
connectAttr "Reindeer_SkeletonRN.phl[141]" "skinCluster2.ifcl[19]";
connectAttr "Reindeer_SkeletonRN.phl[142]" "skinCluster3.ifcl[19]";
connectAttr "Reindeer_SkeletonRN.phl[143]" "skinCluster2.lw[20]";
connectAttr "Reindeer_SkeletonRN.phl[144]" "skinCluster3.lw[20]";
connectAttr "Reindeer_SkeletonRN.phl[145]" "bindPose1.wm[20]";
connectAttr "Reindeer_SkeletonRN.phl[146]" "bindPose1.m[20]";
connectAttr "Reindeer_SkeletonRN.phl[147]" "skinCluster2.ma[20]";
connectAttr "Reindeer_SkeletonRN.phl[148]" "skinCluster3.ma[20]";
connectAttr "Reindeer_SkeletonRN.phl[149]" "skinCluster2.ifcl[20]";
connectAttr "Reindeer_SkeletonRN.phl[150]" "skinCluster3.ifcl[20]";
connectAttr "Reindeer_SkeletonRN.phl[151]" "skinCluster3.lw[21]";
connectAttr "Reindeer_SkeletonRN.phl[152]" "skinCluster2.lw[21]";
connectAttr "Reindeer_SkeletonRN.phl[153]" "bindPose1.wm[29]";
connectAttr "Reindeer_SkeletonRN.phl[154]" "bindPose1.m[29]";
connectAttr "Reindeer_SkeletonRN.phl[155]" "skinCluster3.ma[21]";
connectAttr "Reindeer_SkeletonRN.phl[156]" "skinCluster2.ma[21]";
connectAttr "Reindeer_SkeletonRN.phl[157]" "skinCluster3.ifcl[21]";
connectAttr "Reindeer_SkeletonRN.phl[158]" "skinCluster2.ifcl[21]";
connectAttr "Reindeer_SkeletonRN.phl[159]" "skinCluster3.lw[22]";
connectAttr "Reindeer_SkeletonRN.phl[160]" "skinCluster2.lw[22]";
connectAttr "Reindeer_SkeletonRN.phl[161]" "bindPose1.wm[22]";
connectAttr "Reindeer_SkeletonRN.phl[162]" "bindPose1.m[22]";
connectAttr "Reindeer_SkeletonRN.phl[163]" "skinCluster3.ma[22]";
connectAttr "Reindeer_SkeletonRN.phl[164]" "skinCluster2.ma[22]";
connectAttr "Reindeer_SkeletonRN.phl[165]" "skinCluster3.ifcl[22]";
connectAttr "Reindeer_SkeletonRN.phl[166]" "skinCluster2.ifcl[22]";
connectAttr "Reindeer_SkeletonRN.phl[167]" "skinCluster3.lw[23]";
connectAttr "Reindeer_SkeletonRN.phl[168]" "skinCluster2.lw[23]";
connectAttr "Reindeer_SkeletonRN.phl[169]" "bindPose1.wm[23]";
connectAttr "Reindeer_SkeletonRN.phl[170]" "bindPose1.m[23]";
connectAttr "Reindeer_SkeletonRN.phl[171]" "skinCluster3.ma[23]";
connectAttr "Reindeer_SkeletonRN.phl[172]" "skinCluster2.ma[23]";
connectAttr "Reindeer_SkeletonRN.phl[173]" "skinCluster3.ifcl[23]";
connectAttr "Reindeer_SkeletonRN.phl[174]" "skinCluster2.ifcl[23]";
connectAttr "Reindeer_SkeletonRN.phl[175]" "skinCluster2.lw[24]";
connectAttr "Reindeer_SkeletonRN.phl[176]" "bindPose1.wm[24]";
connectAttr "Reindeer_SkeletonRN.phl[177]" "bindPose1.m[24]";
connectAttr "Reindeer_SkeletonRN.phl[178]" "skinCluster2.ma[24]";
connectAttr "Reindeer_SkeletonRN.phl[179]" "skinCluster2.ifcl[24]";
connectAttr "Reindeer_SkeletonRN.phl[180]" "skinCluster2.lw[25]";
connectAttr "Reindeer_SkeletonRN.phl[181]" "bindPose1.wm[30]";
connectAttr "Reindeer_SkeletonRN.phl[182]" "bindPose1.m[30]";
connectAttr "Reindeer_SkeletonRN.phl[183]" "skinCluster2.ma[25]";
connectAttr "Reindeer_SkeletonRN.phl[184]" "skinCluster2.ifcl[25]";
connectAttr "ReindeerRN.phl[2]" "groupParts4.ig";
connectAttr "ReindeerRN.phl[3]" "groupParts6.ig";
connectAttr "RudolfShapeDeformed.iog" "ReindeerRN.phl[4]";
connectAttr "SatleShapeDeformed.iog" "ReindeerRN.phl[5]";
connectAttr "skinCluster3GroupId.id" "SatleShapeDeformed.iog.og[0].gid";
connectAttr "skinCluster3Set.mwc" "SatleShapeDeformed.iog.og[0].gco";
connectAttr "groupId6.id" "SatleShapeDeformed.iog.og[1].gid";
connectAttr "tweakSet3.mwc" "SatleShapeDeformed.iog.og[1].gco";
connectAttr "skinCluster3.og[0]" "SatleShapeDeformed.i";
connectAttr "tweak3.vl[0].vt[0]" "SatleShapeDeformed.twl";
connectAttr "skinCluster2GroupId.id" "RudolfShapeDeformed.iog.og[0].gid";
connectAttr "skinCluster2Set.mwc" "RudolfShapeDeformed.iog.og[0].gco";
connectAttr "groupId4.id" "RudolfShapeDeformed.iog.og[1].gid";
connectAttr "tweakSet2.mwc" "RudolfShapeDeformed.iog.og[1].gco";
connectAttr "skinCluster2.og[0]" "RudolfShapeDeformed.i";
connectAttr "tweak2.vl[0].vt[0]" "RudolfShapeDeformed.twl";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "ReindeerRNfosterParent1.msg" "ReindeerRN.fp";
connectAttr "bindPose1.w" "bindPose1.p[0]";
connectAttr "bindPose1.m[0]" "bindPose1.p[1]";
connectAttr "bindPose1.m[1]" "bindPose1.p[2]";
connectAttr "bindPose1.m[2]" "bindPose1.p[3]";
connectAttr "bindPose1.m[3]" "bindPose1.p[4]";
connectAttr "bindPose1.m[4]" "bindPose1.p[5]";
connectAttr "bindPose1.m[2]" "bindPose1.p[7]";
connectAttr "bindPose1.m[7]" "bindPose1.p[8]";
connectAttr "bindPose1.m[8]" "bindPose1.p[9]";
connectAttr "bindPose1.m[9]" "bindPose1.p[10]";
connectAttr "bindPose1.m[10]" "bindPose1.p[11]";
connectAttr "bindPose1.m[11]" "bindPose1.p[12]";
connectAttr "bindPose1.m[12]" "bindPose1.p[13]";
connectAttr "bindPose1.m[13]" "bindPose1.p[14]";
connectAttr "bindPose1.m[12]" "bindPose1.p[16]";
connectAttr "bindPose1.m[16]" "bindPose1.p[17]";
connectAttr "bindPose1.m[12]" "bindPose1.p[19]";
connectAttr "bindPose1.m[19]" "bindPose1.p[20]";
connectAttr "bindPose1.m[2]" "bindPose1.p[22]";
connectAttr "bindPose1.m[22]" "bindPose1.p[23]";
connectAttr "bindPose1.m[23]" "bindPose1.p[24]";
connectAttr "bindPose1.m[5]" "bindPose1.p[26]";
connectAttr "bindPose1.m[14]" "bindPose1.p[27]";
connectAttr "bindPose1.m[17]" "bindPose1.p[28]";
connectAttr "bindPose1.m[20]" "bindPose1.p[29]";
connectAttr "bindPose1.m[24]" "bindPose1.p[30]";
connectAttr "skinCluster2GroupParts.og" "skinCluster2.ip[0].ig";
connectAttr "skinCluster2GroupId.id" "skinCluster2.ip[0].gi";
connectAttr "bindPose1.msg" "skinCluster2.bp";
connectAttr "groupParts4.og" "tweak2.ip[0].ig";
connectAttr "groupId4.id" "tweak2.ip[0].gi";
connectAttr "skinCluster2GroupId.msg" "skinCluster2Set.gn" -na;
connectAttr "RudolfShapeDeformed.iog.og[0]" "skinCluster2Set.dsm" -na;
connectAttr "skinCluster2.msg" "skinCluster2Set.ub[0]";
connectAttr "tweak2.og[0]" "skinCluster2GroupParts.ig";
connectAttr "skinCluster2GroupId.id" "skinCluster2GroupParts.gi";
connectAttr "groupId4.msg" "tweakSet2.gn" -na;
connectAttr "RudolfShapeDeformed.iog.og[1]" "tweakSet2.dsm" -na;
connectAttr "tweak2.msg" "tweakSet2.ub[0]";
connectAttr "groupId4.id" "groupParts4.gi";
connectAttr "skinCluster3GroupParts.og" "skinCluster3.ip[0].ig";
connectAttr "skinCluster3GroupId.id" "skinCluster3.ip[0].gi";
connectAttr "bindPose1.msg" "skinCluster3.bp";
connectAttr "groupParts6.og" "tweak3.ip[0].ig";
connectAttr "groupId6.id" "tweak3.ip[0].gi";
connectAttr "skinCluster3GroupId.msg" "skinCluster3Set.gn" -na;
connectAttr "SatleShapeDeformed.iog.og[0]" "skinCluster3Set.dsm" -na;
connectAttr "skinCluster3.msg" "skinCluster3Set.ub[0]";
connectAttr "tweak3.og[0]" "skinCluster3GroupParts.ig";
connectAttr "skinCluster3GroupId.id" "skinCluster3GroupParts.gi";
connectAttr "groupId6.msg" "tweakSet3.gn" -na;
connectAttr "SatleShapeDeformed.iog.og[1]" "tweakSet3.dsm" -na;
connectAttr "tweak3.msg" "tweakSet3.ub[0]";
connectAttr "groupId6.id" "groupParts6.gi";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of Reindeer_Skin.ma
