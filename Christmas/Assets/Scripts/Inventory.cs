﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Inventory : MonoBehaviour {

	public int slotsX, slotsY;
	// Created a GUI Skin in Unity.  Created a custom style and changed the border to 4 (This is so the edge does not stretch.)
	public GUISkin skin;

	// Defining inventory as a list variable.(using our Items Class script)  Then setting the variable as our list.
	public List<Item> inventory = new List<Item>();
	public List<Item> slots = new List<Item>();
	// Pull in the ItemDataBase script so we can access the information in it.
	private ItemDataBase dataBase;
	private bool showToolTip;
	private string toolTip;


	// Use this for initialization
	void Start () {

		for (int i = 0; i < (slotsX * slotsY); i++)
		{
			// add slots in empty slots in the editor
			slots.Add(new Item());
			inventory.Add(new Item());
		}

		// Look for the object in the Hierarchy with the tag "ItemDataBase". Then look for a component that is called "ItemDataBase".
		// 2 separate things.  And object and a component on an object
		dataBase = GameObject.FindGameObjectWithTag("ItemDataBase").GetComponent<ItemDataBase>();
		// Manually adds item to the inventory
		//inventory[0] = dataBase.items[0];
		//inventory[1] = dataBase.items[1];

		// Use this to add a new quest item found.  This means Function 'AddItem' adds item with the ID '1'
		AddItem(1);
		AddItem(0);
		AddItem(2);
		//RemoveItem (0);

	}

	void Update ()
	{

	}

	//-------------------------------------------------------
	// Creates tool tip that can be displayed on the GUI
	//-------------------------------------------------------
	string CreateToolTip(Item item)
	{
		// color changes the text color on the tooltip
		toolTip = "<color=#ffffff>" + item.itemName + "</color>\n\n" + "<color=#ffffff>" + item.itemDesc + "</color>\n";
		return toolTip;
	}
	//stop

	//-------------------------------------------------------
	// Removes Item from the inventory using the item ID
	//-------------------------------------------------------
	void RemoveItem(int id)
	{
		for (int i = 0; i < inventory.Count; i++) 
		{
			if (inventory[i].toyIdItem == id)
			{
				inventory[i] = new Item();
				break;
			}
		}

	}
	//stop

	//-------------------------------------------------------
	// adds Item to the inventory using the item ID
	//-------------------------------------------------------
	void AddItem(int id)
	{
		for (int i = 0; i < inventory.Count; i++) 
		{
			if (inventory[i].itemName == null)
			{
				for (int j = 0; j < dataBase.items.Count; j++)
				{
					if(dataBase.items[j].toyIdItem == id)
					{
						inventory[i] = dataBase.items[j];
					}
				}
				break;
			}
		}
	}
	//stop

	//---------------------------------------------------------------
	// returns an item
	// use bool istead of void because we are going to return a bool.
	//---------------------------------------------------------------
	bool InventoryContains(int id)
	{
		bool result = false;
		for(int i = 0; i < inventory.Count; i++)
		{
			result = inventory[i].toyIdItem == id;
			if (result)
			{
				break;
			}
		}
		return result;
	}
	//stop
}











