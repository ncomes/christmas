﻿using UnityEngine;
using System.Collections;

public class HouseScrollScript : MonoBehaviour {

	public Vector3 target;
	public GameObject EndPoint;
	public float speed = 3;

	private int turnRandom;
	private Vector3 housePos;
	private float houseRotY;

	// Use this for initialization
	void Start () {
		// get the y rotating of the house
		turnHouse();
		// finds the end point by tag
		EndPoint = GameObject.FindWithTag ("Finish");
		// gets the end points position
		target = EndPoint.transform.position;
		// rotates the house to face the camera
		transform.Rotate (0f, houseRotY, 0f);
	}
	
	// Update is called once per frame
	void Update () {
		moveHouse ();
		destroyHouse ();
	
	}
	//-------------------------------------------------------
	// moves the house towards the end position
	//-------------------------------------------------------
	void moveHouse(){
		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position, target, step);
	}
	//-------------------------------------------------------
	// deletes the house once it reaches the end position
	//-------------------------------------------------------
	void destroyHouse(){
		housePos = gameObject.transform.position;

		if (housePos == target) {
			Destroy(gameObject);
		}

	}
	//-------------------------------------------------------
	// Give a random number to rotate the house
	//-------------------------------------------------------
	void turnHouse(){
		turnRandom = Random.Range(1, 10);
		if (turnRandom <= 3)
		{
			houseRotY = -90;
		}
		if (turnRandom >= 4 && turnRandom <= 7)
		{
			houseRotY = 0;
		}
		if (turnRandom >= 8)
		{
			houseRotY = 180;
		}
	}
}
