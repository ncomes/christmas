﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HouseSpawnScript : MonoBehaviour {

	public GameObject[] house;
	public GameObject startNode;

	public GameObject curHouse;
	public Vector3 target;
	public Vector3 housePos;
	public float RandomMinDistance;
	public float RandomMaxDistance;

	private int randRan;
	private string ranHouse;
	public ItemDataBase dataBase;

	// Use this for initialization
	void Start () {
		// start node psoition
		randomHouse ();
		// finds the start location
		startNode = GameObject.FindWithTag ("Start");
		// gets the position where the house will spawn
		target = startNode.transform.position;

		// spawns the house
		curHouse = Instantiate(Resources.Load(ranHouse), new Vector3(target.x, target.y, target.z), Quaternion.identity) as GameObject;
		// get the dataBase script so we can tie the kids to the houses;
		dataBase = gameObject.GetComponent<ItemDataBase>();
	}
	
	// Update is called once per frame
	void Update () {
		spawnHouse ();
		
	}
	//-------------------------------------------------------
	// Spawns a random house and a specific position
	//-------------------------------------------------------
	void spawnHouse(){
		// house position
		float ran = Random.Range (RandomMinDistance, RandomMaxDistance);
		housePos = curHouse.transform.position;
		if (Vector3.Distance (target, housePos) > ran) {
			randomHouse();
			curHouse = Instantiate(Resources.Load(ranHouse), new Vector3(target.x, target.y, target.z), Quaternion.identity) as GameObject;
			// adds in the house info to the database
			dataBase.AddRandItems();
		}

	}
	//-------------------------------------------------------
	// Get a random house from a list of houses
	//-------------------------------------------------------
	void randomHouse(){
		randRan = Random.Range (0, (house.Length));
		ranHouse = house[randRan].name;

	}
}
