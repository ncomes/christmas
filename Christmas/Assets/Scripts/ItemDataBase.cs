﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
// Using Generic.  This gives us the list function.

public class ItemDataBase : MonoBehaviour 
{
	public List<Item> items = new List<Item> ();

	private string[] boysNames = {"Ronin", "Nathan", "John", "Timmy", "Jacob", "Jeffery", "Mathew", "Nicolas", "Jack",
									"Bobby", "Jon"};
	private string[] girlsNames = {"Hera", "Ellie", "Haley", "Kayla", "Emma", "Kristan", "Marie", "Cristi", "Piper",
									"Megan", "Bethany"};

	private string[] toys = {"Toy Truck", "Teddy Bear", "Video Game", "Board Game", "BasketBall", "Football", "Bike", "rollerblades",
							"Action Figure", "VolleyBall", "Computer"};

	private int ranBoyName;
	private int ranGirlName;
	private int ranToy;
	////-------------------------------------------------------
	// Adds items to the inventory slots
	//---------------------------------------------------------

	void Start()
	{
	  //items.Add (new Item ("KidsName", toyId, "ToyName", ItemIcon, Score, Item.ItemType.Structure));
		//items.Add (new Item ("Ronin", 0, "Pokemon", 1, 1, Item.ItemType.house));
		//items.Add (new Item ("Ellie", 1, "ElsaDoll", 0, 0, Item.ItemType.appartment));
		//items.Add (new Item ("Hera", 2, "stuffedBear", 2, 3, Item.ItemType.townhouse));
	}

	void RandomItems(){
		ranBoyName = Random.Range (0, boysNames.Length);
		ranGirlName = Random.Range (0, girlsNames.Length);
		ranToy = Random.Range (0, toys.Length);
	}

	public void AddRandItems(){
		RandomItems ();
		int boyGirl = Random.Range (1, 10);
		if (boyGirl <= 5){
			items.Add(new Item (boysNames[ranBoyName], ranToy, toys[ranToy], 0, 0, Item.ItemType.house));
		}
		else
		{
			items.Add(new Item (girlsNames[ranGirlName], ranToy, toys[ranToy], 0, 0, Item.ItemType.house));
		}

	}
}
